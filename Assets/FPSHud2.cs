﻿using UnityEngine;
using System.Collections;

public class FPSHud2 : MonoBehaviour {

    public float updateInterval = 5F;
    private float lastInterval;
    private int frames = 0;
    private float fps;
    public bool updateColor = true; // Do you want the color to change if the FPS gets low
    private GUIStyle style; // The style the text will be displayed at, based en defaultSkin.label.
    private Color color = Color.white; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )
    public Rect startRect = new Rect(10, 10, 75, 50); // The rect the window is initially displayed at.
    public Rect startRect1 = new Rect(100, 10, 75, 50); // The rect the window is initially displayed at.
    void Start()
    {
        lastInterval = Time.realtimeSinceStartup;
        frames = 0;
    }

    void OnGUI()
    {
        if (style == null)
        {
            style = new GUIStyle(GUI.skin.label);
            style.normal.textColor = Color.white;
            style.alignment = TextAnchor.MiddleCenter;

        }
        GUI.color = updateColor ? color : Color.white;
        startRect1 = GUI.Window(1, startRect1, DoMyWindow1, "");
    }

    void Update()
    {
        ++frames;
        float timeNow = Time.realtimeSinceStartup;
        if (timeNow > lastInterval + updateInterval)
        {
            fps = frames / (timeNow - lastInterval);
            frames = 0;
            lastInterval = timeNow;
        }
    }
    void DoMyWindow1(int windowID1)
    {
        GUI.Label(new Rect(0, 0, startRect1.width, startRect1.height), fps + " FPSAverage", style);
        GUI.DragWindow(new Rect(0, 0, Screen.width, Screen.height));
    }

}
