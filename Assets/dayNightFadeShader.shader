﻿Shader "Custom/dayNightFadeShader" {
Properties {
        _Tint ("Tint Color", Color) = (.9, .9, .9, 1.0)
        _TexMat1 ("Base (RGB)", 2D) = "white" {}
        _TexMat2 ("Base (RGB)", 2D) = "white" {}
        _Blend ("Blend", Range(0.0,1.0)) = 0.0
    }
   
    Category {
        ZWrite On
        Alphatest Greater 0
        Tags {Queue=Transparent}
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask RGB
    SubShader {

        Pass {
           
				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				float _Blend;
				sampler2D _TexMat1;
				sampler2D _TexMat2;
			   
			   struct v2f {
					float4 pos : SV_POSITION;
					float4 color : COLOR0;
					float2 uv : TEXCOORD0;
				};
           
				float4 _TexMat1_ST;
				float4 _TexMat2_ST;

		        v2f vert (appdata_base v)
				{
					v2f o;
					o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
					o.uv = TRANSFORM_TEX (v.texcoord, _TexMat1);
					o.color = (1,1,1,1);
					//float4 color1 = tex2D (_TexMat1, o.uv1);
					//float4 color2 =  tex2D (_TexMat2, o.uv1);
					//half4 color2 = tex2D (_TexMat2, o.uv2);
					//o.color = (1-_Blend)*color1 + _Blend*color2;
					return o;
				}

				half4 frag (v2f i) : COLOR
				{
					 half4 texcol1 = tex2D (_TexMat1, i.uv);
					 half4 texcol2 = tex2D (_TexMat2, i.uv);
					 half4 texcol = (1-_Blend)*texcol1 + _Blend*texcol2;
					 return texcol;
				}

			    ENDCG
        }
    }
    FallBack " Diffuse", 1
}
}