﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Troop : MonoBehaviour {

    public enum BadgeEnum { KnifeBadge, FireBadge, FapBadge}
    public int stamina = 100;

    public Dictionary<ICollectable, int> collectables;
    public Dictionary<BadgeEnum, bool> badgesCollected;
    public float separation = 5;
    public List<Scout> scouts;
    public Pet pet;
    public GameObject jumpTrigger;
    public GameObject duckTrigger;
    public GameObject swipeAnimObject;
    public float distanceTravelled = 0;
    public float reducedDistanceMultiplier = 1f;
    public float reducedDistanceTravelled { get { return distanceTravelled * reducedDistanceMultiplier; } }

    private PowerUp ActivePowerUp;

    public GameObject bear;
    public Vector3 bearDistance;
    #region PowerUpVariables
    private GameObject foxNadoSource;
    private GameObject foxNadoSpriteDone;
    private GameObject foxNadoSpriteStart;
    private GameObject foxInnerParticle;
    private GameObject foxWhiteFlash;
    private GameObject foxNadoOrange;
    private bool foxNadoSpin = false;
    private bool foxNadoShrink = false;
    public bool foxNadoActive = false;

    private GameObject featherStormParticle;
    private GameObject featherDustParticle;
    public bool featherStormActive = false;

    private float foxSpinSpeed;
    [HideInInspector]
    public float absSpeed;
    [HideInInspector]
    public float absRadius;
    [HideInInspector]
    public float absAcceleration;
    [HideInInspector]
    public float absTopSpeed;
    #endregion
    //for making the scouts blink
    #region Blink Variables
    public float blinkTime;
    public int numberOfBlinks;
    private float blinkTimeCoundown;
    private int blinkCoundown;
    #endregion
    public float triggerSpawn = 1.0f;
    public bool challengeFailed = false;

	public float switchAnimDuration = 0.5f;
	public bool switching = false;

    public delegate void DieEventHandler();
    public event DieEventHandler OnDie;

    public PowerUp currentPowerUp;

	AudioManager audioMan;
	private GameObject foxNadoAudioSource;
	private float pitchAddition = 0.05f;
    private int enabledScouts = 0;

    private ParticleSystem swipeRightPS;
    private ParticleSystem swipeLeftPS;

    void Awake()
    {
        collectables = new Dictionary<ICollectable,int>();
        badgesCollected = new Dictionary<BadgeEnum, bool>();
        //get the animator component

    }

	// Use this for initialization
	void Start () {

		if (RankManager.Instance.currentRank == RankManager.tutorialRank) 
		{
			this.stamina = 1;
		}

		audioMan = AudioManager.Instance;
		foxNadoAudioSource = audioMan.transform.FindChild ("FoxNadoAudioPlayer").gameObject;
        bear = ProceduralDatabaseManager.Instance.GetOtherObject(bear);
    //    bear = (GameObject)Instantiate(bear);
        bear.SetActive(false);
        foxNadoSource = transform.FindChild("Tutorial").gameObject.transform.FindChild("FoxNadoSource").gameObject;
        foxNadoSpriteDone = foxNadoSource.transform.FindChild("FoxNadoSpriteDone").gameObject;
        foxNadoSpriteStart = foxNadoSource.transform.FindChild("FoxNadoSpriteStart").gameObject;
        foxInnerParticle = foxNadoSource.transform.FindChild("FoxInnerParticle").gameObject;
        foxWhiteFlash = foxNadoSource.transform.FindChild("FoxWhiteFlash").gameObject;
        foxNadoOrange = foxNadoSource.transform.FindChild("FoxNadoOrange").gameObject;

        featherStormParticle = transform.FindChild("Tutorial").gameObject.transform.FindChild("FeatherStorm@ParticleSystem").gameObject;
        featherDustParticle = transform.FindChild("Tutorial").gameObject.transform.FindChild("FeatherDust@ParticleSystem").gameObject;

        swipeLeftPS = transform.FindChild("Tutorial").gameObject.transform.FindChild("Swipe@ParticleLeft").gameObject.GetComponent<ParticleSystem>();
        swipeRightPS = transform.FindChild("Tutorial").gameObject.transform.FindChild("Swipe@ParticleRight").gameObject.GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //if we have no stamina left we set alive to false
        if (stamina <= 0)
        {
            if (GameManager.Instance.alive && OnDie != null)
                OnDie();

            GameManager.Instance.alive = false;
            
            //tell the ngui that we are not alive
           
        }
        FoxNadoCheck();

        FeatherStormCheck();

        ScoutBlinkCheck();

        //makes the mesh disseaper if the blinktime is over 0
        
        //Enforce seperation between scouts
		if (!switching) for (int i = 0; i < scouts.Count; i++)
        {
            Scout curScout = scouts[i];
            Scout nextScout;
            if (i + 1 == scouts.Count)
            {
                nextScout = scouts[0];
            }
            else
            {
                nextScout = scouts[i + 1];
            }
            int orderSep = Mathf.Abs(curScout.order - nextScout.order);
            //float dist = Mathf.Abs(scouts[i].transform.position.x - scouts[i + 1].transform.position.x);
            float dist = (curScout.transform.position - nextScout.transform.position).magnitude;
            if (Mathf.Abs(dist - separation) > 0.5f && !curScout.jumping && !nextScout.jumping)
            {
                if (curScout.order < nextScout.order)
                {
                    nextScout.transform.position = new Vector3(curScout.transform.position.x - separation * orderSep, nextScout.transform.position.y, nextScout.transform.position.z);
                }
                else
                {
                    curScout.transform.position = new Vector3(nextScout.transform.position.x - separation * orderSep   , curScout.transform.position.y, curScout.transform.position.z);
                }
            }
                
        }

        //calculate distance travelled
        if (Time.timeScale != 0)
        {
            Scout leadScout = GetScoutByOrder(0);
            distanceTravelled += leadScout.currentDirection.x;
        }

        //set postion of troop transform
        // Commented unless we need it, because of performance
        /*Vector3 addedVector = Vector3.zero;
        foreach (Scout s in scouts)
        {
            addedVector += s.transform.position;
        }
        Vector3 averageVector = addedVector / 3;
        transform.DetachChildren();
        float diff = (transform.position - averageVector).magnitude;
        distanceTravelled += diff;
        transform.position = averageVector;
        foreach (Scout s in scouts)
        {
            s.transform.parent = transform;
        }*/
	}

	public void DecrementStamina()
	{
		GameManager.Instance.troop.stamina--;
	}

    /// <summary>
    /// Fades IN a given sprite, currently set to do it over 1.5 seconds.
    /// </summary>
    /// <param name="rend"></param>
    /// <returns></returns>
    IEnumerator FadeIn(SpriteRenderer rend)
    {
        for (float i = 0f; i <= 150f; i += 1f)
        {
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, i / 150f);
            yield return new WaitForSeconds(0.01f);
        }
    }

    /// <summary>
    /// Fades OUT a given sprite, currently set to do it over 1 second.
    /// </summary>
    /// <param name="rend"></param>
    /// <returns></returns>
    IEnumerator FadeOut(SpriteRenderer rend)
    {
        for (float i = 100f; i > 0f; i -= 1f)
        {
            rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, i / 100f);
            yield return new WaitForSeconds(0.01f);
        }
    }
    #region FeatherStorm Methods
    /// <summary>
    /// FeatherStorm
    /// </summary>
    /// 
    public void FeatherStormStart(float absSpeed, float absRadius, float absAcceleration, float absTopSpeed)
    {
		audioMan.PlaySingleSound (gameObject, audioMan.transform.FindChild ("FeatherStormStart").gameObject, 1);
		audioMan.CrossFade (audioMan.transform.FindChild ("FeatherStormActive").gameObject, 1.0f, 1.0f);
		audioMan.transform.FindChild("FeatherStormActive").audio.pitch = 1f;
		audioMan.PitchEnvelope (audioMan.transform.FindChild ("FeatherStormActive").gameObject,10f, 1.5f);
        featherStormParticle.GetComponent<ParticleSystem>().Play();
        featherDustParticle.GetComponent<ParticleSystem>().Play();
        featherStormActive = true;
        this.absSpeed = absSpeed;
        this.absRadius = absRadius;
        this.absAcceleration = absAcceleration;
        this.absTopSpeed = absTopSpeed;

        if (pet != null)
        {
            //Debug.Log("Pet particles start");
            pet.FeatherStormStart();
        }
    }
    public void FeatherStormStop()
    {
		AudioManager.Instance.CrossFade (AudioManager.Instance.transform.FindChild ("FeatherStormActive").gameObject, 1f, 0f);
		AudioManager.Instance.PitchEnvelope (AudioManager.Instance.transform.FindChild ("FeatherStormActive").gameObject,1f, 1f);
        featherStormParticle.GetComponent<ParticleSystem>().Stop();
        featherDustParticle.GetComponent<ParticleSystem>().Stop();
        featherStormActive = false;

        if (pet != null)
        {
            //Debug.Log("Pet particles stop");
            pet.FeatherStormStop();
        }
    }

    public void FeatherStormCheck()
    {
        if (currentPowerUp != null)
        {
            if (featherStormActive)
            {
                CatchLogs(PowerUpsManager.PowerUpOrder.PowerUpEagle);
            }
        }
    }

    #endregion

    #region FoxNado Methods
    /// <summary>
    /// EndFoxNado stops particle systems and spinning. It starts the shrink, white flash, and fades out the sprite.
    /// </summary>
    /// 
    public void EndFoxNado()
    {
        foxNadoOrange.GetComponent<ParticleSystem>().Stop();
        foxInnerParticle.GetComponent<ParticleSystem>().Stop();
        //foxNadoSpriteDone.transform.renderer.enabled = true;
        foxNadoSpin = false;
        StartCoroutine("FadeOut", foxNadoSpriteDone.transform.GetComponent<SpriteRenderer>());
        foxWhiteFlash.GetComponent<ParticleSystem>().Emit(100);
        foxNadoShrink = true;
        Invoke("SwitchFoxNadoSprite", 0.25f);
        Invoke("foxNadoEffectsRemove", 2.5f);
    }

    /// <summary>
    /// Disables the sprite so foxnado is no longer visible and resets the shrink bool.
    /// </summary>
    private void foxNadoEffectsRemove()
    {
		foxNadoAudioSource.audio.Stop ();
		foxNadoAudioSource.audio.volume = 1.0f;
		pitchAddition = 0.05f;

        foxNadoSpriteDone.transform.GetComponent<SpriteRenderer>().enabled = false;
        foxNadoShrink = false;
    }
    /// <summary>
    /// First function of foxnado and starts the spinning, makes the sprite visible and fades it in.
    /// </summary>
    public void StartFoxNado()
    {
        foxNadoSpin = true;
        foxNadoSpriteStart.transform.GetComponent<SpriteRenderer>().enabled = true;
		audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("FoxNadoAudioPlayer").gameObject, 1);
        StartCoroutine("FadeIn", foxNadoSpriteStart.transform.GetComponent<SpriteRenderer>());
        foxSpinSpeed = 190f;
        Invoke("PlayFoxParticles", 1.5f);
        foxNadoSpriteDone.transform.localScale = Vector3.one;
    }

    /// <summary>
    /// Starts the foxnado main particle systems
    /// </summary>
    public void PlayFoxParticles()
    {
        foxNadoOrange.transform.GetComponent<ParticleSystem>().Play();
        foxInnerParticle.transform.GetComponent<ParticleSystem>().Play();
    }

    /// <summary>
    /// Switches the foxnado sprites, and is called during the white flash.
    /// </summary>
    public void SwitchFoxNadoSprite()
    {
        foxNadoSpriteStart.transform.GetComponent<SpriteRenderer>().enabled = false;
        foxNadoSpriteDone.transform.GetComponent<SpriteRenderer>().enabled = true;
		audioMan.CrossFade (foxNadoAudioSource, 0.5f, 0.0f);

    }
    
    /// <summary>
    /// Gets called from update, checks if we should be spinning, if we should be shrinking.
    /// </summary>
    private void FoxNadoCheck()
    {
        if (ActivePowerUp != null)
        {
            
            if (foxNadoSpin)
            {
                foxNadoSpriteStart.transform.Rotate(Vector3.forward * foxSpinSpeed * Time.deltaTime);
                CatchLogs(PowerUpsManager.PowerUpOrder.PowerUpFox);
                foxSpinSpeed += 120f * Time.deltaTime;

	
				foxNadoAudioSource.audio.pitch += pitchAddition * Time.deltaTime;
				pitchAddition *= 1.003f;
            }
            ActivePowerUp.duration -= Time.deltaTime;

			if(ActivePowerUp.Id == 1 && ActivePowerUp.duration <= 1.2f && ActivePowerUp.duration >= 1f && !endSoundStarted)
			{
				endSoundStarted = true;
				StartCoroutine(EndSound());
			}

            if (ActivePowerUp.duration <= 0)
            {
                ClearPowerUp();
            }
        }
        if (foxNadoShrink)
        {
            Transform newT = foxNadoSpriteDone.transform;
            newT.localScale = Vector3.Lerp(newT.localScale, Vector3.zero, 1f * Time.deltaTime);
            foxNadoSpriteDone.transform.localScale = newT.localScale;
        }
    }

	bool endSoundStarted = false;
	public IEnumerator EndSound()
	
		{		
			audioMan.transform.FindChild ("TurtlePowerUpEnd").audio.Play ();	
			yield return new WaitForSeconds (audioMan.transform.FindChild ("TurtlePowerUpEnd").audio.clip.length);
			endSoundStarted = false;
	}

    /// <summary>
    /// Pulls in nearby logs by finding all objects with "log" tag in a new sphere it creates with absradius.
    /// </summary>
    public void CatchLogs(PowerUpsManager.PowerUpOrder powerUp)
    {
        if (absSpeed < absTopSpeed)
            absSpeed += absAcceleration;
        Transform centerObject = GetScoutByOrder(0).transform;
        if (powerUp == PowerUpsManager.PowerUpOrder.PowerUpFox)
        {
            centerObject = foxNadoSource.transform;
        }
        else if (powerUp == PowerUpsManager.PowerUpOrder.PowerUpEagle)
        {
            centerObject = GetScoutByOrder(0).transform;
        }
        Collider[] objects = Physics.OverlapSphere(centerObject.position, absRadius);
        foreach (Collider c in objects)
        {
            if (c.gameObject.transform.tag == "Log")
                c.gameObject.transform.position = Vector3.Lerp(c.gameObject.transform.position, centerObject.transform.position,  absSpeed);
        }
    }
    #endregion

    #region Blink Methods
    public void Blink(int blinks)
    {
        if (RankManager.Instance.currentRank == RankManager.tutorialRank)
            return;

        blinkCoundown = blinks - 1;
        blinkTimeCoundown = blinkTime;
    }
    

    private void ScoutBlinkCheck()
    {
        if (blinkTimeCoundown > 0)
        {
            blinkTimeCoundown -= Time.deltaTime;
            foreach (Scout scout in scouts)
            {
                scout.myMesh.enabled = false;
            }
        }
        else if (blinkTimeCoundown < 0)
        {
            foreach (Scout scout in scouts)
            {
                scout.myMesh.enabled = true;
            }
            
            //if there is still more blinks back we call blink again
            if (blinkCoundown > 0)
            {
                Blink(blinkCoundown);
            }
            else
            {
                blinkTimeCoundown = 0;
            }
        }
    }

    


    #endregion

    public void RightSwitch()
	{
		Scout firstScout = null;
		Scout secondScout = null;
		Scout thirdScout = null;
		
		getOrderedScouts(out firstScout, out secondScout, out thirdScout);
		
		if (firstScout != null && secondScout != null && thirdScout != null && !switching && !challengeFailed)
		{
			/*
            MoveToPositionOf(thirdScout,firstScout);
			MoveToPositionOf(secondScout,thirdScout);
			MoveToPositionOf(firstScout,secondScout);
            */
			
			//SwapState(thirdScout, firstScout);
			
			Vector3 tempPos;
			Vector3 tempDir;
			AnimatorStateInfo tempState;

			//TODO: Enable/disable scout instead of checking on mesh renderer
			//Transform ScoutModel = thirdScout.gameObject.transform.GetChild(1).transform.GetChild(0);
			if (!thirdScout.IsEnabled())
			{
				tempPos = firstScout.transform.position;
				
				firstScout.transform.position = secondScout.transform.position;
				secondScout.transform.position = tempPos;
				
				tempDir = firstScout.currentDirection;
				
				firstScout.currentDirection = secondScout.currentDirection;
				secondScout.currentDirection = tempDir;
				
				tempState = firstScout.GetAnimState();
				
				firstScout.SetAnimState(secondScout.GetAnimState());
				secondScout.SetAnimState(tempState);
				
				//float should be animation time
				firstScout.setSwipeCooldown(0.12f);
				secondScout.setSwipeCooldown(0.12f);
				thirdScout.setSwipeCooldown(0.12f);
				
				// Play sound of new guy in front
				secondScout.SwitchingSound();
				
				thirdScout.order = 2;
				firstScout.order = 1;
				secondScout.order = 0;
				
			} else 
			{
				tempPos = thirdScout.transform.position;
				thirdScout.transform.position = firstScout.transform.position;
				firstScout.transform.position = secondScout.transform.position;
				secondScout.transform.position = tempPos;
				
				tempDir = thirdScout.currentDirection;
				thirdScout.currentDirection = firstScout.currentDirection;
				firstScout.currentDirection = secondScout.currentDirection;
				secondScout.currentDirection = tempDir;
				
				tempState = thirdScout.GetAnimState();
				thirdScout.SetAnimState(firstScout.GetAnimState());
				firstScout.SetAnimState(secondScout.GetAnimState());
				secondScout.SetAnimState(tempState);
				
				//float should be animation time
				firstScout.setSwipeCooldown(0.12f);
				secondScout.setSwipeCooldown(0.12f);
				thirdScout.setSwipeCooldown(0.12f);
				
				// Play sound of new guy in front
				thirdScout.SwitchingSound();
				
				thirdScout.order = 0;
				firstScout.order = 1;
				secondScout.order = 2;
			}
			
			PlaceSwipeAnimation(false);
			InGameGui.Instance.ShowCurrentScoutIcon();
		}
	}
	
	public void LeftSwitch()
	{
		Scout firstScout;
		Scout secondScout;
		Scout thirdScout;
		getOrderedScouts(out firstScout, out secondScout, out thirdScout);

		if (firstScout != null && secondScout != null && thirdScout != null && !switching && !challengeFailed)
		{
			
			Vector3 tempPos;
			Vector3 tempDir;
			AnimatorStateInfo tempState;
			/*
			MoveToPositionOf(firstScout,thirdScout);
			MoveToPositionOf(secondScout,firstScout);
			MoveToPositionOf(thirdScout,secondScout);
            */

            //if (enabledScouts == 1)
            //{
            //    tempPos = firstScout.transform.position;
            //    if (firstScout.IsEnabled())
            //        return;
            //    else if (secondScout.IsEnabled())
            //    {
            //        secondScout.transform.position = tempPos;
            //    }
            //}
            //else if (enabledScouts == 2)
            //{

            //}
            //else if (enabledScouts == 3)
            //{

            //}
			//TODO: Enable/disable scout instead of checking on mesh renderer
		//	Transform ScoutModel = thirdScout.gameObject.transform.GetChild(1).transform.GetChild(0);
			if (!thirdScout.IsEnabled())
			{
				tempPos = firstScout.transform.position;
				firstScout.transform.position = secondScout.transform.position;
				
				secondScout.transform.position = tempPos;
				
				tempDir = firstScout.currentDirection;
				firstScout.currentDirection = secondScout.currentDirection;
				
				secondScout.currentDirection = tempDir;
				
				tempState = firstScout.GetAnimState();
				firstScout.SetAnimState(secondScout.GetAnimState());
				secondScout.SetAnimState(tempState);
				
				//float should be animation time
				firstScout.setSwipeCooldown(0.12f);
				secondScout.setSwipeCooldown(0.12f);
				thirdScout.setSwipeCooldown(0.12f);
				
				// Play sound of new guy in front
				secondScout.SwitchingSound();
				
				firstScout.order = 1;
				secondScout.order = 0;
				thirdScout.order = 2;
				
			} else {
				
				tempPos = firstScout.transform.position;              
				firstScout.transform.position = thirdScout.transform.position;             
				thirdScout.transform.position = secondScout.transform.position;
				secondScout.transform.position = tempPos;
				
				tempDir = firstScout.currentDirection;
				firstScout.currentDirection = thirdScout.currentDirection;
				thirdScout.currentDirection = secondScout.currentDirection;
				secondScout.currentDirection = tempDir;
				
				tempState = firstScout.GetAnimState();
				firstScout.SetAnimState(thirdScout.GetAnimState());
				thirdScout.SetAnimState(secondScout.GetAnimState());
				secondScout.SetAnimState(tempState);
				
				//float should be animation time
				firstScout.setSwipeCooldown(0.12f);
				secondScout.setSwipeCooldown(0.12f);
				thirdScout.setSwipeCooldown(0.12f);
				
				// Play sound of new guy in front
				//HACK against playing the chubby sound in the intro comic
				if (RankManager.Instance.currentRank != RankManager.tutorialRank || GameManager.Instance.troop.distanceTravelled > 0)
					secondScout.SwitchingSound();
				
				firstScout.order = 2;
				secondScout.order = 0;
				thirdScout.order = 1;
			}

			PlaceSwipeAnimation(true);
			InGameGui.Instance.ShowCurrentScoutIcon();
		}
	}

    public bool AreScoutsGrounded()
    {
        foreach (Scout s in scouts)
        {
            if (!s.IsGrounded())
                return false;
        }
        return true;
    }

    public void IncreaseEnabledScouts()
    {
        enabledScouts++;
    }
    public void PlaceSwipeAnimation(bool leftToRight)
    {
        Scout s = GetScoutByOrder(0);
        if(leftToRight)
        {
            swipeLeftPS.startColor = s.scoutColor;
            swipeLeftPS.Emit(1);
            //Instantiate(swipeAnimObject, s.transform.position, Quaternion.identity);
        }
        else
        {
            swipeRightPS.startColor = s.scoutColor;
            swipeRightPS.Emit(1);
            //Instantiate(swipeAnimObject, s.transform.position, Quaternion.identity * Quaternion.Euler(0, 0, 180));
        }
            

    }

    public void getOrderedScouts(out Scout firstScout, out Scout secondScout, out Scout thirdScout)
    {
        firstScout = null;
        secondScout = null;
        thirdScout = null;

        for (int i = 0; i < scouts.Count; i++)
        {
            if (scouts[i].order == 0)
            {
                firstScout = scouts[i];
            }
            else if (scouts[i].order == 1)
            {
                secondScout = scouts[i];
            }
            else if (scouts[i].order == 2)
            {
                thirdScout = scouts[i];
            }
            else
            {
                //Debug.Log("WARNING: SCOUT POSITION HOLDS GARBAGE DATA");
            }
        }
    }

	public Scout GetScoutByOrder(int pos){

		for (int i = 0; i < scouts.Count; i++)
			if (scouts[i].order == pos)
				return scouts[i];

		return null;
	}

    public void Jump()
    {

        Scout firstScout;
        Scout secondScout;
        Scout thirdScout;
        getOrderedScouts(out firstScout, out secondScout, out thirdScout);

        if (firstScout.IsGrounded())
        {
            Instantiate(jumpTrigger, new Vector3(firstScout.transform.position.x + triggerSpawn, firstScout.transform.position.y + 2, firstScout.transform.position.z), Quaternion.identity);
            //Tell StatisticsManager
            StatisticsManager.Instance.IterateStatistic(Statistic.MovementJumpCount);
            StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunJumpCount);
            StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunDuckAndJump);
        }
        //Trigger the jump animation
        /*redScout.Jump(jumpSpeed);
        blueScout.Jump(jumpSpeed);
        greenScout.Jump(jumpSpeed);*/
    }

    public void Duck()
    {

        Scout firstScout;
        Scout secondScout;
        Scout thirdScout;
        getOrderedScouts(out firstScout, out secondScout, out thirdScout);

        Instantiate(duckTrigger, new Vector3(firstScout.transform.position.x + triggerSpawn, firstScout.transform.position.y + 2, firstScout.transform.position.z), Quaternion.identity);
        //Tell StatisticsManager
        StatisticsManager.Instance.IterateStatistic(Statistic.MovementDuckCount);
        StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunDuckCount);
        StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunDuckAndJump);
    }

  

    public void SetPowerUp(PowerUp powerUp)
    {
        this.ActivePowerUp = powerUp;
        powerUp.ToggleEffect();
    }

    public void ClearPowerUp()
    {
        if (ActivePowerUp != null)
        {
            ActivePowerUp.ToggleEffect();
            this.ActivePowerUp = null;
        }
    }

    public PowerUp GetActivePowerUp()
    {
        return ActivePowerUp;
    }

    #region Chase Mode Bear
    public void StartBear()
    {
		audioMan.PlaySingleSound (gameObject, audioMan.transform.FindChild ("BearAppearsAudioPlayer").gameObject, 1f);
        bear.GetComponent<Bear>().stopChase = false;
        bear.SetActive(true);
        bear.GetComponent<Animator>().Play("run");
        bear.transform.position = GetScoutByOrder(2).transform.position + bearDistance;
    }

    public void EndBear()
    {
        bear.GetComponent<Bear>().stopChase = true;
        Invoke("DisableBear", 1);
    }

    public void DisableBear()
    {
        bear.SetActive(false);
        bear.GetComponent<Bear>().currentDirection = Vector3.zero;
    }
    #endregion

    #region scout switching coroutines
    
    private void MoveToPositionOf(Scout s, Scout target){
		switching = true;

		//float xDistToTarget = target.transform.position.x - s.transform.position.x;
        float distToTarget = (target.transform.position - s.transform.position).magnitude;

        if (target.order > s.order)
        {
            distToTarget = -distToTarget;
        }

		float speedBoost = distToTarget / switchAnimDuration;
		s.BoostSpeedForMoment (speedBoost, switchAnimDuration);

		Invoke ("setNotSwitching", switchAnimDuration);
	}

	private void setNotSwitching() {
		switching = false;
	}

    public bool GetSwitching()
    {
        return switching;
    }

    private void SwapState(Scout from, Scout to)
    {

    }

	#endregion
}
