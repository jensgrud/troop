﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Scout : MonoBehaviour {

    public int order = 0;
    public bool soundEnabled = true;
    public float PicthDefault = 1.0f;
    public float PitchMax = 1.7f;
    public float PitchMin = 0.8f;

	public AudioSource FootstepSource;

    public List<AudioClip> SwitchingSoundClips = new List<AudioClip>();
	public List<AudioClip> AbilitySoundClips = new List<AudioClip>();
    public List<AudioClip> JumpSoundClips = new List<AudioClip>();
    public List<AudioClip> LandSoundClips = new List<AudioClip>();
    public List<AudioClip> DuckSoundClips = new List<AudioClip>();
	public List<AudioClip> FailSoundClips = new List<AudioClip>();
	public List<AudioClip> FootstepSoundClips = new List<AudioClip>();
	public List<AudioClip> FallSoundClips = new List<AudioClip>();
    public List<AudioClip> GrumpySoundClips = new List<AudioClip>();


    public GameObject lostLog;

  //  public float gravity = 0.03f;
    public float MaxDistToGround = 0.8f;
    public float MinDistToGround = 0.75f;
    public bool jumping = false;
    private Vector3 groundPos = Vector3.zero;
    private float swipeCooldown = 0;
    private bool isStuck = false;

    public Vector3 currentDirection = Vector3.zero;
    protected Animator anim;
    public SkinnedMeshRenderer myMesh;

    public  float duckLength = 1.5f;
    public GameObject basePosition;
    public Vector3 boxOrigSize = new Vector3 (1, 1.8f, 1);
    public float boxOrigCenter = 0.85f;

    public bool IsFlying { get; set; }

    public Color scoutColor = Color.magenta;

    private bool isEnabled = true;

    private LayerMask layerMask = 1 << 31;
    private RaycastHit hit;

    // Use this for initialization
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float speed = isStuck ? 0 : GameManager.Instance.GetSpeed();
  //      Debug.Log(speed);
  //      RaycastHit hitInf;
        bool aboveGround = Physics.Raycast(basePosition.transform.position, -Vector3.up, out hit, 1000, layerMask);
        Debug.DrawLine(transform.position, hit.point, Color.red, 20);

        if (aboveGround)
        {
            groundPos = hit.point;
        }
        else
        {
            transform.position = groundPos + new Vector3(0, 0.5f, 0);
            currentDirection.y = 0;
            Debug.DrawLine(transform.position, groundPos + new Vector3(0, 0.5f, 0), Color.magenta, 20);
            //Debug.Log("BELOW GROUND - GroundPos: " + groundPos);
        }

        if(!aboveGround)
            Physics.Raycast(basePosition.transform.position, -Vector3.up, out hit, 1000, layerMask);

        if (IsGrounded())
        {
            jumping = false;
        //    RaycastHit hit;
        //    Physics.Raycast(transform.position, -Vector3.up, out hit, MaxDistToGround, layerMask);
            Vector3 normal = hit.normal;
            Quaternion quat = Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            Vector3 forward = quat * normal;
            forward.Normalize();
			currentDirection =  ((speed + speedBoost) * Time.deltaTime) * forward;
            //Stay perpendicular to the ground, don´t use unless we get the smooth terrain into the build.
            //transform.forward = forward;

        }
        else if (IsFlying)
        {
          //  RaycastHit hit;
      //      Physics.Raycast(transform.position, -Vector3.up, out hit, 1000,layerMask);
            Vector3 normal = hit.normal;
            Quaternion quat = Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            Vector3 forward = quat * normal;

            if (transform.position.y < hit.point.y + 5)
                forward.y = 1;
            else if (transform.position.y > hit.point.y + 6)
                forward.y = -1;
            else
                forward.y = 0;
            forward.Normalize();
            currentDirection = ((speed + speedBoost) * Time.deltaTime) * forward;
        }
        else
        {
            currentDirection += new Vector3(0, -GameManager.Instance.GetGravity(), 0);
            //if (currentDirection.x > ((GameManager.Instance.GetSpeed() + speedBoost) * Time.deltaTime))
            //{
            //    currentDirection = new Vector3(((GameManager.Instance.GetSpeed() + speedBoost) * Time.deltaTime), currentDirection.y, currentDirection.z);
            //}
        }
        Vector3 newPos = transform.position + currentDirection;
        newPos.z = 0;
        transform.position = newPos;

        Physics.Raycast(basePosition.transform.position, -Vector3.up, out hit, 100, layerMask);

        if (CheckGroundClipping())
        {
            transform.position += 0.1f * Vector3.up;
            //Debug.Log("up");
        }

        if (IsGrounded())
        {
            //make sure that anim actually have an animator.
            if (anim == null)
                anim = GetComponentInChildren<Animator>();
            AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
			if (stateInfo.nameHash == Animator.StringToHash("Base Layer.air"))
            {
                anim.SetTrigger("landingTrigger");
               // Debug.Log("landed");
            }
        }
        if (swipeCooldown > 0)
        {
            swipeCooldown -= Time.deltaTime;
            if (swipeCooldown < 0)
            {
                swipeCooldown = 0;
            }
        }
    }

    void LateUpdate() {
    }

    public bool IsGrounded()
    {
   //     LayerMask layerMask = 1 << 31;
        //need to get this working i guess
        if (hit.distance <= MaxDistToGround)
            return true;
        else
            return false;
        //bool isGrounded = Physics.Raycast(basePosition.transform.position, -Vector3.up, MaxDistToGround, layerMask);
        //return isGrounded;
 

    }

    public bool CheckGroundClipping()
    {
       // LayerMask layerMask = 1 << 31;
    //    bool check = Physics.Raycast(basePosition.transform.position, -Vector3.up, MinDistToGround, layerMask);

        if (hit.distance < MinDistToGround)
            return true;
        else
            return false;
        //Debug.DrawLine(basePosition.transform.position, new Vector3(basePosition.transform.position.x, basePosition.transform.position.y - MinDistToGround, basePosition.transform.position.z),Color.green, 10);
    //    return check;
    }


    public void Jump(float jumpSpeed)
    {
        if (IsGrounded())
        {

            AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
            float animTime = 0.1f;
            invokeJumpVar = jumpSpeed;
			if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run")||stateInfo.nameHash == Animator.StringToHash("Base Layer.duck")||stateInfo.nameHash == Animator.StringToHash("Base Layer.collision")||stateInfo.nameHash == Animator.StringToHash("Base Layer.land"))
            {
                anim.SetTrigger("jumpTrigger");
                //upVelocity = jumpSpeed;
                //transform.rigidbody.AddForce(Vector3.up * jumpSpeed);
            }
            if (stateInfo.nameHash == Animator.StringToHash("Base Layer.useSkill"))
            {
                anim.SetTrigger("jumpTrigger");
                animTime = 0.1f;
            }

            Invoke("JumpTransform", animTime);
        }
    }

    private float invokeJumpVar;
    void JumpTransform()
    {
        if (swipeCooldown <= 0)
        {
            transform.position += Vector3.up * 0.5f;
            currentDirection.y = invokeJumpVar;
            jumping = true;
        }
    }
    public void Duck (float duckSpeed)
    {

        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run")||stateInfo.nameHash == Animator.StringToHash("Base Layer.land"))
        {
			anim.SetTrigger("duckTrigger");
            StartCoroutine("changeCollider");
        }


    }


    IEnumerator changeCollider ()
    {
       
        BoxCollider boxCollider = this.transform.GetComponent<BoxCollider>();
        //Vector3 boxOrigSize = boxCollider.size;
        //float boxOrigCenter = boxCollider.center.y;
        boxCollider.size = new Vector3(0.7f, 0.7f, 0.7f);
        boxCollider.center = new Vector3(0, 0.5f, 0);
       // basePosition.transform.position = new Vector3(basePosition.transform.position.x, basePosition.transform.position.y + 0.02f, basePosition.transform.position.z);
      
        yield return new WaitForSeconds(duckLength);
        boxCollider.size = boxOrigSize;
        boxCollider.center = new Vector3(0, boxOrigCenter, 0);
        //basePosition.transform.position = new Vector3(basePosition.transform.position.x, basePosition.transform.position.y - 0.02f, basePosition.transform.position.z);

    }


    public Animator getAnimation()
    {
        return this.anim;
    }


    private float speedBoost = 0f;
	public void BoostSpeedForMoment(float speedBoost, float duration){
		this.speedBoost = speedBoost;
		Invoke ("stopBoost", duration);
	}

	private void stopBoost()
    {
		this.speedBoost = 0f;
	}

    //IEnumerator waitToJump()
    //{
    //    yield return new WaitForSeconds(4f);
    //    Debug.Log("waiting to jump");
    //}

    //void waitToJump()
    //{
    //    AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
    //    if (stateInfo.nameHash == Animator.StringToHash("Base Layer.ascend"))
    //    {
    //        Invoke("Jump", 0.5f);
    //    }
    //}

	void AbilitySound()
	{
        Play(this.AbilitySoundClips);
	}

	void JumpSound()
	{
        Play(this.JumpSoundClips);
	}

	void LandSound()
	{
        Play(this.LandSoundClips);
	}

	void DuckSound()
	{
        Play(this.DuckSoundClips);
	}

	void FailSound()
	{
		if (this.order == 0) 
		{
			Play(this.FailSoundClips);
		}
	}

	void FallSound()
	{
		Play(this.FallSoundClips);
	}

	void FootstepSound()
	{
		PlayFootstep(this.FootstepSoundClips);
	}

    public void SwitchingSound()
    {
        Play(this.SwitchingSoundClips);
    }

    /// <summary>
    /// Playing random audio clip from list, with randomized pitch
    /// </summary>
    /// <param name="audioClips">List of audio clips to choose from</param>

    void Play(List<AudioClip> audioClips)
    {



        if (audioClips.Count == 0 || !soundEnabled)
        {
            return;
        }

        int pos = Random.Range(0, audioClips.Count);

        AudioClip audioClip = audioClips[pos];


        audio.pitch = Random.Range(this.PitchMin, this.PitchMax);
        audio.pitch = AudioManager.Instance.TimeShellPitchDrop(audio).pitch; // changes the pitch if the TimeShell is active.
        audio.PlayOneShot(audioClip);
    }

	void PlayFootstep(List<AudioClip> audioClips)
	{
		if (audioClips.Count == 0 || !soundEnabled)
		{
			return;
		}
		
		int pos = Random.Range(0, audioClips.Count);
		
		AudioClip audioClip = audioClips[pos];
		
		//FootstepSource.pitch = Random.Range(this.PitchMin, this.PitchMax);
		FootstepSource.PlayOneShot(audioClip);
	}
	
	public void setSwipeCooldown(float n)
    {
        swipeCooldown = n;
    }

    public float getSwipeCooldown()
    {
        return swipeCooldown;
    }

    public AnimatorStateInfo GetAnimState()
    {
        return anim.GetCurrentAnimatorStateInfo(0);
    }

    public void SetAnimState(AnimatorStateInfo state)
    {
        if (state.nameHash != Animator.StringToHash("Base Layer.useSkill") && state.nameHash != Animator.StringToHash("Base Layer.duck"))
        {
            anim.CrossFade(state.nameHash, 0, 0, state.normalizedTime);
        }
    }

    public void SetStuck(bool val)
    {
        isStuck = val;
    }

    public void SpawnLostLogs(int amount)
    {
        int myAmount;
        if (PlayerWallet.Instance.LogsInThisGame < amount)
        {
            myAmount = PlayerWallet.Instance.LogsInThisGame;
        }
        else
        {
            myAmount = amount;
        }
        for (int i = 0; i < myAmount; i++)
        {
            GameObject log = ProceduralDatabaseManager.Instance.GetOtherObject(lostLog);
            log.transform.Translate(transform.position, Space.World);
            float randAngle = Random.Range(0, 360);
            log.transform.Rotate(new Vector3(0, 0, 1), randAngle);

         //   Instantiate(lostLog, transform.position, Quaternion.identity);
        }
    }

    public void PlayGrumpySound()
    {
        Play(GrumpySoundClips);
    }

    public bool IsEnabled()
    {
        return isEnabled;
    }

    public void SetEnabled(bool state)
    {
        isEnabled = state;
    }

    public bool GetIsStuck()
    {
        return isStuck;
    }
}




