﻿using UnityEngine;
using System.Collections;

public class SwipeAnimationController : MonoBehaviour {

    public float lifetime = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 yOffSet;
        if (transform.rotation.eulerAngles == Vector3.zero)
        {
            yOffSet = new Vector3(0, 1, 0);
        }
        else
        {
            yOffSet = new Vector3(0, 2, 0);
        }
        transform.position = GameManager.Instance.troop.GetScoutByOrder(1).transform.position + yOffSet;
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
	}
}
