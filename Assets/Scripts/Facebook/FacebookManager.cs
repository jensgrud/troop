﻿using UnityEngine;
using System.Collections;

public class FacebookManager : MonoBehaviour {

    private bool takingScreenshot = false;
    private bool postingFeed = false;

	public static FacebookManager Instance;

    void Awake()
    {
		Instance = this;
        FB.Init(SetInit);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnLoggedIn()
    {
        Debug.Log("Logged in. ID: " + FB.UserId);
        if (postingFeed)
        {
            PostOnFeed();
        }
        if (takingScreenshot)
        {
            UploadScreenShot();
        }
    }

    private void SetInit()
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("Already Logged In");
            OnLoggedIn();
        }
    }

    public void Login()
    {
        FB.Login("email, publish_actions", LoginCallBack);
    }


    public void LoginCallBack(FBResult result)
    {
        Debug.Log("LoginCallBack");
        if (FB.IsLoggedIn)
        {
            OnLoggedIn();
        }
    }

    public void UploadScreenShot()
    {
        takingScreenshot = true;
        if (FB.IsLoggedIn)
        {
            StartCoroutine(TakeScreenshot());
            takingScreenshot = false;
        }
        else
        {
            Login();
        }
    }

    public void PostOnFeed()
    {
        postingFeed = true;
        if (FB.IsLoggedIn)
        {
            FB.Feed(FB.UserId);
            postingFeed = false;
        }
        else
        {
            Login();
        }
    }

    private IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        var width = Screen.width;
        var height = Screen.height;
        var tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();
        byte[] screenshot = tex.EncodeToPNG();

        var wwwForm = new WWWForm();
        wwwForm.AddBinaryData("image", screenshot, "ScoutBadges.png");
        wwwForm.AddField("message", "My SCOUTS! badge collection!");

        FB.API("me/photos", Facebook.HttpMethod.POST, ScreenShotCallBack, wwwForm);
    }

    private void ScreenShotCallBack(FBResult res)
    {
        Debug.Log("Post Screenshot Done");

		GuiManager.Instance.InitGenericPopup ("FB Shared Label", GuiManager.Instance.gameObject, "HidePopup",0);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Info);
    }
}
