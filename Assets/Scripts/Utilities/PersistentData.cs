﻿
using System.Xml.Serialization;
using System.IO;
using Polenter.Serialization;
using UnityEngine;

public static class PersistentData
{
    static SharpSerializer serializer;

    private static string GetPath(object sender)
    {
        return Path.Combine(UnityEngine.Application.persistentDataPath, sender.GetType().ToString() + ".xml");
    }

    /// <summary>
    /// Serializes data to an XML file.
    /// </summary>
    /// <param name="data">Data to save.</param>
    /// <param name="sender">The instance you call this from. Usually "this".</param>
    /// <returns>XML file it was saved to.</returns>
    public static string Serialize(object data, object sender)
    {
        string path = GetPath(sender);
        new System.Threading.Timer(obj =>
            {
                if (serializer == null)
                    serializer = new SharpSerializer();
                serializer.Serialize(data, path);
            }, 
            null, 
            0, 
            System.Threading.Timeout.Infinite);
        return path;
    }

    /// <summary>
    /// Deserialize data from an XML file.
    /// </summary>
    /// <typeparam name="T">Type of data to load.</typeparam>
    /// <param name="sender">The instance you call this from. Usually "this".</param>
    /// <returns>Data that was loaded.</returns>
    public static T Deserialize<T>(object sender)
    {
        /*
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        string path = GetPath(sender);

        T ret;

        using (FileStream fs = new FileStream(path, FileMode.Open))
            ret = (T)serializer.Deserialize(fs);

        return ret;
         */


        if (serializer == null)
            serializer = new SharpSerializer();
        return (T)serializer.Deserialize(GetPath(sender));
    }

    /// <summary>
    /// Checks if an XML file for sender already exists.
    /// </summary>
    /// <param name="sender">The instance you call this from. Usually "this".</param>
    /// <returns>True if an XML file exists, otherwise false.</returns>
    public static bool SavedDataExists(object sender)
    {
        return File.Exists(GetPath(sender));
    }

    /// <summary>
    /// Deletes the XML file for sender.
    /// </summary>
    /// <param name="sender">The instance you call this from. Usually "this".</param>
    public static void DeleteSavedData(object sender)
    {
        File.Delete(GetPath(sender));
    }
}
