﻿using UnityEngine;
using System.Collections;
using System.Globalization;
using SmartLocalization;


// EVERYTHING IS TAKEN FROM:
// http://forum.unity3d.com/threads/smart-localization-with-ngui.189253/


[RequireComponent(typeof(UIWidget))]
[AddComponentMenu("NGUI/UI/LocalizeWidget")]
public class LocalizeWidget : MonoBehaviour
{
    // No public variables, we'll get the key from the widget field
    public string key;
    private string mLanguage;
    private LanguageManager loc;

    // Localize the widget on start.
    private void Start()
    {
        // Reference the language manager
        loc = LanguageManager.Instance;

        // Hook up a delegate to run the localize script whenever a language was changed
        loc.OnChangeLanguage += new ChangeLanguageEventHandler(Localize);

        // Initial localize run
        Localize();
    }

    // Incase the script didn't get the message from being inactive
    //private void OnEnable()
    //{
    //    if (mLanguage != loc.LoadedLanguage) // originally loc.language
    //        Localize();
    //}

    // Force-localize the widget.
    private void Localize(LanguageManager thisLanguage = null)
    {
        UIWidget w = GetComponent<UIWidget>();
        UILabel lbl = w as UILabel;
        UISprite sp = w as UISprite;
 
        //// If no localization key has been specified, use the label's text as the key
        //if (lbl != null)
        //    key = lbl.text;
       
        string val = loc.GetTextValue(key);
       
        if(string.IsNullOrEmpty(val))
            val = "Missing String";
 
        if (lbl != null)
        {
            // If this is a label used by input, we should localize its default value instead
            UIInput input = NGUITools.FindInParents<UIInput>(lbl.gameObject);
           
            if (input != null && input.label == lbl) // the && was missing and might be a ||
                input.defaultText = val;
            else
                lbl.text = val;
        }
        else if (sp != null)
        {
            sp.spriteName = val;
            sp.MakePixelPerfect();
        }
       
        // Set this widget's current language
        mLanguage = loc.LoadedLanguage;
    }

	public void Reload(){
		if (loc != null)
			Localize ();
	}
}
