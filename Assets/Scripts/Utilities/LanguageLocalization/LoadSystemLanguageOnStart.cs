﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class LoadSystemLanguageOnStart : MonoBehaviour
{

    public static string savedLanguage = "en-GB";

    // Use this for initialization
    void Start()
    {
        LanguageManager lm = LanguageManager.Instance;

        lm.ChangeLanguage(savedLanguage);


    }
}