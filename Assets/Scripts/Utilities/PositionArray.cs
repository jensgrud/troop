﻿using UnityEngine;
using System.Collections;
using System;

public class PositionArray  {

    private int size;

    private float[] xVals;
    private float[] yVals;

    public PositionArray(int size)
    {
        this.size = size;
        xVals = new float[size];
        yVals = new float[size];
    }

    public void Add(float xVal, float yVal)
    {
        float[] newXArr = new float[size];
        float[] newYArr = new float[size];
        newXArr[0] = xVal;
        newYArr[0] = yVal;

        Array.Copy(xVals, 0, newXArr, 1, size - 1);
        Array.Copy(yVals, 0, newYArr, 1, size - 1);

        xVals = newXArr;
        yVals = newYArr;
    }

    public float GetValue(float key)
    {
        for (int i = 0; i < size; i++)
        {
            if (Mathf.Abs(xVals[i] - key) < 0.1f)
            {
                return yVals[i];
            }
        }
        return -1000;
    }

    public string ToString()
    {
        string str = "";
        for (int i = 0; i < size; i++)
        {
            str += "K: " + xVals[i] + " V: " + yVals[i] + "\n";
        }
        return str;
    }
}
