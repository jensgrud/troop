﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Script for performing builds from the Unity status bar
/// </summary>
public class BuildScript 
{
    /// <summary>
    /// Scenes to be build
    /// </summary>
    private static string[] scences = FindEnabledEditorScenes();

    /// <summary>
    /// Name of the .apk
    /// </summary>
    private static string appName = "Troop";

    /// <summary>
    /// Build directory
    /// </summary>
	private static string directory = "Builds";
	
    /// <summary>
    /// Perform android build
    /// </summary>
	[MenuItem("Build/Android")]
	public static void PerformAndroidBuild()
	{
		string target_dir = appName + ".apk";
		GenericBuild(scences, directory + "/" + target_dir, BuildTarget.Android, BuildOptions.None);
	}
	
    /// <summary>
    /// Find scenes added to build
    /// </summary>
    /// <returns>Array of scenes</returns>
	private static string[] FindEnabledEditorScenes() 
    {
		List<string> editorScenes = new List<string>();
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) 
        {
            if (!scene.enabled)
            {
                continue;
            }

			editorScenes.Add(scene.path);
		}

		return editorScenes.ToArray();
	}
	
    /// <summary>
    /// Perform a generic build
    /// </summary>
    /// <param name="scenes">Scenes to be build</param>
    /// <param name="target_dir">Target directory</param>
    /// <param name="build_target">Platform target</param>
    /// <param name="build_options">Build options</param>
	private static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
	{
		EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
		string result = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
		if (result.Length > 0) 
        {
			throw new System.Exception("BuildPlayer failure: " + result);
		}
	}
}
#endif