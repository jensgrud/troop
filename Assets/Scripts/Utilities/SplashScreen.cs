﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplashScreen : MonoBehaviour {

    public List<Texture> texList;
    public List<float> displayTimerList;
    private float timer = 0;
    private float lastRealTime = 0;
    private int currentSplash = 0;
    public bool gameStarted = false;
    public bool debugMode = true;

    public static SplashScreen Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        if (SplashScreen.Instance != null && SplashScreen.Instance != this)
        {
            SplashScreen.Instance.debugMode = true;
            SplashScreen.Instance.StartGame();
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(transform.gameObject);
    }

	// Use this for initialization
	void Start () {
        lastRealTime = Time.realtimeSinceStartup;
        timer = lastRealTime;
        if (debugMode)
        {
            StartGame();
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!gameStarted && currentSplash == displayTimerList.Count)
        {
            StartGame();
        }
	}

    public void StartGame()
    {
        gameStarted = true;
        GuiManager.Instance.ShowMenu(GuiManager.MenuType.Start);
    }

    public void OnGUI()
    {
        if (!debugMode && texList.Count - 1 >= currentSplash && displayTimerList.Count - 1 >= currentSplash)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texList[currentSplash]);
            if (displayTimerList[currentSplash] < Time.realtimeSinceStartup - lastRealTime)
            {
                lastRealTime = Time.realtimeSinceStartup;
                currentSplash++;
            }
        }
    }

    public void skipSplash()
    {
        if (0.2f < Time.realtimeSinceStartup - timer && displayTimerList.Count -1 >= currentSplash)
        {
            displayTimerList[currentSplash] = 0f;
            timer = Time.realtimeSinceStartup;
        }
    }
}
