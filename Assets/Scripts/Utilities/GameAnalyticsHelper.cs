﻿using UnityEngine;
using System.Collections;

public class GameAnalyticsHelper : MonoBehaviour {

	private static float _GameStartTime;
	public static float GameStartTime {
		set {
			_GameStartTime = value;
			BadgesAmount = 0f;
			RanksAmount = 0f;
		} 
		get {
			return _GameStartTime;
		}
	}
	public static float GameEndTime;
	public static float GameDuration {
		get {return GameEndTime - GameStartTime; } 
	}


	public static float BadgesAmount = 0f;
	public static float RanksAmount = 0f;

}
