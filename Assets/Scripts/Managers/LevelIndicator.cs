﻿using UnityEngine;
using System.Collections;

public class LevelIndicator : MonoBehaviour {

	public GameObject blueScout;
	float distance;

	// Use this for initialization
	void Start () {
		distance = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		UILabel lbl = GetComponent<UILabel>();
		distance = blueScout.transform.position.x;
		
		if (distance < 250)
			lbl.text = "Level 1";
		else if (distance > 250 && distance < 600)
			lbl.text = "Level 2"; 
		else if (distance > 600 && distance < 1050)
			lbl.text = "Level 3"; 
		else if (distance > 1050 && distance < 1600)
			lbl.text = "Level 4"; 
		else if (distance > 1600)
			lbl.text = "Last Level"; 
			 
	}
}
