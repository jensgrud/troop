﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class GameManager : MonoBehaviour {

    public enum GameStateEnum { inField, Camp, Menu}
    public Troop troop;
    public CameraFocusMovement cameraFocus;

    //Debug variables
    public bool invincibleAgainstChallenges = false;//Should this be here or in the scout manager??
    public bool invincibleAgainstObstacles = false;//Should this be here or in the scout manager??
    public bool enableChallenges = true;
    public bool enableBonus = true;

    //Game state vars
    public int numOfDeaths = 0;
    public int noCampStreak = 0;
    public int streakThreshhold = 1;

    public float runSpeed = 6;
    public bool alive = true; //Shoudl this be here or in the scout manager??
    //public float currentSpeed;
    public float timeScaleReadOnly;
    public float maxTimeScale = 2;
    
    private float speedScaleFactor;
    private float baseTimeScale = 1;
    private float tempTimeScaleModifier = 0;

	private float _prevTime;
    private float prevTime {
		get {
			if (_prevTime == 0f)
				_prevTime = Time.timeScale == 0 ? 1 : Time.timeScale;
			return _prevTime;} 
		set {
			if (value != 0f)
				_prevTime = value;
		}
	}

    private float chaseSpeedFactor;
    public float gravity;
    public float jumpSpeed;

    private int activeLevel;

    //TerreinDisplayer for the protptype...
    //Assume only one terrain displayer at a time
    public TerrainDisplayer terrainDisplayer;

    public static GameManager Instance
    {
        get;
        private set;
    }

    public GameStateEnum state = GameStateEnum.Menu;
    public bool slowedDown = false;
    public bool speedUp = false;
    public bool chaseMode = false;

	public float IntroFaceTime = 3f;

    public bool isDeathTutorialComplete = true;

    void Awake()
    {
        Instance = this;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		StartGame();

        if (PersistentData.SavedDataExists(this))
            LanguageManager.Instance.ChangeLanguage(PersistentData.Deserialize<string>(this));

        //TODO: remove when persistant data works for this
        /*if (LanguageManager.Instance.LoadedLanguage == "da-DK")
        {
            LanguageManager.Instance.ChangeLanguage("en-GB");
        }*/
    }

	// Use this for initialization
	void Start () 
	{
	    terrainDisplayer = GameObject.FindObjectOfType(typeof(TerrainDisplayer)) as TerrainDisplayer;
        troop.OnDie += troop_OnDie;
        speedScaleFactor = 0;
       // prevTime = Time.timeScale;
	}

    public void SaveLanguage()
    {
        PersistentData.Serialize(LanguageManager.Instance.LoadedLanguage, this);
    }

	public void StartGame()
	{
		if (RankManager.Instance.currentRank != RankManager.tutorialRank)
		{
			activeLevel = 3;
			troop.IncreaseEnabledScouts();
			troop.IncreaseEnabledScouts();
			troop.IncreaseEnabledScouts();
		} else
		{
			activeLevel = 0;
			TouchManager.Instance.JumpActive(false);
			TouchManager.Instance.DuckActive(false);
			TouchManager.Instance.SwitchActive(false);
		}
	}

    public void DisableTroop()
    {
        GameObject[] scouts = GameObject.FindGameObjectsWithTag("Scout");
		
        foreach (GameObject scout in scouts)
        {
			if (scout.transform.FindChild("colorTagGreen") != null) {
				troop.IncreaseEnabledScouts();
				while (scout.gameObject.GetComponent<Scout>().order != 0)
                	GameManager.Instance.troop.LeftSwitch();
				continue;
			}

            foreach (Transform child in scout.transform)
                child.gameObject.SetActive(false);

            AudioSource[] audio = scout.GetComponents<AudioSource>();
            foreach (AudioSource a in audio)
                a.enabled = false;

            scout.GetComponent<Scout>().SetEnabled(false);
        }
	}

    void troop_OnDie()
    {
        numOfDeaths++;
    }

	// Update is called once per frame
	void Update () {
	    //if the troop is not alive we show pop up that ask for continue
		if (!alive && GuiManager.Instance.CurrentPopup != GuiManager.PopupType.OutOfStamina && GuiManager.Instance.CurrentPopup != GuiManager.PopupType.Info)
        {
            TutorialDefinition def = Tutorial.Instance.TutorialDefinitions.Find(d => d.tutorialClass == Tutorial.TutorialClass.Death);

            if (def.OnlyRunOnce && !def.HasRunOnce)
            {
                AudioManager.Instance.transform.FindChild("Soundtrack").gameObject.audio.volume = 0.0f;
                AudioManager.Instance.transform.FindChild("ChaseModetrack").gameObject.audio.volume = 0.0f;

                GameManager.Instance.StopTime();

                AudioManager.Instance.PlaySingleSound(
                    gameObject,
                    AudioManager.Instance.transform.FindChild("GameOverAudioPlayer").gameObject,
                    1
                );

                GameManager.Instance.isDeathTutorialComplete = false;

                GuiManager.Instance.InitGenericPopup("OnDeathTutorial", gameObject, "OnDeathTutorial", 0);
                GuiManager.Instance.ShowPopup(GuiManager.PopupType.Info);
            }
            else
                GuiManager.Instance.ShowPopup(GuiManager.PopupType.OutOfStamina);
        }
        timeScaleReadOnly = Time.timeScale;
	}

    public void OnDeathTutorial()
    {
        Tutorial.Instance.TutorialDefinitions.Find(d => d.tutorialClass == Tutorial.TutorialClass.Death).HasRunOnce = true;

        PlayerWallet.Instance.Marshmallows += 3;

        //GuiManager.Instance.ShowPopup(GuiManager.PopupType.OutOfStamina);
        //Note: Instead of doing the above, we just let Update run again in order to open it
    }

    public int GetLevel()
    {
        return activeLevel;
    }

    public void ToggleMagnet(float absSpeed, float absRadius, float absAcceleration, float absTopSpeed)
    {
        troop.absSpeed = absSpeed;
        troop.absRadius = absRadius;
        troop.absAcceleration = absAcceleration;
        troop.absTopSpeed = absTopSpeed;
        troop.foxNadoActive = !troop.foxNadoActive;

        if (troop.foxNadoActive)
        {
            troop.StartFoxNado();
        }
        else
        {
            troop.EndFoxNado();
        }
    }

    //public void ToggleSpeedUp()
    //{
    //    if (!speedUp)
    //    {
    //       ScaleSpeed(2f);
    //        speedUp = true;
    //    }
    //    else
    //    {
    //        ScaleSpeed(0.5f);
    //        speedUp = false;
    //    }
    //}

    public void ToggleChallengeInvicibility()
    {
        invincibleAgainstChallenges = !invincibleAgainstChallenges;
    }

    public void ToggleObstacleInvincibility()
    {
        invincibleAgainstObstacles = !invincibleAgainstObstacles;
    }

    private float currentChaseSpeed;
    private float chaseVolume;
    public void SetChaseMode(float chaseSpeed, bool val)
    {
        if (val)
        {
            GameObject soundtrack = GameObject.Find("AudioManager/Soundtrack");
            GameObject chasetrack = GameObject.Find("AudioManager/ChaseModetrack");
            float crossfadeLength = 1;
            chaseVolume = soundtrack.GetComponent<AudioSource>().volume;
            AudioManager.Instance.CrossFade(soundtrack, chasetrack, crossfadeLength, 0, chaseVolume);
            //chaseSpeedFactor = chaseSpeed;
            //ScaleSpeed(chaseSpeedFactor); // we should use addition here
            currentChaseSpeed = chaseSpeed;
            ChangeTempTimeMod(currentChaseSpeed);
            chaseMode = true;
            troop.StartBear();
			GuiManager.Instance.CurrentMenuReference.GetComponent<InGameGui>().EnableChaseIcon(3);
        }
        else if(chaseMode)
        {
            //ScaleSpeed(1 / chaseSpeedFactor); // we should use decrement here
            ProceduralManager.Instance.SetChasing(false);
            ChangeTempTimeMod(-currentChaseSpeed);
            chaseMode = false;
            troop.EndBear();
            GameObject soundtrack = GameObject.Find("AudioManager/Soundtrack");
            GameObject chasetrack = GameObject.Find("AudioManager/ChaseModetrack");
            float crossfadeLength = 3;
            AudioManager.Instance.CrossFade(soundtrack, chasetrack, crossfadeLength, chaseVolume, 0);
        }
    }

    
    private bool stopSpeed = false;

    //public float ScaleSpeed(float scaleFactor)
    //{
    //    if (scaleFactor == 0)
    //    {
    //        StopSpeed();
    //        return 0;
    //    }

    //    speedScaleFactor *= scaleFactor;
    //    return speedScaleFactor;
    //}

    public float GetSpeed()
    {
        if (stopSpeed)
            return 0;
        else
            return speedScaleFactor+runSpeed;
    }

    public void StopSpeed()
    {
        stopSpeed = true;
   
    }
    public float ResetSpeed()
    {
        if (stopSpeed)
            stopSpeed = false;
        else
            speedScaleFactor = 0;
        return speedScaleFactor;
    }

    //this function will change the base value of the speed + the rank spee modifier, it should be called only in ProceduralManager
    public float UpdateLevelSpeed(float newSpeed)
    {
        if (RankManager.Instance.currentRank >= 0 && RankManager.Instance.currentRank < RankManager.Instance.registeredRanks.Count)//Index out of range check
			baseTimeScale = newSpeed + RankManager.Instance.registeredRanks[RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].speedModifier;
        else
            baseTimeScale = newSpeed;
        SetTimeScale();
 
        return runSpeed;
    }
    public float GetTimeScale()
    {
        return Time.timeScale;
    }

    public float ScaleTime(float scaleFactor)
    {
        baseTimeScale *= scaleFactor;
        SetTimeScale();
        /*if(Time.timeScale != 0)
            prevTime = Time.timeScale;
        Time.timeScale *= scaleFactor;*/
        
        return Time.timeScale;
    }

    public float ResetTimeScale()
    {
  //      Debug.Log("Reset Time Called");
        SetTimeScale();
        return Time.timeScale;
        /*Time.timeScale = prevTime;
        return Time.timeScale;*/
    }

    public void StopTime()
    {
//        Debug.Log("Stop Time Called");
        Time.timeScale = 0;
    }

    public void SetTimeScale()
    {
        float newVar = baseTimeScale * (1 + tempTimeScaleModifier);
        if (newVar > maxTimeScale)
            newVar = maxTimeScale;
        Time.timeScale = newVar;
        Time.fixedDeltaTime = 0.01f;// *Time.timeScale;
    }

    public void ChangeTempTimeMod(float modifier)
    {
        tempTimeScaleModifier += modifier;
        SetTimeScale();
    }


    public void AddSpeed(float modifier)
    {
        speedScaleFactor += modifier;
    }

    public void DecrementSpeed (float modifier)
    {
        if (/*speedScaleFactor - modifier != 0 &&*/ !chaseMode)
            speedScaleFactor = 0;
        else
            speedScaleFactor -= modifier;
    }

    public float GetGravity()
    {
        return gravity;
    }

    public float GetJumpSpeed()
    {
        return jumpSpeed;
    }

    public void IncrementStreak()
    {
        noCampStreak++;
        GuiManager.Instance.CurrentMenuReference.GetComponent<InGameGui>().EnableStreakIcon(2);
        GameObject streakSound = GameObject.Find("AudioManager/RiskBonus");
        AudioManager.Instance.PlaySingleSound(streakSound, streakSound, 1);
    }
}


