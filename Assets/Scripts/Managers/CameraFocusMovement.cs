﻿using UnityEngine;
using System.Collections;

public class CameraFocusMovement : MonoBehaviour {

    private Vector3 groundPos = Vector3.zero;
    public float MaxDistToGround = 0.8f;
    public float MinDistToGround = 0.75f;
    public Vector3 currentDirection = Vector3.zero;
    public bool IsFlying { get; set; }
    private RaycastHit hit;
    private LayerMask layermask = 1 << 31 | 1 << 30;
    public float offSet = 5;
    private Vector3 aboveGroundOffset;

	// Use this for initialization
	void Start () {
        transform.position = new Vector3(GameManager.Instance.troop.GetScoutByOrder(0).transform.position.x + offSet, transform.position.y, 0f);
        aboveGroundOffset = new Vector3(0, transform.position.y, 0);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        // Debug.Log(GameManager.Instance.troop.scouts[0].IsFlying);

     //   RaycastHit hitInf;
        bool aboveGround = Physics.Raycast(transform.position, -Vector3.up, out hit, 100, layermask);

        if (aboveGround)
        {
            groundPos = hit.point;
        }
        else
        {
            transform.position = groundPos + aboveGroundOffset;
            currentDirection.y = 0;
        }

        if(!aboveGround)
            Physics.Raycast(transform.position, -Vector3.up, out hit, 100, layermask);

        if (IsGrounded())
        {
         //   RaycastHit hit;

          //  Physics.Raycast(transform.position, -Vector3.up, out hit, MaxDistToGround, layermask);
            Vector3 normal = hit.normal;
            Quaternion quat = Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            Vector3 forward = quat * normal;
            forward.Normalize();
            currentDirection = ((GameManager.Instance.GetSpeed()) * Time.deltaTime) * forward;

        }
        else if (GameManager.Instance.troop.scouts[0].IsFlying)
        {
            //Debug.Log("----------------- fly bitch fly -------------------");

           // RaycastHit hit;
          //  Physics.Raycast(transform.position, -Vector3.up, out hit, 1000, layermask);
            Vector3 normal = hit.normal;
            Quaternion quat = Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            Vector3 forward = quat * normal;

            if (transform.position.y < hit.point.y + 5)
                forward.y = 1;
            else if (transform.position.y > hit.point.y + 6)
                forward.y = -1;
            else
                forward.y = 0;
            forward.Normalize();
            currentDirection = ((GameManager.Instance.GetSpeed()) * Time.deltaTime) * forward;
        }
        else
        {
            currentDirection += new Vector3(0, -GameManager.Instance.GetGravity(), 0);
        }

        //Enforce Position relative to scouts
        Scout front = GameManager.Instance.troop.GetScoutByOrder(0);
        if (!GameManager.Instance.troop.challengeFailed && !front.IsFlying)
        {
            float fration = offSet / (transform.position - front.transform.position).magnitude;

            currentDirection *= fration;
        }
        

        Vector3 newPos = transform.position + currentDirection;
        newPos.z = 0;
        transform.position = newPos;
	}

    public bool IsGrounded()
    {
        //need to get this working i guess        
        if (!GameManager.Instance.troop.scouts[0].IsFlying && hit.distance < MaxDistToGround)
            return true;
        else
            return false;
    }

    public void Jump(float jumpSpeed)
    {

        invokeJumpVar = jumpSpeed;

        Invoke("JumpTransform", 0.1f);
    }

    private float invokeJumpVar;
    void JumpTransform()
    {

        transform.position += Vector3.up * 0.5f;
        currentDirection += Vector3.up * invokeJumpVar;
    }
}
