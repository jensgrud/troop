﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {
    //Logs
    public GameObject logAudioPlayer;
    public float logPitchDefault = 1.0f;
    public float logPitchMax = 1.7f;
    public float logPitchIncrement = 0.2f;
    //Spots
    public GameObject spotAudioPlayer;
    public float spotPitchDefault = 1.0f;
    //Badges
    public GameObject badgeSmallPlayer;
    public GameObject badgeMediumPlayer;
    public GameObject badgeLargePlayer;
    //Footsteps - Currently not used since it is already in scouts code?
    public List<AudioClip> footStepsClips;
    public GameObject footStepsPlayer;
    //Powerups
    public float powerupTimeShellPitchDrop;
    private float timeShellPitchLerp;
    private bool toggleTimeShell;
    public bool lerpingPitch = false;
    private float pitchMin = 0.50f;

    //
    AudioSource AmbienceLoop;
    AudioSource SoundTrack;
    AudioSource ChaseModeTrack;

    //Random clip list example
    //public List<AudioClip> spotClipList;
    //public GameObject spotAudioPlayer;
    public static AudioManager Instance
    {
        get;
        private set;
    }

	public bool SoundsOn = true;
	public bool MusicOn = true;
    private bool soundState;
    private bool musicState;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        AmbienceLoop = gameObject.transform.FindChild("AmbienceLoop").audio;
        SoundTrack = gameObject.transform.FindChild("Soundtrack").audio;
        ChaseModeTrack = gameObject.transform.FindChild("ChaseModetrack").audio;
        soundState = SoundsOn;
        musicState = MusicOn;
    }

    void Update()
    {
        if (toggleTimeShell && !lerpingPitch && GameManager.Instance.troop.GetActivePowerUp().duration <= 5)
        {
            //AmbienceLoop.audio.pitch += Mathf.Lerp(AmbienceLoop.audio.pitch, 1, 2f);
            //SoundTrack.audio.pitch += Mathf.Lerp(SoundTrack.audio.pitch, 1, 2f);
            //ChaceModeTrack.audio.pitch += Mathf.Lerp(ChaceModeTrack.audio.pitch, 1, 2f);
        }
        if (SoundsOn != soundState)
        {
            //Debug.Log("sound off");
            AudioSource[] audios = GameObject.FindObjectsOfType<AudioSource>();
            if (!SoundsOn)
            {
                MuteSounds();
            }
            else
            {
                UnMuteSounds();
            }
            soundState = SoundsOn;            
        }
        if (MusicOn != musicState)
        {
            //Debug.Log("music off");
            if (!MusicOn)
            {
                MuteMusic();
            }
            else
            {
                UnMuteMusic();
            }
            musicState = MusicOn;
        }
    }

    public void PlayDisposableSound(GameObject gO)
    {
        if (SoundsOn)
            PlayClipOnPoint(gO, gO.transform.position);
            //AudioSource.PlayClipAtPoint(gO.audio.clip, gO.transform.position);
    }

    public void PlaySingleSound(GameObject gO, GameObject soundGO, float pitchDefault)
    {
		if (!SoundsOn) 
			return;
//        Debug.Log(gO != null);
        soundGO.transform.position = gO.transform.position;
        
        soundGO.audio.pitch = pitchDefault;
        soundGO.audio.Stop();
        PlaySound(soundGO);
        //soundGO.audio.Play();
    }


    public void PlayConsecutiveSound(GameObject gO, GameObject soundGO, float pitchIncrement, float pitchMax, float pitchDefault)
    {
		if (!SoundsOn) 
			return;

        if (soundGO.audio.isPlaying)
        {
            soundGO.transform.position = gO.transform.position;
            soundGO.audio.Stop();
            soundGO.audio.pitch = soundGO.audio.pitch + pitchIncrement;
            if (soundGO.audio.pitch > pitchMax)
                soundGO.audio.pitch = pitchMax;
            if (soundGO.audio.pitch < pitchMin)
                soundGO.audio.pitch = pitchMin;
            PlaySound(soundGO);
            //soundGO.audio.Play();
        }
        else
        {
            PlaySingleSound(gO, soundGO, pitchDefault);
        }
    }

    public void PlayRandomSound(GameObject gO, GameObject soundGO, List<AudioClip> clipList)
    {
		if (!SoundsOn) 
			return;

        int r = Random.Range(0,clipList.Count);
        AudioClip clip = clipList[r];
        while (clip == soundGO.audio.clip && clipList.Count > 1)
        {
            r = Random.Range(0, clipList.Count);
            clip = clipList[r];
        }
       
        soundGO.audio.clip = clip;
        PlaySingleSound(gO, soundGO, 1f);
    }

	public void ToggleSounds(){
		SoundsOn = !SoundsOn;
	}

	public void ToggleMusic(){
		MusicOn = !MusicOn;
	}


    //after "crossfadingLength" sound1 will have endVolume1 and sound2 will have endVolume2


    public void CrossFade(GameObject SoundObject1, GameObject SoundObject2, float crossfadingLength, float endVolume1, float endVolume2)
    {
        AudioSource audio1 = SoundObject1.GetComponent<AudioSource>();
        AudioSource audio2 = SoundObject2.GetComponent<AudioSource>();
        StartCoroutine(CrossFading(audio1, audio2, crossfadingLength, endVolume1, endVolume2));
    }

    public void CrossFade(GameObject SoundObject1, float crossfadingLength, float endVolume1)
    {
        AudioSource audio1 = SoundObject1.GetComponent<AudioSource>();
        StartCoroutine(CrossFading(audio1, crossfadingLength, endVolume1));
    }

	public void PitchEnvelope(GameObject SoundObject1, float envelopeLength, float endPitch)
	{
		AudioSource audio1 = SoundObject1.GetComponent<AudioSource>();
		StartCoroutine(PitchEnveloping(audio1, envelopeLength, endPitch));
	}

    private IEnumerator CrossFading(AudioSource Sound1, AudioSource Sound2, float crossfadingLength, float endVolume1, float endVolume2)
    {
        float time = 0;
        float percentage = 0f;


        Sound1.enabled = true;
        if (!Sound1.isPlaying)
            PlaySound(Sound1);
            //Sound1.Play();

        Sound2.enabled = true;
        if (!Sound2.isPlaying)
            PlaySound(Sound2);
            //Sound2.Play();

        float startVolume1 = Sound1.volume;
        float startVolume2 = Sound2.volume;


        while (percentage < 1)
        {

            Sound1.volume = Mathf.Lerp(startVolume1, endVolume1, percentage);
            Sound2.volume = Mathf.Lerp(startVolume2, endVolume2, percentage);
            percentage = (time) / crossfadingLength;
            time += Time.unscaledDeltaTime;
            yield return null;

        }

        if (Sound1.volume == 0)
            Sound1.enabled = false;
        if (Sound2.volume == 0)
            Sound2.enabled = false;
    }

    private IEnumerator CrossFading(AudioSource Sound1, float crossfadingLength, float endVolume1)
    {
        float time = 0;
        float percentage = 0f;

        Sound1.enabled = true;
        if (!Sound1.isPlaying)
            PlaySound(Sound1);
            //Sound1.Play();
      
        float startVolume1 = Sound1.volume;
        while (percentage < 1)
        {
            Sound1.volume = Mathf.Lerp(startVolume1, endVolume1, percentage);
            percentage = (time) / crossfadingLength;
            time += Time.unscaledDeltaTime;
            yield return null;

        }

        if (Sound1.volume == 0)
            Sound1.enabled = false;
    }

	private IEnumerator PitchEnveloping( AudioSource Sound1, float envelopeLenght, float endPitch)
	{
		float startTime = Time.time;
		float percentage = 0f;

		Sound1.enabled = true;
		if (!Sound1.isPlaying)
			PlaySound (Sound1);

		float startPitch = Sound1.pitch;

		while (percentage < 1 && Time.time != envelopeLenght + startTime) 
		{

			Sound1.pitch = Mathf.Lerp(startPitch, endPitch, percentage);

			percentage = (Time.time - startTime) / envelopeLenght;
			yield return null;
		}
	}

    /* public void FootStepsUpdate()
     {
         if (!footStepsPlayer.audio.isPlaying)
         {
             PlayRandomSound(gameObject, footStepsPlayer, footStepsClips);
         }
     }*/

    /// <summary>
    /// Drops the pitch of a sound on a Gameobject if the powerup powershell is active 
    /// </summary>
    /// <param name="sound"></param>
    /// <returns></returns>
    public GameObject TimeShellPitchDrop(GameObject soundGo)
    {
        if (toggleTimeShell)
        {
            soundGo.audio.pitch = soundGo.audio.pitch - powerupTimeShellPitchDrop;
        }
        return soundGo;
    }

    /// <summary>
    /// Drops the pitch of a sound on an AudioSource if the powerup powershell is active 
    /// </summary>
    /// <param name="soundAS"></param>
    /// <returns></returns>
    public AudioSource TimeShellPitchDrop(AudioSource soundAS)
    {
        if (toggleTimeShell)
        {
            soundAS.audio.pitch = soundAS.audio.pitch - powerupTimeShellPitchDrop;
        }
        return soundAS;
    }


    /// <summary>
    /// Plays the sound of a GameObject
    /// </summary>
    /// <param name="soundGo"></param>
    private void PlaySound(GameObject soundGo) 
    {
        soundGo = TimeShellPitchDrop(soundGo);
        soundGo.audio.Play();
    }

    /// <summary>
    /// Plays the sound of a audioscource
    /// </summary>
    /// <param name="soundGo"></param>
    private void PlaySound(AudioSource soundAS)
    {
        soundAS = TimeShellPitchDrop(soundAS);
        soundAS.audio.Play();
    }

    /// <summary>
    /// Check if the pitch should be dropped and the use AudioSource.PlayClipAtPoint();
    /// </summary>
    /// <param name="gO"></param>
    /// <param name="position"></param>
    private void PlayClipOnPoint(GameObject gO, Vector3 position)
    {
        gO = TimeShellPitchDrop(gO);
        AudioSource.PlayClipAtPoint(gO.audio.clip, position);
    }

    public void ToggleTimeShell()
    {
        Transform AmbienceLoop = gameObject.transform.FindChild("AmbienceLoop");
        Transform SoundTrack = gameObject.transform.FindChild("Soundtrack");
        Transform ChaceModeTrack = gameObject.transform.FindChild("ChaseModetrack");

        if (toggleTimeShell)
        {
            LerpPitchToNormal();
        }
        else
        {
            AmbienceLoop.audio.pitch -= powerupTimeShellPitchDrop;
            SoundTrack.audio.pitch -= powerupTimeShellPitchDrop;
            ChaceModeTrack.audio.pitch -= powerupTimeShellPitchDrop;
        }
        toggleTimeShell = !toggleTimeShell;
    }


    public void LerpPitchToNormal()
    {
        StartCoroutine(LerpPitch());
    }
    private IEnumerator LerpPitch()
    {
        float startTime = Time.time;
        float percentage = 0f;
        float length = 2f;

        while (percentage < 1)
        {
            AmbienceLoop.pitch = Mathf.Lerp(AmbienceLoop.pitch, 1f, percentage);
            SoundTrack.pitch = Mathf.Lerp(SoundTrack.audio.pitch, 1f, percentage);
            ChaseModeTrack.pitch = Mathf.Lerp(ChaseModeTrack.audio.pitch, 1f, percentage);

            percentage = (Time.time - startTime) / length;
            yield return 0;
        }
        AmbienceLoop.pitch = 1;
        SoundTrack.pitch = 1;
        ChaseModeTrack.pitch = 1;
        lerpingPitch = false;
    }

    public void MuteEverything()
    {
        AudioSource[] audios = GameObject.FindObjectsOfType<AudioSource>();
        foreach (AudioSource audio in audios)
        {
            if (audio.name == "AudioListener" && SoundsOn)
                audio.mute = false;
            else
                audio.mute = true;
        }
        audios = GameObject.Find("Pool").GetComponentsInChildren<AudioSource>(true);
        foreach (AudioSource audio in audios)
        {
            if (audio.name == "AudioListener" && SoundsOn)
                audio.mute = false;
            else
                audio.mute = true;
        }
        audios = GameObject.Find("ProceduralLevel").GetComponentsInChildren<AudioSource>(true);
        foreach (AudioSource audio in audios)
        {
            if (audio.name == "AudioListener" && SoundsOn)
                audio.mute = false;
            else
                audio.mute = true;
        }
    }

    public void UnMuteEverything()
    {
        if(SoundsOn)
            UnMuteSounds();

        if (MusicOn)
            UnMuteMusic();
    }

    private void UnMuteSounds()
    {
        AudioSource[] audios = GameObject.FindObjectsOfType<AudioSource>();
        foreach (AudioSource audio in audios)
        {
            if (audio.name != SoundTrack.name && audio.name != ChaseModeTrack.name)
                audio.mute = false;
        }
        audios = GameObject.Find("Pool").GetComponentsInChildren<AudioSource>(true);
        foreach (AudioSource audio in audios)
        {
            if (audio.name != SoundTrack.name && audio.name != ChaseModeTrack.name)
                audio.mute = false;
        }
        audios = GameObject.Find("ProceduralLevel").GetComponentsInChildren<AudioSource>(true);
        foreach (AudioSource audio in audios)
        {
            if (audio.name != SoundTrack.name && audio.name != ChaseModeTrack.name)
                audio.mute = false;
  
        }

    }

    private void UnMuteMusic()
    {
        SoundTrack.mute = false;
        ChaseModeTrack.mute = false;
    }

    private void MuteSounds()
    {
        AudioSource[] audios = GameObject.FindObjectsOfType<AudioSource>();
        foreach (AudioSource audio in audios)
        {
            if (audio.name != SoundTrack.name && audio.name != ChaseModeTrack.name)
                audio.mute = true;
        }
        audios = GameObject.Find("Pool").GetComponentsInChildren<AudioSource>(true);
        foreach (AudioSource audio in audios)
        {
            if (audio.name != SoundTrack.name && audio.name != ChaseModeTrack.name)
                audio.mute = true;
        }
        audios = GameObject.Find("ProceduralLevel").GetComponentsInChildren<AudioSource>(true);
        foreach (AudioSource audio in audios)
        {
            if (audio.name != SoundTrack.name && audio.name != ChaseModeTrack.name)
                audio.mute = true;

        }

    }

    private void MuteMusic()
    {
        SoundTrack.mute = true;
        ChaseModeTrack.mute = true;
    }
}
