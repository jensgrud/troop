﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public float cameraZOffset; //-30
    public float cameraXOffset; // 20
    public float cameraYOffset; // 6
    Vector3 troopAveragePos;
    Vector3 offsetPosition;
    Vector3 offset;
    Vector3 newPos;
    public float Fraction; //0.01f
    GameObject[] scouts;
    public GameObject cameraFocusPoint;

	// Use this for initialization
	void Start () {
        offset = new Vector3(cameraXOffset, cameraYOffset, cameraZOffset);
	}
	
	// Update is called once per frame
	void Update () {

	}

    void LateUpdate()
    {
        troopAveragePos = Vector3.zero;
        scouts = GameObject.FindGameObjectsWithTag("Scout");
        if (scouts.Length > 0)
        {
            foreach (GameObject scout in scouts)
            {
                troopAveragePos += scout.transform.position;
                      
            }
            troopAveragePos /= 3;
            
        }


        //troopAveragePos = (redScout.transform.position + greenScout.transform.position + blueScout.transform.position) / 3;

       // newPos = transform.position;
        newPos = cameraFocusPoint.transform.position + offset;
		//newPos.y = Mathf.Lerp(transform.position.y, cameraFocusPoint.transform.position.y + offset.y, yFraction);        //The lerp is to make the camera only move 0.01% of the distance per update for smoothness.
        transform.position = Vector3.Lerp(transform.position, newPos, Fraction);
    }
}
