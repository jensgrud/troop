﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchManager : MonoBehaviour
{
    public delegate void SwipeEventHandler(object sender, SwipeEventHandler self, object arg = null);
    public event SwipeEventHandler OnSwipeUp;
    public event SwipeEventHandler OnSwipeDown;
    public event SwipeEventHandler OnSwipeLeft;
    public event SwipeEventHandler OnSwipeRight;
    public event SwipeEventHandler OnTap;

    public float minSwipeDist = 0.5f;
    public float minSwipeVelocity = 0.1f;

    private Vector2? oldMousePos;
    private bool mouseMoveProccesed = false;
    private bool touchProcessed = false;
    private RaycastHit hit;
    private int tapAbleMask;
    public bool normalModeOn = true;

    private bool jumpEnabled = true;
    private bool duckEnabled = true;
    private bool switchEnabled = true;

    private bool mouseDown = false;
    public static TouchManager Instance
    {
        get;
        private set;
    }


    void Awake()
    {
        tapAbleMask = LayerMask.GetMask("Tapable");
        Instance = this;
    }

    void Update()
    {
#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE
        CheckMouseClick();
#endif

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_BLACKBERRY
        CheckTouchClick();
#endif
    }

    void FixedUpdate()
    {

#if UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_STANDALONE
        SwipeMouse();
#endif

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8 || UNITY_BLACKBERRY
        SwipeTouch();
#endif
		if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
		{
			UpSwipe();
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
		{
			DownSwipe();
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
		{
			LeftSwipe();
		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
		{
			RightSwipe();
		}
    }


    void SwipeTouch()
    {
        if (Input.touchCount > 0)
        {
            if (!touchProcessed)
            {
                Touch touch = Input.touches[0];
                if (touch.deltaTime == 0.0)
                {
                    return;
                }
                Vector2 DistInScreenWidths = (touch.deltaPosition / Screen.width);
                if (DistInScreenWidths.magnitude > minSwipeDist)
                {
                    float xSpeedInScreenWidths = DistInScreenWidths.x / touch.deltaTime;
                    float ySpeedInScreenWidths = DistInScreenWidths.y / touch.deltaTime;
                    if (Mathf.Abs(xSpeedInScreenWidths) >= Mathf.Abs(ySpeedInScreenWidths) && Mathf.Abs(xSpeedInScreenWidths) > minSwipeVelocity)
                    {
                        if (xSpeedInScreenWidths > 0)
                        {
                            if (normalModeOn)
                            {
                                RightSwipe();
                            }
                            else
                            {
                                LeftSwipe();
                            }

                        }
                        else
                        {
                            if (normalModeOn)
                            {
                                LeftSwipe();
                            }
                            else
                            {
                                RightSwipe();
                            }

                        }
                    }
                    else if (Mathf.Abs(xSpeedInScreenWidths) < Mathf.Abs(ySpeedInScreenWidths) && Mathf.Abs(ySpeedInScreenWidths) > minSwipeVelocity)
                    {
                        if (ySpeedInScreenWidths > 0)
                        {
                            UpSwipe();
                        }
                        else
                        {
                            DownSwipe();
                        }
                    }
                }
                //tapping
                else
                {
                    Tap(touch.position);
                }
            }
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                touchProcessed = false;
            }
        }
    }

    void SwipeMouse()
    {
        if (Input.GetMouseButton(0) && !mouseMoveProccesed)
        {
            //Touch touch = Input.touches[0];
            if(oldMousePos == null)
            {
                oldMousePos = Input.mousePosition;
                return;
            }
            Vector2? mousePosDelta = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - oldMousePos;
            oldMousePos = Input.mousePosition;
            Vector2? DistInScreenWidths = (mousePosDelta / Screen.width);
            if (DistInScreenWidths.Value.magnitude > minSwipeDist)
            {
                float xSpeedInScreenWidths = DistInScreenWidths.Value.x / Time.deltaTime;
                float ySpeedInScreenWidths = DistInScreenWidths.Value.y / Time.deltaTime;
                if (Mathf.Abs(xSpeedInScreenWidths) >= Mathf.Abs(ySpeedInScreenWidths) && Mathf.Abs(xSpeedInScreenWidths) > minSwipeVelocity)
                {
                    if (xSpeedInScreenWidths > 0)
                    {
                        if (normalModeOn)
                        {
                            RightSwipe();
                        }
                        else
                        {
                            LeftSwipe();
                        }
                    }
                    else
                    {
                        if (normalModeOn)
                        {
                            LeftSwipe();
                        }
                        else
                        {
                            RightSwipe();
                        }
                    }
                }
                else if (Mathf.Abs(xSpeedInScreenWidths) < Mathf.Abs(ySpeedInScreenWidths) && Mathf.Abs(ySpeedInScreenWidths) > minSwipeVelocity)
                {
                    if (ySpeedInScreenWidths > 0)
                    {
                        UpSwipe();
                    }
                    else
                    {
                        DownSwipe();
                    }
                }
            }
            //tapping
            else
            {
                Tap(Input.mousePosition);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            oldMousePos = null;
            mouseMoveProccesed = false;
        }
    }

    
    void Tap(Vector3 position)
    {
        Ray ray = Camera.main.ScreenPointToRay(position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100.0F, tapAbleMask)){
            Debug.DrawLine(ray.origin, hit.point, Color.red, 10);

            //if the tapped object is implements ITapable we use its tap()
            MonoBehaviour[] monoBehaviours = hit.transform.GetComponents<MonoBehaviour>();
            foreach (MonoBehaviour mono in monoBehaviours)
                if (mono is ITapable)
                    ((ITapable)hit.transform.GetComponent<MonoBehaviour>()).tap();
        }

        if (OnTap != null)
            OnTap(this, OnTap, hit);
    }

    void CheckMouseClick()
    {

        if (Input.GetMouseButton(0) && mouseDown == false)
        {
            Click();
            mouseDown = true;
        }
        else if (!Input.GetMouseButton(0))
        {
            mouseDown = false;
        }
    }

    void CheckTouchClick()
    {
        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Click();
            }
        }
    }

    void Click()
    {
        SplashScreen.Instance.skipSplash();
    }

    void UpSwipe()
    {
        touchProcessed = true;
        mouseMoveProccesed = true;
        if(jumpEnabled)
            GameManager.Instance.troop.Jump();

        if (OnSwipeUp != null)
            OnSwipeUp(this, OnSwipeUp);
    }

    void DownSwipe()
    {
        touchProcessed = true;
        mouseMoveProccesed = true;
        if(duckEnabled)
            GameManager.Instance.troop.Duck();

        if (OnSwipeDown != null)
            OnSwipeDown(this, OnSwipeDown);
    }

    void RightSwipe()
    {
        touchProcessed = true;
        mouseMoveProccesed = true;
        RightSwipeFunctionality ();

    }

	public void RightSwipeFunctionality ()
	{
		if (switchEnabled) {
			GameManager.Instance.troop.RightSwitch ();
			//Tell statistic manager
			StatisticsManager.Instance.IterateStatistic (Statistic.MovementChangePositionCount);
			if (OnSwipeRight != null)
				OnSwipeRight (this, OnSwipeRight);
		}
	}

    void LeftSwipe()
    {
        touchProcessed = true;
        mouseMoveProccesed = true;
        if (switchEnabled)
        {
            GameManager.Instance.troop.LeftSwitch();
            //Tell statistic manager
            StatisticsManager.Instance.IterateStatistic(Statistic.MovementChangePositionCount);

            if (OnSwipeLeft != null)
                OnSwipeLeft(this, OnSwipeLeft);
        }
    }

    public bool JumpActive(bool active)
    {
        jumpEnabled = active;
        return jumpEnabled;
    }
    public bool DuckActive(bool active)
    {
        duckEnabled = active;
        return duckEnabled;
    }
    public bool SwitchActive(bool active)
    {
        switchEnabled = active;
        return switchEnabled;
    }
}