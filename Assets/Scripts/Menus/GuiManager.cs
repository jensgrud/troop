﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class GuiManager : MonoBehaviour {

	public static GuiManager Instance;

	#region GUI State
	public enum MenuType {
		Start = 0, Shop, Powerups, Mascots, Currency, Badges, Game, PostGame, Leaderboard, Credits
	}

	public MenuType CurrentMenu { get; private set; }

	private List<MenuType> MenuHistory = new List<MenuType>();
	 
	public enum PopupType {
		None = 0, PayCurrency, GoToShop, LuchBox, BadgeInfo, OutOfStamina, Settings, Question, Info, EnterHighscore
	}

	[HideInInspector]
	public PopupType CurrentPopup { get; private set; }

	#endregion

	#region fields

	public List<Transform> MenuReferencesList;
	public List<Transform> PopupReferencesList;
	public Transform Wallet;

	private Dictionary<MenuType, Transform> MenuReferences = new Dictionary<MenuType, Transform>(); 
	private Dictionary<PopupType, Transform> PopupReferences = new Dictionary<PopupType, Transform>();
    
	public Transform CurrentMenuReference {
		get {
			return MenuReferences[CurrentMenu];
		} 
	}
	public Transform CurrentPopupReference { 
		get {
			return PopupReferences[CurrentPopup];
		}
	}

	public ShopMenu CampMenu {
		get {
			return MenuReferences[MenuType.Shop].GetComponent<ShopMenu>();
		}
	}

	#endregion

	#region initialization

	void Awake(){
		Instance = this;

		FillDictionaries ();
	}

	private void FillDictionaries(){

		for (int i=0; i<MenuReferencesList.Count; ++i){
			MenuReferences.Add((MenuType)i,MenuReferencesList[i]);
		}

		for (int i=0; i<PopupReferencesList.Count; ++i){
			PopupReferences.Add((PopupType)i,PopupReferencesList[i]);
		}
	}

	void Start(){
        GameManager.Instance.StopTime();
		//ShowMenu (MenuType.Start);
	}

	public void Register(MenuType t, Transform m){
		MenuReferences.Add (t, m);
	}

	public void Register(PopupType t, Transform p){
		PopupReferences.Add (t, p);
	}

	#endregion

	#region methods

	public void ShowMenu(MenuType newMenu){

		CurrentPopupReference.gameObject.SetActive (false);
		CurrentMenuReference.gameObject.SetActive (false);

		if (MenuHistory.Count == 0 || MenuHistory[MenuHistory.Count-1] != CurrentMenu)
			MenuHistory.Add (CurrentMenu);

		CurrentMenu = newMenu;

		CurrentMenuReference.gameObject.SetActive (true);
	}

	public void Back(){

		CurrentPopupReference.gameObject.SetActive (false);
		CurrentMenuReference.gameObject.SetActive (false);

		CurrentMenu = MenuHistory[MenuHistory.Count-1];
		MenuHistory.RemoveAt(MenuHistory.Count-1);
		
		CurrentMenuReference.gameObject.SetActive (true);
	}

	public void ShowPopup(PopupType newPopup){

		CurrentPopupReference.gameObject.SetActive (false);

		CurrentPopup = newPopup;

		CurrentPopupReference.gameObject.SetActive (true);
	}

	public void ShowBuyPopup(CurrencyType type, int amount, GameObject target, string method){

		//HACK Start menu checks if you have monies LATER.
		if (CurrentMenu == MenuType.Start || CheckIfPlayerHas(type, amount)){

			ShowPopup (PopupType.PayCurrency);

			PayCurrencyPopup p = CurrentPopupReference.GetComponent<PayCurrencyPopup> ();

			p.AmountLabel.text = amount.ToString();
			p.CurrencySprite.spriteName = type.ToSpriteName ();
			p.Target = target;
			p.MethodToCall = method;
		
		} else {
			ShowPopup (PopupType.GoToShop);
		}
	}

	public bool CheckIfPlayerHas(CurrencyType type, int amount){
		return 
			type == CurrencyType.Logs 
				?
				PlayerWallet.Instance.TotalLogs >= amount 
				:
				PlayerWallet.Instance.Marshmallows >= amount;
	}

	public void ShowBadgePopup(Badge b){
		
		ShowPopup (PopupType.BadgeInfo);
		
		BadgeInfoPopup p = CurrentPopupReference.GetComponent<BadgeInfoPopup> ();
		
		p.Icon.spriteName = b.spriteName;
		p.Overlay.spriteName = BadgeManager.Instance.GetOverlay (b.badgeLevel);
        p.Title.text = LanguageManager.Instance.GetTextValue("Badge Title " + b.badgeId.ToString());

        p.Description.text = LanguageManager.Instance.GetTextValue("Badge Description " + b.badgeId.ToString());

	}

	public void HidePopup(){
		CurrentPopupReference.gameObject.SetActive (false);
		
		CurrentPopup = PopupType.None;
		
		CurrentPopupReference.gameObject.SetActive (true);
	}

	//TODO throw away somewhere else
	public float waitForAnim;
	void Update()
	{
		
	}
	public void EnterCamp(GameObject campAnimator)
    {
        AudioManager.Instance.MuteEverything();
        if (AudioManager.Instance.MusicOn)
        {
            
            AudioManager.Instance.transform.FindChild("Soundtrack").gameObject.audio.mute = false;
        }
        if (AudioManager.Instance.SoundsOn)
        {
            AudioManager.Instance.transform.FindChild("SetUpCampAudioPlayer").gameObject.audio.mute = false;
            AudioManager.Instance.transform.FindChild("AmbienceLoop").gameObject.audio.mute = false;
        }
            

        campAnimator.transform.FindChild("campSiteTents").GetComponent<Animator>().SetTrigger("EnteredCamp");
        campAnimator.transform.FindChild("campFire").GetComponent<Animator>().SetTrigger("LitFire");

		campAnimator.transform.FindChild("JuicyFire@Particles").GetComponent<ParticleSystem>().Play();

		StartCoroutine (ShowShopMenuWithDelay()); 
        GameManager.Instance.StopTime();
		if (campAnimator != null)
		{
            if (campAnimator.GetComponent<CampSite>() != null)
                campAnimator.GetComponent<CampSite>().visited = true;
		}
		
		
		if (GameManager.Instance.troop.pet != null)
			GameManager.Instance.troop.pet.LeaveTroop();
		
		
		//Tell statistic manager
		StatisticsManager.Instance.IterateStatistic(Statistic.VisitedCampSites);
		
		//End streak
        GameManager.Instance.noCampStreak = 0;
	}

	private IEnumerator ShowShopMenuWithDelay(){

		float time = 1f;

		float startTime = Time.realtimeSinceStartup;
		while (startTime + time > Time.realtimeSinceStartup) {
			yield return 0;
		}

		ShowMenu(MenuType.Shop);

	}

	public void ShowWallet(){
		Wallet.gameObject.SetActive (true);
	}

	public void HideWallet(){
		Wallet.gameObject.SetActive (false);
	}

	public void InitGenericPopup(string text, GameObject target, string message, int soundIndex){
		GenericPopup p = PopupReferences[PopupType.Question].GetComponent<GenericPopup>();

		p.Text.key = text;
		p.Text.Reload ();

		p.Target = target;
		p.Message = message;

		foreach(UIPlaySound b in p.YesButtons)
			b.audioClip = p.YesButtonSounds[soundIndex];

	}

	#endregion
}
