﻿using UnityEngine;
using System.Collections;

public class ResizeColliderOnAwake : MonoBehaviour {

	// Use this for initialization
	void Start () {

		BoxCollider b = GetComponent<BoxCollider> ();

		if (b == null){
			b = gameObject.AddComponent<BoxCollider> ();
		}

		b.isTrigger = true;
		b.center = Vector3.zero;

		b.size = new Vector3 (

			Screen.width,
			Screen.height,
			0.1f
		);

	}
}
