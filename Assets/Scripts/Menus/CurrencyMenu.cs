﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class CurrencyMenu : MonoBehaviour {

	[Serializable]
	public struct AmountPricePair{
		public int amount;
		public int price;
	}

	public GameObject ChoiceSection;
	public GameObject LogsSection;
	public GameObject MarshmallowsSection;

	public AmountPricePair[] logAmountsAndTheirPrices;
	public AmountPricePair[] marshmallowAmountsAndTheirPrices;

	public List<UILabel> LogAmountLabels;
	public List<UILabel> LogPriceLabels;

	public List<UILabel> MarshmallowAmountLabels;
	public List<UILabel> MarshmallowPriceLabels;
	
	public List<BuyCurrencyButton> BuyLogsButtons;
	public List<BuyCurrencyButton> BuyMarshmallowsButtons;

	public GameObject LogsAnchor;
	public GameObject MarshmallowsAnchor;
	
	void OnEnable(){

		ChoiceSection.SetActive (true);
		LogsSection.SetActive (false);
		MarshmallowsSection.SetActive (false);

		for (int i = 0; i<LogPriceLabels.Count; i++) {
			LogAmountLabels[i].text = "x "+ logAmountsAndTheirPrices[i].amount;
			LogPriceLabels[i].text =  logAmountsAndTheirPrices[i].price+" DKK";

			BuyLogsButtons[i].Type = CurrencyType.Logs;
			BuyLogsButtons[i].index = i;
		}
		
		for (int i = 0; i<MarshmallowPriceLabels.Count; i++) {
			MarshmallowAmountLabels[i].text = "x "+ marshmallowAmountsAndTheirPrices[i].amount;
			MarshmallowPriceLabels[i].text = marshmallowAmountsAndTheirPrices[i].price+" DKK";

			BuyMarshmallowsButtons[i].Type = CurrencyType.Marshmallows;
			BuyMarshmallowsButtons[i].index = i;
		}

		GuiManager.Instance.ShowWallet ();

		GuiManager.Instance.InitGenericPopup (
			"Mock Up Shop Info",
			GuiManager.Instance.gameObject,
			"HidePopup",
			0
			);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Info);
	}

	void OnDisable(){
		GuiManager.Instance.HideWallet ();
	}

	public void ChooseLogs(){
		ChoiceSection.SetActive (false);
		LogsSection.SetActive (true);
		MarshmallowsSection.SetActive (false);
		Invoke ("DeleteLogsAnchor",0f);
	}

	public void DeleteLogsAnchor(){
		GameObject.Destroy (LogsAnchor);
	}

	public void ChooseMarshmallows(){
		ChoiceSection.SetActive (false);
		LogsSection.SetActive (false);
		MarshmallowsSection.SetActive (true);
		Invoke ("DeleteMarshmallowsAnchor",0f);
	}
	
	public void DeleteMarshmallowsAnchor(){
		GameObject.Destroy (MarshmallowsAnchor);
	}


	private bool canBuy = true;
	void Update(){
		canBuy = true;
	}

	private CurrencyType _type;
	private int _index;

	public void Buy(CurrencyType type, int index){
		if (!canBuy)
			return;

		_type = type;
		_index = index;

		GuiManager.Instance.InitGenericPopup (
			"Mock Up Shop Question",
			this.gameObject,
			"Bought",
			1
			);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Question);

	}

	public void Bought(){

		switch (_type) {
		case CurrencyType.Logs:
			PlayerWallet.Instance.PersistentLogs += logAmountsAndTheirPrices[_index].amount;
			break;
		case CurrencyType.Marshmallows:
			PlayerWallet.Instance.Marshmallows += marshmallowAmountsAndTheirPrices[_index].amount;
			break;
		}
		canBuy = false;

		GuiManager.Instance.Back ();
	}
}
