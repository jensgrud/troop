﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SmartLocalization;

public class InGameGui : MonoBehaviour {

	public UILabel LogsLabel;

	public UISprite PowerupIcon;
	public string[] PowerupIconNames;

	public UISprite CurrentScoutIcon;
	public string[] CurrentScoutIconNames;

	public GameObject ChaseIcon;

    #region Stamina variables
    public GameObject[] StaminaSprites;
    public string staminaSpriteName = "GameplayUI@Stamina";
    public string lostStaminaSpriteName = "GameplayUI@StaminaEmpty";
    public string redStaminaSpriteName = "GameplayUI@StaminaLow";
    public float sizeFractionSpeed = 0.7f;
    public float maxSize = 1.5f;
    private float targetSize = 1.5f;

    public float blinkTime = 0.2f;
    public int numberOfBlinks = 2;
    private float staminaBlinkTimeCoundown;
    private int staminaBlinkCoundown;
    #endregion

	#region CurrentScoutIcon blending

	public float CurrentScoutIconDuration = 2f;
	public float CurrentScoutIconFadeOutDuration = 3f;

	#endregion

    public GameObject StreakSprite;

	public GameObject GUIElements;
	public GameObject IntroElements;

	public bool showComic = true;

	public GameObject Wallet;
	public GameObject BackButton;

	private bool isComicState = false;
	private int comicCounter = 0;

	public List<GameObject> ComicPanels;
	public List<GameObject> ComicAudios;
	public GameObject ComicOverlay;
	private bool ComicTimeFramePassed = false;

    public static InGameGui Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        //StaminaSprites[1].GetComponent<UIWidget>().leftAnchor.target = StaminaSprites[0].transform;
        //StaminaSprites[1].GetComponent<UIWidget>().bottomAnchor.target = StaminaSprites[0].transform;
        //StaminaSprites[1].GetComponent<UIWidget>().rightAnchor.target = StaminaSprites[0].transform;
        UpdateStamina();
    }

	void OnEnable(){

		Wallet.SetActive (true);

		BadgeManager.Instance.ShowPopups();
        UpdateStamina();

		if (RankManager.Instance.currentRank == RankManager.tutorialRank && showComic) {
			showComic = false;
			StartCoroutine(AnotherComicTimer());

			GUIElements.SetActive(false);
			IntroElements.SetActive(true);

			isComicState = true;
			ComicAudios[0].audio.PlayOneShot(ComicAudios[0].audio.clip);
			AudioManager.Instance.CrossFade(AudioManager.Instance.transform.FindChild("AmbienceLoop").gameObject, 1.5f, 0f);
			AudioManager.Instance.CrossFade(AudioManager.Instance.transform.FindChild("Soundtrack").gameObject, 1.5f, 0.15f);
		

		} else {

			GUIElements.SetActive(true);
			IntroElements.SetActive(false);

            if (GameManager.Instance.GetTimeScale() == 0f)
            {
                GameManager.Instance.ResetTimeScale();
                AudioManager.Instance.UnMuteEverything();
            }

			isComicState = false;
				
		}

		ShowCurrentScoutIcon ();
	}

	private void EndComic(){

		comicCounter = 0;
		isComicState = false;
		AudioManager.Instance.transform.FindChild ("Soundtrack").audio.volume = 0.9f;
		AudioManager.Instance.CrossFade (AudioManager.Instance.transform.FindChild ("AltAmbience").gameObject, 1.5f, 0f);
		AudioManager.Instance.CrossFade (AudioManager.Instance.transform.FindChild ("AmbienceLoop").gameObject, 1.5f, 0.25f);
		ComicAudios[1].audio.Stop();
		ComicAudios[2].audio.Stop();

		ComicOverlay.SetActive(false);
		for (int i=0; i<ComicPanels.Count; ++i){
			ComicPanels[i].SetActive(false);
		}

		if (GameManager.Instance.GetTimeScale() == 0f)
			GameManager.Instance.ResetTimeScale();

		IntroElements.SetActive (false);
		GUIElements.SetActive (true);
	}

	void OnDisable(){
		GameManager.Instance.StopTime();
        UpdateStamina();
	}

	void Update() {

		if (isComicState) {

			ComicOverlay.SetActive(true);
			for (int i=0; i<ComicPanels.Count; ++i){
				ComicPanels[i].SetActive(i <= comicCounter);
			}

			if (Input.GetMouseButtonDown(0) || ComicTimeFramePassed) {

				ComicTimeFramePassed = false;
				StopAllCoroutines();
				comicCounter++;

				if (comicCounter >= ComicPanels.Count){
					EndComic();
					//audio.Stop();
				} else {

					ComicAudios[1].audio.Stop();
					ComicAudios[comicCounter].audio.PlayOneShot(ComicAudios[comicCounter].audio.clip);
					StartCoroutine(AnotherComicTimer());
				}
			}

		} else {

			UpdateWallet ();
			UpdateCurrentScoutIcon ();
			UpdatePowerupIcon ();
	        StaminaBlinkCheck();
	        StaminaPulse();

			UpdateBackButton ();

		}

	}

	private IEnumerator AnotherComicTimer(){

		ComicTimeFramePassed = false;

		//TODO externalize
		float time = 4f;

		if (comicCounter == 1) 
		{
			AudioManager.Instance.CrossFade (ComicAudios[0], 1.5f, 0f);
			AudioManager.Instance.CrossFade (AudioManager.Instance.transform.FindChild("AltAmbience").gameObject, 1.5f, 0.25f);
		}
		
		float startTime = Time.realtimeSinceStartup;
		while (startTime + time > Time.realtimeSinceStartup) {
			yield return 0;
		}
		
		ComicTimeFramePassed = true;

	}

	void UpdateBackButton(){
		BackButton.SetActive (RankManager.Instance.savedRank > RankManager.tutorialRank);
	}

	public void UpdateWallet(){
		LogsLabel.text = PlayerWallet.Instance.LogsInThisGame.ToString();
	}
	
	

	public void UpdatePowerupIcon(){

		if (GameManager.Instance.troop.currentPowerUp == null) {

			PowerupIcon.gameObject.SetActive(false);

        }
        else 
        {
            if (PowerUpsManager.Instance.PowerUpAmounts[GameManager.Instance.troop.currentPowerUp.Id] > 0)
            {
                PowerupIcon.gameObject.SetActive(true);
            }
            else
            {
                PowerupIcon.gameObject.SetActive(false);
            }

            PowerupIcon.spriteName = PowerupIconNames[
              PowerUpsManager.Instance.PowerUps.IndexOf(
                GameManager.Instance.troop.currentPowerUp
                )
            ];
        } 
	}

	public void UpdateCurrentScoutIcon(){
		CurrentScoutIcon.spriteName = CurrentScoutIconNames [
			GameManager.Instance.troop.scouts.IndexOf (
				GameManager.Instance.troop.GetScoutByOrder(0)
			)
		];


	}

	public void OpenBadgesMenu(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Badges);
	}

	public void UsePowerup(){
		PowerUpsManager.Instance.UsePowerUp (GameManager.Instance.troop.currentPowerUp);
	}

	public void EnableChaseIcon(int duration){
		ChaseIcon.SetActive (true);
		Invoke("DisableChaseIcon", duration);
	}
	
	public void DisableChaseIcon()
	{
		ChaseIcon.SetActive (false);
	}

    public void EnableStreakIcon(int duration)
    {

        StreakSprite.SetActive(true);


        StreakSprite.GetComponent<UISprite>().spriteName = LanguageManager.Instance.GetTextValue("RiskBonusFrame1");
        Invoke("StreakIconSecondFrame", 0.5f);
        Invoke("DisableStreakIcon", duration);
    }

    public void DisableStreakIcon()
    {
        StreakSprite.SetActive(false);
    }

    public void StreakIconSecondFrame()
    {
        StreakSprite.GetComponent<UISprite>().spriteName = LanguageManager.Instance.GetTextValue("RiskBonusFrame2");
    }

	public void Swipe(){
		TouchManager.Instance.RightSwipeFunctionality ();
	}
    #region Stamina Methods
    public void UpdateStamina()
    {
        if (GameManager.Instance.troop.stamina == 1)
        {
            StaminaSprites[0].GetComponent<UISprite>().spriteName = redStaminaSpriteName;
            StaminaSprites[1].GetComponent<UISprite>().spriteName = lostStaminaSpriteName;
            StaminaSprites[2].GetComponent<UISprite>().spriteName = lostStaminaSpriteName;
        }
        if (GameManager.Instance.troop.stamina == 2)
        {
            StaminaSprites[0].transform.localScale = Vector3.one;
            StaminaSprites[0].GetComponent<UISprite>().spriteName = staminaSpriteName;
            StaminaSprites[1].GetComponent<UISprite>().spriteName = staminaSpriteName;
            StaminaSprites[2].GetComponent<UISprite>().spriteName = lostStaminaSpriteName;
        }
        if (GameManager.Instance.troop.stamina == 3)
        {
            StaminaSprites[0].transform.localScale = Vector3.one;
            StaminaSprites[0].GetComponent<UISprite>().spriteName = staminaSpriteName;
            StaminaSprites[1].GetComponent<UISprite>().spriteName = staminaSpriteName;
            StaminaSprites[2].GetComponent<UISprite>().spriteName = staminaSpriteName;
        }
    }
    public void DisableStaminaIcons()
    {
        for (int i = 0; i < 3; i++)
        {
            StaminaSprites[i].SetActive(false);
        }
    }
    public void EnableStaminaIcons()
    {
        for (int i = 0; i < 3; i++)
        {
            StaminaSprites[i].SetActive(true);
        }
    }

     private void StaminaBlinkCheck()
     {
         if (staminaBlinkTimeCoundown > 0)
         {
             staminaBlinkTimeCoundown -= Time.deltaTime;
             DisableStaminaIcons();
         }
         else if (staminaBlinkTimeCoundown < 0)
         {
             UpdateStamina();
             EnableStaminaIcons();
             //if there is still more blinks back we call blink again
             if (staminaBlinkCoundown > 0)
             {
                 StaminaBlink(staminaBlinkCoundown);
             }
             else
             {
                 staminaBlinkTimeCoundown = 0;
             }
         }
     }

     public void StaminaBlink(int blinks)
     {
         /*
         if (RankManager.Instance.currentRank == RankManager.tutorialRank)
             return;
         */
           
         staminaBlinkCoundown = blinks - 1;
         staminaBlinkTimeCoundown = blinkTime;
     }

     public void StaminaPulse()
     {
         if (GameManager.Instance.troop.stamina == 1)
         {
             if (StaminaSprites[0].transform.localScale.x <= Vector3.one.x * 1.2f)
             {
                 targetSize = maxSize;
             }
             else if (StaminaSprites[0].transform.localScale.x >= Vector3.one.x * maxSize * 0.9f)
             {
                 targetSize = 1f;
             }
             Transform newT = StaminaSprites[0].transform;
             newT.localScale = Vector3.Lerp(StaminaSprites[0].transform.localScale, Vector3.one * targetSize, sizeFractionSpeed * Time.deltaTime);
             StaminaSprites[0].transform.localScale = newT.localScale;
             StaminaSprites[1].transform.localScale = Vector3.one;
         }
     }
    #endregion

	public void ShowCurrentScoutIcon(){
		StopCoroutine ("BlendOutCurrentScoutIcon");
		StartCoroutine("BlendOutCurrentScoutIcon");
	}

	private IEnumerator BlendOutCurrentScoutIcon(){

		CurrentScoutIcon.color = new Color (1f, 1f, 1f, 1f);

		//waitforseconds(CurrentScoutIconDuration)
		float prevTime = Time.realtimeSinceStartup;
		while (prevTime + CurrentScoutIconDuration > Time.realtimeSinceStartup)
			yield return 0;

		float blendStartTime = Time.realtimeSinceStartup;
		while (blendStartTime + CurrentScoutIconFadeOutDuration > Time.realtimeSinceStartup) {

			float percent =  1f - ((Time.realtimeSinceStartup - blendStartTime) / CurrentScoutIconFadeOutDuration);
			CurrentScoutIcon.color = new Color (1f, 1f, 1f, percent);
			yield return 0;
		}

		CurrentScoutIcon.color = new Color (1f, 1f, 1f, 0);
	}

	public void HideWallet(){
		Wallet.SetActive (false);
	}

	public void ShowWallet(){
		Wallet.SetActive (true);
	}

	public void BackToMenu(){

		TouchManager.Instance.JumpActive(true);
		TouchManager.Instance.DuckActive(true);
		TouchManager.Instance.SwitchActive(true);

		if (RankManager.Instance.savedRank > RankManager.tutorialRank)
		{
			// We are replaying tutorial level so reset rank
			RankManager.Instance.ResetRank();
		}

#if UNITY_ANDROID
		GA.API.Design.NewEvent ("Game:End");
		GameAnalyticsHelper.GameEndTime = Time.realtimeSinceStartup;
		GA.API.Design.NewEvent ("Game:Duration",GameAnalyticsHelper.GameDuration);
		GA.API.Design.NewEvent("Game:BadgesEarned",GameAnalyticsHelper.BadgesAmount);
		if (GameAnalyticsHelper.RanksAmount > 0f)
			GA.API.Design.NewEvent("Game:RankEarned",GameAnalyticsHelper.RanksAmount);
#endif
		
		AudioManager.Instance.transform.FindChild ("Soundtrack").gameObject.audio.volume = 0.9f;
		GameManager.Instance.alive = true;
		
		GameManager.Instance.troop.stamina = 3;
		PlayerWallet.Instance.LogsInThisGame = 0;
		
		Application.LoadLevel(Application.loadedLevel);
	}
}
