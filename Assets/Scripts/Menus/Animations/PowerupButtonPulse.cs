﻿using UnityEngine;
using System;

public class PowerupButtonPulse : MonoBehaviour, IiTweenAnimation {
    public UIPanel Parent;
    public UISprite Self;

    void OnEnable()
    {
        this.Run();
    }

	void Start () 
    {
        int parentW = Convert.ToInt32(Parent.baseClipRegion.z);
        int parentH = Convert.ToInt32(Parent.baseClipRegion.w);

        int width = Convert.ToInt32(parentW * 0.2f);
        int height = Convert.ToInt32(parentH * 0.15f);
        Self.SetRect(-width / 2, -(parentH / 2 - parentH * 0.05f), width, height);
	}

    public void Run()
    {
        Expand();
    }

    #region Animation
    void Expand()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1.1f,
            "y", 1.1f,
            "islocal", true,
            "time", 0.2f,
            "easeType", iTween.EaseType.easeInQuad,
            "oncomplete", "Contract",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void ExpandWithDelay()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1.1f,
            "y", 1.1f,
            "islocal", true,
            "time", 0.2f,
            "delay", 1.5f,
            "easeType", iTween.EaseType.easeInQuad,
            "oncomplete", "Contract",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void Contract()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1f,
            "y", 1f,
            "islocal", true,
            "time", 0.3f,
            "easeType", iTween.EaseType.spring,
            "oncomplete", "ExpandWithDelay",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }
    #endregion	
}
