﻿using UnityEngine;
using System;

public class ClaimRewardPulse : PulseButton {
    public UISprite Parent;
    public UISprite Self;

    void OnEnable()
    {
        this.Run();
    }

	void OnStart()
    {
        int width = Convert.ToInt32(Parent.width * 0.3f);
        int height = Convert.ToInt32(Parent.height * 0.1f);
        Self.SetRect(Parent.width * 0.6f, 0, width, height);
    }
}
