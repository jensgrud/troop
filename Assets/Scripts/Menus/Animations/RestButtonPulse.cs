﻿using UnityEngine;
using System;

public class RestButtonPulse : PulseButton {
    public UIPanel Parent;
    public UISprite Self;


    // Use this for initialization
    void Start()
    {
        int parentW = Convert.ToInt32(Parent.baseClipRegion.z);
        int parentH = Convert.ToInt32(Parent.baseClipRegion.w);

        int width = Convert.ToInt32(parentW * 0.2f);
        int height = Convert.ToInt32(parentH * 0.2f);
        Self.SetRect(-width / 2, -(parentH/2 - 50), width, height);
    }
}
