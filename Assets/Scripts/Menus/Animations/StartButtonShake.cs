﻿using UnityEngine;
using System;

public class StartButtonShake : MonoBehaviour, IiTweenAnimation {
    public UIPanel Parent;
    public UISprite Self;

    void OnEnable()
    {
        this.Run();
    }

    void Start()
    {
        int parentW = Convert.ToInt32(Parent.baseClipRegion.z);
        int parentH = Convert.ToInt32(Parent.baseClipRegion.w);

        int width = Convert.ToInt32(parentW * 0.3f);
        int height = Convert.ToInt32(parentH * 0.25f);
        Self.SetRect(-parentW/2 + parentW*0.05f, -parentH / 2 + parentH*0.05f, width, height);
    }

    public void Run()
    {
        Delay();
    }

    #region Animation
    void ShakeLeftOne()
    {
        iTween.RotateBy(gameObject, iTween.Hash(
            "amount", new Vector3(0,0,0.01f),
            "islocal", true,
            "time", 0.05f,
            "easeType", iTween.EaseType.easeInOutQuad,
            "oncomplete", "ShakeRightOne",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void ShakeRightOne()
    {
        iTween.RotateBy(gameObject, iTween.Hash(
            "amount", new Vector3(0, 0, -0.02f),
            "islocal", true,
            "time", 0.05f,
            "easeType", iTween.EaseType.easeInOutQuad,
            "oncomplete", "ShakeLeftTwo",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void ShakeLeftTwo()
    {
        iTween.RotateBy(gameObject, iTween.Hash(
            "amount", new Vector3(0, 0, 0.02f),
            "islocal", true,
            "time", 0.05f,
            "easeType", iTween.EaseType.easeInOutQuad,
            "oncomplete", "ShakeRightTwo",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void ShakeRightTwo()
    {
        iTween.RotateBy(gameObject, iTween.Hash(
            "amount", new Vector3(0, 0, -0.02f),
            "islocal", true,
            "time", 0.05f,
            "easeType", iTween.EaseType.easeInOutQuad,
            "oncomplete", "Reset",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void Reset()
    {
        iTween.RotateBy(gameObject, iTween.Hash(
            "amount", new Vector3(0, 0, 0.01f),
            "islocal", true,
            "time", 0.05f,
            "oncomplete", "Delay", //Because the delay tag is broken
            "oncompletetarget", gameObject,
            "ignoretimescale", true));
    }

    void Delay()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1,
            "y", 1,
            "islocal", true,
            "time", 2,
            "oncomplete", "ShakeLeftOne",
            "oncompletetarget", gameObject,
            "ignoretimescale", true));
    }
    #endregion
}
