﻿using UnityEngine;

public interface IiTweenAnimation {
    void Run();
}