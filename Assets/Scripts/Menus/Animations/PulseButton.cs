﻿using UnityEngine;
using System;

public abstract class PulseButton : MonoBehaviour, IiTweenAnimation {
    public float speedMultiplier = 1f;
    
    public void Run()
    {
        Expand();
    }

    #region Animation
    void Expand()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1.1f,
            "y", 1.1f,
            "islocal", true,
            "time", 0.2f,
            "easeType", iTween.EaseType.easeInQuad,
            "oncomplete", "Contract",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void Contract()
    {
        iTween.ScaleTo(gameObject, iTween.Hash(
            "x", 1f,
            "y", 1f,
            "islocal", true,
            "time", 0.3f,
            "easeType", iTween.EaseType.spring,
            "oncomplete", "Expand",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }
    #endregion
}
