﻿using UnityEngine;
using System;

public class PopupAnimation : MonoBehaviour, IiTweenAnimation {
    public UIPanel Parent;
    public UIPanel Self;
    public float speedMultiplier = 1f;

    bool startScaleAndPosition = false;

    void Start()
    {
        Self.baseClipRegion = new Vector4(0, 0, Convert.ToInt32(Parent.width), Convert.ToInt32(Parent.height));
    }

	public void Run()
    {
        transform.localPosition = new Vector3(0, -500, 0);
        MoveUp();
    }

    #region Animation
    void MoveUp()
    {
        iTween.MoveTo(gameObject, iTween.Hash(
            "y", 50,
            "islocal", true,
            "time", 0.5f * speedMultiplier,
            "easeType", iTween.EaseType.easeInExpo,
            "oncomplete", "BounceOne",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void BounceOne()
    {
        iTween.MoveTo(gameObject, iTween.Hash(
            "y", -25,
            "islocal", true,
            "time", 0.1f * speedMultiplier,
            "easeType", iTween.EaseType.linear,
            "oncomplete", "BounceTwo",
            "oncompletetarget", gameObject,
            "ignoretimescale", true
            ));
    }

    void BounceTwo()
    {
        iTween.MoveTo(gameObject, iTween.Hash(
            "y", 0,
            "islocal", true,
            "time", 0.3f * speedMultiplier,
            "easeType", iTween.EaseType.spring,
            "oncomplete", "ScaleReset",
            "oncompeltetarget", gameObject,
            "ignoretimescale", true
            ));

        startScaleAndPosition = true;
    }
    #endregion
}
