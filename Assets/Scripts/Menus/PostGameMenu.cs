﻿using UnityEngine;
using SmartLocalization;
using System.Collections;
using System.Collections.Generic;

public class PostGameMenu : MonoBehaviour {
	
	public UILabel CurrentLogsCounter;
	public UILabel TotalLogsCounter;
	public UILabel TotalDistanceCounter;
	
	public GameObject HighscoreButton;
	public GameObject ContinueButton; 

	public UISprite LogSprite;
	public UISprite UnanchoredLogSprite;
	public UISprite TotalLogsSprite;
	public UISprite TotalLogsSpriteOverlay;
	public GameObject LogWaypoint;

	public List<UISprite> ScoutFacesSprites;
	public List<string> ScoutHappyFaceNames;
	public List<string> ScoutSadFaceNames;

	public UILabel MultiplierLabel;
	public UILabel RiskBonusLabel;

	public UISprite ScoutFacesOverlay;

	void OnEnable(){

		if (!GameManager.Instance.alive){
			StartCoroutine (EndScreenAnimation());
			AudioManager.Instance.transform.FindChild("Soundtrack").audio.volume = 0f;
			AudioManager.Instance.transform.FindChild("Soundtrack").audio.mute = false;
			AudioManager.Instance.CrossFade(AudioManager.Instance.transform.FindChild("Soundtrack").gameObject, 2.5f, 0.9f);
			AudioManager.Instance.CrossFade(AudioManager.Instance.transform.FindChild("GlitterLoop").gameObject, 2.5f, 1f);
		} else
			ShowFinalValues();
	}
	
	void Update(){

		UnanchoredLogSprite.transform.position = LogSprite.transform.position;

		if (Input.GetMouseButtonDown (0) && !GameManager.Instance.alive){
			StopAllCoroutines ();
			ShowFinalValues();
		}
	}
	
	
	private IEnumerator EndScreenAnimation(){

		ShowInitialValues ();

		//TODO animate log icon
		//TODO particles and shiet
		if (PlayerWallet.Instance.LogsInThisGame > 0){

			yield return StartCoroutine (CountUp (
					CurrentLogsCounter,
					0,
					PlayerWallet.Instance.LogsInThisGame,
					3f,
					true
				));
		}

		//RankBonus sound
		if (RankManager.Instance.currentRank > 0){

			yield return StartCoroutine (Wait (0.5f));
			AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.transform.FindChild("EndScreenRank").gameObject, 1.0f);
			MultiplierLabel.gameObject.SetActive (true);
			yield return StartCoroutine (Wait (0.5f));
			AudioManager.Instance.transform.FindChild("LogAudioPlayer").audio.Play ();
			CurrentLogsCounter.text = (
				(int)
				(
				PlayerWallet.Instance.LogsInThisGame *
				(1f + (RankManager.Instance.editorRanks[RankManager.Instance.currentRank].scoreMultiplier * 0.01f))
				)
				).ToString();
			yield return StartCoroutine (Wait (0.5f));
			MultiplierLabel.gameObject.SetActive (false);
		}

		if (GameManager.Instance.noCampStreak > 0) {

			yield return StartCoroutine (Wait (0.5f));
			AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.transform.FindChild("EndScreenRank").gameObject, 1.0f);
			RiskBonusLabel.gameObject.SetActive (true);
			yield return StartCoroutine (Wait (0.5f));
			AudioManager.Instance.transform.FindChild("LogAudioPlayer").audio.Play ();
			CurrentLogsCounter.text = ( 
			    (int)
			    (
				(PlayerWallet.Instance.LogsInThisGame *
			 	(1f + (RankManager.Instance.editorRanks[RankManager.Instance.currentRank].scoreMultiplier * 0.01f))) +
				(25*GameManager.Instance.noCampStreak)
				)
			    ).ToString();
			yield return StartCoroutine (Wait (0.5f));
			RiskBonusLabel.gameObject.SetActive (false);

		}

		yield return StartCoroutine (Wait (1f));

		if (CurrentLogsCounter.text != "0"){

			ScoutFacesOverlay.gameObject.SetActive (true);
			TotalLogsSpriteOverlay.gameObject.SetActive (true);
			for (int i = 0; i < ScoutFacesSprites.Count; i++) {
				ScoutFacesSprites [i].spriteName = ScoutHappyFaceNames [i];
			}

			AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.transform.FindChild("CountLogsSound").gameObject, 1.0f);
			StartCoroutine (CountUp (
				CurrentLogsCounter,
				PlayerWallet.Instance.LogsInThisGame,
				0,
				2f
				));

			StartCoroutine (CountUp (
				TotalLogsCounter,
				PlayerWallet.Instance.PersistentLogs,
				PlayerWallet.Instance.TotalLogs,
				2f
				));

			StartCoroutine (FlowLogs ());
		}

		yield return StartCoroutine (CountUp (
			TotalDistanceCounter,
			StatisticsManager.Instance.Statistics[Statistic.TraveledMeters],
			(int) (StatisticsManager.Instance.Statistics[Statistic.TraveledMeters] + GameManager.Instance.troop.distanceTravelled),
			2f
			));

		ShowFinalValues ();
		AudioManager.Instance.CrossFade(AudioManager.Instance.transform.FindChild("GlitterLoop").gameObject, 2.5f, 0f);
	}

	private IEnumerator FlowLogs(){

		UISprite newLogSprite;

		float startTime = Time.realtimeSinceStartup;
		while (startTime + 2 > Time.realtimeSinceStartup) {

			newLogSprite = Instantiate (
				UnanchoredLogSprite, 
				LogSprite.transform.position, 
				LogSprite.transform.rotation) as UISprite;

			newLogSprite.transform.parent = LogSprite.transform.parent;
			newLogSprite.transform.localScale = LogSprite.transform.localScale;

			iTween.MoveTo (
				newLogSprite.gameObject,
				iTween.Hash (
					"islocal", true,
					"easeType", iTween.EaseType.easeInExpo,
					"ignoretimescale", true,
					"position",TotalLogsSprite.transform.localPosition + (Vector3.right * (TotalLogsSprite.width / 2)),
					//"path", new Transform[] {LogWaypoint.transform, TotalLogsSprite.transform},
					"time",0.2f,
					"oncomplete", "DestroySelf",
					"oncompletetarget", newLogSprite.gameObject

			));

			yield return Wait (0.2f);
		}
	}

	private IEnumerator CountUp(UILabel label, int from, int to, float time, bool withSound = false){

		float distance = to - from;

		float startTime = Time.realtimeSinceStartup;
		while (startTime + time > Time.realtimeSinceStartup) {
			
			float percent = (Time.realtimeSinceStartup - startTime)/time;

			if(withSound && label.text != ((int)(from + (distance * percent))).ToString() && !AudioManager.Instance.transform.FindChild("LogAudioPlayer").audio.isPlaying){
				AudioManager.Instance.transform.FindChild("LogAudioPlayer").audio.Play ();
			}

			label.text = ((int)(from + (distance * percent))).ToString();
			yield return 0;
		}

		label.text = to.ToString ();
		AudioManager.Instance.transform.FindChild("LogAudioPlayer").audio.Play ();
	}
	
	private IEnumerator Wait(float time){
		float startTime = Time.realtimeSinceStartup;
		while (startTime + time > Time.realtimeSinceStartup) {
			yield return 0;
		}
	}

	void ShowInitialValues ()
	{
		CurrentLogsCounter.text = "" + 0;
		TotalLogsCounter.text = "" + PlayerWallet.Instance.PersistentLogs;
		TotalDistanceCounter.text = "" + StatisticsManager.Instance.Statistics [Statistic.TraveledMeters];
		for (int i = 0; i < ScoutFacesSprites.Count; i++) {
			ScoutFacesSprites [i].spriteName = ScoutSadFaceNames [i];
		}
		HighscoreButton.SetActive (false);
		ContinueButton.SetActive (false);

		MultiplierLabel.text = 
			LanguageManager.Instance.GetTextValue("Rank Label") + " " +
			RankManager.Instance.currentRank + " " +
			LanguageManager.Instance.GetTextValue("Multiplier");

		ScoutFacesOverlay.gameObject.SetActive (false);
		TotalLogsSpriteOverlay.gameObject.SetActive (false);
	}
	
	private void ShowFinalValues(){
		
		ContinueButton.SetActive(true);
		HighscoreButton.SetActive(true);

		CurrentLogsCounter.text = "" + 0;

		float scoreMul = (1f + (RankManager.Instance.editorRanks [RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].scoreMultiplier * 0.01f));

		TotalLogsCounter.text = (
			(int)(   PlayerWallet.Instance.PersistentLogs +
				(PlayerWallet.Instance.LogsInThisGame *
		 		(1f + (RankManager.Instance.editorRanks [RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].scoreMultiplier * 0.01f))) +
				(25 * GameManager.Instance.noCampStreak)
			)
			).ToString ();

		TotalDistanceCounter.text = "" + (int) (StatisticsManager.Instance.Statistics[Statistic.TraveledMeters] + GameManager.Instance.troop.distanceTravelled);
		for (int i = 0; i < ScoutFacesSprites.Count; i++) {
			ScoutFacesSprites [i].spriteName = ScoutHappyFaceNames [i];
		}

		ScoutFacesOverlay.gameObject.SetActive (true);
		TotalLogsSpriteOverlay.gameObject.SetActive (true);

		if (!GameManager.Instance.alive)
			EndGame ();
		else
			HighscoreButton.SetActive(false);
	}
	
	//TODO THIS SHOULDN'T BE IN GUI SCRIPT
	private void EndGame ()
	{
		StatisticsManager.Instance.IterateStatistic (Statistic.TraveledMeters, (int)GameManager.Instance.troop.reducedDistanceTravelled);
		GameManager.Instance.alive = true;
		GameManager.Instance.troop.stamina = 3;
		PlayerWallet.Instance.PersistentLogs += (int)(
			(PlayerWallet.Instance.LogsInThisGame *
			 (1f + (RankManager.Instance.editorRanks [RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].scoreMultiplier * 0.01f))) +
			(25 * GameManager.Instance.noCampStreak));
		PlayerWallet.Instance.LogsInThisGame = 0;
	}
	
	public void PostScore(){
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.EnterHighscore);
	}
	
	public void Continue(){
		Application.LoadLevel(Application.loadedLevel);
	}
}
