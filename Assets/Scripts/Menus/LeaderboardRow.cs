﻿using UnityEngine;
using System.Collections;

public class LeaderboardRow : MonoBehaviour {

	public UILabel Name;
	public UILabel Score;
    public UILabel Badges;
	public UILabel Rank;
}
