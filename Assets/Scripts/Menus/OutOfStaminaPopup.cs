﻿using UnityEngine;
using System.Collections;

public class OutOfStaminaPopup : MonoBehaviour {

	public UILabel AmountLabel;

	public UISprite MarshmallowSprite1; 
	public UISprite MarshmallowSprite2;

	public UISprite YesButton;

	void OnEnable(){
        GameManager.Instance.StopTime();
		AmountLabel.text = "x " + Shop.Instance.GetContinueCost ();

        GetComponent<PopupAnimation>().Run();
        if (Shop.Instance.GetContinueCost() <= PlayerWallet.Instance.Marshmallows)
        {
            YesButton.gameObject.SetActive(true);
        }
        else
        {
            YesButton.gameObject.SetActive(false);
        }

        if (!GameManager.Instance.isDeathTutorialComplete)
            GameManager.Instance.isDeathTutorialComplete = true;
        else
        {
            AudioManager.Instance.MuteEverything();

            if (AudioManager.Instance.SoundsOn)
            {
                AudioManager.Instance.transform.FindChild("GameOverAudioPlayer").audio.mute = false;
                AudioManager.Instance.transform.FindChild("CountLogsSound").audio.mute = false;
                AudioManager.Instance.transform.FindChild("GlitterLoop").audio.mute = false;
				AudioManager.Instance.transform.FindChild("EndScreenRank").audio.mute = false;
				AudioManager.Instance.transform.FindChild("LogAudioPlayer").audio.mute = false;
				AudioManager.Instance.transform.FindChild("RestSound").audio.mute = false;
            }
               

            AudioManager.Instance.PlaySingleSound(
                gameObject,
                AudioManager.Instance.transform.FindChild("GameOverAudioPlayer").gameObject,
                1
            );

            StartCoroutine("MeltCoroutine");
        }

		GuiManager.Instance.CurrentMenuReference.GetComponent<InGameGui> ().HideWallet ();
		GuiManager.Instance.ShowWallet ();
	}

	void OnDisable(){
		if (GuiManager.Instance.CurrentMenu == GuiManager.MenuType.Game){
			GuiManager.Instance.CurrentMenuReference.GetComponent<InGameGui> ().ShowWallet ();
			GuiManager.Instance.HideWallet ();
		}
	}

	public void Yes(){

		StopAllCoroutines ();

		//TODO this shouldn't be in the gui script!
		PlayerWallet.Instance.SpendMarshmallows(Shop.Instance.GetContinueCost());
		GameManager.Instance.alive = true;
		GameManager.Instance.troop.stamina = 3;
        AudioManager.Instance.UnMuteEverything();
        GameManager.Instance.ToggleObstacleInvincibility();
        GameManager.Instance.Invoke("ToggleObstacleInvincibility", 2f);
		//AudioManager.Instance.transform.FindChild ("ChaseModetrack").gameObject.audio.volume = 1.0f;

		GuiManager.Instance.HidePopup ();
		GameManager.Instance.ResetTimeScale ();
	}

	public void No(){

		StopAllCoroutines ();

#if UNITY_ANDROID
		GA.API.Design.NewEvent ("Game:End");
		GameAnalyticsHelper.GameEndTime = Time.realtimeSinceStartup;
		GA.API.Design.NewEvent ("Game:Duration",GameAnalyticsHelper.GameDuration);
		GA.API.Design.NewEvent("Game:BadgesEarned",GameAnalyticsHelper.BadgesAmount);
		if (GameAnalyticsHelper.RanksAmount > 0f)
			GA.API.Design.NewEvent("Game:RankEarned",GameAnalyticsHelper.RanksAmount);
#endif

		GuiManager.Instance.ShowMenu (GuiManager.MenuType.PostGame);
		//AudioManager.Instance.transform.FindChild ("ChaseModetrack").gameObject.audio.volume = 1.0f;
	}

	UISprite ActiveSprite;
	UISprite BufferSprite;
	int blendingIterator;

	private IEnumerator MeltCoroutine(){

		ActiveSprite = MarshmallowSprite1;
		BufferSprite = MarshmallowSprite2;

		ActiveSprite.color = new Color (1f, 1f, 1f, 1f);
		BufferSprite.color = new Color (1f, 1f, 1f, 0f);

		float startTime = Time.realtimeSinceStartup;
		while (startTime + 1 > Time.realtimeSinceStartup) {
			yield return 0;
		}

		for (blendingIterator=1; blendingIterator<=5; blendingIterator++) {
			yield return StartCoroutine("BlendSprites");

			UISprite b = ActiveSprite;
			ActiveSprite = BufferSprite;
			BufferSprite = b;
		}

		//yield return StartCoroutine("BlendOutYesButton");
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.PostGame);

	}

	private IEnumerator BlendSprites () {

		ActiveSprite.spriteName = "GUI@MarshmallowMelting" + blendingIterator;
		BufferSprite.spriteName = "GUI@MarshmallowMelting" + (blendingIterator+1);

		float startTime = Time.realtimeSinceStartup;
		while (startTime + 1 > Time.realtimeSinceStartup) {

			float percent = Time.realtimeSinceStartup - startTime;

			ActiveSprite.color = new Color (1f, 1f, 1f, 1-percent);
			BufferSprite.color = new Color (1f, 1f, 1f, percent);

			yield return 0;
		}
	}

	private IEnumerator BlendOutYesButton(){

		YesButton.color = new Color (1f, 1f, 1f, 1f);

		float startTime = Time.realtimeSinceStartup;
		while (startTime + 1 > Time.realtimeSinceStartup) {
			
			float percent = Time.realtimeSinceStartup - startTime;
			YesButton.color = new Color (1f, 1f, 1f, 1-percent);
			yield return 0;
		}

		YesButton.gameObject.SetActive (false);
		YesButton.color = new Color (1f, 1f, 1f, 1f);
	}
}
