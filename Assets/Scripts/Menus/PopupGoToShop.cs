﻿using UnityEngine;
using System.Collections;

public class PopupGoToShop : MonoBehaviour {

	public void Yes(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Currency);
	}

	public void No(){
		GuiManager.Instance.HidePopup ();
	}
}
