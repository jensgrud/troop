﻿using UnityEngine;
using System.Collections.Generic;

public class BadgesMenu : MonoBehaviour {
	
	public NewBadgeButton BadgePrefab;
	
	public UIGrid BadgesGrid;
	
	public List<NewBadgeButton> BadgeButtons;
	
	public UISprite MedalSprite;
	public LocalizeWidget BackButtonLabel;
	
	void OnEnable(){

        AudioManager.Instance.MuteEverything();
        if (AudioManager.Instance.MusicOn)
        {      
            AudioManager.Instance.transform.FindChild("Soundtrack").gameObject.audio.mute = false;
        }
        if (AudioManager.Instance.SoundsOn)
        {
            AudioManager.Instance.transform.FindChild("AmbienceLoop").gameObject.audio.mute = false;
        }
            

		if (BadgeButtons.Count == 0){
			
			float leftpercentage = BadgesGrid.GetComponent<UIWidget>().leftAnchor.relative;
			float rightpercentage = BadgesGrid.GetComponent<UIWidget>().rightAnchor.relative;
			
			float toppercentage = BadgesGrid.GetComponent<UIWidget>().topAnchor.relative;
			float bottompercentage = BadgesGrid.GetComponent<UIWidget>().bottomAnchor.relative;
			
			float widgetWidth = Screen.width * (rightpercentage - leftpercentage);
			float widgetHeight = Screen.height * (toppercentage - bottompercentage);
			
			float BadgeWidth = widgetWidth / (6.5f);
			float BadgeHeight = widgetHeight / (4);
			
			BadgesGrid.cellWidth = BadgeWidth;
			BadgesGrid.cellHeight = BadgeHeight;
			
			for (int i = 0; i < 20; i++) {
				NewBadgeButton badgeSprite = Instantiate(BadgePrefab) as NewBadgeButton;
				badgeSprite.transform.parent = BadgesGrid.transform;
				badgeSprite.transform.localScale = Vector3.one;
				badgeSprite.GetComponent<UISprite>().SetDimensions((int)(BadgeWidth*0.8f),(int)(BadgeHeight*0.8f));
				badgeSprite.GetComponent<UIButton>().isEnabled = false;
				
				BadgeButtons.Add(badgeSprite);
			}
		} else {
			for (int i = 0; i < 20; i++) {
				BadgeButtons[i].GetComponent<UIButton>().isEnabled = false;
				BadgeButtons[i].Icon.gameObject.SetActive(false);
			}
		}
		
		List<Badge> currentUsersBadges = BadgeManager.Instance.getCurrentBadgesList ();
		int currentIconIndex = 0;
		
		foreach (Badge b in currentUsersBadges) {
			
			BadgeButtons[currentIconIndex].GetComponent<UISprite>().spriteName = BadgeManager.Instance.GetOverlay(b.badgeLevel);
			BadgeButtons[currentIconIndex].GetComponent<UIButton>().normalSprite = BadgeManager.Instance.GetOverlay(b.badgeLevel);
			BadgeButtons[currentIconIndex].Icon.gameObject.SetActive(true);
			BadgeButtons[currentIconIndex].Icon.spriteName = b.spriteName;
			BadgeButtons[currentIconIndex].Badge = b;
			BadgeButtons[currentIconIndex].GetComponent<UIButton>().isEnabled = true;
			
			currentIconIndex++;
		}		
		
		MedalSprite.spriteName = "GUI@Rank"+RankManager.Instance.currentRank+"Medal";
		
		BackButtonLabel.key = GameManager.Instance.troop.distanceTravelled > 0 ? "Continue Label" : "Back Label";
		BackButtonLabel.Reload ();
		
		
		GuiManager.Instance.ShowWallet ();
	}
	
	void OnDisable(){
		GuiManager.Instance.HideWallet ();
	}
	
	public void Settings(){
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Settings);
	}
	
	public void PostToFacebook(){
		
		GuiManager.Instance.InitGenericPopup ("FB Share? Label", this.gameObject, "PostedToFacebook",0);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Question);
	}
	
	public void PostedToFacebook() {
		FacebookManager.Instance.UploadScreenShot ();
	}
	
	public void OpenLeaderboards(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Leaderboard);
	}
	
	public void Home(){
		
		GuiManager.Instance.InitGenericPopup ("Home? Label", this.gameObject, "Homed",0);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Question);
	}
	
	public void Homed(){
		
		#if UNITY_ANDROID
		GA.API.Design.NewEvent ("Game:End");
		GameAnalyticsHelper.GameEndTime = Time.realtimeSinceStartup;
		GA.API.Design.NewEvent ("Game:Duration",GameAnalyticsHelper.GameDuration);
		GA.API.Design.NewEvent("Game:BadgesEarned",GameAnalyticsHelper.BadgesAmount);
		if (GameAnalyticsHelper.RanksAmount > 0f)
			GA.API.Design.NewEvent("Game:RankEarned",GameAnalyticsHelper.RanksAmount);
		#endif
		
		AudioManager.Instance.transform.FindChild ("Soundtrack").gameObject.audio.volume = 0.9f;
		GameManager.Instance.alive = true;
		
		GameManager.Instance.troop.stamina = 3;
		PlayerWallet.Instance.LogsInThisGame = 0;

		if (RankManager.Instance.savedRank > RankManager.tutorialRank)
		{
			// We are replaying tutorial level so reset rank
			RankManager.Instance.ResetRank();
		}
		
		Application.LoadLevel(Application.loadedLevel);
	}
	
	public void Credits(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Credits);
	}
}
