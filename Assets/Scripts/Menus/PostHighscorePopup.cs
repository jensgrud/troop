﻿using UnityEngine;
using System.Collections;

public class PostHighscorePopup : MonoBehaviour {

	public UIInput InputField;

    HighscoreClient client;

	void OnEnable(){
		InputField.label.text = "";
	}

	public void Apply(){

		if (InputField.label.text.Length == 0)
			return;

        if (HighscoreClient.Instance == null)
            client = new HighscoreClient();
        else
            client = HighscoreClient.Instance;

		try {

			client.Insert(new HighscoreClient.HighscoreEntry(
				InputField.label.text,
				(int) GameManager.Instance.troop.reducedDistanceTravelled,
	            BadgeManager.Instance.earnedBadges.Count,
				RankManager.Instance.currentRank
			));

			GuiManager.Instance.ShowMenu (GuiManager.MenuType.Leaderboard);

		} catch (System.Exception e){

			GuiManager.Instance.InitGenericPopup(
				"Cannot Post Highscore",
				GuiManager.Instance.gameObject,
				"HidePopup",
				0
			);

			GuiManager.Instance.ShowPopup(GuiManager.PopupType.Info);

		}
			
	}

	public void HidePopup(){
		GuiManager.Instance.HidePopup ();
	}
}
