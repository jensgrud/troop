﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class PowerupsAndMascotsMenu : MonoBehaviour {

	public GameObject PowerupsSubMenu;

	public GameObject PowerupBuySection;

	public UISprite PowerupSprite;
	public UILabel PowerupDescription;
	public UILabel PowerupPriceLogs;
	public UILabel PowerupPriceMarshmallows;

	public UISprite PowerupActivateButton;
	public UISprite PowerupActivatedSprite;

	public GameObject MascotsSubMenu;

	public GameObject MascotBuySection;

	public UISprite MascotSprite;
	public UILabel MascotDescription;
	public UILabel MascotPrice;
	
	public UISprite MascotActivateButton;
	public UISprite MascotActivatedSprite;

	public GameObject MascotEffectSection;
	public UILabel MascotEffectDescription;
	public UISprite MascotEffectSprite;

	public string[] PowerupSpriteNames;
	public string[] MascotSpriteNames;

	public UILabel TitleLabel;

	private int _CurrentIndex;
	public int CurrentIndex{
		get {
			return _CurrentIndex;
				}
		set {
			_CurrentIndex = value;

			int maxValue = 
				(GuiManager.Instance.CurrentMenu == GuiManager.MenuType.Powerups) ?
				PowerupSpriteNames.Length - 1:
				MascotSpriteNames.Length - 1;

			if (_CurrentIndex > maxValue)
				_CurrentIndex = 0;
			else if (_CurrentIndex < 0)
				_CurrentIndex = maxValue;

		}
	}

	void OnEnable(){
		if (GuiManager.Instance.CurrentMenu == GuiManager.MenuType.Powerups) {

			PowerupsSubMenu.SetActive(true);
			MascotsSubMenu.SetActive(false);

            TitleLabel.text = LanguageManager.Instance.GetTextValue("Power-up shop Label");


        }
        else if (GuiManager.Instance.CurrentMenu == GuiManager.MenuType.Mascots)
        {

            PowerupsSubMenu.SetActive(false);
            MascotsSubMenu.SetActive(true);
            TitleLabel.text = LanguageManager.Instance.GetTextValue("Mascot shop Label");

		}

		UpdateCurrent();

		GuiManager.Instance.ShowWallet ();

	}

	public void OnDisable(){
		GuiManager.Instance.HideWallet ();
	}

	private void UpdateCurrent(){

		if (GuiManager.Instance.CurrentMenu == GuiManager.MenuType.Powerups) {
			UpdatePowerups ();
		} else {
			UpdateMascots ();
		}
	}

	public void UpdatePowerups(){

		if (PowerUpsManager.Instance.PowerUpAmounts[CurrentIndex] == 0 )
		{
			PowerupBuySection.SetActive(true);
			PowerupActivateButton.gameObject.SetActive(false);
			PowerupActivatedSprite.gameObject.SetActive(false);

			PowerupPriceLogs.text = "x"+PowerUpsManager.Instance.PowerUps [CurrentIndex].PriceFirewood;
			PowerupPriceMarshmallows.text = "x"+PowerUpsManager.Instance.PowerUps [CurrentIndex].PriceMarshmallows;

		} else if (GameManager.Instance.troop.currentPowerUp == null || GameManager.Instance.troop.currentPowerUp.Id != PowerUpsManager.Instance.PowerUps[CurrentIndex].Id){

			PowerupBuySection.SetActive(false);
			PowerupActivateButton.gameObject.SetActive(true);
			PowerupActivatedSprite.gameObject.SetActive(false);

		} else {

			PowerupBuySection.SetActive(false);
			PowerupActivateButton.gameObject.SetActive(false);
			PowerupActivatedSprite.gameObject.SetActive(true);

		}

		PowerupSprite.spriteName = PowerupSpriteNames [CurrentIndex];
        PowerupDescription.text = LanguageManager.Instance.GetTextValue(PowerUpsManager.Instance.PowerUps[CurrentIndex].Description);
        
	}

	public void UpdateMascots(){

		//TODO add greying out pets which are not found yet

        //if pet not unlocked show locked screen
        //if pet unlocked show buy screen
        //if pet bought show equip screen
        //if pet pet equiped show equiped screen

        //If the pet is not unlocked it cannot be bought and the price is ?
        if (!PetsManager.Instance.UnlockedPets[CurrentIndex])
        {
            MascotBuySection.SetActive(true);//buy button
            MascotActivateButton.gameObject.SetActive(false);//Equip button
            MascotActivatedSprite.gameObject.SetActive(false);//Equipped button
            MascotEffectSection.SetActive(false);

            MascotPrice.text = "x ?";
        }
        //If pet is unlocked and not bought it can be bought
        else if (PetsManager.Instance.UnlockedPets[CurrentIndex] && !PetsManager.Instance.BoughtPets[CurrentIndex])
		{
			MascotBuySection.SetActive(true);//buy button
			MascotActivateButton.gameObject.SetActive(false);//Equip button
			MascotActivatedSprite.gameObject.SetActive(false);//Equipped button
			MascotEffectSection.SetActive(false);

			MascotPrice.text = "x"+PetsManager.Instance.Pets [CurrentIndex].PriceMarshmallows;
			
		}
        //If the pet is bought but not the pet of the troop it can be equipped
        else if (PetsManager.Instance.BoughtPets[CurrentIndex] == true && 
            (GameManager.Instance.troop.pet == null || GameManager.Instance.troop.pet.Id != PetsManager.Instance.Pets[CurrentIndex].Id)) 
        {
            MascotBuySection.SetActive(false);//buy button
            MascotActivateButton.gameObject.SetActive(true);//Equip button
            MascotActivatedSprite.gameObject.SetActive(false);//Equipped button
            MascotEffectSection.SetActive(false);
        }
         else {
			
			MascotBuySection.SetActive(false);
			MascotActivateButton.gameObject.SetActive(false);
			MascotActivatedSprite.gameObject.SetActive(true);
			MascotEffectSection.SetActive(true);
			
		}

        //Change sprites here
        MascotSprite.spriteName = MascotSpriteNames[CurrentIndex] + (PetsManager.Instance.UnlockedPets[CurrentIndex] == true ? "" : "Locked");
		MascotDescription.text = !PetsManager.Instance.UnlockedPets[CurrentIndex] ? LanguageManager.Instance.GetTextValue("Locked Pet") : LanguageManager.Instance.GetTextValue(PetsManager.Instance.Pets[CurrentIndex].Description);

		MascotEffectSprite.spriteName = PowerupSpriteNames [CurrentIndex];
		// TODO MascotEffectDescription.text = ??
	}

	public void Right(){
		CurrentIndex++;

		UpdateCurrent ();

	}

	public void Left(){
		CurrentIndex--;

		UpdateCurrent ();
	}

	public void BuyPowerupLogs(){
		GuiManager.Instance.ShowBuyPopup(
			CurrencyType.Logs,
			PowerUpsManager.Instance.PowerUps[CurrentIndex].PriceFirewood,
			this.gameObject,
			"BoughtPowerupLogs"
		);
	}

	public void BuyPowerupMarshmallows(){
		GuiManager.Instance.ShowBuyPopup(
			CurrencyType.Marshmallows,
			PowerUpsManager.Instance.PowerUps[CurrentIndex].PriceMarshmallows,
			this.gameObject,
			"BoughtPowerupMarshmallows"
		);
	}

	public void BuyPet(){
        //Can only be bought if the pet is unlocked
        if (PetsManager.Instance.UnlockedPets[CurrentIndex] == true)
        {
            GuiManager.Instance.ShowBuyPopup(
                CurrencyType.Marshmallows,
                PetsManager.Instance.Pets[CurrentIndex].PriceMarshmallows,
                this.gameObject,
                "BoughtPet"
            );
        }
	}

	public void BoughtPowerupLogs(){
		Shop.Instance.BuyPowerUp (
			PowerUpsManager.Instance.PowerUps [CurrentIndex].Id,
			false
		);

		ActivatePowerup ();
		GuiManager.Instance.Back ();
	}

	public void BoughtPowerupMarshmallows(){
		Shop.Instance.BuyPowerUp (
			PowerUpsManager.Instance.PowerUps [CurrentIndex].Id,
			true
		);

		ActivatePowerup ();
		GuiManager.Instance.Back ();
	}

	public void BoughtPet(){
		Shop.Instance.BuyPet (
			PetsManager.Instance.Pets [CurrentIndex].Id,
			true
		);

		ActivatePet ();
		GuiManager.Instance.Back ();
	}

	public void ActivatePet(){
		
		if (GameManager.Instance.troop.pet != null)
			GameManager.Instance.troop.pet.Destroy ();

		GameManager.Instance.troop.pet = Instantiate(PetsManager.Instance.Pets [CurrentIndex]) as Pet;
		GameManager.Instance.troop.pet.isPermanent = true;
		PetsManager.Instance.ChangeHats(GameManager.Instance.troop.pet);

		UpdateMascots ();
	}

	public void ActivatePowerup(){

		GameManager.Instance.troop.currentPowerUp = 
			PowerUpsManager.Instance.PowerUps [CurrentIndex];

		UpdatePowerups ();
	}
}
