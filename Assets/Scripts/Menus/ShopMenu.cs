﻿using UnityEngine;
using System.Collections;

public class ShopMenu : MonoBehaviour {

	public GameObject[] StaminaSprites;

	public GameObject RestButton;
	public UILabel RestButtonEquationLabel;
    public float sizeFractionSpeed = 0.7f;
    public float maxSize = 1.5f;
    private float targetSize = 1.5f;

	public UILabel DistanceLabel;

	public delegate void ExitCampHandler();
	public event ExitCampHandler OnExitCamp;

	public GameObject FirePlace;

	public LocalizeWidget BackButtonLabel;

    void Update()
    {
        //RestPulse();
    }

	void OnEnable() {
		
		UpdateStamina ();
		UpdateDistance ();

		UpdateRestButton ();
		GuiManager.Instance.ShowWallet ();

        //Rest pulse
        if (GameManager.Instance.troop.stamina < 3)
        {
            RestButton.GetComponent<RestButtonPulse>().Run();
        }

		BackButtonLabel.key = GameManager.Instance.troop.distanceTravelled > 0 ? "Continue Label" : "Back Label";
		BackButtonLabel.Reload ();
	}

	void OnDisable(){

		GuiManager.Instance.PopupReferencesList [(int)(GuiManager.PopupType.PayCurrency)].transform.FindChild("Button - Yes")
			.GetComponent<UIPlaySound> ().enabled = true;

		GuiManager.Instance.HideWallet ();

		if (OnExitCamp != null)
			OnExitCamp();
	}

	void UpdateRestButton ()
	{
        if (GameManager.Instance.troop.stamina == 3)
            RestButton.SetActive(false);
        else
        {
            RestButton.SetActive(GameManager.Instance.troop.distanceTravelled > 0);
            RestButtonEquationLabel.text = Shop.Instance.GetRestCost() + "x";
        }
	}

	public void UpdateStamina (){
		if (InGameGui.Instance != null) {
        	InGameGui.Instance.UpdateStamina();

			if (GameManager.Instance.troop.stamina == 1)
			{
				StaminaSprites[0].GetComponent<UISprite>().spriteName = InGameGui.Instance.redStaminaSpriteName;
				StaminaSprites[1].GetComponent<UISprite>().spriteName = InGameGui.Instance.lostStaminaSpriteName;
				StaminaSprites[2].GetComponent<UISprite>().spriteName = InGameGui.Instance.lostStaminaSpriteName;
			}
			if (GameManager.Instance.troop.stamina == 2)
			{
				StaminaSprites[0].transform.localScale = Vector3.one;
				StaminaSprites[0].GetComponent<UISprite>().spriteName = InGameGui.Instance.staminaSpriteName;
				StaminaSprites[1].GetComponent<UISprite>().spriteName = InGameGui.Instance.staminaSpriteName;
				StaminaSprites[2].GetComponent<UISprite>().spriteName = InGameGui.Instance.lostStaminaSpriteName;
			}
			if (GameManager.Instance.troop.stamina == 3)
			{
				StaminaSprites[0].transform.localScale = Vector3.one;
				StaminaSprites[0].GetComponent<UISprite>().spriteName = InGameGui.Instance.staminaSpriteName;
				StaminaSprites[1].GetComponent<UISprite>().spriteName = InGameGui.Instance.staminaSpriteName;
				StaminaSprites[2].GetComponent<UISprite>().spriteName = InGameGui.Instance.staminaSpriteName;
			}
		}

        RestButton.transform.localScale = Vector3.one;
	}

	public void UpdateDistance(){
		DistanceLabel.gameObject.SetActive (
			GameManager.Instance.troop.distanceTravelled > 0
		);
		DistanceLabel.text = "Distance\n"+((int)GameManager.Instance.troop.reducedDistanceTravelled).ToString();
	}

	public void OpenExchangeMenu(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Currency);
	}

	public void OpenMascotsMenu(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Mascots);
	}

	public void OpenPowerupsMenu(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Powerups);
	}

	public void OpenBadgesMenu(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Badges);
	}

	public void Rest(){

		GuiManager.Instance.PopupReferencesList [(int)(GuiManager.PopupType.PayCurrency)].transform.FindChild("Button - Yes")
			.GetComponent<UIPlaySound> ().enabled = false;

		GuiManager.Instance.ShowBuyPopup (
			CurrencyType.Logs, 
			Shop.Instance.GetRestCost (), 
			this.gameObject, 
			"Rested"
		);

	}

	public void Rested(){

		AudioManager.Instance.transform.FindChild("RestSound").audio.mute = false;
		AudioManager.Instance.PlaySingleSound (gameObject, AudioManager.Instance.transform.FindChild ("RestSound").gameObject, 1.0f);
		Shop.Instance.Rest ();
		UpdateStamina ();
		UpdateRestButton ();

		GuiManager.Instance.PopupReferencesList [(int)(GuiManager.PopupType.PayCurrency)].transform.FindChild("Button - Yes")
			.GetComponent<UIPlaySound> ().enabled = true;

//		Debug.Log("im resting now!");
//		foreach (ParticleSystem ps in FirePlace.GetComponentsInChildren<ParticleSystem>())
//		{
//			ps.GetComponent<ParticleEmitter>().enabled = true;
//			//ps.Play();
//		}
	}
    /*
    public void RestPulse()
    {
        if (GameManager.Instance.troop.stamina < 3)
        {
            if (RestButton.transform.localScale.x <= Vector3.one.x * 1.2f)
            {
                targetSize = maxSize;
            }
            else if (RestButton.transform.localScale.x >= Vector3.one.x * maxSize * 0.9f)
            {
                targetSize = 1f;
            }
            Transform newT = RestButton.transform;
            newT.localScale = Vector3.Lerp(RestButton.transform.localScale, Vector3.one * targetSize, sizeFractionSpeed * Time.unscaledDeltaTime);
            RestButton.transform.localScale = newT.localScale;
        }
    }*/
}
