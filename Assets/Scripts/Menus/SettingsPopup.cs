﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class SettingsPopup : MonoBehaviour {

	public UISprite LanguageButton;
	public string languageDanishSprite;
	public string languageEnglishSprite;

	public UISprite SoundsButton;
	public string soudsOnSpriteName;
	public string soudsOffSpriteName;
	
	public UISprite MusicButton;
	public string musicOnSpriteName;
	public string musicOffSpriteName;

    void OnEnable()
    {
        string newSpriteName;
        
        if (LanguageManager.Instance.LoadedLanguage == "da-DK")
            newSpriteName = languageDanishSprite;
        else
            newSpriteName = languageEnglishSprite;

        LanguageButton.spriteName = newSpriteName;
        LanguageButton.GetComponent<UIButton>().normalSprite = newSpriteName;
    }

	public void ToggleSounds(){
		AudioManager.Instance.ToggleSounds ();
		string newSpriteName = SoundsButton.spriteName.Equals (soudsOnSpriteName) ? soudsOffSpriteName : soudsOnSpriteName; 
		
		SoundsButton.spriteName = newSpriteName;
		SoundsButton.GetComponent<UIButton> ().normalSprite = newSpriteName;
	}
	
	public void ToggleMusic(){
		AudioManager.Instance.ToggleMusic ();
		
		string newSpriteName = MusicButton.spriteName.Equals (musicOnSpriteName) ? musicOffSpriteName : musicOnSpriteName;
		
		MusicButton.spriteName = newSpriteName;
		MusicButton.GetComponent<UIButton> ().normalSprite = newSpriteName;
	}

	public void ToggleLanguage(){

		//TODO actually toggle the language!
        if (LanguageManager.Instance.LoadedLanguage == "da-DK")
        {
            LanguageManager.Instance.ChangeLanguage("en-GB");
        }
        else
        {
            LanguageManager.Instance.ChangeLanguage("da-DK");
        }
        //Debug.Log(LanguageManager.Instance.LoadedLanguage.ToString());


		string newSpriteName = LanguageButton.spriteName.Equals (languageDanishSprite) ? languageEnglishSprite : languageDanishSprite;
		
		LanguageButton.spriteName = newSpriteName;
		LanguageButton.GetComponent<UIButton> ().normalSprite = newSpriteName;

        GameManager.Instance.SaveLanguage();
	}

	public void Back(){
		GuiManager.Instance.HidePopup ();
	}
}
