﻿using UnityEngine;
using System.Collections;

public class GUIWallet : MonoBehaviour {

	public UILabel TotalLogsLabel;
	public UILabel MarshmallowsLabel;

	// Update is called once per frame
	void Update () {
		TotalLogsLabel.text = PlayerWallet.Instance.TotalLogs.ToString();
		MarshmallowsLabel.text = PlayerWallet.Instance.Marshmallows.ToString();
	}
}
