﻿using UnityEngine;
using System.Collections;

public class InGameButton : MonoBehaviour, ITapable {

	public string MethodToCall;
	public GameObject argument;  
	AudioManager audioMan;
    //private bool isBlinking = false;
    //public float blinkLifetime = 0.5f;
    //private Camera mainCam;
	void Start () {
		audioMan = AudioManager.Instance;
       // mainCam = Camera.main;
	}

	public void tap(){

        

		if (argument != null) {
			GuiManager.Instance.SendMessage (MethodToCall, argument);
			audioMan.PlaySingleSound (gameObject, audioMan.transform.FindChild ("SetUpCampAudioPlayer").gameObject, 1);
		
		}
		else
			GuiManager.Instance.SendMessage (MethodToCall);

	}


    // //something cool when you pass the campsite, like blinking or fading out. WAITING FOR DESIGNER´S DECISION

    //void Update()
    //{
    //    if (this.transform.position.x < mainCam.transform.position.x && !isBlinking)
    //    {
    //        isBlinking = true;
    //        StartCoroutine("Fade");
    //    }
    //}

    //IEnumerator Fade()
    //{
    //    SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
    //    for (float blinkTime = blinkLifetime; blinkTime >= 0; blinkTime -= 0.1f)
    //    {
    //        sprite.enabled = false;
    //        yield return new WaitForSeconds(0.1f);
    //        sprite.enabled = true;

    //        yield return new WaitForSeconds(blinkTime);
    //    }
    //    sprite.enabled = false;
    //}

    //void OnEnable()
    //{
    //    isBlinking = false;
    //}
				
}
