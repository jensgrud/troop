﻿using UnityEngine;
using System.Collections;

public class PayCurrencyPopup : MonoBehaviour {

	public UILabel AmountLabel;
	public UISprite CurrencySprite;

	public GameObject Target;
	public string MethodToCall;

	public void Buy(){
		Close ();
		Target.SendMessage (MethodToCall);
	}

	public void Close(){
		GuiManager.Instance.HidePopup ();
	}
}
