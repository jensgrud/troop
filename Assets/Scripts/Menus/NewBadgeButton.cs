﻿using UnityEngine;
using System.Collections;

public class NewBadgeButton : MonoBehaviour {

	public Badge Badge;

	public UISprite Icon;

	public void OnClick(){
		GuiManager.Instance.ShowBadgePopup (Badge);
	}
}
