﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreditsScreen : MonoBehaviour {

	public float BlendTime = 2f;
	public float DisplayTime = 3f;

	public List<string> Roles;
	public List<string> Names;

	private int CurrentIndex = 0;

	public UILabel RoleLabel;
	public UILabel NameLabel;

	// Use this for initialization
	void OnEnable () {

		RoleLabel.color = new Color(0f,0f,0f,0f);
		NameLabel.color = new Color(0f,0f,0f,0f);

		StopAllCoroutines ();
		CurrentIndex = 0;
		StartCoroutine ("Credits");
	}
	
	// Update is called once per frame
	void OnDisable () {
		StopAllCoroutines ();
	}
	
	private IEnumerator Credits(){
	
		while (true) {

			RoleLabel.text = Roles[CurrentIndex];
			NameLabel.text = Names[CurrentIndex];

			yield return StartCoroutine("BlendIn");
			yield return StartCoroutine("Wait",DisplayTime);
			yield return StartCoroutine("BlendOut");

			CurrentIndex++;
			if (CurrentIndex >= Roles.Count)
				CurrentIndex = 0;
		}
	}

	private IEnumerator BlendIn(){

		float startTime = Time.realtimeSinceStartup;
		float duration = (BlendTime / 2);

		while (startTime + duration > Time.realtimeSinceStartup){

			float percent = (Time.realtimeSinceStartup - startTime)/duration;

			RoleLabel.color = new Color(0f,0f,0f,percent);
			NameLabel.color = new Color(0f,0f,0f,percent);

			yield return 0;
		}

		RoleLabel.color = new Color(0f,0f,0f,1f);
		NameLabel.color = new Color(0f,0f,0f,1f);

	}

	private IEnumerator BlendOut(){
		
		float startTime = Time.realtimeSinceStartup;
		float duration = (BlendTime / 2);
		
		while (startTime + duration > Time.realtimeSinceStartup){
			
			float percent = (Time.realtimeSinceStartup - startTime)/duration;
			
			RoleLabel.color = new Color(0f,0f,0f,1f-percent);
			NameLabel.color = new Color(0f,0f,0f,1f-percent);

			yield return 0;
		}
		
		RoleLabel.color = new Color(0f,0f,0f,0f);
		NameLabel.color = new Color(0f,0f,0f,0f);
		
	}

	private IEnumerator Wait(float duration){

		float startTime = Time.realtimeSinceStartup;

		while (startTime + duration > Time.realtimeSinceStartup){
			yield return 0;
		}

	}

	public void Back(){
		GuiManager.Instance.Back ();
	}
}
