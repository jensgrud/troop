﻿using UnityEngine;
using System.Collections;

public class LunchboxPopup : MonoBehaviour {

	public GameObject LunchboxOff;
	public GameObject LunchboxOn;
    public GameObject MaxRankNotification;

	public UISprite CurrencySprite;
	public UILabel AmountLabel;

	public UISprite Marshmallow;
	public UITexture Overlay;
	public UILabel BonusLabel;
	public UISprite AcceptButton;
	public UISprite OpenBox;
    public UISprite Shining;
    public GameObject Fireworks;

	void OnEnable(){

        if (RankManager.Instance.currentRank != RankManager.Instance.maxRank)
        {
            LunchboxOff.SetActive(true);
            LunchboxOn.SetActive(false);

            OpenBox.gameObject.SetActive(false);
            Marshmallow.gameObject.SetActive(false);

            Overlay.gameObject.SetActive(false);
            AmountLabel.gameObject.SetActive(false);
            BonusLabel.gameObject.SetActive(false);

            AcceptButton.gameObject.SetActive(false);
        }
        else
        {
            LunchboxOn.SetActive(false);
            LunchboxOff.SetActive(false);
            MaxRankNotification.SetActive(true);
        }
		

	}

	public void OpenLunchbox(){

		AudioManager.Instance.PlaySingleSound (gameObject, AudioManager.Instance.transform.FindChild ("RankUpAudioPlayer").gameObject, 1f);
		AudioManager.Instance.transform.FindChild ("Soundtrack").audio.volume = 0.25f;
        Fireworks.transform.GetComponent<ParticleUnscaled>().SimulatePart();
        
		AmountLabel.text = "x "+
			RankManager.Instance.registeredRanks[
			    RankManager.Instance.currentRank + 1
			].rewardMarshmallows;

		LunchboxOff.SetActive (false);
        LunchboxOn.SetActive(true);
		StartCoroutine ("MarshmallowLevitateCoroutine");

	}

	private IEnumerator MarshmallowLevitateCoroutine(){

		RankManager.Instance.RankUp ();

		StartCoroutine("BlendIn",Marshmallow.gameObject);
		yield return StartCoroutine("BlendIn",OpenBox.gameObject);

		StartCoroutine("BlendIn",AmountLabel.gameObject);
		StartCoroutine("BlendIn",BonusLabel.gameObject);
		yield return StartCoroutine("BlendIn",Overlay.gameObject);

		yield return StartCoroutine("BlendOut",OpenBox.gameObject);
		yield return StartCoroutine("BlendIn",AcceptButton.gameObject);
		
		yield return 0;
	}

	private IEnumerator BlendIn(GameObject obj){

		obj.SetActive (true);

		UIWidget widget = null;
		if (obj.GetComponent<UILabel>()!= null)
			widget = obj.GetComponent<UILabel>();
		else if (obj.GetComponent<UISprite>()!= null)
			widget = obj.GetComponent<UISprite>();
		else if (obj.GetComponent<UITexture>()!= null)
			widget = obj.GetComponent<UITexture>();

		if (widget != null) {

			obj.SetActive (true);
			widget.color = new Color (1f, 1f, 1f, 0f);

			float startTime = Time.realtimeSinceStartup;
			while (startTime + 1 > Time.realtimeSinceStartup) {
				
				float percent = Time.realtimeSinceStartup - startTime;
				widget.color = new Color (1f, 1f, 1f, percent);
				
				yield return 0;
			}

			widget.color = new Color (1f, 1f, 1f, 1f);
		}
	}

	private IEnumerator BlendOut(GameObject obj){
		
		UIWidget widget = null;
		if (obj.GetComponent<UILabel>()!= null)
			widget = obj.GetComponent<UILabel>();
		else if (obj.GetComponent<UISprite>()!= null)
			widget = obj.GetComponent<UISprite>();
		else if (obj.GetComponent<UITexture>()!= null)
			widget = obj.GetComponent<UITexture>();

		if (widget != null) {
		
			obj.SetActive (true);
			widget.color = new Color (1f, 1f, 1f, 1f);
			
			float startTime = Time.realtimeSinceStartup;
			while (startTime + 1 > Time.realtimeSinceStartup) {
				
				float percent = Time.realtimeSinceStartup - startTime;
				widget.color = new Color (1f, 1f, 1f, 1-percent);
				
				yield return 0;
			}
			
			widget.color = new Color (1f, 1f, 1f, 1f);
		}
		obj.SetActive(false);
	}

	public void Accept(){
		AudioManager.Instance.transform.FindChild ("Soundtrack").audio.volume = 0.9f;
		GuiManager.Instance.CurrentMenuReference.GetComponent<StartMenu> ().UpdateRanksAndBadges ();
		GuiManager.Instance.HidePopup ();
        Fireworks.transform.GetComponent<ParticleUnscaled>().simulate = false;
        Fireworks.transform.GetComponent<ParticleUnscaled>().particle.Simulate(0f, true, true);
	}

    public void ResetRanks()
    {
        RankManager.Instance.currentRank = 0;
        StatisticsManager.Instance.ResetStatistics();
        BadgeManager.Instance.ResetBadges();
        GuiManager.Instance.CurrentMenuReference.GetComponent<StartMenu>().UpdateRanksAndBadges();
        MaxRankNotification.SetActive(false);
        GuiManager.Instance.HidePopup();
    }
    public void DontResetRanks()
    {
        MaxRankNotification.SetActive(false);
        GuiManager.Instance.HidePopup();
    }

}
