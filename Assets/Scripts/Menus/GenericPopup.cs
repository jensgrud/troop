﻿using UnityEngine;
using System.Collections.Generic;

public class GenericPopup: MonoBehaviour {

	public GameObject Target;
	public string Message;

	public GameObject YesNoButtons;
	public GameObject ApplyButton;

	public LocalizeWidget Text;

	public List<UIPlaySound> YesButtons;
	public List<AudioClip> YesButtonSounds;

	void OnEnable(){
		if (GuiManager.Instance.CurrentPopup == GuiManager.PopupType.Question) {

			YesNoButtons.SetActive(true);
			ApplyButton.SetActive(false);

		} else {

			YesNoButtons.SetActive(false);
			ApplyButton.SetActive(true);

		}
	}

	public void Yes(){
		Target.SendMessage (Message);
		GuiManager.Instance.HidePopup ();
	}

	public void No(){
		GuiManager.Instance.HidePopup ();
	}
}
