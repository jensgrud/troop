﻿using UnityEngine;
using System.Collections.Generic;
using SmartLocalization;


public class StartMenu : MonoBehaviour {
	
	#region fields
	
	public GameObject Advanced;
	
	public UILabel RankLabel;
	public UISprite RankButton;
	public UISprite ClaimRewardButton;
	
	public UISprite[] BadgeSprites;
	public UISprite[] BadgeOverlays;
	public UILabel[] BadgeLabels;
	public UIButton[] SkipBadgeButtons;
	
	#endregion
	
	#region initialisation
	
	void OnEnable() {
		
		if (RankManager.Instance.currentRank == RankManager.tutorialRank) {
			
			Advanced.SetActive(false);
			
		} else {
			
			Advanced.SetActive(true);
			
			Invoke("UpdateRanksAndBadges",0f);
		}
		
		GuiManager.Instance.ShowWallet ();
	}

	void OnDisable(){
		GuiManager.Instance.HideWallet ();
	}
	
	public void UpdateRanksAndBadges()
	{
		
		RankLabel.text = "Rank: " + RankManager.Instance.currentRank.ToString();
		
		int i = 0;
		int badgesEarned = 0;
		List<int> requiredBadges = RankManager.Instance.registeredRanks[RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].badgesToCompleteIds;
		
		foreach (int ID in requiredBadges)
		{
			foreach (Badge badge in BadgeManager.Instance.allBadges)
			{
				if (badge.badgeId == ID)
				{
					BadgeLabels[i].text = badge.description;
					string loadedLang = LanguageManager.Instance.LoadedLanguage;
					BadgeLabels[i].text = LanguageManager.Instance.GetTextValue("Badge Description " + badge.badgeId.ToString());
					
					if (BadgeManager.Instance.IsBadgeEarned(badge))
					{
						BadgeSprites[i].gameObject.SetActive(true);
						BadgeSprites[i].spriteName = badge.spriteName;
						BadgeSprites[i].color = Color.white;
						BadgeOverlays[i].color = Color.white;
						BadgeOverlays[i].spriteName = BadgeManager.Instance.GetOverlay(badge.badgeLevel);
						badgesEarned++;
						SkipBadgeButtons[i].gameObject.SetActive(false);
					}
					else
					{
						BadgeSprites[i].gameObject.SetActive(false);
						BadgeOverlays[i].spriteName = "Badge@Bronze";
						BadgeOverlays[i].color = new Color(124f/255f,82f/255f,0f/255f,100f/255f);
						SkipBadgeButtons[i].gameObject.SetActive(
							PlayerWallet.Instance.Marshmallows >= badge.priceMarshmallows);
					}
				}
			}
			i++;
		}
		
		if (badgesEarned == 5)
		{
			RankButton.gameObject.SetActive(false);
			ClaimRewardButton.gameObject.SetActive(true);

			if (RankManager.Instance.currentRank == 7)
				ClaimRewardButton.transform.FindChild("Label - ClaimReward").GetComponent<LocalizeWidget>().key = "Finish Game Label";
			else
				ClaimRewardButton.transform.FindChild("Label - ClaimReward").GetComponent<LocalizeWidget>().key = "Claim Reward Label";

			ClaimRewardButton.transform.FindChild("Label - ClaimReward").GetComponent<LocalizeWidget>().Reload();
		}
		else
		{
			RankButton.gameObject.SetActive(true);
			ClaimRewardButton.gameObject.SetActive(false);
			
		}
	}
	
	#endregion
	
	#region buttons
	
	public void OpenBadgesMenu(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Badges);
	}
	
	public void OpenShop(){
		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Shop);
	}
	
	public void LoginToFacebook(){
		FacebookManager.Instance.Login ();
	}
	
	public void StartTutorialGame(){
        //Reset single run statistics
        StatisticsManager.Instance.ResetSingleRunStatistics();

		GuiManager.Instance.InitGenericPopup (
			"Start Tutorial Again?",
			this.gameObject,
			"StartedTutorialGame",
			0
			);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Question);
		
	}
	
	/// <summary>
	/// Start tutorial by resetting rank, starting game and resetting terrain
	/// </summary>
	
	public void StartedTutorialGame(){
		
		// Save the current rank for letting player replay tutorial
		RankManager.Instance.SaveRank();
		StartGame();
		ProceduralManager.Instance.Restart();
		GameManager.Instance.DisableTroop();
	}
	
	public void StartGame(){
		//Reset single run statistics
        StatisticsManager.Instance.ResetSingleRunStatistics();

		GuiManager.Instance.ShowMenu (GuiManager.MenuType.Game);

		if (RankManager.Instance.currentRank != RankManager.tutorialRank)
			AudioManager.Instance.PlaySingleSound(gameObject,AudioManager.Instance.transform.FindChild("IntroFanfare").gameObject, 1);
		else
			GameManager.Instance.DisableTroop();
		
		//If the Scout leader is in the game this is needed:
		
		//AudioManager.Instance.transform.FindChild ("Soundtrack").gameObject.audio.volume = 0.3f;
		//AudioManager.Instance.CrossFade (AudioManager.Instance.transform.FindChild ("Soundtrack").gameObject, 0.1f, 0.9f);
		
		GameManager.Instance.numOfDeaths = 0;
		GameManager.Instance.StartGame();
		
		#if UNITY_ANDROID
		GA.API.Design.NewEvent ("Game:Start");
		GameAnalyticsHelper.GameStartTime = Time.realtimeSinceStartup;
		#endif
	}
	
	public void ClaimReward(){
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.LuchBox);
	}
	
	public void SkipBadge(int badgePosition){
		GuiManager.Instance.ShowBuyPopup (CurrencyType.Marshmallows, GetBadge(badgePosition).priceMarshmallows, this.gameObject, "BoughtBadge"+badgePosition);
	}
	
	private Badge GetBadge(int badgePosition){
		return BadgeManager.Instance.GetBadge (
			RankManager.Instance.registeredRanks [RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].badgesToCompleteIds [badgePosition-1]
			);
	}
	
	public void BuyBadge1(){BuyBadge (1);}
	public void BuyBadge2(){BuyBadge (2);}
	public void BuyBadge3(){BuyBadge (3);}
	public void BuyBadge4(){BuyBadge (4);}
	public void BuyBadge5(){BuyBadge (5);}
	
	public void BuyBadge(int badgePosition){
		GuiManager.Instance.ShowBuyPopup (
			CurrencyType.Logs,
			Shop.Instance.BadgePricesLogs[GetBadge (badgePosition).badgeId],
			this.gameObject,
			"BoughtBadge"+badgePosition
			);
	}
	
	public void BoughtBadge1(){BoughtBadge (1);}
	public void BoughtBadge2(){BoughtBadge (2);}
	public void BoughtBadge3(){BoughtBadge (3);}
	public void BoughtBadge4(){BoughtBadge (4);}
	public void BoughtBadge5(){BoughtBadge (5);}
	
	public void BoughtBadge(int badgePosition){
		
		if (GuiManager.Instance.CheckIfPlayerHas(
			CurrencyType.Logs,
			Shop.Instance.BadgePricesLogs[GetBadge (badgePosition).badgeId] )) { 
			
			Shop.Instance.BuyBadge (GetBadge (badgePosition).badgeId, false);
			UpdateRanksAndBadges ();
			
		} else {
			GuiManager.Instance.ShowPopup(GuiManager.PopupType.GoToShop);
		}
	}
	
	#endregion
	
	public void Quit(){
		
		GuiManager.Instance.InitGenericPopup ("Quit? Label", this.gameObject, "Quitted",0);
		GuiManager.Instance.ShowPopup (GuiManager.PopupType.Question);
	}
	
	public void Quitted(){
		
		Application.Quit ();
		#if UNITY_EDITOR
		//Debug.Log("Quit the game");
		#endif
	}
}
