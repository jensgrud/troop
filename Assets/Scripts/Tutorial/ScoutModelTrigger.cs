﻿using UnityEngine;
using System.Collections;

public class ScoutModelTrigger : MonoBehaviour {


    void OnTriggerEnter(Collider c)
    {    
        if (c.GetComponent<Scout>() == null)
            return;

        string scoutName = transform.GetChild(0).tag;
        if (c.transform.GetChild(0).tag == scoutName)
        {
            if (c.transform.GetChild(0).tag == "Sling" && GameManager.Instance.troop.GetScoutByOrder(0) is ScoutGreen)
                GameManager.Instance.troop.LeftSwitch();

            foreach (Transform child in c.transform)
                child.gameObject.SetActive(true);

            AudioSource[] audio = c.GetComponents<AudioSource>();
            foreach (AudioSource a in audio)
                a.enabled = true;

            GameObject.Destroy(this.gameObject);
            c.GetComponent<Scout>().SetEnabled(true);
            GameManager.Instance.troop.IncreaseEnabledScouts();

            while (c.gameObject.GetComponent<Scout>().order != 0)
                GameManager.Instance.troop.LeftSwitch();
        }
    }
}
