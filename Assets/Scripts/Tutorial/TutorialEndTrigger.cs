﻿using UnityEngine;
using System.Collections;

public class TutorialEndTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider c)
    {
		if (c.transform.tag == "Scout" && RankManager.Instance.currentRank == RankManager.tutorialRank)
        {
            TouchManager.Instance.JumpActive(true);
            TouchManager.Instance.DuckActive(true);
            TouchManager.Instance.SwitchActive(true);

			if (RankManager.Instance.savedRank > 0)
			{
				// We are replaying tutorial level so reset rank
				RankManager.Instance.ResetRank();

			} else 
			{
            	RankManager.Instance.RankUp();
			}
        }
    }
}
