﻿using UnityEngine;
using System.Collections.Generic;

public class TutorialTrigger : MonoBehaviour {

    public Tutorial.TutorialClass TutorialClass = Tutorial.TutorialClass.Undefined;
    public bool HasSignGraphic = false;
    public bool HasArrowGraphic = false;
    public bool HasDisplayTimer = false;
    public float DisplayTime = 0.0f;
    public bool RequiresAction = false;
    public List<Tutorial.Action> Actions = new List<Tutorial.Action>();
    public bool PauseGame = false;

    [HideInInspector]
    public bool HasDisplaytimeExpired = false;
}
