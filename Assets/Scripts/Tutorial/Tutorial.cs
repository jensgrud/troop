﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour {

    public delegate void TutorialEventHandler(object sender, TutorialDefinition def, TutorialTrigger trigger);
    public event TutorialEventHandler OnTutorialComplete;

    public enum TutorialClass {Undefined, Jump, Duck, AxeChallgende, RopeChallenge, SlingChallenge, FoxPowerup, EaglePowerup, TurtlePowerup, CampSite, Collectable, Pet, Death};
    public enum Action { SwipeUp, SwipeDown, SwipeLeft, SwipeRight, Tap}

    //Singleton
    public static Tutorial Instance { get; protected set; }

    GameObject soundGameObject;
    AudioSource audioSource;
	public float TutorialSoundLevel;

    public List<TutorialDefinition> TutorialDefinitions = new List<TutorialDefinition>() 
    {
        new TutorialDefinition(TutorialClass.Jump),
        new TutorialDefinition(TutorialClass.Duck),
        new TutorialDefinition(TutorialClass.AxeChallgende),
        new TutorialDefinition(TutorialClass.RopeChallenge),
        new TutorialDefinition(TutorialClass.SlingChallenge),
        new TutorialDefinition(TutorialClass.FoxPowerup),
        new TutorialDefinition(TutorialClass.EaglePowerup),
        new TutorialDefinition(TutorialClass.TurtlePowerup),
        new TutorialDefinition(TutorialClass.CampSite),
        new TutorialDefinition(TutorialClass.Collectable),
        new TutorialDefinition(TutorialClass.Pet),
        new TutorialDefinition(TutorialClass.Death)
    };

    /*
    public TutorialDefinition JumpTutorial = new TutorialDefinition(TutorialClass.Jump);
    public TutorialDefinition DuckTutorial = new TutorialDefinition(TutorialClass.Duck);
    public TutorialDefinition AxeChallengeTutorial = new TutorialDefinition(TutorialClass.AxeChallgende);
    public TutorialDefinition RopeChallengeTutorial = new TutorialDefinition(TutorialClass.RopeChallenge);
    public TutorialDefinition SlingChallengeTutorial = new TutorialDefinition(TutorialClass.SlingChallenge);
    public TutorialDefinition FoxPowerupTutorial = new TutorialDefinition(TutorialClass.FoxPowerup);
    public TutorialDefinition EaglePowerupTutorial = new TutorialDefinition(TutorialClass.EaglePowerup);
    public TutorialDefinition TurtlePowerupTutorial = new TutorialDefinition(TutorialClass.TurtlePowerup);
    public TutorialDefinition CampSiteTutorial = new TutorialDefinition(TutorialClass.CampSite);
    public TutorialDefinition CollectableTutorial = new TutorialDefinition(TutorialClass.Collectable);
    public TutorialDefinition PetTutorial = new TutorialDefinition(TutorialClass.Pet);
     */

    //For placing box collider
    Scout leadingScout;
    public Vector3 tutorialColliderPos = new Vector3(3f, 0, 0);

    //GUI
    public GameObject panel;
    UITexture arrowGraphic, signGraphic;

    //Timer/threading stuff
    System.Threading.Timer timer;
    bool endTutorialByThread = false;
    public List<Func<bool>> threadFuncList = new List<Func<bool>>();
    
    //References for conditionals
    public GameObject shopScreen;

    //For serializing only once stuff
    Dictionary<string, bool> hasRunOnceContainer = new Dictionary<string, bool>();

    void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (PersistentData.SavedDataExists(this))
        {
            hasRunOnceContainer = PersistentData.Deserialize<Dictionary<string, bool>>(this);

            foreach (KeyValuePair<string, bool> pair in hasRunOnceContainer)
                foreach (TutorialDefinition def in TutorialDefinitions)
                    if (pair.Key == def.tutorialClass.ToString())
                        def.HasRunOnce = pair.Value;
        }
    }

	// Use this for initialization
	void Start () {

        soundGameObject = AudioManager.Instance.transform.FindChild("TutorialAudioPlayer").gameObject;
        audioSource = AudioManager.Instance.transform.FindChild("Soundtrack").audio;

        NGUITools.SetActive(panel, true);

        UITexture[] texs = panel.GetComponentsInChildren<UITexture>();

        foreach (UITexture t in texs)
            if (t.name == "ArrowGraphic")
                arrowGraphic = t;
            else if (t.name == "SignGraphic")
                signGraphic = t;

        //Disable gui initially
        NGUITools.SetActive(panel, false);

        //TODO: Possibly remove, this is a bad way of handling serialization
        GameManager.Instance.troop.OnDie += delegate { Serialize(); };
        BadgeManager.Instance.OnBadgeEarned += delegate { Serialize(); };
        GuiManager.Instance.CampMenu.OnExitCamp += delegate { Serialize(); };
        Tutorial.Instance.OnTutorialComplete += delegate { Serialize(); };
        Shop.Instance.OnPurches += delegate { Serialize(); };
	}
	
	// Update is called once per frame
	void Update () 
    {
        leadingScout = GameManager.Instance.troop.GetScoutByOrder(0);
        transform.position = leadingScout.transform.position + tutorialColliderPos;

        if (endTutorialByThread)
        {
            for (int i = 0; i < threadFuncList.Count; i++)
                if (threadFuncList[i] != null && threadFuncList[i]())
                    threadFuncList.RemoveAt(i);

            if (threadFuncList.Count == 0)
                endTutorialByThread = false;
        }
	}

    void OnTriggerEnter(Collider c)
    {
        TutorialTrigger trigger = c.GetComponent<TutorialTrigger>();

        if (trigger == null)
            return;

        foreach(TutorialDefinition def in TutorialDefinitions)
            if (def.tutorialClass == trigger.TutorialClass)
            {
                StartTutorial(def, trigger);
                break;
            }

        /*
        if (trigger.TutorialClass == TutorialClass.Jump)
            StartTutorial(JumpTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.Duck)
            StartTutorial(DuckTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.AxeChallgende)
            StartTutorial(AxeChallengeTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.RopeChallenge)
            StartTutorial(RopeChallengeTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.SlingChallenge)
            StartTutorial(SlingChallengeTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.FoxPowerup)
            StartTutorial(FoxPowerupTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.EaglePowerup)
            StartTutorial(EaglePowerupTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.TurtlePowerup)
            StartTutorial(TurtlePowerupTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.CampSite)
            StartTutorial(CampSiteTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.Collectable)
            StartTutorial(CollectableTutorial, trigger);
        else if (trigger.TutorialClass == TutorialClass.Pet)
            StartTutorial(PetTutorial, trigger);
        else
            Debug.Log("No valid TriggerClass");
         * */
    }

    void StartTutorial(TutorialDefinition def, TutorialTrigger trigger)
    {
        //Escape if at wrong rank
        if (def.ActiveAtRank != RankManager.Instance.currentRank)
            return;

        //Escape if already run once
        if (def.OnlyRunOnce && def.HasRunOnce)
            return;

        if (trigger.TutorialClass == TutorialClass.Jump)
            JumpTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.Duck)
            DuckTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.AxeChallgende)
            AxeChallengeTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.RopeChallenge)
            RopeChallengeTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.SlingChallenge)
            SlingChallengeTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.FoxPowerup)
            FoxPowerupTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.EaglePowerup)
            EaglePowerupTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.TurtlePowerup)
            TurtlePowerupTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.CampSite)
            CampSiteTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.Collectable)
            CollectableTutorialOnStart();
        else if (trigger.TutorialClass == TutorialClass.Pet)
            PetTutorialOnStart();
		
        TouchManager.SwipeEventHandler handler = (sender, self, arg) =>
        {
            EndTutorial(def, trigger, self, arg);
        };

        //Register action events
        if (trigger.RequiresAction)
            foreach (Action a in trigger.Actions)
                if (a == Action.SwipeUp)
                    TouchManager.Instance.OnSwipeUp += handler;
                else if (a == Action.SwipeDown)
                    TouchManager.Instance.OnSwipeDown += handler;
                else if (a == Action.SwipeLeft)
                    TouchManager.Instance.OnSwipeLeft += handler;
                else if (a == Action.SwipeRight)
                    TouchManager.Instance.OnSwipeRight += handler;
                else if (a == Action.Tap)
                    TouchManager.Instance.OnTap += handler;

        //Display timer
        if (trigger.HasDisplayTimer)
        {
            timer = new System.Threading.Timer(obj =>
                {
                    endTutorialByThread = true;
                    threadFuncList.Add(delegate()
                    {
                        return EndTutorial(def, trigger, handler);
                    });
                },
                null,
                Convert.ToInt32(trigger.DisplayTime * 1000 + 0.5f),
                System.Threading.Timeout.Infinite);
        }

        //Sign graphic
        if (trigger.HasSignGraphic)
        {
            signGraphic.mainTexture = def.SignGraphic;
            signGraphic.transform.localPosition = def.SignGraphicPosition;
            signGraphic.transform.localRotation = def.SignGraphicRotation;
            signGraphic.width = def.SignGraphicWidth;
            signGraphic.height = def.SignGraphicHeight;
            signGraphic.enabled = true;
        }

        //Arrow graphic
        if (trigger.HasArrowGraphic)
        {
            arrowGraphic.mainTexture = def.ArrowGraphic;
            arrowGraphic.transform.localPosition = def.ArrowGraphicPosition;
            arrowGraphic.transform.localRotation = def.ArrowGraphicRotation;
            arrowGraphic.width = def.ArrowGraphicWidth;
            arrowGraphic.height = def.ArrowGraphicHeight;
            arrowGraphic.enabled = true;
        }

        //Pause game
        if (trigger.PauseGame)
		{
            GameManager.Instance.StopSpeed();
			//Play Audio
			AudioManager.Instance.PlaySingleSound(gameObject, soundGameObject, 1);
			audioSource.volume = TutorialSoundLevel;
		}

        //Show tutorial
        NGUITools.SetActive(panel, true);
    }

    bool EndTutorial(TutorialDefinition def, TutorialTrigger trigger, TouchManager.SwipeEventHandler handler, object arg = null)
    {
        bool tutorialComplete = false;

        //Check if tutorial is actually completed
        if (trigger.TutorialClass == TutorialClass.Jump)
            tutorialComplete = JumpTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.Duck)
            tutorialComplete = DuckTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.AxeChallgende)
            tutorialComplete = AxeChallengeTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.RopeChallenge)
            tutorialComplete = RopeChallengeTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.SlingChallenge)
            tutorialComplete = SlingChallengeTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.FoxPowerup)
            tutorialComplete = FoxPowerupTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.EaglePowerup)
            tutorialComplete = EaglePowerupTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.TurtlePowerup)
            tutorialComplete = TurtlePowerupTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.CampSite)
            tutorialComplete = CampSiteTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.Collectable)
            tutorialComplete = CollectableTutorialCompleted(arg);
        else if (trigger.TutorialClass == TutorialClass.Pet)
            tutorialComplete = PetTutorialCompleted(arg);

        if (tutorialComplete)
        {
            if (trigger.TutorialClass == TutorialClass.Jump)
                JumpTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.Duck)
                DuckTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.AxeChallgende)
                AxeChallengeTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.RopeChallenge)
                RopeChallengeTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.SlingChallenge)
                SlingChallengeTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.FoxPowerup)
                FoxPowerupTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.EaglePowerup)
                EaglePowerupTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.TurtlePowerup)
                TurtlePowerupTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.CampSite)
                CampSiteTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.Collectable)
                CollectableTutorialOnEnd(arg);
            else if (trigger.TutorialClass == TutorialClass.Pet)
                PetTutorialOnEnd(arg);

            //Reset audio
            audioSource.volume = 0.9f;

            //Reset trigger action events
            if (trigger.RequiresAction)
                foreach (Action a in trigger.Actions)
                {
                    if (a == Action.SwipeUp)
                        TouchManager.Instance.OnSwipeUp -= handler;
                    else if (a == Action.SwipeDown)
                        TouchManager.Instance.OnSwipeDown -= handler;
                    else if (a == Action.SwipeLeft)
                        TouchManager.Instance.OnSwipeLeft -= handler;
                    else if (a == Action.SwipeRight)
                        TouchManager.Instance.OnSwipeRight -= handler;
                    else if (a == Action.Tap)
                        TouchManager.Instance.OnTap -= handler;
                }

            //Stop timer
            if (trigger.HasDisplayTimer)
                timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);

            if (def.OnlyRunOnce)
                def.HasRunOnce = true;

            //Hide tutorial
            signGraphic.enabled = false;
            arrowGraphic.enabled = false;
            NGUITools.SetActive(panel, false);

            //Unpause game
            if (trigger.PauseGame)
                GameManager.Instance.ResetSpeed();

            //Fire completion event
            if (OnTutorialComplete != null)
                OnTutorialComplete(this, def, trigger);

            return true;
        }
        else
            return false;
    }

    void Serialize()
    {
        hasRunOnceContainer.Clear();
        foreach (TutorialDefinition def in TutorialDefinitions)
            if (def.OnlyRunOnce)
                hasRunOnceContainer.Add(def.tutorialClass.ToString(), def.HasRunOnce);

        PersistentData.Serialize(hasRunOnceContainer, this);
    }

    #region Completed checks
    bool JumpTutorialCompleted(object arg)
    {
        //Completed as soon as the user gives the correct input
        return true;
    }

    bool DuckTutorialCompleted(object arg)
    {
        //Completed as soon as the user gives the correct input
        return true;
    }

    bool AxeChallengeTutorialCompleted(object arg)
    {
        if (GameManager.Instance.troop.GetScoutByOrder(0).transform.GetChild(0).tag != "Axe")
            return false;

        return true;
    }

    bool RopeChallengeTutorialCompleted(object arg)
    {
        if (GameManager.Instance.troop.GetScoutByOrder(0).transform.GetChild(0).tag != "Rope")
            return false;

        return true;
    }

    bool SlingChallengeTutorialCompleted(object arg)
    {
        if (GameManager.Instance.troop.GetScoutByOrder(0).transform.GetChild(0).tag != "Sling")
            return false;

        return true;
    }

    bool FoxPowerupTutorialCompleted(object arg)
    {
        return true;
    }

    bool EaglePowerupTutorialCompleted(object arg)
    {
        return true;
    }

    bool TurtlePowerupTutorialCompleted(object arg)
    {
        return true;
    }

    bool CampSiteTutorialCompleted(object arg)
    {
		//TODO: return NGUITools.GetActive(shopScreen);
		return true;
    }

    bool CollectableTutorialCompleted(object arg)
    {
        try
        {
            RaycastHit hit = (RaycastHit)arg;
            return arg != null && hit.collider != null && hit.collider.GetComponentInParent<BonusItem>() != null;
        }
        catch (NullReferenceException e)
        {
            return true;
        }
    }

    bool PetTutorialCompleted(object arg)
    {
        try
        {
            RaycastHit hit = (RaycastHit)arg;
            return arg != null && hit.collider != null && hit.collider.GetComponentInParent<PetUnlocker>() != null;
        }
        catch (NullReferenceException e)
        {
            return true;
        }
    }
    #endregion

    #region specific starts
    void JumpTutorialOnStart()
    {
        TouchManager.Instance.JumpActive(true);
    }

    void DuckTutorialOnStart()
    {
        TouchManager.Instance.DuckActive(true);
    }

    void AxeChallengeTutorialOnStart()
    {

    }

    void RopeChallengeTutorialOnStart()
    {

    }

    void SlingChallengeTutorialOnStart()
    {
        //Escape if the wrong scout is at the front
        if (GameManager.Instance.troop.GetScoutByOrder(0).transform.GetChild(0).tag != "Sling")
            return;
        TouchManager.Instance.SwitchActive(false);
//        Debug.Log("Sling Challenge Tutorial ended...");
		//audioMan.transform.FindChild ("Soundtrack").audio.volume = 0.9f;

    }

    void FoxPowerupTutorialOnStart()
    {

    }

    void EaglePowerupTutorialOnStart()
    {

    }

    void TurtlePowerupTutorialOnStart()
    {

    }

    void CampSiteTutorialOnStart()
    {

    }

    void CollectableTutorialOnStart()
    {

    }

    void PetTutorialOnStart()
    {

    }
    #endregion

    #region specific ends
    void JumpTutorialOnEnd(object arg)
    {

    }

    void DuckTutorialOnEnd(object arg)
    {

    }

    void AxeChallengeTutorialOnEnd(object arg)
    {

    }

    void RopeChallengeTutorialOnEnd(object arg)
    {
        TouchManager.Instance.SwitchActive(true);
    }

    void SlingChallengeTutorialOnEnd(object arg)
    {
        if (GameManager.Instance.troop.GetScoutByOrder(0).transform.GetChild(0).tag != "Sling")
            return;

        TouchManager.Instance.SwitchActive(true);
    }

    void FoxPowerupTutorialOnEnd(object arg)
    {

    }

    void EaglePowerupTutorialOnEnd(object arg)
    {

    }

    void TurtlePowerupTutorialOnEnd(object arg)
    {

    }

    void CampSiteTutorialOnEnd(object arg)
    {

    }

    void CollectableTutorialOnEnd(object arg)
    {

    }

    void PetTutorialOnEnd(object arg)
    {
        try
        {
            RaycastHit hit = (RaycastHit)arg;

            if (arg != null && hit.collider != null)
            {
                PetUnlocker unlocker = hit.collider.GetComponentInParent<PetUnlocker>();
                if (unlocker != null)
                {
                    GameManager.Instance.troop.currentPowerUp = PowerUpsManager.Instance.PowerUps[unlocker.PetToUnlock.Id];
                    PowerUpsManager.Instance.UnlockPowerUp(unlocker.PetToUnlock.Id);
                }
            }
        }
        catch (NullReferenceException e)
        {
            return;
        }
    }
    #endregion
}

[Serializable]
public class TutorialDefinition
{
	public string Name = "Tutorial Name";
    public Tutorial.TutorialClass tutorialClass;
    public Texture SignGraphic;
    public Vector3 SignGraphicPosition;
    public Quaternion SignGraphicRotation;
    public int SignGraphicWidth;
    public int SignGraphicHeight;
    public Texture ArrowGraphic;
    public Vector3 ArrowGraphicPosition;
    public Quaternion ArrowGraphicRotation;
    public int ArrowGraphicWidth;
    public int ArrowGraphicHeight;
    public int ActiveAtRank = 0;
    public bool OnlyRunOnce;    

    [HideInInspector]
    public bool HasRunOnce;

    public TutorialDefinition() : this(Tutorial.TutorialClass.Undefined) { }

    public TutorialDefinition(Tutorial.TutorialClass tutorialClass)
    {
        this.tutorialClass = tutorialClass;
    }
}
