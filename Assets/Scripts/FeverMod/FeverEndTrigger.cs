﻿using UnityEngine;
using System.Collections;

public class FeverEndTrigger : Recyclable
{

    private bool isTriggered = false;
	// Use this for initialization


    void OnTriggerEnter(Collider c)
    {
        if (!isTriggered)
        {
//            ProceduralManager.Instance.ExitFeverMode();
            isTriggered = true;
        }
    }

    void OnEnable()
    {
        isTriggered = false;
    }
}
