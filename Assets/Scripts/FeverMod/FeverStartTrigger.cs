﻿using UnityEngine;
using System.Collections;

public class FeverStartTrigger : Recyclable
{

    private bool isTriggered = false;
    // Use this for initialization


    void OnTriggerEnter(Collider c)
    {
        if (!isTriggered)
        {
//            ProceduralManager.Instance.EnterFeverMode();
            isTriggered = true;
        }
    }

    void OnEnable()
    {
        isTriggered = false;
    }
}
