﻿using UnityEngine;
using System.Collections;

public class FeverItem : Recyclable, ICollectable, ITapable
{
    public string audioPlayerName;
    public bool disposableSound;
    public Statistic statistic;
    private GameObject audioPlayer;
    private GameObject ParticleObject;
    public float rotationSpeed = 100f;


    void Start()
    {
        ParticleObject = transform.GetChild(0).gameObject;
    }

    public void tap()
    {
        GameObject Emitter = Instantiate(ParticleObject) as GameObject;
        Emitter.transform.position = ParticleObject.transform.position;
        Emitter.GetComponent<ParticleSystem>().Play();
        StatisticsManager.Instance.IterateStatistic(statistic);
//        ProceduralManager.Instance.UpdateFeverCounter();
        //AudioManager.Instance.PlayRandomSound(gameObject, AudioManager.Instance.spotAudioPlayer, AudioManager.Instance.spotClipList);
        if (!disposableSound)
        {
            audioPlayer = AudioManager.Instance.transform.FindChild(audioPlayerName).gameObject;
            audioPlayer.audio.clip = audio.clip;
            AudioManager.Instance.PlaySingleSound(gameObject, audioPlayer, AudioManager.Instance.spotPitchDefault);
        }
        else
        {
            AudioManager.Instance.PlayDisposableSound(gameObject);
        }
        MeshRenderer[] meshes = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mesh in meshes)
        {
            mesh.enabled = false;
        }
    }
   
    public void Update()
    {
		transform.Rotate(Vector3.up * Time.deltaTime*rotationSpeed);
		transform.position = new Vector3(transform.position.x, 0.2f*Mathf.Sin(Time.time*4), transform.position.z);
        Recycle();
    }
    
	public void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Scout")
			tap();
	}

    void OnEnable()
    {
        MeshRenderer[] meshes = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mesh in meshes)
        {
            mesh.enabled = true;
        }
    }
}