﻿using UnityEngine;
using System.Collections;

public interface ITapable {
    void tap();
}
