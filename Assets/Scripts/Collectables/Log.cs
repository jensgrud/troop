﻿using UnityEngine;
using System.Collections;

public class Log : Recyclable, ICollectable {

    AudioManager audioMan;
    private bool isCollided = false;
    private Vector3 startingPosition;
    private GameObject SmallStars;
    private GameObject BigStar;
    private MeshRenderer[] meshes;
	// Use this for initialization
    //private Vector3 mainPosition;
    //private Quaternion mainRotation;
    //private float floatingOffset;
    //private float floatingDuration;
    //private float startingTime;
    //private float startingPoint;
    //private float endingPoint;
    //private float time = 0f;
    void Awake()
    {
        distanceFromCamera = defaultDistance + (BlocksLength - 1) * defaultBlockSize;
        startingPosition = this.gameObject.transform.localPosition;

        children = gameObject.GetComponentsInChildren<Recyclable>(true);
        meshes = gameObject.GetComponentsInChildren<MeshRenderer>();

        if (transform.childCount > 3)
        {
            SmallStars = transform.FindChild("SmallStarsParticles").gameObject;
            BigStar = transform.FindChild("BigStarParticles").gameObject;
        }
    }

   void Start () {
       //SmallStars = GameManager.Instance.SmallStars;
       //BigStar = GameManager.Instance.BigStar;
   //    startingPosition = this.gameObject.transform.localPosition;
       
      //    distanceFromCamera += 4.5f;
    //    audioMan = AudioManager.Instance;
    //    //mainPosition = transform.position + positionOffset;
    //    //mainRotation = transform.rotation;
    //    //startingTime = Time.time;
    //    //floatingDuration = 1f;
    //    //floatingOffset = 0.5f;
    //    //startingPoint = mainPosition.y;
    //    //endingPoint = mainPosition.y + floatingOffset;
    }

	
    //// Update is called once per frame
    void Update()
    {

        if (Mathf.Abs(transform.position.x - Camera.main.transform.position.x) < distanceFromCamera)
        {
            //Vector3 newPos = mainPosition;
            //time += Time.deltaTime;
            //float percentage = (time) / floatingDuration;

            //newPos.y = Mathf.Lerp(startingPoint, endingPoint, percentage);
            ////Debug.Log("new y:" + newPos.y);
            //if (percentage > 1)
            //{
            //    float temp = startingPoint;
            //    startingPoint = endingPoint;
            //    endingPoint = temp;

            //    startingTime = Time.time;
            //    time = 0;
            //}


            //transform.position = newPos;
            transform.Rotate(0, 40 * Time.deltaTime, 0);
        }



        bool cameraTest = (transform.position.x + distanceFromCamera < mainCam.transform.position.x);
        if (cameraTest)
            StartCoroutine(Recycle());

    }

    public void OnTriggerEnter(Collider c)
    {
        bool pickupLog = false;
        audioMan = AudioManager.Instance;
        //c.gameObject.transform.parent.audio.clip = gameObject.transform.audio.clip; //Transfer audio from the log to the troop
        //c.gameObject.transform.parent.audio.pitch = (float)Random.Range(pitchMin, pitchMax); ; //Add a random pitch to the sound
        //c.gameObject.transform.parent.audio.Play(); //play audio source from troop

        //Log is picked up when it collides with a Scout or an active Foxnado 
        if (!isCollided && (c.transform.tag == "Scout" || (c.transform.tag == "Foxnado" && (GameManager.Instance.troop.GetActivePowerUp() != null && GameManager.Instance.troop.GetActivePowerUp().Id == 0))))
        {
            audioMan.PlayConsecutiveSound(gameObject, audioMan.logAudioPlayer, audioMan.logPitchIncrement, audioMan.logPitchMax, audioMan.logPitchDefault);
            StatisticsManager.Instance.IterateStatistic(Statistic.CollectedFirewood);
            PlayerWallet.Instance.LogsInThisGame++;
            isCollided = true;
            
            foreach (MeshRenderer mesh in meshes)
            {
                mesh.enabled = false;
            }
            if (SmallStars != null && BigStar != null)
            {
                BigStar.GetComponent<ParticleSystem>().Emit(1);
                SmallStars.GetComponent<ParticleSystem>().Emit(3);
            }
            //LogSplashObject.GetComponent<SpriteRenderer>().enabled = true;
            //Invoke("DisableSprite", 0.3f);
        }



        
    }

    void DisableSprite()
    {
        
    }

    void OnEnable()
    {
        isCollided = false;
        gameObject.transform.localPosition = startingPosition;
       
        foreach (MeshRenderer mesh in meshes)
        {
            mesh.enabled = true;
        }
    }
    
}
