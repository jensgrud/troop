﻿using UnityEngine;
using System.Collections;

public interface ICollectable {

    void OnTriggerEnter(Collider c);
}
