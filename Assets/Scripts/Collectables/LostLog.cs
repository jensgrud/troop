﻿using UnityEngine;
using System.Collections;

public class LostLog : MonoBehaviour {

    public float lifetime = 2;
    public int xForceRange = 10;
    public int yForceRange = 5;
    public int zForceRange = 5;

	// Use this for initialization
    //void Start () {
    //    float randx = Random.Range(-xForceRange ,0);
    //    float randy = Random.Range(200, yForceRange);
    //    transform.rigidbody.AddForce(new Vector3(randx, randy, 0));
    //}
	
	// Update is called once per frame
	void Update () {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
           // gameObject.rigidbody.Sleep();
            gameObject.rigidbody.velocity = Vector3.zero;
            gameObject.rigidbody.angularVelocity = Vector3.zero;
            ProceduralDatabaseManager.Instance.Recycle(gameObject);
           // gameObject.rigidbody.Sleep();
            //Destroy(gameObject);
        }
	}

    void OnEnable()
    {
       // transform.rigidbody.WakeUp();
        lifetime = 2;
        float randx = Random.Range(-xForceRange, xForceRange);
        float randy = Random.Range(400, yForceRange);
        float randz = Random.Range(-zForceRange, -700);
        transform.rigidbody.AddForce(new Vector3(randx, randy, randz));
        transform.rigidbody.AddTorque(0, 0, randz);
    }
}
