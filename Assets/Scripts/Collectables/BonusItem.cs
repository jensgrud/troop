﻿using UnityEngine;
using System.Collections;

public class BonusItem : Recyclable, ITapable, ICollectable
{
    public string audioPlayerName;
    public bool disposableSound;
    public Statistic statistic;
    private GameObject audioPlayer;
    private GameObject ParticleObject1;
    private GameObject ParticleObject2;
    private ParticleSystem Emmitter1;
    private ParticleSystem Emmitter2;
    private bool isTapped = false;
	// Use this for initialization
	void Start () {
        //Vector3 tempPosition = gameObject.transform.position;
        //tempPosition.y = (float)Random.Range(7F, 40F);  
        //gameObject.transform.position = tempPosition;
        ParticleObject1 = transform.FindChild("FlowerParticles1").gameObject;
        if (transform.transform.FindChild("FlowerParticles2") != null)
        {
            ParticleObject2 = transform.FindChild("FlowerParticles2").gameObject;
        }
	}
	
	// Update is called once per frame
    //void Update()
    //{
    //    if (!GameManager.Instance.enableBonus)
    //    {
    //    }
    //}

    public void tap()
    {
        if (!isTapped)
        {
            isTapped = true;
            Emmitter1 = ParticleObject1.GetComponent<ParticleSystem>();
            Emmitter1.Play();
            if (ParticleObject2 != null)
            {
                Emmitter2 = ParticleObject2.GetComponent<ParticleSystem>();
                Emmitter2.Play();
            }
            StatisticsManager.Instance.IterateStatistic(statistic);
            //AudioManager.Instance.PlayRandomSound(gameObject, AudioManager.Instance.spotAudioPlayer, AudioManager.Instance.spotClipList);
            if (!disposableSound)
            {
                audioPlayer = AudioManager.Instance.transform.FindChild(audioPlayerName).gameObject;
                audioPlayer.audio.clip = audio.clip;
                AudioManager.Instance.PlaySingleSound(gameObject, audioPlayer, AudioManager.Instance.spotPitchDefault);
            }
            else
            {
                AudioManager.Instance.PlayDisposableSound(gameObject);
            }
            MeshRenderer[] meshes = gameObject.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mesh in meshes)
            {
                mesh.enabled = false;
            }
            if (!Emmitter1.IsAlive())
                {
				Object.Destroy (gameObject);	
                }

            //Disable tutorial trigger
            TutorialTrigger trigger = transform.GetComponentInChildren<TutorialTrigger>();
            if (trigger != null)
                trigger.gameObject.SetActive(false);
        }

     //   Invoke("destroyObject", 0.2f);
        

    }

    //public void destroyObject()
    //{
    //    Destroy(gameObject);
    //}
    public void OnTriggerEnter(Collider c)
    { 
    
    }

    void OnEnable()
    {
        isTapped = false;
        MeshRenderer[] meshes = gameObject.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mesh in meshes)
        {
            mesh.enabled = true;
        }
    }
}
