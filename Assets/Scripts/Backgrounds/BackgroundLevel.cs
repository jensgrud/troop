﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Helper = BackgroundHelper;

public class BackgroundLevel : MonoBehaviour {
    //public Texture tex;
    public float speed;

    float stepSize;
    float start = 0.0f;

    Mesh mesh;
    Vector3[] vertices;
    int[] triangles;

    Vector2[] uvs;
    Vector3 ver1;
    Vector3 ver2;
    Vector3 ver3;
    Vector2 uv1;
    Vector2 uv2;
    Vector2 uv3;

    // Use this for initialization
    void Start()
    {
        //GetComponent<Renderer>().material.mainTexture = tex;
        stepSize = 1f;

        mesh = Helper.UnshareMeshVertices(GetComponent<MeshFilter>().mesh);
        GetComponent<MeshFilter>().mesh = mesh;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        vertices = mesh.vertices;
        triangles = mesh.triangles;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateUVWithTriangles();
    }

    void UpdateUVWithTriangles()
    {
        uvs = new Vector2[vertices.Length];

        for (int i = 0; i < triangles.Length; i += 3)
        {
            ver1 = vertices[triangles[i]];
            ver2 = vertices[triangles[i + 1]];
            ver3 = vertices[triangles[i + 2]];

            uv1 = new Vector2(
                Helper.Plane2UV(ver1.x + start * speed),
                Helper.Plane2UV(ver1.z));
            uv2 = new Vector2(
                Helper.Plane2UV(ver2.x + start * speed),
                Helper.Plane2UV(ver2.z));
            uv3 = new Vector2(
                Helper.Plane2UV(ver3.x + start * speed),
                Helper.Plane2UV(ver3.z));

            Helper.UVNormalizer(uv1);
            Helper.UVNormalizer(uv2);
            Helper.UVNormalizer(uv3);

            uvs[triangles[i]] = uv1;
            uvs[triangles[i + 1]] = uv2;
            uvs[triangles[i + 2]] = uv3;
        }

        start += stepSize;

        mesh.uv = uvs;
    }
}
