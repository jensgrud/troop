﻿using UnityEngine;


public class BackgroundHelper {
    //From http://answers.unity3d.com/questions/702893/how-do-i-avoid-shared-vertices.html
    public static Mesh UnshareMeshVertices(Mesh sourceMesh)
    {
        Vector3[] sourceVertices = sourceMesh.vertices;

        int[] sourceIndices = sourceMesh.GetTriangles(0);
        Vector3[] sourceVerts = sourceMesh.vertices;
        Vector2[] sourceUVs = sourceMesh.uv;

        int[] newIndices = new int[sourceIndices.Length];
        Vector3[] newVertices = new Vector3[sourceIndices.Length];
        Vector2[] newUVs = new Vector2[sourceIndices.Length];

        // Create a unique vertex for every index in the original Mesh:
        for (int i = 0; i < sourceIndices.Length; i++)
        {
            newIndices[i] = i;
            newVertices[i] = sourceVertices[sourceIndices[i]];
            newUVs[i] = sourceUVs[sourceIndices[i]];
        }

        Mesh unsharedVertexMesh = new Mesh();
        unsharedVertexMesh.vertices = newVertices;
        unsharedVertexMesh.uv = newUVs;

        unsharedVertexMesh.SetTriangles(newIndices, 0);

        return unsharedVertexMesh;
    }

    public static float Plane2UV(float num)
    {
        return (num + 5) / 10;
    }

    public static Vector2 UVNormalizer(Vector2 uv)
    {
        if (uv.x > 1.0f)
            uv.x -= 1.0f;

        return uv;
    }

}
