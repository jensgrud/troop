﻿using UnityEngine;
using System.Collections;

public class MeshLevel : MonoBehaviour {

    public float speed;
    public GameObject prefab;

    GameObject meshOne, meshTwo;
    Vector3 movementVec;

	// Use this for initialization
	void Start () {
        meshOne = (GameObject)Instantiate(prefab, new Vector3 (0,0,0), transform.parent.transform.rotation);
        meshTwo = (GameObject)Instantiate(prefab, new Vector3(29.85f, 0, 0), transform.parent.transform.rotation);
        //29.85f
        //Debugging colors
        //meshOne.renderer.material.color = Color.red;
        //meshTwo.renderer.material.color = Color.blue;

        meshOne.transform.parent = transform;
        meshTwo.transform.parent = transform;

        movementVec = new Vector3(speed, 0, 0);
        meshOne.transform.localPosition = new Vector3(meshOne.transform.localPosition.x - movementVec.x, 0, 0);
        meshTwo.transform.localPosition = new Vector3(meshTwo.transform.localPosition.x - movementVec.x, 0, 0);
	}
	
	// Update is called once per frame
    void Update()
    {
		if (GameManager.Instance.GetTimeScale() > 0f && GameManager.Instance.GetSpeed() > 0f)
        {
            meshOne.transform.localPosition = new Vector3(meshOne.transform.localPosition.x - movementVec.x, 0, 0);
            meshTwo.transform.localPosition = new Vector3(meshTwo.transform.localPosition.x - movementVec.x, 0, 0);
        }


        if (meshOne.transform.localPosition.x < -10)
        {
            meshOne.transform.localPosition += new Vector3(19.85f, 0, 0);
        }
        if (meshTwo.transform.localPosition.x < -10)
        {
            meshTwo.transform.localPosition += new Vector3(19.85f, 0, 0);
        }
    }
}
