﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurtleSounds : MonoBehaviour {
	
	public List<AudioClip> turtleSounds = new List<AudioClip>();
	
	
	public void TurtleSound()
		
	{
		
		int proper = Random.Range (1, 15);
		
		if(proper == 1)
		{
			AudioManager.Instance.PlayRandomSound(gameObject, AudioManager.Instance.transform.FindChild("TurtleSound").gameObject, turtleSounds);
		}
	}
}
