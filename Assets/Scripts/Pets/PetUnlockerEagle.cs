﻿using UnityEngine;
using System.Collections;

public class PetUnlockerEagle : PetUnlocker, ITapable {

    /// <summary>
    /// The cage that pet i trapped in
    /// </summary>
    public GameObject rope;

    /// <summary>
    /// Boolean used to control that actions that happens when the pet i unlocked
    /// </summary>
    private bool unlocked = false;

    public void tap()
    {
        PetsManager.Instance.UnlockPet(PetToUnlock);

        //Set bool unlocked to true
        unlocked = true;

        //starts fadeout of the cage
        StartCoroutine(Fade());

        //play release sound
        AudioManager.Instance.PlaySingleSound(rope, AudioManager.Instance.transform.FindChild("ReleaseEagleAudioPlayer").gameObject, AudioManager.Instance.transform.FindChild("ReleaseEagleAudioPlayer").audio.pitch);

        //Tell the statistic manager
        StatisticsManager.Instance.IterateStatistic(statistic);

        //Disable tutorial trigger
        TutorialTrigger trigger = transform.GetComponentInChildren<TutorialTrigger>();
        if (trigger != null)
            trigger.gameObject.SetActive(false);

        EnablePet();
        DisablePetUnlocker();
    }
	

    private IEnumerator Fade()
    {
        float time = 1;
        float targetAlpha = 0;
        float percentage = 0.0f;
        float currentAlpha = rope.transform.renderer.material.color.a;
        Color currentColor = rope.transform.renderer.material.color;
        while (percentage <= 1)
        {
            currentColor.a = Mathf.Lerp(currentAlpha, targetAlpha, percentage);
            rope.transform.renderer.material.color = currentColor;
            percentage += Time.deltaTime / time;
            yield return 0;
        }
        currentColor.a = 0;
        rope.transform.renderer.material.color = currentColor;
    }
}
