﻿using UnityEngine;
using System.Collections;

class PetUnlockerTurtle : PetUnlocker, ITapable
{
    private bool isTapped = false;
    public void tap()
    {
        if (isTapped == false)
        {
            PetsManager.Instance.UnlockPet(PetToUnlock);
            AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.transform.FindChild("ReleaseTurtleAudioPlayer").gameObject, AudioManager.Instance.transform.FindChild("ReleaseTurtleAudioPlayer").audio.pitch);

            transform.FindChild("turtle").GetComponent<Animator>().SetTrigger("isHelped");
            Invoke("EnablePet", 1f);
            Invoke("DisablePetUnlocker", 1f);
                
            //Tell the statistic manager
            StatisticsManager.Instance.IterateStatistic(statistic);

            //Disable tutorial trigger
            TutorialTrigger trigger = transform.GetComponentInChildren<TutorialTrigger>();
            if (trigger != null)
                trigger.gameObject.SetActive(false);

            isTapped = true;
        }
    }
}