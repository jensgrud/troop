﻿using UnityEngine;
using System.Collections;

public class CampsitePetUnlocker : ProceduralBlock {

    //void Start()
    //{
    //    base.distanceFromCamera = defaultDistance+ 5f;
    //}
    void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Pet>() != null)
        {
            if (!c.GetComponent<Pet>().isPermanent)
                c.GetComponent<Pet>().Destroy();
        }
    }

    void OnEnable()
    {
        Animator[] anims = gameObject.GetComponentsInChildren<Animator>();
      //  Debug.Log("anim size " + anims.Length);
        foreach (Animator anim in anims)
        {
//            Debug.Log(anim.name);
            anim.SetTrigger("Reset");
        }
        
    }
}
