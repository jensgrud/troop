﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FoxBarksSound : MonoBehaviour {

	public List<AudioClip> foxBarks = new List<AudioClip>();


	public void FoxBarkSound()

	{
	
		int proper = Random.Range (1, 15);

		if(proper == 1)
		{
			AudioManager.Instance.PlayRandomSound(gameObject, AudioManager.Instance.transform.FindChild("FoxBark").gameObject, foxBarks);
		}
	}
}
