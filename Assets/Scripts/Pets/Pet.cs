﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pet : MonoBehaviour {

	#region fields

	/// <summary>
	/// E.g. LittleFox, HugeBadBear, Rasmus
	/// </summary>
	public string Name;

	/// <summary>
	/// E.g. Strengthens the raw power of your ear muscles.
	/// </summary>
	public string Description;

	/// <summary>
	/// Price in marshmallows for this pet in the shop
	/// </summary>
	public int PriceMarshmallows;

    /// <summary>
    /// Price in firewood for this pet in the shop
    /// </summary>
    public int PriceFirewood;

    /// <summary>
    /// The id of the pet
    /// </summary>
    public int Id;

    /// <summary>
    /// The statistic that is iterated when the pet is bought
    /// </summary>
    public Statistic buyStatistic;

    /// <summary>
    /// Name of the hat object associated with the pet
    /// </summary>
    public string hatName;

    /// <summary>
    /// if true, the player has bought the pet, if false the pet 
    /// will leave at the first campsite
    /// </summary>
    public bool isPermanent = false;

    /// <summary>
    /// The pets position offset from the last scout
    /// </summary>
    public Vector3 PetFromLastScoutsOffset;

    /// <summary>
    /// The featherStormParticleSystems
    /// </summary>
    private List<ParticleSystem> featherStormParticles = new List<ParticleSystem>();

    // variables used to follow the scouts
    private PositionArray scoutYPositions;
    private float scoutLastUpdateYPos = 0;

    private bool disappeared = false;

	#endregion

	#region Unity native events 
    void Awake()
    {

        scoutYPositions = new PositionArray(30);
        Scout midScout = GameManager.Instance.troop.GetScoutByOrder (1);
        Vector3 offset = PetFromLastScoutsOffset;

        transform.position = midScout.transform.position + offset;

		if (transform.name == "Fox" || transform.name == "Fox(Clone)") {
			transform.FindChild("fox").GetComponent<Animator>().SetTrigger("foxStartRun");
		}

        
        //transform.FindChild("SmokeAppear@ParticleSystem").transform.GetComponent<ParticleSystem>().Emit(800);
    }

    void Start()
    {
        foreach (ParticleSystem ps in gameObject.GetComponentsInChildren<ParticleSystem>())
            featherStormParticles.Add(ps);

        iTween.ScaleFrom(gameObject, iTween.Hash("time", 2f, "scale", Vector3.zero));
    }

    void FixedUpdate()
    {
        Scout midScout = GameManager.Instance.troop.GetScoutByOrder(1);

        scoutYPositions.Add(midScout.transform.position.x, midScout.transform.position.y);        
        
        //transform.position = lastScout.transform.position + PetsManager.Instance.PetFromLastScoutsOffset;
        float yPos = scoutYPositions.GetValue(transform.position.x);
        if (yPos > -1000)
        {
            yPos += PetFromLastScoutsOffset.y;
        }
        else
        {
            yPos = midScout.transform.position.y + PetFromLastScoutsOffset.y;
        }

        transform.position = new Vector3(midScout.transform.position.x + PetFromLastScoutsOffset.x, yPos, midScout.transform.position.z + PetFromLastScoutsOffset.z);
    }
	
	#endregion

	#region Pet methods

    /// <summary>
    /// the pet leaves the troop at the first campsite
    /// </summary>
    public void LeaveTroop()
    {
        if (!isPermanent)
        {
            if (!disappeared)
            {
                iTween.ScaleTo(gameObject, iTween.Hash("time", 2f, "scale", Vector3.zero));
                Invoke("PlayDestroyParticles", 0.3f);
                Invoke("Destroy", 3f);
                disappeared = true;
            }
        }
    }
    public void PlayDestroyParticles()
    {
        transform.FindChild("SmokeDisappear@ParticleSystem").transform.GetComponent<ParticleSystem>().Emit(40);
    }
    
	/// <summary>
	/// Called when the pet disappears.
	/// </summary>
	public void Destroy(){
		//TODO nice animation and all, delay
		GameObject.Destroy (this.gameObject);
        //ProceduralDatabaseManager.Instance.Recycle(this.gameObject);
	}

    public void FeatherStormStart()
    {
        foreach (ParticleSystem ps in featherStormParticles)
            ps.Play();
    }

    public void FeatherStormStop()
    {
        foreach (ParticleSystem ps in featherStormParticles)
            ps.Stop(); 
    }

	#endregion

}
