using UnityEngine;
using System.Collections.Generic;
using Polenter.Serialization;

public class PetsManager : MonoBehaviour {

    public bool debugMode;
	/// <summary>
	/// List of all the possible pets prefabs. 
	/// </summary>
	public List<Pet> Pets;
	
	/// <summary>
	/// list specifying which pet is currently unlocked.
	/// </summary>
	public List<bool> UnlockedPets;

    /// <summary>
    /// List of the pets that have been bought in the shop.
    /// </summary>
    public List<bool> BoughtPets;

    public List<GameObject> hatHolders;

    private PetManagerWrapper petManagerWrapper = new PetManagerWrapper();


	#region singleton

	public static PetsManager Instance {
		get;
		private set;
	}

	#endregion

	#region Unity native events

	void Awake(){
		Instance = this;
        if (debugMode)
        {
			UnlockedPets = new List<bool>() { false, false, false };
            BoughtPets = new List<bool>() { false, false, false };
        }
        else
        {

            if (PersistentData.SavedDataExists(this))
            {
                petManagerWrapper = PersistentData.Deserialize<PetManagerWrapper>(this);
            }

            UnlockedPets = petManagerWrapper.UnlockedPets;
            BoughtPets = petManagerWrapper.BoughtPets;
        }
	}

    void Start()
    {
        Shop.Instance.AddPetsToPrices();

        //TODO: Possibly remove, this is a bad way of handling serialization
        GameManager.Instance.troop.OnDie += delegate { Serialize(); };
        BadgeManager.Instance.OnBadgeEarned += delegate { Serialize(); };
        GuiManager.Instance.CampMenu.OnExitCamp += delegate { Serialize(); };
        Tutorial.Instance.OnTutorialComplete += delegate { Serialize(); };
        Shop.Instance.OnPurches += delegate { Serialize(); };
    }

    private void Serialize()
    {
        petManagerWrapper.BoughtPets = BoughtPets;
        petManagerWrapper.UnlockedPets = UnlockedPets;

        PersistentData.Serialize(petManagerWrapper, this);
    }
	#endregion

	#region pets manager

	public bool isPetUnlocked(int petId){

        return UnlockedPets [petId];
	}
	public bool isPetUnlocked(Pet p){
		return isPetUnlocked(Pets.IndexOf(p));
	}


    public bool isPetBought(int petId)
    {
        return BoughtPets[petId];
    }
    public bool isPetBought(Pet p)
    {
        return isPetBought(Pets.IndexOf(p));
    }

    //For when the pet is bought in the shop
	public void BuyPet(int petId){
		BoughtPets [petId] = true;
        BuyPetStatistic(petId);
	}
	public void BuyPet(Pet p){
		BuyPet (Pets.IndexOf(p));
	}

    //For when the pet is tapped to be rescued
    public void UnlockPet(int petId)
    {
		if (!UnlockedPets [petId]) {
			StatisticsManager.Instance.IterateStatistic(Statistic.RescuedAnimals);
			UnlockedPets [petId] = true;
		}
    }
    public void UnlockPet(Pet p)
    {
        UnlockPet(Pets.IndexOf(p));
    }

    //For unlocking the statistic ofr buying a pet
    private void BuyPetStatistic(int petId)
    {
        BuyPetStatistic(Pets[petId]);
    }
    private void BuyPetStatistic(Pet p)
    {
        StatisticsManager.Instance.IterateStatistic(p.buyStatistic);
        StatisticsManager.Instance.IterateStatistic(Statistic.BoughtAnimals);
    }

    public void ChangeHats(Pet p)
    {
        for (int j = 0; j < 3; j++)
        {
            Transform tempTrans = hatHolders[j].transform;
            for (int i = 0; i < tempTrans.childCount; i++)
            {
                if (tempTrans.GetChild(i).name == p.hatName)
                {
                    tempTrans.GetChild(i).transform.GetComponent<SkinnedMeshRenderer>().enabled = true;
                }
                else
                {
                    tempTrans.GetChild(i).transform.GetComponent<SkinnedMeshRenderer>().enabled = false;
                }
            }
        }
    }

    private void ResetHats()
    {
       // GameManager.Instance.troop.scouts[]
    }
	#endregion
}

[System.Serializable]
public class PetManagerWrapper
{
    public List<bool> BoughtPets
    {
        get ;
        set ;
    }

    public List<bool> UnlockedPets
    {
        get ;
        set ;
    }

    public PetManagerWrapper()
    {
        UnlockedPets = new List<bool>() { false, false, false};
        BoughtPets = new List<bool>() { false, false, false};
    }
}
