﻿using UnityEngine;
using System.Collections;

public class PetUnlocker : Recyclable, ITapable
{
	/// <summary>
	/// Prefab of the pet to be unlocked
	/// </summary>
	public Pet PetToUnlock;

    /// <summary>
    /// The statistic that is iterated when the pet is unlocked
    /// </summary>
    public Statistic statistic;

    public void tap()
    { 
    
    }

    /// <summary>
    /// Removes all the same petunlockers from the scene
    /// </summary>
    protected void DisablePetUnlocker()
    {
        GameObject[] pets = GameObject.FindGameObjectsWithTag("PetUnlocker");
        foreach (GameObject pet in pets)
        {
            if (pet.transform.name == this.transform.name)
            {
                pet.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Enables the pet that was unlocked as mascot
    /// </summary>
    protected void EnablePet()
    {
        //If there is no other pet this will be the pet of the troop
        if (GameManager.Instance.troop.pet == null)
        {
            Pet newPet = Instantiate(PetToUnlock) as Pet;
            newPet.isPermanent = false;
            GameManager.Instance.troop.pet = newPet;
        }
    }
}
