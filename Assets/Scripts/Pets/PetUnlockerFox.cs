﻿using UnityEngine;
using System.Collections;

public class PetUnlockerFox : PetUnlocker, ITapable
{
    /// <summary>
    /// The cage that pet i trapped in
    /// </summary>
    public GameObject cage;

    /// <summary>
    /// Boolean used to control that actions that happens when the pet i unlocked
    /// </summary>
    private bool unlocked = false;

    private Vector3 startPos;

	public void tap(){
		PetsManager.Instance.UnlockPet (PetToUnlock);	
	
        //Set bool unlocked to true
        unlocked = true;
        //play release sound
        AudioManager.Instance.PlaySingleSound(cage, AudioManager.Instance.transform.FindChild("ReleaseFoxAudioPlayer").gameObject,  AudioManager.Instance.transform.FindChild("ReleaseFoxAudioPlayer").audio.pitch);

        //Tell the statistic manager
        StatisticsManager.Instance.IterateStatistic(statistic);

        //Disable tutorial trigger
        TutorialTrigger trigger = transform.GetComponentInChildren<TutorialTrigger>();
        if (trigger != null)
            trigger.gameObject.SetActive(false);
	}

    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        if (unlocked)
        {
            cage.transform.Translate(Vector3.up * Time.deltaTime * 2);
        }
        if (cage.transform.position.y > startPos.y + 2)
        {
            EnablePet();
            DisablePetUnlocker();
        }
    }
}
