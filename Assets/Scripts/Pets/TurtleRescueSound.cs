﻿using UnityEngine;
using System.Collections;

public class TurtleRescueSound : MonoBehaviour {
	
	AudioManager audioMan;
	
	void Start () {
		audioMan = AudioManager.Instance;
	}
	
	public void HappyTurtleSound()
	{
		audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("HappyTurtleAudioPlayer").gameObject, 1);
	}
}
