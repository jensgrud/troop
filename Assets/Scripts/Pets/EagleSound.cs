﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EagleSound : MonoBehaviour {
	
	public List<AudioClip> eagleSounds = new List<AudioClip>();
	
	public void eagleSound()
		
	{

		int proper = Random.Range (1, 16);

		if(proper == 1)
		{
			AudioManager.Instance.PlayRandomSound(gameObject, AudioManager.Instance.transform.FindChild("EagleCallSound").gameObject, eagleSounds);
		}
	}

	public void FlapSound()
	{
		AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.transform.FindChild("EagleFlapSound").gameObject, Random.Range(0.8f, 1.2f));
	}
}
