﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rank
{
    public string title;
    public int rankId; //Maybe not needed?
    public int rewardFirewook;
    public int rewardMarshmallows;
    public float speedModifier;
    public float scoreMultiplier;
    public List<int> badgesToCompleteIds;
}
