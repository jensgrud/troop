﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RankManager : MonoBehaviour
{
	public static int tutorialRank = -1;

    #region fields
    public List<Rank> registeredRanks = new List<Rank>();
	public int currentRank = tutorialRank;
    public int maxRank = 2;
    public bool debugMode = false;
	
    [HideInInspector]
	public int savedRank = tutorialRank;
    #endregion

    #region unity ediotor serializable
    //Makes input available through the unity editor
    [System.Serializable]
    public class EditorRanks
    {
        [SerializeField]
        public string title = "Rank 1: Scout";
        public int rankId = 1;
        public int rewardFirewood = 0;
        public int rewardMarshmallows = 250;
        public int scoreMultiplier;
        public List<int> badgesToCompleteIds = new List<int>();
    }
    public List<EditorRanks> editorRanks;
    #endregion

    #region unity native methods
    public static RankManager Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        Instance = this;
        ConvertEditorRankToRank();
        //Loads the current rank
        if (!debugMode && PersistentData.SavedDataExists(this))
        {
            currentRank = PersistentData.Deserialize<int>(this);
        }
    }

    void OnDestroy()
    {
        //Saves the current rank
		if (currentRank != RankManager.tutorialRank)
        	PersistentData.Serialize(currentRank, this);
    }

    void Start()
    {
        //TODO: Possibly remove, this is a bad way of handling serialization
        GameManager.Instance.troop.OnDie += delegate { PersistentData.Serialize(currentRank, this); };
        BadgeManager.Instance.OnBadgeEarned += delegate { PersistentData.Serialize(currentRank, this); };
		GuiManager.Instance.CampMenu.OnExitCamp += delegate { PersistentData.Serialize(currentRank, this); };
        Tutorial.Instance.OnTutorialComplete += delegate { PersistentData.Serialize(currentRank, this); };
        Shop.Instance.OnPurches += delegate { PersistentData.Serialize(currentRank, this); };
    }
    #endregion

    #region class Methods
    
    
    /// <summary>
    /// Returns true if all badges needed to complete a rank is earned
    /// </summary>
    /// <returns></returns>
    public bool CheckIfRankCompleted() 
    {
        bool result = false;
		foreach (int requiredBadge in registeredRanks[currentRank < 0 ? 0 : RankManager.Instance.currentRank].badgesToCompleteIds) //zero based
        {
            foreach (Badge earnedBadge in BadgeManager.Instance.earnedBadges)
            {
                if (earnedBadge.badgeId == requiredBadge)
                {
                    result = true;
                    break;
                }
                else
                {
                    result = false;
                }
            }
            if (!result)
            {
                break;
            }
        }
        return result;
    }

    /// <summary>
    /// Adds one to he current rank unless it is already maz rank
    /// </summary>
    public void RankUp()
    {
        if (currentRank < maxRank) 
        {
            //TODO: Show the user the reward he earns
			if (currentRank != RankManager.tutorialRank) 
			{
                StatisticsManager.Instance.ResetStatisticsMinusFirewoodAndDistance();
				PlayerWallet.Instance.Marshmallows += registeredRanks[currentRank < 0 ? 0 : RankManager.Instance.currentRank].rewardMarshmallows;
				PlayerWallet.Instance.PersistentLogs += registeredRanks[currentRank < 0 ? 0 : RankManager.Instance.currentRank].rewardFirewook;
			}

            currentRank++;

            PersistentData.Serialize(currentRank, this);
        }
    }

    /// <summary>
    /// Converts the unity EditorRank input actual Rank objects
    /// </summary>
    private void ConvertEditorRankToRank()
    {
        foreach (EditorRanks rank in editorRanks)
        {
            Rank newrank = new Rank();
            newrank.title = rank.title;
            newrank.rankId = rank.rankId;
            newrank.badgesToCompleteIds = rank.badgesToCompleteIds;
            newrank.rewardMarshmallows = rank.rewardMarshmallows;
            newrank.rewardFirewook = rank.rewardFirewood;
            registeredRanks.Add(newrank);
        }
    }

    /// <summary>
    /// Calculates the extra logs earned by the scoreMultiplier on the rank
    /// </summary>
    /// <param name="score"></param>
    /// <returns></returns>
    public int BonusScore(int score)
    {
		return (int) Mathf.Round(score * registeredRanks[currentRank < 0 ? 0 : RankManager.Instance.currentRank].scoreMultiplier);
    }

	public void SaveRank()
	{
		// Save the current rank for letting player replay tutorial
		RankManager.Instance.savedRank = RankManager.Instance.currentRank;
		RankManager.Instance.currentRank = RankManager.tutorialRank;

        //Make campsite, collectable, and pet tutorials appear again
        foreach (TutorialDefinition def in Tutorial.Instance.TutorialDefinitions)
            if (def.tutorialClass == Tutorial.TutorialClass.CampSite && def.OnlyRunOnce)
                def.HasRunOnce = false;
            else if (def.tutorialClass == Tutorial.TutorialClass.Pet && def.OnlyRunOnce)
                def.OnlyRunOnce = false;
            else if (def.tutorialClass == Tutorial.TutorialClass.Collectable && def.OnlyRunOnce)
                def.OnlyRunOnce = false;
	}

	public void ResetRank()
	{
		RankManager.Instance.currentRank = RankManager.Instance.savedRank;
	}
    #endregion
}
