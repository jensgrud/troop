﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class SwipingTest : MonoBehaviour {
	
	void Start () {
		Invoke ("SaveFirstScout", 1f);
		Invoke ("InputRight", 2f);
		Invoke ("CompareNotEqualFirstScout", 3f);
		Invoke ("InputLeft", 4f);
		Invoke ("CompareEqualFirstScout", 5f);
		Invoke ("Pass", 6f);
	}

	void InputRight(){
		GameObject.FindObjectOfType<TouchManager> ().SendMessage ("RightSwipe");
	}
	void InputLeft(){
		GameObject.FindObjectOfType<TouchManager> ().SendMessage ("LeftSwipe");
	}

	private Scout FirstScout;
	void SaveFirstScout(){
		FirstScout = GameObject.FindObjectOfType<Troop> ().scouts.OrderBy (x => x.transform.position.x).Last ();
	}

	void CompareNotEqualFirstScout(){

		if (FirstScout == GameObject.FindObjectOfType<Troop> ().scouts.OrderBy (x => x.transform.position.x).Last ())
			IntegrationTest.Fail(gameObject);
	}

	void CompareEqualFirstScout(){
		if (FirstScout != GameObject.FindObjectOfType<Troop> ().scouts.OrderBy (x => x.transform.position.x).Last ())
			IntegrationTest.Fail(gameObject);
	}

	void Pass(){
			IntegrationTest.Pass(gameObject);
	}
}
