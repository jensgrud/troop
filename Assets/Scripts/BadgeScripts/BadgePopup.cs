﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class BadgePopup : MonoBehaviour {

    public string spriteName;
	public int BadgeLevel = 0;

    public Vector3 targetPos;
    public float moveFraction = 0.5f;
    public float timeBeforeMoving = 3f;
    public float timeBeforeDestroy = 5f;

    #region unity native methods
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
			if (transform.GetChild(i).name == "Overlay")
			{
				transform.GetChild(i).transform.GetComponent<UISprite>().spriteName = BadgeManager.Instance.GetOverlay(BadgeLevel);
			}
            else if (transform.GetChild(i).name == "Icon")
            {
                transform.GetChild(i).transform.GetComponent<UISprite>().spriteName = spriteName;
            }
        }

        transform.FindChild("BadgeEarnedSprite").transform.GetComponent<UISprite>().spriteName = LanguageManager.Instance.GetTextValue("BadgeEarnedSprite");

		StartCoroutine("TimeScaleIndependentUpdate");
    }
	IEnumerator TimeScaleIndependentUpdate()
    {
		float timeOfMoving = Time.realtimeSinceStartup + timeBeforeMoving;
		float timeOfDestroy = Time.realtimeSinceStartup + timeBeforeDestroy;

		float prevTime = Time.realtimeSinceStartup;

		while (true) {

			float deltaTime = Time.realtimeSinceStartup - prevTime;

			if (Time.realtimeSinceStartup < timeOfMoving){
				prevTime = Time.realtimeSinceStartup;
				yield return 0;
				continue;
			}

			SlowMove (deltaTime);

			if (Time.realtimeSinceStartup < timeOfDestroy){
				prevTime = Time.realtimeSinceStartup;
				yield return 0;
				continue;
			}

			break;
		}

		DestroyImmediate(gameObject);
	}

    #endregion

    #region Class Methods

    /// <summary>
    /// Slowly moves the object towards a target (offscreen) while making it smaller
    /// </summary>
    void SlowMove(float deltaTime)
    {
        transform.position = Vector3.Lerp(transform.position, targetPos, moveFraction * deltaTime);
        Transform newT = transform;
		newT.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, moveFraction * deltaTime);
        transform.localScale = newT.localScale;
        transform.position = newT.position;
    }
    #endregion
}
