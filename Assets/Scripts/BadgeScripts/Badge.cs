﻿using UnityEngine;
using System.Collections.Generic;
public class Badge {
    public string title
    {
        get;
        set;
    }
    public int badgeId
    {
        get;
        set;
    }
    public Statistic statistic
    {
        get;
        set;
    }
    public int requiredStat
    {
        get;
        set;
    }
    public string description
    {
        get;
        set;
    }
    public string spriteName
    {
        get;
        set;
    }
    public int badgeLevel
    {
        get;
        set;
    }
    public int priceLogs
    {
        get;
        set;
    }
    public int priceMarshmallows
    {
        get;
        set;
    }
}
