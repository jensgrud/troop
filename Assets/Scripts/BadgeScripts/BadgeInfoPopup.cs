﻿using UnityEngine;
using System.Collections;

public class BadgeInfoPopup : MonoBehaviour {

	public UISprite Icon;
	public UISprite Overlay;

    public UILabel Title;
	public UILabel Description;

	void Update(){
		if (Input.GetMouseButtonDown(0))
			GuiManager.Instance.HidePopup();
	}

}
