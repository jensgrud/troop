﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SmartLocalization;

public class BadgeManager : MonoBehaviour {
	
    public List<Badge> registeredBadges = new List<Badge>();
    public List<Badge> earnedBadges = new List<Badge>();
    public GameObject badgePopup;
    public GameObject uiRoot;
    public GameObject emitter;
    public GameObject popupMoveTarget;
    public GameObject popupRank;
    public bool debugMode = false;
    public int currentDepth = 0;
	public int RewardInLogsForCollectingABadge = 5;
    public float timeBeforeBadgeDisplay = 3f;

    private Queue<Badge> badgeQueue = new Queue<Badge>();
    private float displayTimer;
	
    public delegate void BadgeEarnedHandler();
    public event BadgeEarnedHandler OnBadgeEarned;

	[HideInInspector]
	public List<Badge> allBadges = new List<Badge>();

    [System.Serializable]
    public class EditorBadge
    {
        [SerializeField]
        public string title = "Bronze: Get Wood";
        public int id = 0;
        public Statistic statistic = Statistic.CollectedFirewood;
        public int requiredStat = 10;
        public string description = "Collect 10 Firewood Logs";
        public string spriteName;
        public int badgeLevel;
        public int priceLogs;
        public int priceMarshmallows;
    }
    public List<EditorBadge> editorBadges;

    #region singleton
    public static BadgeManager Instance
    {
        get;
        private set;
    }
    #endregion

    #region unity native methods
    void Awake()
    {
        Instance = this;

        //Loads the earned badges
        if (!debugMode && PersistentData.SavedDataExists(this))
        {
            earnedBadges = PersistentData.Deserialize<List<Badge>>(this);
        }

        foreach (EditorBadge eBadge in editorBadges)
        {
            Badge newBadge = new Badge();
            newBadge.title = eBadge.title;
            newBadge.badgeId = eBadge.id;
            newBadge.statistic = eBadge.statistic;
            newBadge.requiredStat = eBadge.requiredStat;
            newBadge.description = eBadge.description;
            newBadge.spriteName = eBadge.spriteName;
            newBadge.badgeLevel = eBadge.badgeLevel;
            newBadge.priceLogs = eBadge.priceLogs;
            newBadge.priceMarshmallows = eBadge.priceMarshmallows;

            registeredBadges.Add(newBadge);
        }

		allBadges.AddRange (registeredBadges);

        //Removes the earned badges from the registered badges
        foreach (Badge eb in earnedBadges)
        {
            foreach (Badge rb in registeredBadges)
            {
                if (rb.badgeId == eb.badgeId)
                {
                    registeredBadges.Remove(rb);
                    break;
                }
            }
        }
    }

    void Start()
    {
        Shop.Instance.AddBadgesToPrices();
		
        //TODO: Possibly remove, this is a bad way of handling serialization
        GameManager.Instance.troop.OnDie += delegate { PersistentData.Serialize(earnedBadges, this); };
        BadgeManager.Instance.OnBadgeEarned += delegate { PersistentData.Serialize(earnedBadges, this); };
        GuiManager.Instance.CampMenu.OnExitCamp += delegate { PersistentData.Serialize(earnedBadges, this); };
        Tutorial.Instance.OnTutorialComplete += delegate { PersistentData.Serialize(earnedBadges, this); };
        Shop.Instance.OnPurches += delegate { PersistentData.Serialize(earnedBadges, this); };

        displayTimer = timeBeforeBadgeDisplay;

		NotifyStatisticChanged();
    }

    void Update()
    {
        displayTimer += Time.deltaTime;
        CheckQueue();
    }

    void OnDestroy()
    {
        //Saves the earnedBadges
        PersistentData.Serialize(earnedBadges, this);
    }
    #endregion

    #region Class Methods
    //Statman tells badgeman that a statstic has changed and goes through all not earned badges to see if they are completed and move completed badges to earned badges.
    public void NotifyStatisticChanged()
    {
        for (int i = registeredBadges.Count - 1; i >= 0; i--)
        {
            if (BadgeCondition(registeredBadges[i]) == true)
            {
                badgeQueue.Enqueue(registeredBadges[i]);
                //Put badge in earned badges and remove from registered badges so we don't iterate already earned badges
                earnedBadges.Add(registeredBadges[i]);
                registeredBadges.RemoveAt(i);

                if (OnBadgeEarned != null)
                    OnBadgeEarned();

				GameAnalyticsHelper.BadgesAmount++;

				PlayerWallet.Instance.LogsInThisGame += RewardInLogsForCollectingABadge;

                //Check if rank completed
                if (RankManager.Instance.CheckIfRankCompleted())
                {
                    Invoke("DisplayRankUp", 2.5f);
                    Invoke("RemoveRankUp", 4f);
                }
            }
        }
    }

    public bool BadgeCondition(Badge b)
    {
        
        if (b.requiredStat <= StatisticsManager.Instance.Statistics[b.statistic])
        {
			foreach (int badgeId in RankManager.Instance.registeredRanks[RankManager.Instance.currentRank < 0 ? 0 : RankManager.Instance.currentRank].badgesToCompleteIds)
            {
                if (badgeId == b.badgeId)
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Adds a badge to earnedBadges and remove it from registeredBadges
    /// </summary>
    /// <param name="badgeId"></param>
    public void BuyBadge(int badgeId)
    {
        foreach (Badge badge in registeredBadges)
        {
            if (badge.badgeId == badgeId)
            {
                earnedBadges.Add(badge);
                registeredBadges.Remove(badge);
                break;
            }
        }
    }

    public void DisplayBadge(Badge earnedBadge)
    {
        GameObject newPopup = (GameObject)GameObject.Instantiate(badgePopup);
        newPopup.transform.parent = uiRoot.transform;

        newPopup.transform.localPosition = Vector3.zero;
        newPopup.transform.localScale = Vector3.one;

        emitter.GetComponent<ParticleSystem>().Play();
		newPopup.GetComponent<BadgePopup>().spriteName = earnedBadge.spriteName;
		newPopup.GetComponent<BadgePopup>().BadgeLevel = earnedBadge.badgeLevel;
		newPopup.GetComponent<BadgePopup>().targetPos = popupMoveTarget.transform.position;
        if (earnedBadge.badgeLevel == 0)
        {
            AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.badgeSmallPlayer, 1f);
        }
        else if (earnedBadge.badgeLevel == 1)
        {
            AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.badgeMediumPlayer, 1f);
        }
        else if (earnedBadge.badgeLevel == 2)
        {
            AudioManager.Instance.PlaySingleSound(gameObject, AudioManager.Instance.badgeLargePlayer, 1f);
        }
        else
        {
//            Debug.Log("unassigned badge level sound!");
        }
    }

    public void CheckQueue()
    {
        if (displayTimer > timeBeforeBadgeDisplay)
        {
            if (badgeQueue.Count > 0)
            {
                displayTimer = 0f;
                DisplayBadge(badgeQueue.Dequeue());
            }
        }
    }

    public bool IsBadgeEarned(Badge badgeCheck)
    {
        foreach (Badge badge in earnedBadges)
        {
            if (badge.badgeId == badgeCheck.badgeId)
                return true;
        }
        return false;
    }

    public void DisplayRankUp()
    {
        emitter.GetComponent<ParticleSystem>().Play();
        popupRank.gameObject.SetActive(true);
        popupRank.GetComponent<UISprite>().spriteName = LanguageManager.Instance.GetTextValue("RankEarnedSprite");
    }

    public void RemoveRankUp()
    {
        popupRank.gameObject.SetActive(false);
    }

    public void HidePopups()
    {
        uiRoot.SetActive(false);
        emitter.transform.parent.gameObject.SetActive(false);
    }
    public void ShowPopups()
    {
        uiRoot.SetActive(true);
        emitter.transform.parent.gameObject.SetActive(true);
    }

	public Badge GetBadge(int id){
		foreach (Badge b in allBadges)
			if (b.badgeId == id)
				return b;

		return null;
	}

	public List<Badge> getCurrentBadgesList(){

		Dictionary<Statistic,Badge> dict = new Dictionary<Statistic, Badge> ();

		foreach (Badge b in earnedBadges) {
			if (!dict.ContainsKey(b.statistic))
				dict.Add(b.statistic,b);
			else
				if (b.badgeLevel > dict[b.statistic].badgeLevel)
					dict[b.statistic] = b;
		}

		return dict.Values.ToList ();
	}

	public string GetOverlay(int id){

		switch (id){
		case -1:
			return "Badge@Diamond";
		case 0:
			return "Badge@Bronze";
		case 1:
			return "Badge@Silver";
		case 2:
			return "Badge@Gold";
		}

		return null;
	}

    public void ResetBadges()
    {
        foreach (EditorBadge eBadge in editorBadges)
        {
            Badge newBadge = new Badge();
            newBadge.title = eBadge.title;
            newBadge.badgeId = eBadge.id;
            newBadge.statistic = eBadge.statistic;
            newBadge.requiredStat = eBadge.requiredStat;
            newBadge.description = eBadge.description;
            newBadge.spriteName = eBadge.spriteName;
            newBadge.badgeLevel = eBadge.badgeLevel;
            newBadge.priceLogs = eBadge.priceLogs;
            newBadge.priceMarshmallows = eBadge.priceMarshmallows;

            registeredBadges.Add(newBadge);
        }
        earnedBadges.Clear();


    }
    #endregion
}

	
