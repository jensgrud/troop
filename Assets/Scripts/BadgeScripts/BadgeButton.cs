﻿using UnityEngine;
using System.Collections;

public class BadgeButton : MonoBehaviour {

	public Badge Badge;
	public BadgeInfoPopup BadgePopup;

	public void OnClick(){
		BadgePopup.Icon.spriteName = Badge.spriteName;
		BadgePopup.Title.text = Badge.title;
		BadgePopup.Description.text = Badge.description;

		BadgePopup.gameObject.SetActive (true);
	}
}
