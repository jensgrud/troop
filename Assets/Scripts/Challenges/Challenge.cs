﻿using UnityEngine;
using System.Collections;

public class Challenge : MonoBehaviour {

    public GameObject ClearedSign;
    public GameObject FailedSign;
    public GameObject ClearedParticles;

    public bool checkIfChallengesEnabled()
    {
        if (GameManager.Instance.invincibleAgainstChallenges)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void EnableClearedSign()
    {
        ClearedSign.SetActive(true);
        PlayParticles();
    }

    public void EnableFailedSign()
    {
        FailedSign.SetActive(true);
    }

    public void DisableSigns()
    {
        ClearedSign.SetActive(false);
        FailedSign.SetActive(false);
    }

    void OnEnable()
    {
        DisableSigns();
    }

    private void PlayParticles()
    {
        foreach (ParticleSystem ps in ClearedParticles.GetComponents<ParticleSystem>())
            ps.Play();

        Invoke("StopParticles", 0.5f);
    }
    private void StopParticles()
    {
        foreach (ParticleSystem ps in ClearedParticles.GetComponents<ParticleSystem>())
            ps.Stop();
    }
}
