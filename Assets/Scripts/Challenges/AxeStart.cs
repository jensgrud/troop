﻿using UnityEngine;
using System.Collections;

public class AxeStart : Challenge {
    bool collided = false;
    public GameObject megalog;
    public GameObject megaRenderer;

	void OnTriggerEnter(Collider c)
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!collided)
            {
                collided = true;

                if (c.gameObject.GetComponent<Scout>() != null)
                {
                    megalog.GetComponent<Animator>().SetTrigger("startRoll");
                    megaRenderer.GetComponent<SkinnedMeshRenderer>().enabled = true;
                }
            }
        }
    }

    void OnEnable()
    {
        collided = false;
        megalog.GetComponent<Animator>().SetTrigger("Reset");
        megaRenderer.GetComponent<SkinnedMeshRenderer>().enabled = false;
        DisableSigns();
    }
}
