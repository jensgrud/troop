﻿using UnityEngine;
using System.Collections;

public class AxeChallenge : Challenge {
    
	public bool collided = false;
	AudioManager audioMan;


	void Start () {
		audioMan = AudioManager.Instance;
	}

	void TreeBreakSound()
	{
		audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("TreeBreakAudioPlayer").gameObject, 1);
	}

    public void OnTriggerEnter(Collider c) 
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!collided)
            {
                collided = true;

                //Stop scouts and let camera run
                foreach (Scout s in GameManager.Instance.troop.scouts)
                {
                    s.SetStuck(true);
                }
                GameManager.Instance.troop.challengeFailed = true;
                Invoke("ContinueScouts", 1);

                //Disable Chasemode
                GameManager.Instance.SetChaseMode(0, false);

                //enable failed sign
                base.EnableFailedSign();

                //run blink "animation"
                //GameManager.Instance.troop.Blink(GameManager.Instance.troop.numberOfBlinks);

                if (c.gameObject.GetComponent<Scout>() != null)
                {
                    Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();

                    AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                    if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run"))
                    {
                        anim.SetBool("collisionTrigger", true);
                    }
                    Invoke("DecrementStamina", stateInfo.length);
                    //Debug.Log(stateInfo.length);
                }
            }
        }
    }

    public void ContinueScouts()
    {
        //Continue scouts and let camera run
        foreach (Scout s in GameManager.Instance.troop.scouts)
        {
            s.SetStuck(false);
            s.PlayGrumpySound();
        }
        GameManager.Instance.troop.challengeFailed = false;
    }

    void OnEnable()
    {
        collided = false;
        DisableSigns();
    }

    private void DecrementStamina()
    {
        GameManager.Instance.troop.DecrementStamina();
        InGameGui.Instance.StaminaBlink(GameManager.Instance.troop.numberOfBlinks);
    }
}
