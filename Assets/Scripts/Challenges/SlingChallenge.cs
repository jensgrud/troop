﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlingChallenge: Challenge {
    public bool collided = false;
	public GameObject bearTrap;

	AudioManager audioMan;

	void Start () {
		audioMan = AudioManager.Instance;
        
	}

	void Activated()
	{
        audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("TrapActivatedAudioPlayer").gameObject, 1);
	}
	
	void TrapSnap()
	{
        audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("TrapSnapAudioPlayer").gameObject, 1);
	}
    
    //If the sling scout is in front the challenge is complete, else it fails
    public void OnTriggerEnter(Collider c)
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!collided)
            {
                collided = true;

                //Stop scouts and let camera run
                foreach (Scout s in GameManager.Instance.troop.scouts)
                {
                    s.SetStuck(true);
                }
                GameManager.Instance.troop.challengeFailed = true;
                Invoke("ContinueScouts", 1);

                //Disable chase mode
                GameManager.Instance.SetChaseMode(0, false);

                //Enable Failed Sign
                base.EnableFailedSign();

                //run blink "animation"
                //GameManager.Instance.troop.Blink(GameManager.Instance.troop.numberOfBlinks);

                bearTrap.GetComponent<Animator>().SetTrigger("ActivateTrapFail");



                if (c.gameObject.GetComponent<Scout>() != null)
                {
                    Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();

                    AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                    if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run"))
                    {
                        anim.SetBool("collisionTrigger", true);

                    }
                    Invoke("DecrementStamina", stateInfo.length);
                    //Debug.Log(stateInfo.length);
                }
            //}
            }
        }
    }

    public void ContinueScouts()
    {
        //Continue scouts and let camera run
        foreach (Scout s in GameManager.Instance.troop.scouts)
        {
            s.SetStuck(false);
            s.PlayGrumpySound();
        }
        GameManager.Instance.troop.challengeFailed = false;
    }

    void OnEnable()
    {
        DisableSigns();
        collided = false;
        bearTrap.GetComponent<Animator>().SetTrigger("Reset");
    }

    private void DecrementStamina()
    {
        GameManager.Instance.troop.DecrementStamina();
        InGameGui.Instance.StaminaBlink(GameManager.Instance.troop.numberOfBlinks);
    }
}