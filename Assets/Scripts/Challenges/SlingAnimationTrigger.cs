﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlingAnimationTrigger : Challenge
{
    private bool collidedEnter = false;
    private bool collidedExit = false;
    public GameObject bearTrap;
    private bool trapDisabled = false;
	public float waitTime = 0.3f;

    public GameObject trapParticles;

    //If the sling scout is in front the challenge is complete, else it fails
    public void OnTriggerEnter(Collider c)
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!collidedEnter)
            {
                collidedEnter = true;
                if (c.transform.GetChild(0).tag == "Sling")
                {
                    //tell statistic manager
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilitySlingCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilitySling);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilities);

                    

					Invoke ("bearTrapActivate",waitTime);
                    Invoke("clearSign", 1f);
                    

                    trapDisabled = true;
                    gameObject.transform.parent.GetComponentInChildren<SlingChallenge>().collided = true;
                    if (c.gameObject.GetComponent<Scout>() != null)
                    {
                        Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();

                       // AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                        //if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run"))
                        //{
                            anim.SetTrigger("slingTrigger");
                        //}
                    }
                }
                

            }
        }
    }
	void bearTrapActivate(){
		bearTrap.GetComponent<Animator>().SetTrigger("ActivateTrap");
        //play Particles
        foreach (ParticleSystem ps in trapParticles.GetComponentsInChildren<ParticleSystem>())
        {
            ps.Play();
        }
	}

    public void OnTriggerExit(Collider c)
    {
       
        if (base.checkIfChallengesEnabled())
        {
            if (!collidedExit && !trapDisabled)
            {
                collidedExit = true;
                if (GameManager.Instance.troop.GetScoutByOrder(0).transform.GetChild(0).tag == "Sling")
                {
                    Invoke("bearTrapActivate", waitTime);
                    trapDisabled = true;
                    Invoke("clearSign", 1f);


                    gameObject.transform.parent.GetComponentInChildren<SlingChallenge>().collided = true;
                    if (c.gameObject.GetComponent<Scout>() != null)
                    {
                        Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();

                        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                        if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run"))
                        {
                            anim.SetTrigger("slingTrigger");
                        }
                    }
                }
            }
        }
    }

    void OnEnable()
    {
        collidedEnter = false;
        collidedExit = false;
        trapDisabled = false;
        DisableSigns();
        bearTrap.GetComponent<Animator>().SetTrigger("Reset");
    }
    void clearSign()
    {
        base.EnableClearedSign();
    }
}