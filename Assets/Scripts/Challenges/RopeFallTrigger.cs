﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RopeFallTrigger : MonoBehaviour
{
    private GameObject moveDest;
    private bool collided = false;
    public GameObject RopeGameobject;
    Scout firstScout;
    Scout secondScout;
    Scout thirdScout;

    public GameObject thornParticles;

    void Awake()
    {
        moveDest = transform.parent.transform.FindChild("ScoutRespawn").gameObject;
    }

    void Start()
    {
    }


    public void OnTriggerEnter(Collider c)
    {
        if(!collided){
            collided = true;

            //Debug.Log("ropefail");
            //c.transform.position = moveDest.transform.position;

            Invoke("troopFallWait",1.2f);
            //play Particles
            foreach (ParticleSystem ps in thornParticles.GetComponentsInChildren<ParticleSystem>())
            {
                ps.Play();
            }
        }
    }
    public void troopFallWait()
    {
        RopeGameobject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = true;
        RopeGameobject.transform.GetComponent<Animator>().SetTrigger("onThrowing");
       
        GameManager.Instance.troop.DecrementStamina();
        GameManager.Instance.troop.getOrderedScouts(out firstScout, out secondScout, out thirdScout);
        firstScout.transform.position = moveDest.transform.position + new Vector3(3.6f, 0f, 0f);
        secondScout.transform.position = moveDest.transform.position + new Vector3(1.8f, 0f, 0f);
        thirdScout.transform.position = moveDest.transform.position + new Vector3(0f, 0f, 0f);
        transform.parent.transform.FindChild("rope_placeholder").collider.enabled = true;
        InGameGui.Instance.StaminaBlink(GameManager.Instance.troop.numberOfBlinks);
        firstScout.PlayGrumpySound();
        secondScout.PlayGrumpySound();
        thirdScout.PlayGrumpySound();


    }

    void OnEnable(){
        collided = false;
    }
}