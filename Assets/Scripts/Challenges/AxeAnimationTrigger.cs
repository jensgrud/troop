﻿using UnityEngine;
using System.Collections;

public class AxeAnimationTrigger : Challenge
{
    private bool collided = false;
    private float waitTime = 0.18f;


    public GameObject particleSystems;

    //If the sling scout is in front the challenge is complete, else it fails
    public void OnTriggerEnter(Collider c)
    {
        //Debug.Log(c.name);
        if (base.checkIfChallengesEnabled())
        {
            if (!collided)
            {
                collided = true;
                if (c.transform.GetChild(0).tag == "Axe")
                {

                    //tell statistic manager
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityAxeCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilityAxe);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilities);

                    Invoke("clearSign", 0.5f);

                    gameObject.transform.parent.GetComponent<AxeChallenge>().collided = true;
                    if (c.gameObject.GetComponent<Scout>() != null)
                    {
                        Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();

                        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
						//if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run")||stateInfo.nameHash == Animator.StringToHash("Base Layer.land")||stateInfo.nameHash == Animator.StringToHash("Base Layer.land"))
                        //{
                            anim.SetTrigger("axeTrigger");
//                            Debug.Log("running Axe animation");

                            //fireParticles
                            foreach (ParticleSystem ps in particleSystems.GetComponentsInChildren<ParticleSystem>())
                            {
                                ps.Play();
                                //Debug.Log("Splinter Particles");
                            }
                        //}
                        Invoke("breakObstacle", waitTime);

                    }
                }


            }
        }
    }
    void breakObstacle()
    {
        gameObject.transform.parent.GetComponent<Animator>().SetTrigger("OnChopping");
    }

    void OnEnable()
    {
        collided = false;
        gameObject.transform.parent.GetComponent<Animator>().SetTrigger("Reset");
        DisableSigns();
    }
    void clearSign()
    {
        base.EnableClearedSign();
    }
}