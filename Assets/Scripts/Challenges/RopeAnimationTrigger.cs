﻿using UnityEngine;
using System.Collections;

public class RopeAnimationTrigger : Challenge
{
    private bool collided = false;
    public bool challengeSolved = false;
    public GameObject ropePlaceHolder;
    public GameObject rope;
    public GameObject ropeMesh;
	private GameObject Fox;
    //If the sling scout is in front the challenge is complete, else it fails

	public void OnTriggerEnter(Collider c)
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!collided)
            {
                collided = true;

                if (c.transform.GetChild(0).tag == "Rope")
                {
                    challengeSolved = true;
                    //tell statistic manager
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityRopeCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilityRope);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilities);

                    Invoke("clearSign",0.7f);

                    gameObject.transform.parent.GetComponentInChildren<RopeChallenge>().collided = true;
                    if (c.gameObject.GetComponent<Scout>() != null)
                    {
                        Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();
                        
                        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                        if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run") || stateInfo.nameHash == Animator.StringToHash("Base Layer.land") || stateInfo.nameHash == Animator.StringToHash("Base Layer.duck"))
                        {
                            anim.SetTrigger("ropeTrigger");
                        }
                        
                        Invoke("ropeThrow", 0.1f);
					}
                    //ropePlaceHolder.GetComponent<MeshRenderer>().enabled = true;
					if (GameObject.Find ("Fox(Clone)") != null) {
						Fox = GameObject.Find("Fox(Clone)");
						Fox.transform.FindChild("fox").GetComponent<Animator>().SetTrigger("foxJump");
                        Fox.transform.FindChild("FoxFly@ParticleSystem").transform.GetComponent<ParticleSystem>().Play();
						Invoke("FoxLand", 2.2f);
					}
                    if (GameObject.Find("Turtle(Clone)") != null)
                    {
                        GameObject turtle = GameObject.Find("Turtle(Clone)");
                        //Debug.Log("the fox is:" + Fox.name);
                        turtle.transform.FindChild("turtle").GetComponent<Animator>().SetTrigger("turtleJump");
                        turtle.transform.FindChild("LimeTurtleFly@ParticleSystem").transform.GetComponent<ParticleSystem>().Play();
                        Invoke("TurtleLand", 2f);
                    }
				}
            }
        }
    }

    void OnEnable()
    {
        collided = false;
        rope.transform.GetComponent<Animator>().SetTrigger("Reset");
        DisableSigns();
        ropeMesh.GetComponent<SkinnedMeshRenderer>().enabled = false;
    }
    void clearSign()
    {
        base.EnableClearedSign();
    }
	void FoxLand() {
        if (GameObject.Find("Fox(Clone)") != null)
        {
            Fox = GameObject.Find("Fox(Clone)");
            Fox.transform.FindChild("FoxFly@ParticleSystem").transform.GetComponent<ParticleSystem>().Stop();
            Fox.transform.FindChild("fox").GetComponent<Animator>().SetTrigger("foxLand");
        }
	}
    void TurtleLand()
    {
        if (GameObject.Find("Turtle(Clone)") != null)
        {
            GameObject turtle = GameObject.Find("Turtle(Clone)");
            turtle.transform.FindChild("turtle").GetComponent<Animator>().SetTrigger("turtleLand");
            turtle.transform.FindChild("LimeTurtleFly@ParticleSystem").transform.GetComponent<ParticleSystem>().Stop();
        }
        
    }
    void ropeThrow()
    {
        rope.transform.GetComponent<Animator>().SetTrigger("onThrowing");
        ropeMesh.GetComponent<SkinnedMeshRenderer>().enabled = true;
    }
}