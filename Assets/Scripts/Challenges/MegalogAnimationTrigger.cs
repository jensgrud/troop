﻿using UnityEngine;
using System.Collections;

public class MegalogAnimationTrigger : Challenge
{
    private bool collided = false;
    private float waitTime = 0.5f;
    public GameObject megalog;
    public GameObject logCollider;
    //If the sling scout is in front the challenge is complete, else it fails
    public void OnTriggerEnter(Collider c)
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!collided)
            {
                collided = true;
                if (c.transform.GetChild(0).tag == "Axe")
                {

                    //tell statistic manager
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityAxeCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.AbilityCount);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilityAxe);
                    StatisticsManager.Instance.IterateStatistic(Statistic.SingleRunAbilities);

                    logCollider.GetComponent<AxeChallenge>().collided = true;


                    if (c.gameObject.GetComponent<Scout>() != null)
                    {
                        Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();

                        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                        if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run"))
                        {
                            anim.SetTrigger("axeTrigger");
                        }

                        //Animator animMegalog = megalog.GetComponent<Animator>();
                        //AnimatorStateInfo stateInfoMega = animMegalog.GetCurrentAnimatorStateInfo(0);

                        // megalog.GetComponent<Animator>().SetFloat("startRoll",0.5f);

                        megalog.GetComponent<Animator>().SetTrigger("startBreak");

                    }

                }
            }


        }
    }

    void OnEnable()
    {
        DisableSigns();
        collided = false;
        megalog.GetComponent<Animator>().SetTrigger("Reset");
    }
    void clearSign()
    {
        base.EnableClearedSign();
    }
}
//private void breakTree()
//{
//    megalog.GetComponent<Animator>().SetTrigger("startBreak");
//}