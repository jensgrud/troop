﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RopeChallenge : Challenge, IObstacle {
    public bool collided = false;
    public RopeAnimationTrigger ropeanimationTrigger;
    private int animationPlays;

	AudioManager audioMan;

	void Start () {
		audioMan = AudioManager.Instance;
	}

	public void RopeAttatchSound()
	{
		audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("RopeAttachAudioPlayer").gameObject, 1);
	}

    //If the rope scout is in front the challenge is complete, else it fails
    public void OnTriggerEnter(Collider c)
    {
        if (base.checkIfChallengesEnabled())
        {
            if (!ropeanimationTrigger.challengeSolved)
            {
                if (c.gameObject.GetComponent<Scout>() != null && animationPlays < 3)
                {
                    Animator anim = c.gameObject.GetComponent<Scout>().getAnimation();
                    animationPlays++;
                   // AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                    //if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run") || stateInfo.nameHash == Animator.StringToHash("Base Layer.duck") || stateInfo.nameHash == Animator.StringToHash("Base Layer.landing") || stateInfo.nameHash == Animator.StringToHash("Base Layer.air"))
                    //{
                        //anim.SetBool("collisionTrigger", true);
                        anim.SetTrigger("fallTrigger");
                        //Debug.Log("Run fail animation");
                    //}
                }
            }
            if (!collided)
            {
                collided = true;

                //run blink "animation"

                //Blink is disabled so you can see animations, blinks should happend when they reappear
                //GameManager.Instance.troop.Blink(GameManager.Instance.troop.numberOfBlinks);

                //Enable Failed sign
                base.EnableFailedSign();

                //gameObject.GetComponent<Animator>().SetTrigger("ActivateTrap");
              
                transform.parent.transform.FindChild("rope_placeholder").collider.enabled = false;
                GameManager.Instance.troop.challengeFailed = true;
                Invoke("DisableSetChallengeFailedFlag", 2f);
                GameManager.Instance.SetChaseMode(0, false);
            }
        }
    }
    public void DisableSetChallengeFailedFlag()
    {
        GameManager.Instance.troop.challengeFailed = false;
    }

    void OnEnable()
    {
        collided = false;
        animationPlays = 0;
        DisableSigns();
       // transform.parent.transform.FindChild("rope_placeholder").collider.enabled = false;
    }
}
