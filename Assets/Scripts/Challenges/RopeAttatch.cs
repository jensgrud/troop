﻿using UnityEngine;
using System.Collections;

public class RopeAttatch : MonoBehaviour {

	AudioManager audioMan;
	
	void Start () {
		audioMan = AudioManager.Instance;
	}
	
	public void RopeAttatchSound()
	{
		audioMan.PlaySingleSound(gameObject,audioMan.transform.FindChild("RopeGrabAudioPlayer").gameObject, 1);
	}
}
