﻿using UnityEngine;
using System.Collections;

public class ProceduralManager : MonoBehaviour {

    
    //public bool allowConsecutiveObstacles = true;
    //private bool canBeObstacle = true;

    public GameObject startPrefab;
    private GameObject nextPrefab;
    private Vector3 nextNormalModePosition;
    private int defaultBlockSize = 9;
    private Vector3 nextSpawnPosition;

    private int count;
    private int maxBlocks = 5;

  //  public Vector3 feverStartingPosition = Vector3.zero;
  //  private Vector3 feverNextPos = Vector3.zero;
  //  private bool feverState = false;
  //  private bool isFeverInitialized = false;
  //  private int feverBlockCount = 0;
  //  private int feverMaxCount = 5;
  //  public float feverLength = 0;
  //  private int feverCounter = 0;
  //  public float feverThreshold = 5;
  //  private Vector3[] ScoutOldPositions;
  //  public GameObject feverEndTrigger;
  //  private bool feverEndTriggerSpawned = false;

    private GameObject spawnableObject;
    public GameObject campsitePrefab;
    public GameObject startingChaseTrigger;
    public GameObject endingChaseTrigger;
    public GameObject roadSign100;
    public GameObject roadSign200;
    public GameObject roadSign300;
    public int campsiteDistance;
    private bool isChasing = false;
    private float lastChasePosition;
    private float lastChaseSpeed;
    //this variable is used to track the campsites in the last level. 
    private int campsiteCounts;
    private bool isCampsite = false;
    private int activeCampsite;
    private int[] campsitePositions;
    private float chaseCounts;
    
    
    //variables to check the animal spawning
    private Vector3 lastAnimalSpawnPosition;
    private float animalDistance = 30;


  //  private LayerMask ground = 31;

    [System.Serializable]
    public class TerrainProbs
    {
        [SerializeField]
        private string Name;
       // public float terrainProb;
        public float startingChallengeProb;
        public float endingChallengeProb;
        public float startingObstacleProb;
        public float endingObstacleProb;
        public float softCurrencyProb;
        public float hardCurrencyProb;
        public float collectableProb;
        public int levelStartingPoint;
        public float trappedAnimalProb;
        public float chaseModeProb;
        public int chaseMinDistance;
        public int chaseLength;
        public float chaseSpeedMultiplier;
        public float maxSpeedMultiplier;
        public float levelSpeedMultiplier;
        public bool allowConsecutiveObs;
        public bool spawnCampsite = true;
        
        
    }

    //[System.Serializable]
    //public class FeverProbs
    //{
    //    [SerializeField]
    //    private string Name;
    //    // public float terrainProb;   
    //    public float softCurrencyProb;
    //    public float hardCurrencyProb;

    //}
    public TerrainProbs[] levelSettings;
 //   public FeverProbs feverSettings;

    private float obstacleProb;
    private float challengeProb;

   // public float[] levelsStartingDistance;
    private GameObject proceduralLevel;
    private GameObject terrain;
    private GameObject background;
    //private GameObject feverLevel;
    //private GameObject feverTerrain;
    //private GameObject feverBackground;
    private GameObject obj;
    private int activeLevel;
    private int scoutLevel;
    private bool isPrevChallenge = false;
    private bool isPrevObs = false;
    private bool isChallenge = false;
    private int terrainAfterChallenge = 2;
    private int terrainAfterChallengeCount;
    private float currencyOffset;
    private Vector3 surfaceNormal;
    private Vector3 surfaceCentre;
    private int[] originalStartingPoint;
	// Use this for initialization
    public static ProceduralManager Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        Instance = this;
    }

	void Start () {
        originalStartingPoint = new int[levelSettings.Length];
        for (int i = 0; i < originalStartingPoint.Length; i++)
            originalStartingPoint[i] = levelSettings[i].levelStartingPoint;

        Restart();
        //proceduralLevel = new GameObject("ProceduralLevel");
        //terrain = new GameObject("Terrain");
        //terrain.transform.parent = proceduralLevel.transform;
        //terrain.isStatic = true;
        //background = new GameObject("Background");
        //background.transform.parent = proceduralLevel.transform;
        //background.isStatic = true;

        //nextNormalModePosition = Vector3.zero;
        //nextNormalModePosition.x = -2*defaultBlockSize;
        //nextSpawnPosition = nextNormalModePosition;
        //lastAnimalSpawnPosition = new Vector3(0, 0, 0);
        //activeLevel = GameManager.Instance.GetLevel();
        //if (activeLevel != 0)
        //{
        //    int startingPoint = 0;
        //    if (levelSettings.Length > 1)
        //        startingPoint = levelSettings[1].levelStartingPoint;
        //    for (int i = 1; i < levelSettings.Length; i++)
        //        levelSettings[i].levelStartingPoint -= startingPoint;
        //}
        //scoutLevel = activeLevel;
        //campsitePositions = GetCampsitesPosition();
        //Vector3 tempPosition;
        //nextPrefab = startPrefab;
        //for (int i = 0; i < 3; i++)
        //{
        //    obj = Instantiate(nextPrefab, nextNormalModePosition, Quaternion.identity) as GameObject;
        //    obj.transform.parent = proceduralLevel.transform;
        //    count++;
        //    tempPosition = nextSpawnPosition;
        //    nextSpawnPosition = GetNextPosition(nextPrefab);
        //    SpawnRoadSign(tempPosition);
        //}


        //lastChasePosition = - levelSettings[activeLevel].chaseMinDistance;
        //ScoutOldPositions = new Vector3[3];
        //InitializeScene();
	}

    public void Restart()
    {
        count = 0;
        campsiteCounts = 0;
        activeCampsite = 0;
        chaseCounts = 0;
        terrainAfterChallengeCount = 0;
        currencyOffset = 0f;
        surfaceNormal = Vector3.zero;
        surfaceCentre = Vector3.zero;
        for (int i = 0; i < levelSettings.Length; i++)
        {
            levelSettings[i].levelStartingPoint = originalStartingPoint[i];
        }
            //tutorial is played again, so I need to remove the terrain already created
        if (proceduralLevel != null)
        {
            Recyclable[] objects = proceduralLevel.GetComponentsInChildren<Recyclable>(true);
            foreach (Recyclable o in objects)
            {
                if(o.isRecyclable)
                    ProceduralDatabaseManager.Instance.Recycle(o.gameObject);
            }
                
            ProceduralDatabaseManager.Instance.Recycle(proceduralLevel);
        }

        proceduralLevel = new GameObject("ProceduralLevel");
        terrain = new GameObject("Terrain");
        terrain.transform.parent = proceduralLevel.transform;
      //  terrain.isStatic = true;
        background = new GameObject("Background");
        background.transform.parent = proceduralLevel.transform;
      //  background.isStatic = true;

        //starting position of the terrain
        nextNormalModePosition = Vector3.zero;
        nextNormalModePosition.x = -2 * defaultBlockSize;
        nextSpawnPosition = nextNormalModePosition;
        lastAnimalSpawnPosition = new Vector3(0, 0, 0);

        //if tutorial is skipped, level 1 starts in position 0
        activeLevel = GameManager.Instance.GetLevel();
        if (activeLevel != 0)
        {
            int startingPoint = 0;
            if (levelSettings.Length > 1)
				startingPoint = levelSettings[activeLevel].levelStartingPoint;
            for (int i = 1; i < levelSettings.Length; i++)
                levelSettings[i].levelStartingPoint -= startingPoint;
        }
        scoutLevel = activeLevel;
        //campsite positions (used to spawn roadsigns)
        campsitePositions = GetCampsitesPosition();
        Vector3 tempPosition;
        //initial flat blocks
        nextPrefab = startPrefab;
        for (int i = 0; i < 1; i++)
        {
          //obj = Instantiate(nextPrefab, nextNormalModePosition, Quaternion.identity) as GameObject;
            obj = ProceduralDatabaseManager.Instance.GetOtherObject(nextPrefab);
            obj.transform.Translate(nextNormalModePosition);
            //Debug.Log(obj.transform.position);
            obj.transform.parent = proceduralLevel.transform;
            count++;
            tempPosition = nextSpawnPosition;
            nextSpawnPosition = GetNextPosition(nextPrefab);
            SpawnRoadSign(tempPosition);
        }


        lastChasePosition = -levelSettings[activeLevel].chaseMinDistance;
       // ScoutOldPositions = new Vector3[3];
        GameManager.Instance.UpdateLevelSpeed(levelSettings[scoutLevel].levelSpeedMultiplier);
        GameManager.Instance.StopTime();
        InitializeScene();
    }


	// Update is called once per frame
	void Update () {
      //  time += Time.deltaTime;
        CheckScoutLevel();
        //Terrain generation when fever mode is off
        if (count < maxBlocks)// && !feverState)
        {
            ChangeLevel();
         

            currencyOffset = 0f;
            //checking if the next block of terrain should be a challenge or a normal block
			float rnd = Random.Range(0f,1f);

            if (isCampsite)
            {
                SpawnCampsite();
            }
            else if (rnd > challengeProb || (!levelSettings[activeLevel].allowConsecutiveObs && (isPrevChallenge || isPrevObs)))
            {
                SpawnTerrain();
            }
            else
            {
                SpawnChallenge();
            }

            if (nextPrefab != null && nextPrefab.GetComponent<ProceduralBlock>().allowSpawn)
            {
                //checking if we should spawn an obstacle on this block of terrain
                rnd = Random.Range(0f, 1f);
                if (rnd < obstacleProb && !isChallenge && !isPrevObs && !isPrevChallenge)
                {
                    SpawnObstacle();
                    isPrevObs = true;
                }
                else
                    isPrevObs = false;

                GameObject obstacle = spawnableObject;
                
                //checking if we should spawn soft currency object on this obstacle
                if (obstacle == null || obstacle.GetComponent<JumpableObstacle>() == null || obstacle.GetComponent<JumpableObstacle>().allowCurrencySpawn)
                {
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].softCurrencyProb)
                    {
                        SpawnSoftCurrency();
                    }

                    //checking if we should spawn hard currency object on this block of terrain
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].hardCurrencyProb)
                    {
                        SpawnHardCurrency();
                    }

                }

                if (obstacle == null || obstacle.GetComponent<JumpableObstacle>()  == null || obstacle.GetComponent<JumpableObstacle>().allowOthersSpawn)
                {
                    //checking if a bear cave should be spawned
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].chaseModeProb && (lastChasePosition + levelSettings[activeLevel].chaseMinDistance < nextNormalModePosition.x))
                    {
                        SpawnChaseTrigger();
                        lastChasePosition = nextNormalModePosition.x;
                        isChasing = true;
                    }
                    if (isChasing && (lastChasePosition + levelSettings[activeLevel].chaseLength) < nextNormalModePosition.x)
                    {
                        SpawnChaseEnd();
                        isChasing = false;
                    }

                    //checking if we should spawn a collectable object on this block of terrain
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].collectableProb && !isChasing)
                    {
                        SpawnCollectable();
                    }


                    //checking if an animal should be spawned
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].trappedAnimalProb && !isChasing)
                    {
                        SpawnTrappedAnimal();
                    }

 
                }

                SpawnBackgroundObject();

            }
            
            if (nextPrefab != null)
            {
                Vector3 tempPos = nextSpawnPosition;
                nextSpawnPosition = GetNextPosition(nextPrefab);
                SpawnRoadSign(tempPos);

                int blockSize = 1;
                if (nextPrefab.GetComponent<ProceduralBlock>() != null)
                    blockSize = (int)nextPrefab.GetComponent<Recyclable>().numberOfBlocks;

                count += blockSize;
            }
            
            challengeProb = IncreaseChallengeDifficulty();
            obstacleProb = IncreaseObstacleDifficulty();



        }
        //intialization of fever mode
        //else if (feverState && !isFeverInitialized)
        //{
        //    InitializeFever();
        //    isFeverInitialized = !isFeverInitialized;
        //}
        ////fever mode is on
        //else if (isFeverInitialized)
        //{
        //    if (feverBlockCount < feverMaxCount)
        //    {


        //        currencyOffset = 0f;
        //        //checking if the next block of terrain should be a challenge or a normal block

        //        SpawnTerrain();
        //        float rnd = 0;

        //        if (nextPrefab != null && nextPrefab.GetComponent<ProceduralBlock>().allowSpawn)
        //        {

        //            //checking if we should spawn soft currency object on this block of terrain
        //            rnd = Random.Range(0f, 1f);
        //            if (rnd < feverSettings.softCurrencyProb)
        //            {
        //                SpawnSoftCurrency();
        //            }

        //            //checking if we should spawn hard currency object on this block of terrain
        //            rnd = Random.Range(0f, 1f);
        //            if (rnd < feverSettings.hardCurrencyProb)
        //            {
        //                SpawnHardCurrency();
        //            }

        //            SpawnBackgroundObject();

        //            if (feverStartingPosition.x + feverLength < feverNextPos.x && !feverEndTriggerSpawned)
        //            {
        //                feverEndTriggerSpawned = !feverEndTriggerSpawned;
        //                SpawnFeverEndTrigger();
        //            }

        //        }

        //        if (nextPrefab != null)
        //        {
        //            Vector3 tempPos = nextSpawnPosition;
        //            nextSpawnPosition = GetNextPosition(nextPrefab);
        //            SpawnRoadSign(tempPos);
        //        }

        //        int blockSize = 1;
        //        if (nextPrefab.GetComponent<ProceduralBlock>())// != null && nextPrefab.GetComponent<ProceduralBlock>().isGroup)
        //            blockSize = (int)nextPrefab.GetComponent<Recyclable>().numberOfBlocks;

        //        feverBlockCount += blockSize;
        //    }
        //}

//        Debug.Log(nextSpawnPosition);
	}

    public void InitializeScene()
    {
        while(count < maxBlocks)
        {
            ChangeLevel();
            currencyOffset = 0f;
            //checking if the next block of terrain should be a challenge or a normal block
            float rnd = Random.Range(0f, 1f);
            if (isCampsite)
            {
                SpawnCampsite();
             }
            else if (rnd > challengeProb || (!levelSettings[activeLevel].allowConsecutiveObs && (isPrevChallenge || isPrevObs)))
            {
                SpawnTerrain();
            }
            else
            {
                SpawnChallenge();
            }

            if (nextPrefab != null && nextPrefab.GetComponent<ProceduralBlock>().allowSpawn)
            {
                //checking if we should spawn an obstacle on this block of terrain
                rnd = Random.Range(0f, 1f);

                if (rnd < obstacleProb && !isChallenge && !isPrevObs && !(isPrevChallenge))
                {
                    SpawnObstacle();
                    isPrevObs = true;
                }
                else
                    isPrevObs = false;


                GameObject obstacle = spawnableObject;

                //checking if we should spawn soft currency object on this block of terrain
                if (obstacle == null || obstacle.GetComponent<JumpableObstacle>() == null || obstacle.GetComponent<JumpableObstacle>().allowCurrencySpawn)
                {
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].softCurrencyProb)
                    {
                        SpawnSoftCurrency();
                    }

                    //checking if we should spawn hard currency object on this block of terrain
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].hardCurrencyProb)
                    {
                        SpawnHardCurrency();
                    }

                }

                if (obstacle == null || obstacle.GetComponent<JumpableObstacle>() == null || obstacle.GetComponent<JumpableObstacle>().allowOthersSpawn)
                {

                    //checking if a bear cave should be spawned
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].chaseModeProb && (lastChasePosition + levelSettings[activeLevel].chaseMinDistance < nextNormalModePosition.x))
                    {
                        SpawnChaseTrigger();
                        lastChasePosition = nextNormalModePosition.x;
                        isChasing = true;
                    }
                    if (isChasing && (lastChasePosition + levelSettings[activeLevel].chaseLength) < nextNormalModePosition.x)
                    {
                        SpawnChaseEnd();
                        isChasing = false;
                    }

                    //checking if we should spawn a collectable object on this block of terrain
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].collectableProb && !isChasing)
                    {
                        SpawnCollectable();
                    }


                    //checking if an animal should be spawned
                    rnd = Random.Range(0f, 1f);
                    if (rnd < levelSettings[activeLevel].trappedAnimalProb && !isChasing)
                    {
                        SpawnTrappedAnimal();
                    }

                }

                SpawnBackgroundObject();


            }

          //  isPrevChallenge = isChallenge;
            if (nextPrefab != null)
            {
                Vector3 tempPos = nextSpawnPosition;
                nextSpawnPosition = GetNextPosition(nextPrefab);
                SpawnRoadSign(tempPos);
                int blockSize = 1;
                if (nextPrefab.GetComponent<ProceduralBlock>() != null)// && nextPrefab.GetComponent<ProceduralBlock>().isGroup)
                    blockSize = (int)nextPrefab.GetComponent<Recyclable>().numberOfBlocks;

                count += blockSize;
            }

            challengeProb = IncreaseChallengeDifficulty();
            obstacleProb = IncreaseObstacleDifficulty();

        }
    }

    public bool SetChasing(bool flag)
    {
        isChasing = flag;
        return isChasing;
    }
    //Initialization of fever mode
    //public void InitializeFever()
    //{
    //    feverNextPos = feverStartingPosition;
    //    while(feverBlockCount < feverMaxCount)
    //    {

    //        currencyOffset = 0f;
    //        //checking if the next block of terrain should be a challenge or a normal block

    //        SpawnTerrain();
    //        float rnd = 0;

    //        if (nextPrefab != null && nextPrefab.GetComponent<ProceduralBlock>().allowSpawn)
    //        {

    //            //checking if we should spawn soft currency object on this block of terrain
    //            rnd = Random.Range(0f, 1f);
    //            if (rnd < feverSettings.softCurrencyProb)
    //            {
    //                SpawnSoftCurrency();
    //            }

    //            //checking if we should spawn hard currency object on this block of terrain
    //            rnd = Random.Range(0f, 1f);
    //            if (rnd < feverSettings.hardCurrencyProb)
    //            {
    //                SpawnHardCurrency();
    //            }

    //            SpawnBackgroundObject();

    //            if (feverStartingPosition.x + feverLength < feverNextPos.x && !feverEndTriggerSpawned)
    //            {
    //                feverEndTriggerSpawned = !feverEndTriggerSpawned;
    //                SpawnFeverEndTrigger();
    //            }
    //            if (nextPrefab != null)
    //            {
    //                Vector3 tempPos = nextSpawnPosition;
    //                nextSpawnPosition = GetNextPosition(nextPrefab);
    //                SpawnRoadSign(tempPos);
    //            }
    //        }

    //        int blockSize = 1;
    //        if (nextPrefab.GetComponent<ProceduralBlock>() != null)// && nextPrefab.GetComponent<ProceduralBlock>().isGroup)
    //            blockSize = (int)nextPrefab.GetComponent<Recyclable>().numberOfBlocks;

    //        feverBlockCount += blockSize;
    //    }
    //    Scout first = GameManager.Instance.troop.GetScoutByOrder(0);
    //    Scout second = GameManager.Instance.troop.GetScoutByOrder(1);
    //    Scout third = GameManager.Instance.troop.GetScoutByOrder(2);
    //    ScoutOldPositions[0] = first.transform.position;
    //    ScoutOldPositions[1] = second.transform.position;
    //    ScoutOldPositions[2] = third.transform.position;
    //    first.transform.position = feverStartingPosition + new Vector3(9,5,0);
    //    second.transform.position = feverStartingPosition + new Vector3(7, 5, 0);
    //    third.transform.position = feverStartingPosition + new Vector3(5, 5, 0);
    //    GameManager.Instance.cameraFocus.transform.position = feverStartingPosition + new Vector3(10, 5, 0);
    //    // MOVE TROOP BACK TO THE ORIGIN 
    //}

    ////this function starts the fever mode
    //public void EnterFeverMode()
    //{
    //    feverState = true;
    //    isFeverInitialized = false;
    //    feverNextPos = feverStartingPosition;
    //    nextSpawnPosition = feverStartingPosition;
    //    feverEndTriggerSpawned = false;
    //    feverLevel = new GameObject("FeverLevel");
    //    feverLevel.isStatic = true;
    //    feverTerrain = new GameObject("FeverTerrain");
    //    feverTerrain.isStatic = true;
    //    feverBackground = new GameObject("FeverBackground");
    //    feverBackground.isStatic = true;
    //    feverTerrain.transform.parent = feverLevel.transform;
    //    feverBackground.transform.parent = feverLevel.transform;
    //    feverBlockCount = 0;
    //    proceduralLevel.SetActive(false);
        
    //}

    ////this function ends the fever mode
 
    //public void ExitFeverMode()
    //{
    //    feverState = false;
    //    isFeverInitialized = false;
    //    proceduralLevel.SetActive(true);
    //    Scout first = GameManager.Instance.troop.GetScoutByOrder(0);
    //    Scout second = GameManager.Instance.troop.GetScoutByOrder(1);
    //    Scout third = GameManager.Instance.troop.GetScoutByOrder(2);
    //    first.transform.position = ScoutOldPositions[0] + new Vector3(5,3,0);
    //    second.transform.position = ScoutOldPositions[1] + new Vector3(5, 3, 0);
    //    third.transform.position = ScoutOldPositions[2] + new Vector3(5,3, 0);
    //    GameManager.Instance.cameraFocus.transform.position = first.transform.position + new Vector3(5, 3, 0);
    //    feverNextPos = feverStartingPosition;
    //    nextSpawnPosition = nextNormalModePosition;
    //    feverCounter = 0;
    //    Recyclable[] objects = feverLevel.GetComponentsInChildren<Recyclable>(true);
    //    foreach (Recyclable elem in objects)
    //    {
    //        ProceduralDatabaseManager.Instance.Recycle(elem.gameObject);
    //    }
    //    GameObject.Destroy(feverTerrain);
    //    GameObject.Destroy(feverBackground);
    //    GameObject.Destroy(feverLevel);
    //    proceduralLevel.SetActive(true);
        
    //}

    //public void UpdateFeverCounter()
    //{
    //    feverCounter++;
    //    if (feverCounter == feverThreshold)
    //        EnterFeverMode();
    //}

    //this function increase the number of challenges as the level progresses
    private float IncreaseChallengeDifficulty()
    {
        float result = 0f;
        if (activeLevel + 1 < levelSettings.Length)
        {
            float percentage = (nextNormalModePosition.x-levelSettings[activeLevel].levelStartingPoint) 
                /(levelSettings[activeLevel+1].levelStartingPoint-levelSettings[activeLevel].levelStartingPoint);
            result = Mathf.Lerp(levelSettings[activeLevel].startingChallengeProb, levelSettings[activeLevel].endingChallengeProb, percentage);
        }
        else
        {
            float percentage = (nextNormalModePosition.x - levelSettings[activeLevel].levelStartingPoint)
                / (levelSettings[activeLevel].levelStartingPoint);
            result = Mathf.Lerp(levelSettings[activeLevel].startingChallengeProb, levelSettings[activeLevel].endingChallengeProb, percentage);
        }

        return result;
    }

    //this function increase the number of obstacles as the level progresses
    private float IncreaseObstacleDifficulty()
    {
        float result = 0f;
        if (activeLevel + 1 < levelSettings.Length)
        {
            float percentage = (nextNormalModePosition.x - levelSettings[activeLevel].levelStartingPoint)
                / (levelSettings[activeLevel + 1].levelStartingPoint - levelSettings[activeLevel].levelStartingPoint);
            result = Mathf.Lerp(levelSettings[activeLevel].startingObstacleProb, levelSettings[activeLevel].endingObstacleProb, percentage);
        }
        else
        {
            float percentage = (nextNormalModePosition.x - levelSettings[activeLevel].levelStartingPoint)
                / (levelSettings[activeLevel].levelStartingPoint);
            result = Mathf.Lerp(levelSettings[activeLevel].startingObstacleProb, levelSettings[activeLevel].endingObstacleProb, percentage);
        }

        return result;
    }


    //function to check if the level is changed
    private bool ChangeLevel()
    {
        bool levelChanged = false;
        while (activeLevel + 1 < levelSettings.Length && levelSettings[activeLevel + 1].levelStartingPoint < nextNormalModePosition.x)
        {
                activeLevel++;
                if (levelSettings[activeLevel].spawnCampsite)
                    isCampsite = true;
                levelChanged = true;
        }

        if(activeLevel + 1 == levelSettings.Length)
        {
            if (levelSettings[activeLevel].levelStartingPoint + (campsiteCounts + 1) * campsiteDistance < nextNormalModePosition.x)
            {
                if (levelSettings[activeLevel].spawnCampsite)
                {
                    isCampsite = true;
                    campsiteCounts++;
                 
                }

            }
        }
        return levelChanged;
        
    }


    private int[] GetCampsitesPosition()
    {
        int size = 0;
        
        for (int i = activeLevel+1; i < levelSettings.Length; i++)
        {
            if (levelSettings[i].spawnCampsite)
                size++;
        }

        int[] results = new int[size];
        int index = 0;

        for (int i = activeLevel+1; i < levelSettings.Length; i++)
        {
            if (levelSettings[i].spawnCampsite)
            {
                results[index] = levelSettings[i].levelStartingPoint; 
              //  Debug.Log("campsite number " + index +" position = " + results[index]);
                index++;
            }
                
        }
      //  Debug.Log("campsites number " +results.Length);
        return results;
    }


    private void SpawnRoadSign(Vector3 prevPosition)
    {
        Vector3 roadSignPosition = Vector3.zero;
        GameObject roadSign = null;
        int campsitePosition = 0;

        //if there are no campsites (even in the last level) return;
        if (campsitePositions.Length == 0 && !levelSettings[levelSettings.Length-1].spawnCampsite)
            return;
        //there are campsites to spawn before the last level
        if (activeCampsite < campsitePositions.Length)
            campsitePosition = campsitePositions[activeCampsite];
        //campsites to spawn in the last level
        else if (activeLevel == levelSettings.Length-1)
            campsitePosition = levelSettings[activeLevel].levelStartingPoint + (campsiteCounts + 1) * campsiteDistance;

        LayerMask layer = 1 << 31;
        if (prevPosition.x <= campsitePosition - 300 && nextSpawnPosition.x >= campsitePosition - 300)
        {
            float roadSignOffset = 0;      
            if (isChallenge)
                roadSignOffset = prevPosition.x;
            else
                roadSignOffset = (campsitePosition - 300) - prevPosition.x; 

            roadSign = ProceduralDatabaseManager.Instance.GetOtherObject(roadSign300);
            RaycastHit hit;
            Physics.Raycast(prevPosition + nextPrefab.renderer.bounds.extents.y * Vector3.up + roadSignOffset * Vector3.right, -Vector3.up, out hit, 100 ,layer);
            roadSign.transform.Translate(hit.point, Space.World);
            Vector3 offset = roadSign.GetComponent<Recyclable>().GetPositionOffset();
            roadSign.transform.Translate(offset);
            SetParentLevel(roadSign);
        }
        if (prevPosition.x <= campsitePosition - 200 && nextSpawnPosition.x >= campsitePosition - 200)
        {
            float roadSignOffset = 0;
            if (isChallenge)
                roadSignOffset = prevPosition.x;
            else
                roadSignOffset = (campsitePosition - 200) - prevPosition.x; 
            roadSign = ProceduralDatabaseManager.Instance.GetOtherObject(roadSign200);
            RaycastHit hit;
            Physics.Raycast(prevPosition + nextPrefab.renderer.bounds.extents.y * Vector3.up + roadSignOffset * Vector3.right, -Vector3.up, out hit, 100,layer);
            roadSign.transform.Translate(hit.point, Space.World);
            Vector3 offset = roadSign.GetComponent<Recyclable>().GetPositionOffset();
            roadSign.transform.Translate(offset);
            SetParentLevel(roadSign);
        }
        if (prevPosition.x <= campsitePosition - 100 && nextSpawnPosition.x >= campsitePosition - 100)
        {
            float roadSignOffset = 0;
            if (isChallenge)
                roadSignOffset = prevPosition.x;
            else
                roadSignOffset = (campsitePosition - 100) - prevPosition.x; 
            roadSign = ProceduralDatabaseManager.Instance.GetOtherObject(roadSign100);
            RaycastHit hit;
            Physics.Raycast(prevPosition + nextPrefab.renderer.bounds.extents.y * Vector3.up + roadSignOffset * Vector3.right, -Vector3.up, out hit,100, layer);
            roadSign.transform.Translate(hit.point, Space.World);
            Vector3 offset = roadSign.GetComponent<Recyclable>().GetPositionOffset();
            roadSign.transform.Translate(offset);
            SetParentLevel(roadSign);

        }
    }
    //this function finds the position for placing the next block

    private Vector3 GetNextPosition(GameObject prefab)
    {

        Vector3 newPosition = prefab.GetComponent<Recyclable>().GetPositionOffset();
       // newPosition.x = prefab.renderer.bounds.size.x;
        //if (!feverState)
        //{
            nextNormalModePosition += newPosition;
            return nextNormalModePosition;
        //}
        //else 
        //{
        //    feverNextPos += newPosition;
        //    return feverNextPos;
        //}

    }

    //function to spawn a campsite
    private void SpawnCampsite()
    {
        nextPrefab = campsitePrefab;
        isChallenge = false;
        
//        Debug.Log(startPrefab.transform.name);
        if (campsitePrefab != null)
        {
            terrainAfterChallengeCount += nextPrefab.GetComponent<Recyclable>().BlocksLength;
            if (terrainAfterChallengeCount >= terrainAfterChallenge)
                isPrevChallenge = false;
        //    obj = Instantiate(startPrefab, nextSpawnPosition, Quaternion.identity) as GameObject;
            obj = ProceduralDatabaseManager.Instance.GetOtherObject(nextPrefab);
            obj.transform.Translate(nextSpawnPosition, Space.World);
            SetParentLevel(obj);
            RaycastHit hit;
            Physics.Raycast(nextSpawnPosition + obj.renderer.bounds.extents.y * Vector3.up + obj.renderer.bounds.extents.x * Vector3.right, -Vector3.up, out hit);
            surfaceNormal = hit.normal;
            surfaceCentre = hit.point;
        }
        isCampsite = false;
        activeCampsite++;
        
    }

    private void SpawnTerrain()
    {
        nextPrefab = ProceduralDatabaseManager.Instance.GetTerrain(activeLevel);//, feverState);
        
        isChallenge = false;
        
        if (nextPrefab != null)
        {
          //  obj = Instantiate(startPrefab, nextSpawnPosition, Quaternion.identity) as GameObject;
            terrainAfterChallengeCount += nextPrefab.GetComponent<Recyclable>().BlocksLength;
            if (terrainAfterChallengeCount >= terrainAfterChallenge)
                isPrevChallenge = false;
            obj = nextPrefab;
            obj.transform.Translate(nextSpawnPosition, Space.World);
         //   Debug.Log(startPrefab.transform.position);
            SetParentLevel(obj, "terrain");
            RaycastHit hit;
            Physics.Raycast(nextSpawnPosition + obj.renderer.bounds.extents.y * Vector3.up + obj.renderer.bounds.extents.x * Vector3.right, -Vector3.up, out hit);
            surfaceNormal = hit.normal;
            surfaceCentre= hit.point;
        }
        
    }

    private void SpawnChallenge()
    {
        nextPrefab = ProceduralDatabaseManager.Instance.GetChallenge(activeLevel);//, feverState);
        isChallenge = true;
        isPrevChallenge = true;
        terrainAfterChallengeCount = 0;
        if (nextPrefab != null)
        {
        //    obj = Instantiate(startPrefab, nextSpawnPosition, Quaternion.identity) as GameObject;
            obj = nextPrefab;
            obj.transform.Translate(nextSpawnPosition, Space.World);
            SetParentLevel(obj, "terrain");
            RaycastHit hit;
            Physics.Raycast(nextSpawnPosition + obj.renderer.bounds.extents.y * Vector3.up + obj.renderer.bounds.extents.x * Vector3.right, -Vector3.up, out hit);
            surfaceNormal = hit.normal;
            surfaceCentre = hit.point;
            
        }
    }

    private void SpawnObstacle()
    {
        spawnableObject = ProceduralDatabaseManager.Instance.GetObstacle(activeLevel);//, feverState);
        if (spawnableObject != null)
        {
            Vector3 offset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset();
            currencyOffset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset().y + spawnableObject.renderer.bounds.size.y;
            Quaternion rotation = Quaternion.identity;
            if (spawnableObject.GetComponent<Recyclable>().followTerrain)
                rotation = Quaternion.FromToRotation(spawnableObject.transform.up, surfaceNormal);

           // obj = Instantiate(spawnableObject, surfaceCentre, rotation) as GameObject;
            obj = spawnableObject;
            obj.transform.Translate(surfaceCentre, Space.World);
            obj.transform.Rotate(rotation.eulerAngles, Space.World);
            obj.transform.Translate(offset);
         //   obj.transform.Rotate(objRotation, Space.World);
            SetParentLevel(obj);
        }
    }

    private void SpawnSoftCurrency()
    {
        spawnableObject = ProceduralDatabaseManager.Instance.GetSoftCurrency(activeLevel);//, feverState);
        if (spawnableObject != null)
        {
            Vector3 offset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset() + new Vector3(0, currencyOffset, 0);
            Quaternion rotation = Quaternion.identity;
            if (spawnableObject.GetComponent<Recyclable>().followTerrain)
                rotation = Quaternion.FromToRotation(spawnableObject.transform.up, surfaceNormal);

          //  obj = Instantiate(spawnableObject, surfaceCentre, rotation) as GameObject;
            obj = spawnableObject;
            obj.transform.Translate(surfaceCentre, Space.World);
            obj.transform.Rotate(rotation.eulerAngles, Space.World);
            obj.transform.Translate(offset);
         //   obj.transform.Rotate(obj.transform.up, spawnableObject.transform.rotation.eulerAngles.y, Space.World);
            SetParentLevel(obj);
        }
    }

    private void SpawnHardCurrency()
    {
        spawnableObject = ProceduralDatabaseManager.Instance.GetHardCurrency(activeLevel);//, feverState);
        if (spawnableObject != null)
        {
            Vector3 offset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset();
            Quaternion rotation = Quaternion.identity;
            if (spawnableObject.GetComponent<Recyclable>().followTerrain)
                rotation = Quaternion.FromToRotation(spawnableObject.transform.up, surfaceNormal);

        //    obj = Instantiate(spawnableObject, surfaceCentre, rotation) as GameObject;
            obj = spawnableObject;
            obj.transform.Translate(surfaceCentre, Space.World);
            obj.transform.Rotate(rotation.eulerAngles, Space.World);
            obj.transform.Translate(offset);
        //    obj.transform.Rotate(obj.transform.up, spawnableObject.transform.rotation.eulerAngles.y, Space.World);
            SetParentLevel(obj);
        }
    }

    private void SpawnCollectable()
    {
        spawnableObject = ProceduralDatabaseManager.Instance.GetCollectable(RankManager.Instance.currentRank);
        if (spawnableObject != null)
        {
            Vector3 offset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset();
            Quaternion rotation = Quaternion.identity;
            if (spawnableObject.GetComponent<Recyclable>().followTerrain)
                rotation = Quaternion.FromToRotation(spawnableObject.transform.up, surfaceNormal);

            //obj = Instantiate(spawnableObject, surfaceCentre, rotation) as GameObject;
            obj = spawnableObject;
            obj.transform.Translate(surfaceCentre, Space.World);
            obj.transform.Rotate(rotation.eulerAngles, Space.World);
            obj.transform.Translate(offset);
            //obj.transform.Rotate(obj.transform.up, spawnableObject.transform.rotation.eulerAngles.y, Space.World);
            SetParentLevel(obj);
        }

    }

    private void SpawnTrappedAnimal()
    {

        if (lastAnimalSpawnPosition.x + animalDistance > surfaceCentre.x)
            return;


        spawnableObject = ProceduralDatabaseManager.Instance.GetTrappedAnimal(RankManager.Instance.currentRank);

        if (spawnableObject != null)
        {
            Vector3 offset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset();
            Quaternion rotation = Quaternion.identity;
            if (spawnableObject.GetComponent<Recyclable>().followTerrain)
                rotation = Quaternion.FromToRotation(spawnableObject.transform.up, surfaceNormal);

           // obj = Instantiate(spawnableObject, surfaceCentre, rotation) as GameObject;
            obj = spawnableObject;
            obj.transform.Translate(surfaceCentre, Space.World);
            obj.transform.Rotate(rotation.eulerAngles, Space.World);
            obj.transform.Translate(offset);
          //  obj.transform.Rotate(obj.transform.up, spawnableObject.transform.rotation.eulerAngles.y, Space.World);
            SetParentLevel(obj);
            lastAnimalSpawnPosition = surfaceCentre;
        }
    }

    private void SpawnBackgroundObject()
    {
        spawnableObject = ProceduralDatabaseManager.Instance.GetBackgrounObject(activeLevel);//, feverState);
        if (spawnableObject != null)
        {
            Vector3 offset = spawnableObject.GetComponent<Recyclable>().GetPositionOffset();
            Quaternion rotation = Quaternion.identity;

            if (spawnableObject.GetComponent<Recyclable>().followTerrain)
                rotation = Quaternion.FromToRotation(spawnableObject.transform.up, surfaceNormal);

           // obj = Instantiate(spawnableObject, surfaceCentre, rotation) as GameObject;
            obj = spawnableObject;
            obj.transform.Translate(surfaceCentre, Space.World);
            obj.transform.Rotate(rotation.eulerAngles, Space.World);
            obj.transform.Translate(offset);   
         //   obj.transform.Rotate(obj.transform.up, spawnableObject.transform.rotation.eulerAngles.y, Space.World);
            SetParentLevel(obj, "background");
        }
    }

    private void SpawnChaseTrigger()
    {
        Vector3 spawnPosition = surfaceCentre + startingChaseTrigger.GetComponent<Recyclable>().GetPositionOffset();
       // GameObject objTemp = Instantiate(startingChaseTrigger, spawnPosition, Quaternion.identity) as GameObject;
        GameObject objTemp = ProceduralDatabaseManager.Instance.GetOtherObject(startingChaseTrigger);
        objTemp.transform.Translate(spawnPosition, Space.World);
        SetParentLevel(objTemp);
        lastChaseSpeed = levelSettings[activeLevel].chaseSpeedMultiplier;
        objTemp.GetComponent<ChaseTrigger>().SetSpeed(lastChaseSpeed);

    }

    private void SpawnChaseEnd()
    {
        Vector3 spawnPosition = surfaceCentre + endingChaseTrigger.GetComponent<Recyclable>().GetPositionOffset();
       // GameObject obj = Instantiate(endingChaseTrigger, spawnPosition, Quaternion.identity) as GameObject;
        GameObject objTemp = ProceduralDatabaseManager.Instance.GetOtherObject(endingChaseTrigger);
        objTemp.transform.Translate(spawnPosition, Space.World);
        SetParentLevel(objTemp);
        objTemp.GetComponent<ChaseTriggerEnd>().SetSpeedFactor(lastChaseSpeed);

    }

    //public void SpawnFeverEndTrigger()
    //{
        
    //    if (feverEndTrigger != null)
    //    {
    //        Vector3 offset = feverEndTrigger.GetComponent<Recyclable>().GetPositionOffset();
    //        Quaternion rotation = Quaternion.identity;

    // //       obj = Instantiate(feverEndTrigger, nextSpawnPosition + offset, rotation) as GameObject;
    //        obj = ProceduralDatabaseManager.Instance.GetOtherObject(feverEndTrigger);
    //        obj.transform.Translate(nextSpawnPosition + offset, Space.World);
    //        SetParentLevel(obj);
    //    } 
    //}


    public void DecreaseCount()
    {
        //if (!feverState)
            count--;
        //else
        //    feverBlockCount--;
    }

    public void SetParentLevel(GameObject obj, string objType = "")
    {
        if (objType == "terrain")
            obj.transform.parent = terrain.transform;
        else if (objType == "background")
            obj.transform.parent = background.transform;
        else
            obj.transform.parent = proceduralLevel.transform;
        //else if (objType == "terrain")
        //    obj.transform.parent = feverTerrain.transform;
        //else if (objType == "background")
        //    obj.transform.parent = feverBackground.transform;
        //else
        //    obj.transform.parent = feverLevel.transform;
    }

    public float GetMaxSpeed()
    {
        if (levelSettings[scoutLevel].maxSpeedMultiplier == 0)
            return levelSettings[scoutLevel].chaseSpeedMultiplier;
        return levelSettings[scoutLevel].maxSpeedMultiplier;
    }

    public void CheckScoutLevel()
    {
        if (scoutLevel + 1 < levelSettings.Length)
        {
            if (levelSettings[scoutLevel + 1].levelStartingPoint < GameManager.Instance.troop.GetScoutByOrder(0).transform.position.x)
            {
                scoutLevel++;

                GameManager.Instance.UpdateLevelSpeed(levelSettings[scoutLevel].levelSpeedMultiplier);
            }
        }

    }

}
