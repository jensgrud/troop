﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolManager
{


    private static GameObject Pool;
    private Dictionary<string, Pool> poolDictionary;


    public PoolManager(ProceduralDatabaseManager.PoolObject[] poolInfo)
    {
        Pool = new GameObject("Pool");
        poolDictionary = new Dictionary<string, Pool>();
        for (int i = 0; i < poolInfo.Length; i++)
        {
            Pool pool = new Pool(poolInfo[i].ObjectPooled, poolInfo[i].poolSize);
            poolDictionary.Add(poolInfo[i].ObjectPooled.name, pool);
        }
    }

    public static void SetPoolParent(GameObject obj)
    {
        obj.transform.parent = Pool.transform;
    }

    public GameObject GetObject(GameObject obj)
    {
        if (obj == null)
            return null;
        Pool temp;
       // poolDictionary.TryGetValue(obj.transform.name, out temp);
        if (poolDictionary.TryGetValue(obj.transform.name, out temp))
        {
            return temp.GetObject();
        }
        else
        {
            GameObject result = MonoBehaviour.Instantiate(obj, Vector3.zero, obj.transform.rotation) as GameObject;
            result.name = obj.name;
            SetPoolParent(result);
            return result;
        }      
    }

    public void Recycle(GameObject obj)
    {
        obj.isStatic = false;
        Pool temp;
      //  Debug.Log( poolDictionary.TryGetValue(obj.transform.name, out temp));
        if (poolDictionary.TryGetValue(obj.transform.name, out temp))
        {

            temp.RecycleObject(obj);
        }
        else
        {

            GameObject.Destroy(obj);
        }    
    }
}
