﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProceduralDatabaseManager : MonoBehaviour
{
    public static ProceduralDatabaseManager Instance
    {
        get;
        private set;
    }

    [System.Serializable]
    public class ProceduralBlock
    {
        [SerializeField]
        private string Name = "Obstacle";
        public GameObject prefab;
        public float spawnProbability;
    }

    [System.Serializable]
    public class Level
    {
        [SerializeField]
        private string Name = "Level";

        public List<ProceduralBlock> terrainBlocks;
        public List<ProceduralBlock> challengeBlocks;
        public List<ProceduralBlock> obstacles;
    //    public List<ProceduralBlock> collectables;
        public List<ProceduralBlock> softCurrencies;
        public List<ProceduralBlock> hardCurrencies;
    //    public List<Rank> animals;
        public List<ProceduralBlock> backgroundObject;
    }

    //[System.Serializable]
    //public class Fever
    //{
    //    [SerializeField]
    //    private string Name = "Fevel";

    //    public List<ProceduralBlock> terrainBlocks;
    //    public List<ProceduralBlock> softCurrencies;
    //    public List<ProceduralBlock> hardCurrencies;
    //    //    public List<Rank> animals;
    //    public List<ProceduralBlock> backgroundObject;
    //}

    [System.Serializable]
    public class Rank
    {
        [SerializeField]
        private string Name = "Rank";
        public List<ProceduralBlock> trappedAnimals;
        public List<ProceduralBlock> collectables;
    }

    [System.Serializable]
    public class PoolObject
    {
        [SerializeField]
        private string Name = "Rank";
        public GameObject ObjectPooled;
        public int poolSize;
    }
    public Level[] Levels;
 //   public Fever FeverMode;
    public Rank[] Ranks;
    public PoolObject[] PoolDatabase;
    private PoolManager poolManager;
    //public Level[] lvlTerrainProgression;
    //public Level[] lvlObstacleProgression;
    //public Level[] lvlCollectableProgression;
    //public Level[] lvlSoftCurrencyProgression;
    //public Level[] lvlHardCurrencyProgression;

    void Awake()
    {
        Instance = this;
        poolManager = new PoolManager(PoolDatabase);
    }


    //return one of the terrain blocks available in level. The block is chosen randomly
    public GameObject GetTerrain(int level)//, bool feverState)
    {

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;
        Level currentLevel = Levels[level];
        //if (!feverState)
        //{
            for (int i = 0; i < currentLevel.terrainBlocks.Count; i++)
            {
                if (rnd < currentLevel.terrainBlocks[i].spawnProbability + offset)
                {
                    result = poolManager.GetObject(currentLevel.terrainBlocks[i].prefab);
                    break;
                }
                offset += currentLevel.terrainBlocks[i].spawnProbability;
            }
        //}
        //else
        //{
        //    for (int i = 0; i < FeverMode.terrainBlocks.Count; i++)
        //    {
        //        if (rnd < FeverMode.terrainBlocks[i].spawnProbability + offset)
        //        {
        //            result = poolManager.GetObject(FeverMode.terrainBlocks[i].prefab);
        //            break;
        //        }
        //        offset += FeverMode.terrainBlocks[i].spawnProbability;
        //    }
        //}



        return result;
    }

    //return one of the challenge blocks available in level. The block is chosen randomly
    public GameObject GetChallenge(int level)//, bool feverState)
    {

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;
        Level currentLevel;

        currentLevel = Levels[level];


        for (int i = 0; i < currentLevel.challengeBlocks.Count; i++)
        {
            if (rnd < currentLevel.challengeBlocks[i].spawnProbability + offset)
            {
                result = poolManager.GetObject(currentLevel.challengeBlocks[i].prefab);
                break;                
            }
            offset += currentLevel.challengeBlocks[i].spawnProbability;
        }

        return result;
    }

    //return one of the obstacles available in level. The block is chosen randomly
    public GameObject GetObstacle(int level)//, bool feverState)
    {

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;
        Level currentLevel;

        currentLevel = Levels[level];

        for (int i = 0; i < currentLevel.obstacles.Count; i++)
        {
            if (rnd < currentLevel.obstacles[i].spawnProbability + offset)
            {
                result = poolManager.GetObject(currentLevel.obstacles[i].prefab);
                break;
            }
            offset += currentLevel.obstacles[i].spawnProbability;
        }
        return result;
    }

    //return one of the collectable objects available in level. The block is chosen randomly
    //public GameObject GetCollectable(int level, bool feverState)
    //{

    //    float rnd = Random.Range(0f, 1f);
    //    float offset = 0f;
    //    GameObject result = null;
    //    Level currentLevel;

    //    currentLevel = Levels[level];


    //    for (int i = 0; i < currentLevel.collectables.Count; i++)
    //    {
    //        if (rnd < currentLevel.collectables[i].spawnProbability + offset)
    //        {
    //            result = poolManager.GetObject(currentLevel.collectables[i].prefab);
    //            break;
    //        }
    //        offset += currentLevel.collectables[i].spawnProbability;
    //    }
    //    return result;
    //}

    //return one of the soft currency available in level. The block is chosen randomly
    public GameObject GetSoftCurrency(int level)//, bool feverState)
    {

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;
        Level currentLevel = Levels[level];
        //if (!feverState)
        //{
            for (int i = 0; i < currentLevel.softCurrencies.Count; i++)
            {
                if (rnd < currentLevel.softCurrencies[i].spawnProbability + offset)
                {
                    result = poolManager.GetObject(currentLevel.softCurrencies[i].prefab);
                    break;
                }
                offset += currentLevel.softCurrencies[i].spawnProbability;
            }
        //}
        //else
        //{
        //    for (int i = 0; i < FeverMode.softCurrencies.Count; i++)
        //    {
        //        if (rnd < FeverMode.softCurrencies[i].spawnProbability + offset)
        //        {
        //            result = poolManager.GetObject(FeverMode.softCurrencies[i].prefab);
        //            break;
        //        }
        //        offset += FeverMode.softCurrencies[i].spawnProbability;
        //    }
        //}

        return result;
    }

    //return one of the hard currency available in level. The block is chosen randomly
    public GameObject GetHardCurrency(int level)//, bool feverState)
    {

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;
        Level currentLevel = Levels[level];
        //if (!feverState)
        //{
            for (int i = 0; i < currentLevel.hardCurrencies.Count; i++)
            {
                if (rnd < currentLevel.hardCurrencies[i].spawnProbability + offset)
                {
                    result = poolManager.GetObject(currentLevel.hardCurrencies[i].prefab);
                    break;
                }
                offset += currentLevel.hardCurrencies[i].spawnProbability;
            }

        //}
        //else
        //{
        //    for (int i = 0; i < FeverMode.hardCurrencies.Count; i++)
        //    {
        //        if (rnd < FeverMode.hardCurrencies[i].spawnProbability + offset)
        //        {
        //            result =poolManager.GetObject( FeverMode.hardCurrencies[i].prefab);
        //            break;
        //        }
        //        offset += FeverMode.hardCurrencies[i].spawnProbability;
        //    }
        //}


        return result;
    }

    //return one of the hard currency available in level. The block is chosen randomly
    public GameObject GetTrappedAnimal(int rank)
    {
		if (rank < 0)
		{
			return null;
		}

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;

        for (int i = 0; i < Ranks[rank].trappedAnimals.Count; i++)
        {
            if (rnd < Ranks[rank].trappedAnimals[i].spawnProbability + offset)
            {
                result = poolManager.GetObject(Ranks[rank].trappedAnimals[i].prefab);
                break;
            }
            offset += Ranks[rank].trappedAnimals[i].spawnProbability;
        }
        if (result != null && result.GetComponent<PetUnlocker>() != null)
        {
            if (PetsManager.Instance.isPetUnlocked(result.GetComponent<PetUnlocker>().PetToUnlock))
            {
                Recycle(result);
                return null;
            }
                
        }
        return result;
    }

    public GameObject GetCollectable(int rank)
    {

        if (rank < 0)
        {
            return null;
        }

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;

        for (int i = 0; i < Ranks[rank].collectables.Count; i++)
        {
            if (rnd < Ranks[rank].collectables[i].spawnProbability + offset)
            {
                result = poolManager.GetObject(Ranks[rank].collectables[i].prefab);
                break;
            }
            offset += Ranks[rank].collectables[i].spawnProbability;
        }
        return result;
    }

    public GameObject GetBackgrounObject(int level)//, bool feverState)
    {

        float rnd = Random.Range(0f, 1f);
        float offset = 0f;
        GameObject result = null;
        Level currentLevel = Levels[level];
        //if (!feverState)
        //{
            for (int i = 0; i < currentLevel.backgroundObject.Count; i++)
            {
                if (rnd < currentLevel.backgroundObject[i].spawnProbability + offset)
                {
                    result = poolManager.GetObject(currentLevel.backgroundObject[i].prefab);
                    break;
                }
                offset += currentLevel.backgroundObject[i].spawnProbability;
            }
        //}
        //else
        //{
        //    for (int i = 0; i < FeverMode.backgroundObject.Count; i++)
        //    {
        //        if (rnd < FeverMode.backgroundObject[i].spawnProbability + offset)
        //        {
        //            result = poolManager.GetObject(FeverMode.backgroundObject[i].prefab);
        //            break;
        //        }
        //        offset += FeverMode.backgroundObject[i].spawnProbability;
        //    }
        //}

        return result;
    }

    public GameObject GetOtherObject(GameObject obj)
    {
       return poolManager.GetObject(obj);
    }

    public void Recycle(GameObject obj)
    {
        poolManager.Recycle(obj);
    }
}
