﻿using UnityEngine;
using System.Collections;

public class Recyclable : MonoBehaviour
{
    public int BlocksLength = 1;
    public int numberOfBlocks = 1;
    protected float distanceFromCamera;
    protected float defaultDistance = 30f;
    public Vector3 positionOffset = Vector3.zero;
    public bool followTerrain = false;
    public bool isRecyclable = true;
    protected Camera mainCam = Camera.main;
    protected int defaultBlockSize = 9;
    protected Recyclable[] children; 
	// Use this for initialization
	void Awake () {
        distanceFromCamera = defaultDistance + (BlocksLength-1)*defaultBlockSize;
        children = gameObject.GetComponentsInChildren<Recyclable>(true);
	}
	
	// Update is called once per frame
	void Update () {
        bool cameraTest = (transform.position.x + distanceFromCamera < mainCam.transform.position.x);
        if (cameraTest)
            StartCoroutine(Recycle());
      
	}

    public Vector3 GetPositionOffset()
    {
        return positionOffset;
    }


    public IEnumerator Recycle()
    {

   //     bool cameraTest = (transform.position.x + distanceFromCamera < mainCam.transform.position.x);
        if (isRecyclable)// && cameraTest)
        {
            ProceduralDatabaseManager.Instance.Recycle(this.gameObject);
           // Object.Destroy(this.gameObject);
        }
        else if (!isRecyclable)// && cameraTest)
        {

            //gameObject.SetActive(false);
         //   Recyclable[] allRecyclables = gameObject.GetComponentsInChildren<Recyclable>(true);

            foreach (Recyclable child in children)
            {
                child.gameObject.SetActive(false);
            }

        }
        yield break;
    }

    void OnEnable()
    {

    }
}
