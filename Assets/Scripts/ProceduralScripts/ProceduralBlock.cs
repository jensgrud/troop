﻿using UnityEngine;
using System.Collections;


public class ProceduralBlock : Recyclable{

    public bool allowSpawn = true;
    //public bool isGroup = false;
    

    void Update()
    {
        bool cameraTest = (transform.position.x + distanceFromCamera < mainCam.transform.position.x);
        if(cameraTest)
            StartCoroutine(Recycle());

    }

    public IEnumerator Recycle()
    {
     //   bool cameraTest = (transform.position.x + distanceFromCamera < mainCam.transform.position.x);
//        Debug.Log("name " + gameObject.name + " distance " + distanceFromCamera);
        if (isRecyclable)// && cameraTest)
        {
            ProceduralDatabaseManager.Instance.Recycle(this.gameObject);
            ProceduralManager.Instance.DecreaseCount();
        }
        else if (!isRecyclable)// && cameraTest)
        {
           // Recyclable[] allRecyclables = gameObject.GetComponentsInChildren<Recyclable>();
            foreach (Recyclable child in children)
            {
                child.gameObject.SetActive(false);
            }
            ProceduralManager.Instance.DecreaseCount();
        }
        yield break;
    }

    void OnEnable()
    {

    }
}
