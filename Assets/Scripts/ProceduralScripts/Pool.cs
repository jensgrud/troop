﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool{

    private Stack pool;
    private GameObject obj;
    private int maxSize;


    // Create a pool of storedObject
    public Pool(GameObject storedObject, int size)
    {
        pool = new Stack();
        obj = storedObject;
        maxSize = size;
        for (int i = 0; i < maxSize; i++)
        {
            GameObject temp = MonoBehaviour.Instantiate(obj, Vector3.zero, obj.transform.rotation) as GameObject;
            temp.name = obj.name;
            temp.isStatic = false;
            PoolManager.SetPoolParent(temp);
            temp.SetActive(false);
            pool.Push(temp);
        }
    }

    public GameObject GetObject()
    {
        GameObject result;
        if (pool.Count != 0)
        {
            result = pool.Pop() as GameObject;
            result.SetActive(true);
            Recyclable[] allRecyclables = result.GetComponentsInChildren<Recyclable>(true);
            foreach (Recyclable child in allRecyclables)
            {
//                Debug.Log(child.name);
                child.gameObject.SetActive(true);
            }
        }

        else
        {
            result = MonoBehaviour.Instantiate(obj, Vector3.zero, obj.transform.rotation) as GameObject;
            result.name = obj.name;
        }

        return result;
    }

    public void RecycleObject(GameObject recObj)
    {

        if (pool.Count == maxSize)
        {
            GameObject.Destroy(recObj);
            return;
        }
            
        pool.Push(recObj);
        recObj.SetActive(false);
        PoolManager.SetPoolParent(recObj);
        recObj.transform.position = Vector3.zero;
        recObj.transform.rotation = obj.transform.rotation;
        
    }



}
