﻿using UnityEngine;
using System.Collections;

public class PowerUpFox : PowerUp {

    public float absorptionSpeed;
    public float absorptionRadius;
    public float absorptionTopSpeed;
    public float absorptionAcceleration;
    public float Duration
    {
        get { return duration; }
        set { duration = value; }
    }

    public override void ToggleEffect()
    {
        GameManager.Instance.ToggleMagnet(absorptionSpeed, absorptionRadius, absorptionAcceleration, absorptionTopSpeed);
        if (isToggled)
        {
            ResetDuration();

            //consume the powerup
            PowerUpsManager.Instance.PowerUpAmounts[Id] = 0;
        }
        else
        {
            //if (GameManager.Instance.troop.pet != null && GameManager.Instance.troop.pet.Id == this.Id)
            //{
                duration += 3;
            //}
        }
        isToggled = !isToggled;
    }

    public override void ResetDuration()
    {
        Duration = durationMax;
    }

}
