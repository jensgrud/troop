﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PowerUpsManager : MonoBehaviour
{

    #region Singleton
    public static PowerUpsManager Instance
    {
        get;
        private set;
    }
    #endregion

    [HideInInspector]
    public enum PowerUpOrder {None = -1, PowerUpTurtle, PowerUpFox, PowerUpEagle}

    public delegate void UsePowerupEventHandler(object sender, PowerUp powerUp);
    public event UsePowerupEventHandler OnPowerupUsed;

    public bool DebugMode = false;

    public List<PowerUp> PowerUps;
    public List<int> PowerUpAmounts;
    private bool dataWasLoaded = false;

    public struct persistantPowerUpData{
        public List<int> savedPowerUpAmounts;
    }

    #region Unity native methods

    public void Awake()
    {
        Instance = this;
        //PowerUps = new List<PowerUp>();
        /* I dont want to reset all the powerups at awake
        for (int i = 0; i < PowerUpAmounts.Count; i++)
        {
            PowerUps.Add(null);
        }*/
        if (!DebugMode)
        {
            if (PersistentData.SavedDataExists(this))
            {
                PowerUpAmounts = PersistentData.Deserialize<List<int>>(this);
                for (int i = 0; i < PowerUpAmounts.Count; i++)
                {
                    if (PowerUpAmounts[i] > 0)
                    {
                        UnlockPowerUp(i, true);
                    }
                }
                dataWasLoaded = true;
            }
        }
        /* Should be done in editor...
        if (!dataWasLoaded)
        {

            //Automatically instatiated powerups for testing, Remove once GUI is implemented
            PowerUps[0] = new PowerUpFox();
            PowerUps[1] = new PowerUpTurtle();
            PowerUps[2] = new PowerUpEagle();
            //End remove
        }*/
    }

    // Use this for initialization
	void Start () {
        Shop.Instance.AddPowerUpsToPrices();

        //TODO: Possibly remove, this is a bad way of handling serialization
        GameManager.Instance.troop.OnDie += delegate { PersistentData.Serialize(PowerUpAmounts, this); };
        BadgeManager.Instance.OnBadgeEarned += delegate { PersistentData.Serialize(PowerUpAmounts, this); };
		GuiManager.Instance.CampMenu.OnExitCamp += delegate { PersistentData.Serialize(PowerUpAmounts, this); };
        Tutorial.Instance.OnTutorialComplete += delegate { PersistentData.Serialize(PowerUpAmounts, this); };
        Shop.Instance.OnPurches += delegate { PersistentData.Serialize(PowerUpAmounts, this); };
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnDestroy()
    {
        PersistentData.Serialize(PowerUpAmounts, this);
    }
    #endregion

    #region Class Method
    /// <summary>
    /// Returns true if the requested powerup is used. The power up is used if we have it.
    /// </summary>
    /// <param name="powerUp"></param>
    /// <returns></returns>
    public bool UsePowerUp(PowerUp powerUp)
    {
		if (powerUp == null)
			return false;

        if(PowerUpAmounts[powerUp.Id] > 0)
        {
            if (GameManager.Instance.troop.GetActivePowerUp() == null)
            {
                GameManager.Instance.troop.SetPowerUp(powerUp);
                //PowerUpAmounts[powerUp.Id]--;
                //Tell statistic manager
                StatisticsManager.Instance.IterateStatistic(powerUp.statisticUse);
                StatisticsManager.Instance.IterateStatistic(Statistic.UsedPowerups);

                if (OnPowerupUsed != null)
                    OnPowerupUsed(this, powerUp);

                return true;
            }
        }
        return false;
    }
    
    /// <summary>
    /// Returns true when it unlocks a powerup from the provided powerUpId
    /// </summary>
    /// <param name="powerUpId"></param>
    /// <param name="isOnLoad"></param>
    /// <returns></returns>
    public bool UnlockPowerUp(int powerUpId, bool isOnLoad = false)
    {
        foreach (PowerUp powerUp in PowerUps)
        {
            if (powerUp.Id == powerUpId)
            {
                PowerUpAmounts[powerUpId] += 1;
                return true;
            }
        }
        return false;
    }

    
    /// <summary>
    /// Assigns the powerUp to the scout
    /// </summary>
    /// <param name="powerUp"></param>
    /// <param name="scout"></param>
    public void AssignPowerUp(PowerUp powerUp, Scout scout)
    {
        if (PowerUps[powerUp.Id] != null && PowerUpAmounts[powerUp.Id] > 0) 
        {
            GameManager.Instance.troop.currentPowerUp = PowerUps[powerUp.Id];
            //scout.currentPowerUp = PowerUps[powerUp.Id];
        }
    }
    #endregion
}
