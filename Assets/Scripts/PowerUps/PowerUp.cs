﻿using UnityEngine;
using System.Collections;

public abstract class PowerUp : MonoBehaviour  {

    #region fields

    /// <summary>
    /// Name of the powerup 
    /// </summary>
    public string Name;

    /// <summary>
    /// Description of the powerup
    /// </summary>
    public string Description;

    /// <summary>
    /// Price in marshmallows for this powerup
    /// /// </summary>
    public int PriceMarshmallows;

    /// <summary>
    /// Price in firewood for this powerup
    /// </summary>
    public int PriceFirewood;

    /// <summary>
    /// The id of the powerup
    /// </summary>
    public int Id;

    /// <summary>
    /// The statistic that is iterated when the powerup is bought
    /// </summary>
    public Statistic statisticBuy;

    /// <summary>
    /// The statistic that is iterated when the powerup is used
    /// </summary>
    public Statistic statisticUse;

    /// <summary>
    /// The duration in seconds of the powerup
    /// </summary>
    public float duration;

    /// <summary>
    /// The max duration in seconds of the powerup
    /// </summary>
    public float durationMax;

    /// <summary>
    /// True if the powerup is active
    /// </summary>
    public bool isToggled = false;

    #endregion

    public virtual void ToggleEffect() { }

    public virtual void ResetDuration() { }

}
