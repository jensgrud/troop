﻿using UnityEngine;
using System.Collections;

public class PowerUpEagle : PowerUp {

    public float eagleIncrement = 2.0f;
    public float SpeedModifier;

    public float absorptionSpeed;
    public float absorptionRadius;
    public float absorptionTopSpeed;
    public float absorptionAcceleration;

    System.Threading.Timer timer;
    Vector3 boxColliderSize = new Vector3(1, 8, 1);
    Vector3 prevBoxColliderSize;
    Vector3 boxColliderPos = new Vector3(0, -2, 0);
    Vector3 prevBoxColliderPos;

    public float Duration {
        get 
        {
            return duration;
        } 
        set
        {
            duration = value;
	
        }
    }

    public override void ToggleEffect()
    {
        //GameManager.Instance.ToggleSpeedUp();

        if (GameManager.Instance.troop.pet != null && GameManager.Instance.troop.pet.Name == "eagle")
            duration += eagleIncrement;
        

        foreach (Scout s in GameManager.Instance.troop.scouts)
        {
            s.Jump(1);
            s.IsFlying = !s.IsFlying;

            if (prevBoxColliderSize == Vector3.zero)
                prevBoxColliderSize = s.GetComponent<BoxCollider>().size;

            if (prevBoxColliderPos == Vector3.zero)
                prevBoxColliderPos = s.GetComponent<BoxCollider>().center;

        }

        if (isToggled)
        {
            GameManager.Instance.troop.FeatherStormStop();
            GameManager.Instance.DecrementSpeed(SpeedModifier);

            //Makes the troop invincible for two seconds after they hit the ground
            timer = new System.Threading.Timer(obj => ToggleInvincibility(), null, 2000, System.Threading.Timeout.Infinite);

            //resets the duration of the powerup
            ResetDuration();

            //consume the powerup
            PowerUpsManager.Instance.PowerUpAmounts[Id] = 0;
        }
        else
        {
			GameManager.Instance.troop.FeatherStormStart(absorptionSpeed, absorptionRadius, absorptionAcceleration, absorptionTopSpeed);
            GameManager.Instance.AddSpeed(SpeedModifier);
            if (GameManager.Instance.troop.pet != null && GameManager.Instance.troop.pet.Id == this.Id)
            {
                duration += 3;
            }
            ToggleInvincibility();
        }

        isToggled = !isToggled;
    }

    void ToggleInvincibility()
    {
        GameManager.Instance.ToggleChallengeInvicibility();
        GameManager.Instance.ToggleObstacleInvincibility();
    }

    public void ResetDuration()
    {
        Duration = durationMax;
    }
}
