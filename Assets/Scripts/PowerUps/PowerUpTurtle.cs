﻿using UnityEngine;
using System.Collections;

public class PowerUpTurtle : PowerUp {

    public UITexture turtleHazeTexture;
    public float slowdownFactor = 0.5f;

	AudioManager audioMan;

    public float Duration
    {
        get { return duration; }
        set { duration = value; }
    }

    void Start()
    {
        turtleHazeTexture.enabled = false;
		audioMan = AudioManager.Instance;
    }

    public override void ToggleEffect()
    {
        if (isToggled)
        {

            //GameManager.Instance.ResetSpeed();
            GameManager.Instance.ChangeTempTimeMod(slowdownFactor);
            
			audioMan.transform.FindChild("TurtlePowerUpActive").audio.Stop();
            turtleHazeTexture.enabled = false;
            AudioManager.Instance.ToggleTimeShell();
            ResetDuration();

            //consume the powerup
            PowerUpsManager.Instance.PowerUpAmounts[Id] = 0;
        }
        else
        {
            //Give extra duration if the same animal is the pet of the troop
            if (GameManager.Instance.troop.pet != null && GameManager.Instance.troop.pet.Id == this.Id)
            {
                duration += 3;
            }

            //Show the overlay
            turtleHazeTexture.enabled = true;
            //Drop sound pitch

			audioMan.ToggleTimeShell();
			audioMan.PlaySingleSound(gameObject, audioMan.transform.FindChild("TurtlePowerUpStart").gameObject, 1);
			audioMan.PlaySingleSound(gameObject, audioMan.transform.FindChild("TurtlePowerUpActive").gameObject, 1);
			audioMan.transform.FindChild("TurtlePowerUpActive").audio.pitch = 1.25f;
			/*AudioSource src = AudioManager.Instance.transform.FindChild("TurtlePowerUpEnd").audio;
			src.PlayScheduled(AudioSettings.dspTime + duration-3);
			yield return new WaitForSeconds(src.time);
*/
            GameManager.Instance.ChangeTempTimeMod(-slowdownFactor);
            //GameManager.Instance.ScaleSpeed(0.5f);

        }

        isToggled = !isToggled;
    }

    public override void ResetDuration()
    {
        Duration = durationMax;
    }
}
