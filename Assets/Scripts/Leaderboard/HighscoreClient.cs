﻿using System;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections.Generic;

public class HighscoreClient {
	public static HighscoreClient Instance { get; set; }
    public string Server = "http://dadiuteam3.hints.me/index.php";

    WebClient webClient = new WebClient();

    enum QueryType {Select, Insert};

    public HighscoreClient()
    {
        if (Instance == null)
            Instance = this;
    }

    private string Query(QueryType type, string dataStr)
    {
        string addr = Server + "?type=";

        if (type == QueryType.Select)
            addr += "select";
        else if (type == QueryType.Insert)
            addr += "insert";

        addr += dataStr;

        Stream data = webClient.OpenRead(addr);
        StreamReader reader = new StreamReader(data);
        string s = reader.ReadToEnd();
        data.Close();
        reader.Close();
        return s;
    }

    public List<HighscoreEntry> GetData(int limit = -1)
    {
        string[] htmlFilter = {
                                "<body>",
                                "</body>",
                                "<br>"
                             };
        string[] columnSplitter = { "##" };
        string dataStr = "";
        if (limit != -1)
            dataStr = "&limit=" + limit;
        List<HighscoreEntry> list = new List<HighscoreEntry>();

        try
        {
            //Query the data
            string data = Query(QueryType.Select, dataStr);

            //Filter html away
            string[] rows = data.Split(htmlFilter, System.StringSplitOptions.RemoveEmptyEntries);

            //Get rows
            foreach (string row in rows)
            {
                try
                {
                    //Split columns
                    string[] values = row.Split(columnSplitter, System.StringSplitOptions.RemoveEmptyEntries);
                    HighscoreEntry t = new HighscoreEntry();

                    //Skip zero index, as we're not interested in the id
                    t.name = values[1];
                    t.distance = Convert.ToInt32(values[2]);
                    t.badges = Convert.ToInt32(values[3]);
                    t.rank = Convert.ToInt32(values[4]);
                    list.Add(t);
                }
                catch (IndexOutOfRangeException e)
                {
					//to catch it in the GUI and show a nice popup
                    UnityEngine.Debug.Log(e.Message);
					throw new Exception(e.Message);
                }
            }
        }
        catch (WebException e)
        {
			//to catch it in the GUI and show a nice popup
			UnityEngine.Debug.Log(e.Message);
			throw new Exception(e.Message);
        }

        return list;
    }

    public void Insert(HighscoreEntry t)
    {
        string dataInsert = "&data=('" + t.name + "', '" + t.distance + "', '" + t.badges + "', '" + t.rank + "')";
        string query = Query(QueryType.Insert, dataInsert);
    }

    public class HighscoreEntry
    {
        public string name;
        public int distance;
        public int badges;
        public int rank;

        public HighscoreEntry()
        { }

        public HighscoreEntry(string name, int distance, int badges, int rank)
        {
            this.name = name;
            this.distance = distance;
            this.badges = badges;
            this.rank = rank;
        }
    }
}
