﻿using UnityEngine;
using System.Collections.Generic;

public class Leaderboard : MonoBehaviour {
    public static Leaderboard Instance { get; protected set; }

    public GameObject[] rows;
    public GameObject window, frame;
    HighscoreClient client;

	public GameObject ListAnchor;

	// Use this for initialization
	void Awake () {
		if (Instance == null)
			Instance = this;
		Invoke ("DestroyAnchor", 0f);
	}
	public void DestroyAnchor(){
		GameObject.Destroy (ListAnchor);
	}

	public void OnEnable()
	{
		if (HighscoreClient.Instance == null)
			client = new HighscoreClient();
		else
			client = HighscoreClient.Instance;

		try {
			List<HighscoreClient.HighscoreEntry> list = client.GetData(rows.Length);
		
		
			for (int i = 0; i < rows.Length; i++)
				if (i < list.Count)
					PopulateRow(rows[i], list[i]);
			    else
				    break;

		} catch (System.Exception e){
		
			GuiManager.Instance.InitGenericPopup(
				"Cannot Display Highscores",
				GuiManager.Instance.gameObject,
				"HidePopup",
				0
				);
			
			GuiManager.Instance.ShowPopup(GuiManager.PopupType.Info);
			
		}
	}

    void PopulateRow(GameObject r, HighscoreClient.HighscoreEntry data)
    {
		LeaderboardRow row = r.GetComponent<LeaderboardRow>();

        row.Name.text = data.name;
        row.Score.text = data.distance.ToString();
        row.Badges.text = data.badges.ToString() + "/" + BadgeManager.Instance.allBadges.Count;
        row.Rank.text = "RANK " + data.rank.ToString();
    }
	
	public void Facebook(){
		FacebookManager.Instance.UploadScreenShot ();
	}
}
