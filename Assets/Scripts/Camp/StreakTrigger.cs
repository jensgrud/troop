﻿using UnityEngine;
using System.Collections;

public class StreakTrigger : MonoBehaviour {

	public CampSite campsite;
    private bool incrementSet = false;

    void OnTriggerEnter()
    {
        if (!campsite.visited && !incrementSet)
        {
            GameManager.Instance.IncrementStreak();
            incrementSet = true;
        }
    }

    void OnEnable()
    {
        incrementSet = false;
    }
}
