﻿using UnityEngine;
using System.Collections;

public class CampSite : ProceduralBlock {

    public bool visited = false;

    void OnTriggerEnter()
    {
        if (GameManager.Instance.troop.pet != null)
            GameManager.Instance.troop.pet.LeaveTroop(); 
    }

    void OnEnable()
    {
        visited = false;
        Animator[] anims = gameObject.GetComponentsInChildren<Animator>();
        foreach (Animator anim in anims)
        {
            anim.SetTrigger("Reset");
        }
		foreach (ParticleSystem ps in transform.GetComponentsInChildren<ParticleSystem>())
		{
			ps.Stop();
		}

    }
}
