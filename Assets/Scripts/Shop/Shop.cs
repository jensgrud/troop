﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shop : MonoBehaviour
{
    #region Fields
    //Prize -1 is to show that is cannot be bought for the currency
    //Prices on pets, powerups, badges and ranks that can be unlocked with soft currency
    public Dictionary<int, int> PetPricesLogs = new Dictionary<int, int>();
    public Dictionary<int, int> PowerUpPricesLogs = new Dictionary<int, int>();
    public Dictionary<int, int> BadgePricesLogs = new Dictionary<int, int>();
   
    //Prices on pets, powerups, badges and ranks that can be unlocked with hard currency
    public Dictionary<int, int> PetPricesMarshmellows = new Dictionary<int, int>();
    public Dictionary<int, int> PowerUpPricesMarshmallows = new Dictionary<int, int>();
    public Dictionary<int, int> BadgePricesMarshmallows = new Dictionary<int, int>();

    public enum PriceListType { ContinueCosts, RestCosts}
    [System.Serializable]
    public class PriceRelation
    {
        public string name;
        public int limit;
        public int price;
    }
    [System.Serializable]
    public class PriceList
    {
        public string name = "name";
        public PriceListType type;
        public List<PriceRelation> prices;
    }
    public List<PriceList> priceLists;

    public delegate void OnPurchaseHandler(object sender);
    public event OnPurchaseHandler OnPurches;
    #endregion

    #region singleton
    public static Shop Instance
    {
        get;
        private set;
    }
    #endregion

    #region Unity native methos
    void Awake()
    {
        Instance = this;
    }


	// Use this for initialization
	void Start () {
		AudioManager.Instance.transform.FindChild("RestSound").audio.mute = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    #endregion

    #region Class Methods
    /// <summary>
    /// Buy a pet from petId, with either hard or soft currency
    /// </summary>
    /// <param name="petId"></param>
    /// <param name="hardCurrency"></param>
    public void BuyPet(int petId, bool hardCurrency) //to take care of both hard and soft corrency, subject to change :)
    {
        //Check if we use hard currency
        if (hardCurrency)
        {
            //Check if the pet can be bought for hard currency
            if (PetPricesMarshmellows.ContainsKey(petId) && PetPricesMarshmellows[petId] != -1)
            {
                //Check if there is enough hard currency in the wallet.
                if (PlayerWallet.Instance.Marshmallows >= PetPricesMarshmellows[petId])
                {
                    //Unlock the pet and pay for the pet
                    PetsManager.Instance.BuyPet(petId);
                    PlayerWallet.Instance.SpendMarshmallows(PetPricesMarshmellows[petId]);
                    if (OnPurches != null)
                    {
                        OnPurches(this);
                    }
                }
                else
                {
                    //TODO: Make this a message to the user
                    //Debug.Log("Sorry you do not have enough Marshmallows");
                }
            }
            else
            {
                //TODO: Make this a message to the user
                //Debug.Log("Sorry this pet cannot be bought with Marshmallows");
            }
        }
        else
        {
            //Check if the pet can be bought for soft currency
            if (PetPricesLogs.ContainsKey(petId) && PetPricesLogs[petId] != -1)
            {
                //Check if there is enough soft currency in the wallet
                if (PlayerWallet.Instance.TotalLogs >= PetPricesLogs[petId])
                {
                    //Unlock the pet and pay for the pet
                    PetsManager.Instance.BuyPet(petId);
                    PlayerWallet.Instance.SpendLogs(PetPricesLogs[petId]);
                    if (OnPurches != null)
                    {
                        OnPurches(this);
                    }
                }
                else
                {
                    //TODO: Make this a message to the user
                    //Debug.Log("Sorry you do not have enough Logs");
                }
            }
            else
            {
                //TODO: Make this a message to the user
                //Debug.Log("Sorry this pet cannot be bought with Logs");
            }
        }
    }

    /// <summary>
    /// Buy a powerup with either hard of soft currency
    /// </summary>
    /// <param name="powerUpId"></param>
    /// <param name="hardCurrency"></param>
    public void BuyPowerUp(int powerUpId, bool hardCurrency)
    {
        //Check if we use hard currency
        if (hardCurrency)
        {
            //Check if the powerup can be bought for hard currency
            if (PowerUpPricesMarshmallows.ContainsKey(powerUpId) && PowerUpPricesMarshmallows[powerUpId] != -1)
            {
                //Check if there is enough hard currency in the wallet
                if (PlayerWallet.Instance.Marshmallows >= PowerUpPricesMarshmallows[powerUpId])
                {
                    //If the powerup is unlocked it will be payed for
                    if (PowerUpsManager.Instance.UnlockPowerUp(powerUpId))
                    {
                        PlayerWallet.Instance.SpendMarshmallows(PowerUpPricesMarshmallows[powerUpId]);
                        //Tell statistic manager
                        StatisticsManager.Instance.IterateStatistic(PowerUpsManager.Instance.PowerUps[powerUpId].statisticBuy);

                        //Debug.Log("bought powerup: " + PowerUpsManager.Instance.PowerUps[powerUpId]); 
                    }
                }
                else
                {
                    //TODO: Make this a message to the user
                    //Debug.Log("Sorry you do not have enough Marshmallows");
                }
            }
            else
            {
                //TODO: Make this a message to the user
                //Debug.Log("Sorry this powerup cannot be bought with Marshmallows");
            }
        }
        else
        {
            //Check if the powerup can be bought for soft currency
            if (PowerUpPricesLogs.ContainsKey(powerUpId) && PowerUpPricesLogs[powerUpId] != -1)
            {
                //Check if there is enough soft currency in the wallet
                if (PlayerWallet.Instance.TotalLogs >= PowerUpPricesLogs[powerUpId])
                {
                    //If the powerup is unlocked it is paid for
                    if (PowerUpsManager.Instance.UnlockPowerUp(powerUpId))
                    {
                        PlayerWallet.Instance.SpendLogs(PowerUpPricesLogs[powerUpId]);
                        //tell statistic manager
                        StatisticsManager.Instance.IterateStatistic(PowerUpsManager.Instance.PowerUps[powerUpId].statisticBuy);
                    }
                }
                else
                {
                    //TODO: Make this a message to the user
                    //Debug.Log("Sorry you do not have enough Logs");
                }
            }
            else
            {
                //TODO: Make this a message to the user
                //Debug.Log("Sorry this powerup cannot be bought with Logs");
            }
        }

    }

    /// <summary>
    /// Buys a badge from badgeId, with either hard or soft currency
    /// </summary>
    /// <param name="badgeId"></param>
    /// <param name="hardCurrency"></param>
    public void BuyBadge(int badgeId, bool hardCurrency)
    { 
        //check if we use hard currency
        if (hardCurrency)
        {
            //Check if the the badge can be bought for hard currency
            if (BadgePricesMarshmallows.ContainsKey(badgeId) && BadgePricesMarshmallows[badgeId] != -1)
            {
                //Check if there is enough hard currency in the wallet
                if (PlayerWallet.Instance.Marshmallows >= BadgePricesMarshmallows[badgeId])
                {
                    //Buy the badge
                    BadgeManager.Instance.BuyBadge(badgeId);
                    PlayerWallet.Instance.SpendMarshmallows(BadgePricesMarshmallows[badgeId]);
                }
                else
                {
                    //TODO: Make this a message to the user
                    //Debug.Log("Sorry you do not have enough Marshmallows");
                }
            }
            else
            {
                //TODO: Make this a message to the user
                //Debug.Log("Sorry this badge cannot be bought with Marshmallows");
            }
        }
        else
        {
            //Check if the the badge can be bought for soft currency
            if (BadgePricesLogs.ContainsKey(badgeId) && BadgePricesLogs[badgeId] != -1)
            {
                //Check if there is enough soft currency in the wallet
                if (PlayerWallet.Instance.TotalLogs >= BadgePricesLogs[badgeId])
                {
                    //Buy the badge
                    BadgeManager.Instance.BuyBadge(badgeId);
                    PlayerWallet.Instance.SpendLogs(BadgePricesLogs[badgeId]);
                }
                else
                {
                    //TODO: Make this a message to the user
                    //Debug.Log("Sorry you do not have enough Logs");
                }
            }
            else
            {
                //TODO: Make this a message to the user
                //Debug.Log("Sorry this badge cannot be bought with Logs");
            }
        }
    }

    /// <summary>
    /// Adds the pets prices into the dictionaries.
    /// </summary>
    public void AddPetsToPrices()
    {
        foreach (Pet pet in PetsManager.Instance.Pets)
        {
            PetPricesLogs.Add(pet.Id, pet.PriceFirewood);
            PetPricesMarshmellows.Add(pet.Id, pet.PriceMarshmallows);
        }
    }

    /// <summary>
    /// Adds the powerup prices into the dictionaries.
    /// </summary>
    public void AddPowerUpsToPrices()
    {
        foreach (PowerUp powerup in PowerUpsManager.Instance.PowerUps)
        {
            PowerUpPricesLogs.Add(powerup.Id, powerup.PriceFirewood);
            PowerUpPricesMarshmallows.Add(powerup.Id, powerup.PriceMarshmallows);
        }
    }

    /// <summary>
    /// //Adds the Badge prices into the dictonaries
    /// </summary>
    public void AddBadgesToPrices()
    {
//        Debug.Log("add badges prices");
        foreach (Badge badge in BadgeManager.Instance.allBadges)
        {
            BadgePricesLogs.Add(badge.badgeId, badge.priceLogs);
            BadgePricesMarshmallows.Add(badge.badgeId, badge.priceMarshmallows);
        }
    }

    public int GetContinueCost()
    {
        if (GameManager.Instance != null)
        {
            foreach (PriceList p in priceLists)
            {
                if (p.type == PriceListType.ContinueCosts)
                {
                    for (int i = 0; i < p.prices.Count; i++)
                    {
                        if (GameManager.Instance.numOfDeaths <= p.prices[i].limit)
                        {
                            return p.prices[i].price;
                        }
                    }
                    return p.prices[p.prices.Count - 1].price;
                }
            }
        }
        return -1;

    }

    public int GetRestCost()
    {
        foreach (PriceList p in priceLists)
        {
            if (p.type == PriceListType.RestCosts)
            {
                for (int i = 0; i < p.prices.Count; i++)
                {
                    if (GameManager.Instance.troop.distanceTravelled <= p.prices[i].limit)
                    {
                        return p.prices[i].price;
                    }
                }
                return p.prices[p.prices.Count - 1].price;
            }
        }
        return -1;
      
    }

	public void Rest(){

		int cost = GetRestCost();

		if (PlayerWallet.Instance.TotalLogs >= GetRestCost())
		{
			PlayerWallet.Instance.SpendLogs(cost);
			GameManager.Instance.troop.stamina = 3;
		}
	}

    #endregion
}