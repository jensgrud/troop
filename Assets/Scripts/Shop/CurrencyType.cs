﻿public enum CurrencyType {

	Logs, 
	Marshmallows

}

public static class CurrencyTypeExtensions
{
	public static string ToSpriteName(this CurrencyType me)
	{
		switch(me)
		{
		case CurrencyType.Logs:
			return "Currency@Log";
		case CurrencyType.Marshmallows:
			return "Currency@Marshmellow";
		default:
			return string.Empty;
		}
	}
}
