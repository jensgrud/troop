﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerWallet : MonoBehaviour
{
    #region Fields

	public int PersistentLogs;
	public int LogsInThisGame;
	
	public int TotalLogs{
		get { return PersistentLogs + LogsInThisGame; }
	}

    public int Marshmallows;

    #endregion

    #region Singleton
    public static PlayerWallet Instance
    {
        get;
        private set;
    }
    #endregion

    #region Unity native methods
    void Awake()
    {
        Instance = this;
        //Loads the current rank
        WalletWrapper walletWrapper = new WalletWrapper();
        if (PersistentData.SavedDataExists(this))
        {
            walletWrapper = PersistentData.Deserialize<WalletWrapper>(this);
			this.PersistentLogs = walletWrapper.logs;
            this.Marshmallows = walletWrapper.marshmallows;
        }
    }
    
    void OnDestroy()
    {
        //Saves the current content of the wallet
        WalletWrapper walletWrapper = new WalletWrapper(TotalLogs, Marshmallows);
        PersistentData.Serialize(walletWrapper, this);
    }

    void Start()
    {
        //TODO: Possibly remove, this is a bad way of handling serialization
        WalletWrapper walletWrapper = new WalletWrapper(TotalLogs, Marshmallows);
        GameManager.Instance.troop.OnDie += delegate { PersistentData.Serialize(walletWrapper, this); };
        BadgeManager.Instance.OnBadgeEarned += delegate { PersistentData.Serialize(walletWrapper, this); };
		GuiManager.Instance.CampMenu.OnExitCamp += delegate { PersistentData.Serialize(walletWrapper, this); };
        Tutorial.Instance.OnTutorialComplete += delegate { PersistentData.Serialize(walletWrapper, this); };
        Shop.Instance.OnPurches += delegate { PersistentData.Serialize(walletWrapper, this); };
    }
    #endregion

    #region Class Methods
    /// <summary>
    /// Subtacts logsToSpend from logs in PlayerWallet
    /// </summary>
    /// <param name="logsToSpend"></param>
    public void SpendLogs(int logsToSpend)
    {
		if (LogsInThisGame > 0) {
			if (LogsInThisGame > logsToSpend)
				this.LogsInThisGame -= logsToSpend;
			else {
				this.PersistentLogs += LogsInThisGame;
				this.LogsInThisGame = 0;

				this.PersistentLogs -= logsToSpend;
			}
		} else {
			this.PersistentLogs -= logsToSpend;
		}

        StatisticsManager.Instance.IterateStatistic(Statistic.SpentFirewood, logsToSpend);
    }

    /// <summary>
    /// Subtracts marshmallowsToSpend from marshmallows in PlayerWallet
    /// </summary>
    /// <param name="marshmallowsToSpend"></param>
    public void SpendMarshmallows(int marshmallowsToSpend)
    {
        this.Marshmallows -= marshmallowsToSpend;
		if (Marshmallows < 0)
			Marshmallows = 0;
        //Tell StatisticsManager
        StatisticsManager.Instance.IterateStatistic(Statistic.SpentMarshmallow, marshmallowsToSpend);
    }

	public int LoseInGameLogs(int amount)
	{
		if (LogsInThisGame < amount) {
			LogsInThisGame = 0;
		} else {
			LogsInThisGame = LogsInThisGame - amount;
		}
		
		return LogsInThisGame;
	}
	#endregion
}

#region Wrapper class for saving data
[System.Serializable]
public class WalletWrapper
{
    public int logs
    {
        get;
        set;
    }
    public int marshmallows
    {
        get;
        set;
    }
    public WalletWrapper()
    {
    }
    public WalletWrapper(int logs, int marshmallows)
    {
        this.logs = logs;
        this.marshmallows = marshmallows;
    }
}
#endregion