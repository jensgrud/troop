﻿using UnityEngine;
using System.Collections;

public class JumpableObstacle : Recyclable, IObstacle
{
    protected bool collided = false;
    public bool allowCurrencySpawn;
    public bool allowOthersSpawn;
    //If a scout collides with the obstacle the troop lose a stamina
    public void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.GetComponent<Scout>() != null)
        {
            Scout s = c.gameObject.GetComponent<Scout>();
            if (!GameManager.Instance.invincibleAgainstObstacles && !collided && s.order == 0)
            {
                collided = true;

                //Spawn lost logs on scout
                s.SpawnLostLogs(5);

				PlayerWallet.Instance.LoseInGameLogs(5);
             //   GameManager.Instance.troop.DecrementStamina();


                //If the troop have more stamina run animation so the complete the challenge
                if (GameManager.Instance.troop.stamina > 0)
                {
                    
                    //run blink "animation"
                    
                   // GameManager.Instance.troop.Blink(GameManager.Instance.troop.numberOfBlinks);

                    foreach (Scout scout in GameManager.Instance.troop.scouts)
                    {
                       Animator anim = scout.gameObject.GetComponent<Scout>().getAnimation();
                       anim.SetBool("rockTrigger", true);
                        //animationPlays++;
                    }
                    //Animator anim = c.gameObject.GetComponentInChildren<Scout>().getAnimation();

                    //AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);
                    //if (stateInfo.nameHash == Animator.StringToHash("Base Layer.run"))
                    //{
                        //anim.SetBool("rockTrigger", true);
                    //}
                }
            }
        }
    }

    void OnEnable()
    {
        collided = false;
    }
}

