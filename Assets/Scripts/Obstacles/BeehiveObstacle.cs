﻿using UnityEngine;
using System.Collections;

public class BeehiveObstacle : JumpableObstacle {

    public void OnTriggerEnter(Collider c)
    {
        base.OnTriggerEnter(c);
       // Debug.Log(c.gameObject.name);
        if (gameObject.GetComponentInChildren<Animator>() != null && c.gameObject.GetComponent<Scout>() != null && c.gameObject.GetComponent<Scout>().order == 0)
        {
            gameObject.GetComponentInChildren<Animator>().Play(0);
            //ParticleSystem calmParticle =  transform.FindChild("beeHiveBees@calm").GetComponent<ParticleSystem>();
            Transform calmBees = transform.FindChild("beeHiveBees@calm");
            calmBees.gameObject.SetActive(false);
            Transform angryBees = transform.FindChild("beeHiveBees@aggressive");
            angryBees.gameObject.SetActive(true);
            angryBees.GetComponent<ParticleSystem>().enableEmission = true;
			AngrySound();	
        }
    }

	void AngrySound()
	{
		if (audio == null)
			return;

		audio.pitch = 1.3f;
        audio.pitch = AudioManager.Instance.TimeShellPitchDrop(audio).pitch;
		audio.volume = 0.19f;
	}

    void OnEnable()
    {
        Transform calmBees = transform.FindChild("beeHiveBees@calm");
        calmBees.gameObject.SetActive(true);
        Transform angryBees = transform.FindChild("beeHiveBees@aggressive");
        angryBees.gameObject.SetActive(false);
        calmBees.GetComponent<ParticleSystem>().enableEmission = true;
        if (audio == null)
            return;
        audio.pitch = 1;
        audio.volume = 0.1f;
        //audio.volume?;
    }
}
