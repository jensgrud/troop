﻿using UnityEngine;
using System.Collections;

public class DuckTrigger : MonoBehaviour {

    public float duckSpeed = 0.8f;
    public float lifetime = 10;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerStay(Collider c)
    {
        if (c.GetComponent<Scout>() != null)
        {
            c.GetComponent<Scout>().Duck(duckSpeed);
        }
    }
}