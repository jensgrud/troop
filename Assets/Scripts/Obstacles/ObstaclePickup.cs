﻿using UnityEngine;
using System.Collections;


//This is a Challenge, sorry for the bad naming
public class ObstaclePickup : MonoBehaviour {

    void Update()
    {
        if (!GameManager.Instance.enableChallenges) 
        {
            if (GameManager.Instance.terrainDisplayer != null && GameManager.Instance.terrainDisplayer.PrefabManager != null && GameManager.Instance.terrainDisplayer.PrefabManager.Pool != null)
            {
                GameManager.Instance.terrainDisplayer.PrefabManager.Pool.Remove(this.gameObject);
            }
        }

    }

    void OnTriggerEnter(Collider collided) 
    {
        //If the color of the first scout is the same as the obstacle we pick it up, else the scout dies.
        if (gameObject.transform.tag == collided.transform.GetChild(0).tag)
        {

            if (GameManager.Instance.terrainDisplayer != null && GameManager.Instance.terrainDisplayer.PrefabManager != null && GameManager.Instance.terrainDisplayer.PrefabManager.Pool != null)
            {
                GameManager.Instance.terrainDisplayer.PrefabManager.Pool.Remove(this.gameObject);
            }
        }
        else 
        {
            if (!GameManager.Instance.invincibleAgainstChallenges)
            {
                GameManager.Instance.troop.DecrementStamina();
                //make the troops blink to show they lost a stamina
                //GameManager.Instance.troop.Blink(GameManager.Instance.troop.numberOfBlinks);

                //Remove the object
                if (GameManager.Instance.terrainDisplayer != null && GameManager.Instance.terrainDisplayer.PrefabManager != null && GameManager.Instance.terrainDisplayer.PrefabManager.Pool != null)
                {
                    GameManager.Instance.terrainDisplayer.PrefabManager.Pool.Remove(this.gameObject);
                }
                
                //If the troop have no more stamina we tell the game manager that it is nolonger alive.
                if (GameManager.Instance.troop.stamina <= 0) 
                {
                    GameManager.Instance.alive = false;
                }
            }
        }
    }
}
