﻿using UnityEngine;
using System.Collections;

public interface IObstacle  {

    void OnTriggerEnter(Collider c);
}
