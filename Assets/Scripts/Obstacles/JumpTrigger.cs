﻿using UnityEngine;
using System.Collections;

public class JumpTrigger : MonoBehaviour {
    public float lifetime = 10;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        lifetime -= Time.deltaTime;
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider c)
    {
        if (c.GetComponent<Scout>() != null && c.GetComponent<Scout>().IsGrounded())
        {
            c.GetComponent<Scout>().Jump(GameManager.Instance.GetJumpSpeed());
        }
    }
}
