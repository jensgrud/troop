﻿using UnityEngine;
using System.Collections;

public class CrouchObstacle : Recyclable, IObstacle {

    //If a scout collides with the obstacle the troop lose a stamina
    public void OnTriggerEnter(Collider c)
    {
        PlayerWallet.Instance.LoseInGameLogs(5);

        //If the troop have more stamina run animation so the complete the challenge
        if (GameManager.Instance.troop.stamina > 0)
        {
        }
    }
}
