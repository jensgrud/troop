﻿using UnityEngine;
using System.Collections;

public enum Statistic {
    //Spots
    SpottedBirds,
    SpottedWoodpeckers,
    SpottedKingfishers,
    SpottedBaldEagles,
    SpottedFlowers,
    SpottedPlants,
    SpottedFlyAgarics,
    SpottedButterflyBushes,
    SpottedDeers,
    SpottedLynxs,
    SpottedMooses,
    SpottedLochNess,
    SpottedBigfoot,

    //Rescues
    RescuedAnimals,
    RescuedFoxes,
    RescuedBeavers,
    RescuedBunnies,
    RescuedTurtles,
    RescuedEagle,

    //Resources
    CollectedFirewood,
    SpentFirewood,
    SpentMarshmallow,
    BoughtFoxes,
    BoughtTurtles,
    BoughtEagles,
    BoughtBeavers,
    BoughtBunnies,
    BoughtAnimals,
    BoughtPowerupFox,
    BoughtPowerupTurtle,
    BoughtPowerupEagle,
    BoughtPowerups,

    UsedPowerupFox,
    UsedPowerupTurtle,
    UsedPowerupEagle,
    UsedPowerups,

    //Abilities
    AbilityAxeCount,
    AbilityRopeCount,
    AbilitySlingCount,
    AbilityCount,

    //Movement
    MovementJumpCount,
    MovementDuckCount,
    MovementChangePositionCount,

    //Other
    TraveledMeters,
    VisitedCampSites,

    //Single run statistics
    SingleRunJumpCount,
    SingleRunDuckCount,
    SingleRunDuckAndJump,
    SingleRunAbilityAxe,
    SingleRunAbilityRope,
    SingleRunAbilitySling,
    SingleRunAbilities,
    SingleRunUsedPowerupSloth,
    SingleRunUsedPowerupFox,
    SingleRunUsedPowerupEagle,
    SingleRunUsedPowerups,
    SingleRunSurviveBear,

    //ekstra movement
    MovementJumpRockCount,
    MovementDuckBehiveCount
}