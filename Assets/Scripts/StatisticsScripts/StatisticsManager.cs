﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class StatisticsManager : MonoBehaviour
{
    public Dictionary<Statistic, int> Statistics;
    public bool debugMode;

    public static StatisticsManager Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        Instance = this;
        int i = 0;
        Statistic stat = (Statistic)i;
        if (!debugMode && PersistentData.SavedDataExists(this))
        {
			try {
           	 	Statistics = PersistentData.Deserialize<Dictionary<Statistic, int>>(this);
			} finally {
				if (Statistics == null) {
					Statistics = new Dictionary<Statistic, int>();
					while (i - 1 != (int)Enum.GetValues(typeof(Statistic)).Cast<Statistic>().Last())
					{
						Statistics.Add(stat, 0);
						i++;
						stat = (Statistic)i;
					}
				}
			}
        }
        else
        {
            Statistics = new Dictionary<Statistic, int>();
            //Set each statistic to initialize to 0, delete if they are loaded from somewhere
            while (i - 1 != (int)Enum.GetValues(typeof(Statistic)).Cast<Statistic>().Last())
            {
                Statistics.Add(stat, 0);
                i++;
                stat = (Statistic)i;
            }
        }
    }

    void Start()
    {
        //TODO: Possibly remove, this is a bad way of handling serialization
        GameManager.Instance.troop.OnDie += delegate { PersistentData.Serialize(Statistics, this); };
        BadgeManager.Instance.OnBadgeEarned += delegate { PersistentData.Serialize(Statistics, this); };
		GuiManager.Instance.CampMenu.OnExitCamp += delegate { PersistentData.Serialize(Statistics, this); };
        Tutorial.Instance.OnTutorialComplete += delegate { PersistentData.Serialize(Statistics, this); };
        Shop.Instance.OnPurches += delegate { PersistentData.Serialize(Statistics, this); };
    }


    public void IterateStatistic(Statistic statistic)
    {
        if (RankManager.Instance.currentRank == RankManager.tutorialRank)
            return;

        int i = Statistics[statistic];
        i++;
        Statistics[statistic] = i;

        // Call badgemanager to see if there is enough stats for a badge
        BadgeManager.Instance.NotifyStatisticChanged();
    }

    /// <summary>
    /// Overload for when statistics increases by more than 1 at a time.
    /// </summary>
    /// <param name="statistic"></param>
    /// <param name="amount"></param>
    public void IterateStatistic(Statistic statistic, int amount)
    {
        if (RankManager.Instance.currentRank == RankManager.tutorialRank)
            return;
        int i = Statistics[statistic];
        i += amount;
        Statistics[statistic] = i;

        // Call badgemanager to see if there is enough stats for a badge
        BadgeManager.Instance.NotifyStatisticChanged();
    }

    /// <summary>
    /// Set all SingleRun statistics to 0
    /// </summary>
    public void ResetSingleRunStatistics()
    { 
        Statistics[Statistic.SingleRunAbilities] = 0;
        Statistics[Statistic.SingleRunAbilityAxe] = 0;
        Statistics[Statistic.SingleRunAbilityRope] = 0;
        Statistics[Statistic.SingleRunAbilitySling] = 0;
        Statistics[Statistic.SingleRunDuckAndJump] = 0;
        Statistics[Statistic.SingleRunDuckCount] = 0;
        Statistics[Statistic.SingleRunJumpCount] = 0;
        Statistics[Statistic.SingleRunUsedPowerupEagle] = 0;
        Statistics[Statistic.SingleRunUsedPowerupFox] = 0;
        Statistics[Statistic.SingleRunUsedPowerups] = 0;
        Statistics[Statistic.SingleRunUsedPowerupSloth] = 0;
        Statistics[Statistic.SingleRunSurviveBear] = 0;
    }

    void OnDestroy()
    {
        //PersistentData has problems with dictionaries, awaiting fix.
        PersistentData.Serialize(Statistics, this);
    }

    public void ResetStatistics()
    {
        int i = 0;
        Statistic stat = (Statistic)i;
        while (i - 1 != (int)Enum.GetValues(typeof(Statistic)).Cast<Statistic>().Last())
        {
            Statistics[stat] = 0;
            i++;
            stat = (Statistic)i;
        }
    }

    public void ResetStatisticsMinusFirewoodAndDistance()
    {
        int i = 0;
        Statistic stat = (Statistic)i;
        while (i - 1 != (int)Enum.GetValues(typeof(Statistic)).Cast<Statistic>().Last())
        {
            if (!(stat == Statistic.CollectedFirewood || stat == Statistic.MovementChangePositionCount))
                Statistics[stat] = 0;

            i++;
            stat = (Statistic)i;
        }
    }
}
