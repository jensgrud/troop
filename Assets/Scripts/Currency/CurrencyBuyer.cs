using UnityEngine;
using System;
using System.Collections.Generic;

public class CurrencyBuyer : MonoBehaviour {

	public static CurrencyBuyer Instance {
		get;
		private set;
	}

	[Serializable]
	public struct AmountPricePair{
		public int amount;
		public int price;
	}
	
	public AmountPricePair[] logAmountsAndTheirPricesInOre;
	public AmountPricePair[] marshmallowAmountsAndTheirPricesInOre;

	public List<UILabel> LogPriceLabels;
	public List<UILabel> MarshmallowPriceLabels;

	public List<BuyCurrencyButton> BuyLogsButtons;
	public List<BuyCurrencyButton> BuyMarshmallowsButtons;

	void Awake(){
		Instance = this;

		for (int i = 0; i<LogPriceLabels.Count; i++) {
			LogPriceLabels[i].text = logAmountsAndTheirPricesInOre[i].amount + " - " + logAmountsAndTheirPricesInOre[i].price+" DKK";
			BuyLogsButtons[i].Type = CurrencyType.Logs;
			BuyLogsButtons[i].index = logAmountsAndTheirPricesInOre[i].amount;
		}

		for (int i = 0; i<MarshmallowPriceLabels.Count; i++) {
			MarshmallowPriceLabels[i].text = marshmallowAmountsAndTheirPricesInOre[i].amount + " - " + marshmallowAmountsAndTheirPricesInOre[i].price+" DKK";
			BuyLogsButtons[i].Type = CurrencyType.Marshmallows;
			BuyLogsButtons[i].index = marshmallowAmountsAndTheirPricesInOre[i].amount;
		}
	}

	private bool canBuy = true;
	void Update(){
		canBuy = true;
	}

	public void Buy(CurrencyType type, int index){
		if (!canBuy)
			return;

		switch (type) {
		case CurrencyType.Logs:
			PlayerWallet.Instance.PersistentLogs += logAmountsAndTheirPricesInOre[index].amount;
			break;
		case CurrencyType.Marshmallows:
			PlayerWallet.Instance.Marshmallows += marshmallowAmountsAndTheirPricesInOre[index].amount;
			break;
		}
		canBuy = false;
	}

	public void GoBack(){
		this.gameObject.SetActive (false);
	}
}
