﻿using UnityEngine;
using System.Collections;

public class BuyCurrencyButton : MonoBehaviour {

	public CurrencyType Type;
	public int index;

	public void OnClick(){
		if (GuiManager.Instance.CurrentMenu == GuiManager.MenuType.Currency)
			GuiManager.Instance.CurrentMenuReference.GetComponent<CurrencyMenu> ().Buy (Type, index);
	}
}
