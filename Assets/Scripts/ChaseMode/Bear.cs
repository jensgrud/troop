﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bear : MonoBehaviour {

    public float MaxDistToGround = 0.8f;
    public float offset = 5;
    public float MinDistToGround = 0.75f;
    private Vector3 groundPos = Vector3.zero;

    public Vector3 currentDirection = Vector3.zero;
    public GameObject basePosition;

    public bool stopChase = false;
	public float PicthDefault = 1.0f;
	public float PitchMax = 1.7f;
	public float PitchMin = 0.8f;
    private LayerMask layerMask = 1 << 31;
    private RaycastHit hit;
	public List<AudioClip> StepSoundClips = new List<AudioClip>();

	// Use this for initialization
	void Start () {
		
		//gameObject.audio.PlayScheduled (2);	
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        bool aboveGround = Physics.Raycast(basePosition.transform.position, -Vector3.up, out hit, 1000, layerMask);

        if (aboveGround)
        {
            groundPos = hit.point;
        }
        else
        {
            if ((transform.position - GameManager.Instance.troop.GetScoutByOrder(2).transform.position).magnitude < offset + 10)
            {
                transform.position = groundPos + new Vector3(0, 1f, 0);
                currentDirection.y = 0;
            }
            else
            {
                transform.position = GameManager.Instance.troop.GetScoutByOrder(2).transform.position + GameManager.Instance.troop.bearDistance;
            }
        }

        if(!aboveGround)
            Physics.Raycast(basePosition.transform.position, -Vector3.up, out hit, 1000, layerMask);

        if (IsGrounded())
        {

        //    Physics.Raycast(transform.position, -transform.up, out hit, MaxDistToGround, layerMask);
            Vector3 normal = hit.normal;
            Quaternion quat = Quaternion.AngleAxis(-90, new Vector3(0, 0, 1));
            Vector3 forward = quat * normal;
            forward.Normalize();
            currentDirection = ((GameManager.Instance.GetSpeed()) * Time.deltaTime) * forward;
            //Stay perpendicular to the ground, don´t use unless we get the smooth terrain into the build.
            //transform.forward = forward;

            Scout last = GameManager.Instance.troop.GetScoutByOrder(2);
            if (stopChase)
            {
                currentDirection *= 0.5f;
            }
            else if (!GameManager.Instance.troop.challengeFailed && !last.IsFlying)
            {
                float fration = Mathf.Abs(last.transform.position.x - transform.position.x) / offset;
                float maxSpeed = 1.7f;
                currentDirection *= fration;
                if (currentDirection.magnitude > maxSpeed)
                {
                    currentDirection.Normalize();
                    currentDirection *= maxSpeed;
                }
            }

        }
        else
        {
            currentDirection += new Vector3(0, -GameManager.Instance.GetGravity(), 0);
            if (currentDirection.x > ((GameManager.Instance.GetSpeed()) * Time.deltaTime))
            {
                currentDirection = new Vector3(((GameManager.Instance.GetSpeed()) * Time.deltaTime), currentDirection.y, currentDirection.z);
            }

            //float magnitudeReference = (((GameManager.Instance.GetSpeed() + speedBoost) * Time.deltaTime) * Vector3.right * 3).magnitude;
            //currentDirection = Vector3.ClampMagnitude(currentDirection, magnitudeReference);
        }


        Vector3 newPos = transform.position + currentDirection;
        newPos.z = 0;
        transform.position = newPos;
        if (CheckGroundClipping())
        {
            transform.position += 0.01f * transform.up;
        }
	}

    public bool IsGrounded()
    {
       
        //need to get this working i guess
        if (hit.distance <= MaxDistToGround)
            return true;
        else
            return false;
        //bool isGrounded = Physics.Raycast(basePosition.transform.position, -Vector3.up, MaxDistToGround, layerMask);
        //return isGrounded;
    }

    public bool CheckGroundClipping()
    {
        if (hit.distance <= MinDistToGround)
            return true;
        else 
            return false;
        //bool check = Physics.Raycast(basePosition.transform.position, -Vector3.up, MinDistToGround, layerMask);
        //Debug.DrawLine(basePosition.transform.position, new Vector3(basePosition.transform.position.x, basePosition.transform.position.y - MinDistToGround, basePosition.transform.position.z), Color.green, 10);
        //return check;
    }
	void StepSound()
	{
        audio.pitch = AudioManager.Instance.TimeShellPitchDrop(audio).pitch;
		Play(this.StepSoundClips);
	}
	void Play(List<AudioClip> audioClips)
	{
		if (audioClips.Count == 0)
		{
			return;
		}
		
		int pos = Random.Range(0, audioClips.Count);
		
		AudioClip audioClip = audioClips[pos];
		
		audio.pitch = Random.Range(this.PitchMin, this.PitchMax);
		audio.PlayOneShot(audioClip);
	}

}
