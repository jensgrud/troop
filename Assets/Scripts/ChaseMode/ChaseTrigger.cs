﻿using UnityEngine;
using System.Collections;

public class ChaseTrigger : Recyclable {

    private bool isTriggered = false;
    private float chaseSpeed;
	// Use this for initialization


    public void SetSpeed(float speed)
    {
        chaseSpeed = speed;
    }

	// Update is called once per frame
    void OnTriggerEnter(Collider c)
    {
        bool isFlying = false;
        foreach (Scout s in GameManager.Instance.troop.scouts)
        {
            isFlying = s.IsFlying;
            if (isFlying)
                break;
        }

        if (!isTriggered && !isFlying)
        {
            //float speedFactor = chaseSpeed / GameManager.Instance.GetSpeed();
            GameManager.Instance.SetChaseMode(chaseSpeed, true);
         //   Debug.Log("ChaseSpeed: " + chaseSpeed);
            isTriggered = true;
        }
            
    }

    void OnEnable()
    {
        isTriggered = false;
    }
}
