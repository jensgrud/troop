﻿using UnityEngine;
using System.Collections;

public class ChaseTriggerEnd : Recyclable
{

    private bool isTriggered = false;
    private float chaseSpeed;
    // Use this for initialization
    void Start()
    {

    }

    public void SetSpeedFactor(float speed)
    {
        chaseSpeed = speed;
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider c)
    {
        if (!isTriggered)
        {
            GameManager.Instance.SetChaseMode(chaseSpeed, false);
            isTriggered = true;
        }
    }

    void OnEnable()
    {
        isTriggered = false;
    }
}
