%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: MeiaThrowMask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: joints
    m_Weight: 1
  - m_Path: joints/local
    m_Weight: 1
  - m_Path: joints/local/hips
    m_Weight: 1
  - m_Path: joints/local/hips/pelvis
    m_Weight: 1
  - m_Path: joints/local/hips/pelvis/L_up_leg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_up_leg/L_lo_leg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_up_leg/L_lo_leg/L_foot
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_up_leg/L_lo_leg/L_foot/L_toe
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_up_leg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_up_leg/R_lo_leg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_up_leg/R_lo_leg/R_foot
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_up_leg/R_lo_leg/R_foot/R_toe
    m_Weight: 0
  - m_Path: joints/local/hips/spine
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_clavicle
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_clavicle/L_up_arm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_clavicle/L_up_arm/L_lo_arm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_clavicle/L_up_arm/L_lo_arm/L_hand
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_clavicle/L_up_arm/L_lo_arm/L_hand/L_finger
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/ears
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/hat
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_eye_ball
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/ponyTail_base
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/ponyTail_base/ponyTail_tip
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_eye_ball
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/tounge_base
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/tounge_base/tounge_mid
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/tounge_base/tounge_mid/tounge_tip
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/extra01
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/extra01/extra00
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/extra01/extra02
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/extra01/extra02/extra03
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/extra01/extra02/extra03/extra04
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/extra01/extra02/extra03/extra04/extra05
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_clavicle/R_up_arm/R_lo_arm/hand/finger
    m_Weight: 1
  - m_Path: mesh
    m_Weight: 1
  - m_Path: mesh/polySurface26
    m_Weight: 1
  - m_Path: mesh/polySurface26/meia_hat_GEO
    m_Weight: 1
  - m_Path: mesh/polySurface26/meiablue_GEO
    m_Weight: 1
