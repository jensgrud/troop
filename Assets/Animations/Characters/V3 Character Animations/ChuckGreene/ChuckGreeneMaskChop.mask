%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: ChuckGreeneMaskChop
  m_Mask: 01000000010000000100000000000000000000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: chuckGreen_mesh
    m_Weight: 1
  - m_Path: chuckGreen_mesh/chuck_hat_GEO
    m_Weight: 0
  - m_Path: chuckGreen_mesh/chuckGreene_GEO
    m_Weight: 0
  - m_Path: chuckGreen_mesh/feather_hat_GEO
    m_Weight: 0
  - m_Path: chuckGreen_mesh/fox_hat_GEO
    m_Weight: 0
  - m_Path: chuckGreen_mesh/turtle_hat_GEO
    m_Weight: 0
  - m_Path: joints
    m_Weight: 1
  - m_Path: joints/local
    m_Weight: 1
  - m_Path: joints/local/hips
    m_Weight: 1
  - m_Path: joints/local/hips/pelvis
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_UpLeg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_UpLeg/L_Loleg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_UpLeg/L_Loleg/L_Foot
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/L_UpLeg/L_Loleg/L_Foot/L_Toe
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_UpLeg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_UpLeg/R_Loleg
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_UpLeg/R_Loleg/R_Foot
    m_Weight: 0
  - m_Path: joints/local/hips/pelvis/R_UpLeg/R_Loleg/R_Foot/R_Toe
    m_Weight: 0
  - m_Path: joints/local/hips/spine
    m_Weight: 1
  - m_Path: joints/local/hips/spine/belly
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_shoulder
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_shoulder/L_UpArm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_shoulder/L_UpArm/L_LoArm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_shoulder/L_UpArm/L_LoArm/L_hand
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/L_shoulder/L_UpArm/L_LoArm/L_hand/L_finger
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/hat
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_eyeBall
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_eyeBall/L_back_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_eyeBall/L_bot_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_eyeBall/L_front_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_eyeBall/L_top_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_lip_mid1
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_lo_lip_mid1
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_lo_lip_mid2
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_up_lip_mid3
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/L_up_lip_mid4
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/lo_lip_mid
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_eyeBall
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_eyeBall/R_back_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_eyeBall/R_bot_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_eyeBall/R_front_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_eyeBall/R_top_eye
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_lip_mid1
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_lo_lip_mid1
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_lo_lip_mid2
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_up_lip_mid3
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/R_up_lip_mid4
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/tounge_base
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/tounge_base/tounge_mid
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/tounge_base/tounge_mid/tounge_tip
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/neck/head/up_lip_mid
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/necktie_base
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/necktie_base/necktie_mid
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/necktie_base/necktie_mid/necktie_tip
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_shoulder
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_shoulder/R_UpArm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_shoulder/R_UpArm/R_LoArm
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_shoulder/R_UpArm/R_LoArm/R_hand
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_shoulder/R_UpArm/R_LoArm/R_hand/extra
    m_Weight: 1
  - m_Path: joints/local/hips/spine/chest/R_shoulder/R_UpArm/R_LoArm/R_hand/R_finger
    m_Weight: 1
