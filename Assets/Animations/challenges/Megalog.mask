%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Megalog
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: joints
    m_Weight: 0
  - m_Path: joints/local
    m_Weight: 0
  - m_Path: joints/local/log
    m_Weight: 0
  - m_Path: joints/local/log/log1
    m_Weight: 1
  - m_Path: joints/local/log/log2
    m_Weight: 0
  - m_Path: mesh
    m_Weight: 0
  - m_Path: mesh/MEGALOG_GEO
    m_Weight: 0
