<?xml version="1.0" encoding="utf-8"?>
<root><!-- 
    Microsoft ResX Schema 
    
    Version 2.0
    
    The primary goals of this format is to allow a simple XML format 
    that is mostly human readable. The generation and parsing of the 
    various data types are done through the TypeConverter classes 
    associated with the data types.
    
    Example:
    
    ... ado.net/XML headers & schema ...
    <resheader name="resmimetype">text/microsoft-resx</resheader>
    <resheader name="version">2.0</resheader>
    <resheader name="reader">System.Resources.ResXResourceReader, System.Windows.Forms, ...</resheader>
    <resheader name="writer">System.Resources.ResXResourceWriter, System.Windows.Forms, ...</resheader>
    <data name="Name1"><value>this is my long string</value><comment>this is a comment</comment></data>
    <data name="Color1" type="System.Drawing.Color, System.Drawing">Blue</data>
    <data name="Bitmap1" mimetype="application/x-microsoft.net.object.binary.base64">
        <value>[base64 mime encoded serialized .NET Framework object]</value>
    </data>
    <data name="Icon1" type="System.Drawing.Icon, System.Drawing" mimetype="application/x-microsoft.net.object.bytearray.base64">
        <value>[base64 mime encoded string representing a byte array form of the .NET Framework object]</value>
        <comment>This is a comment</comment>
    </data>
                
    There are any number of "resheader" rows that contain simple 
    name/value pairs.
    
    Each data row contains a name, and value. The row also contains a 
    type or mimetype. Type corresponds to a .NET class that support 
    text/value conversion through the TypeConverter architecture. 
    Classes that don't support this are serialized and stored with the 
    mimetype set.
    
    The mimetype is used for serialized objects, and tells the 
    ResXResourceReader how to depersist the object. This is currently not 
    extensible. For a given mimetype the value must be set accordingly:
    
    Note - application/x-microsoft.net.object.binary.base64 is the format 
    that the ResXResourceWriter will generate, however the reader can 
    read any of the formats listed below.
    
    mimetype: application/x-microsoft.net.object.binary.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            : and then encoded with base64 encoding.
    
    mimetype: application/x-microsoft.net.object.soap.base64
    value   : The object must be serialized with 
            : System.Runtime.Serialization.Formatters.Soap.SoapFormatter
            : and then encoded with base64 encoding.

    mimetype: application/x-microsoft.net.object.bytearray.base64
    value   : The object must be serialized into a byte array 
            : using a System.ComponentModel.TypeConverter
            : and then encoded with base64 encoding.
    -->
  <xsd:schema id="root" xmlns="" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
    <xsd:import namespace="http://www.w3.org/XML/1998/namespace" />
    <xsd:element name="root" msdata:IsDataSet="true">
      <xsd:complexType>
        <xsd:choice maxOccurs="unbounded">
          <xsd:element name="metadata">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" />
              </xsd:sequence>
              <xsd:attribute name="name" use="required" type="xsd:string" />
              <xsd:attribute name="type" type="xsd:string" />
              <xsd:attribute name="mimetype" type="xsd:string" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="assembly">
            <xsd:complexType>
              <xsd:attribute name="alias" type="xsd:string" />
              <xsd:attribute name="name" type="xsd:string" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="data">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
                <xsd:element name="comment" type="xsd:string" minOccurs="0" msdata:Ordinal="2" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" msdata:Ordinal="1" />
              <xsd:attribute name="type" type="xsd:string" msdata:Ordinal="3" />
              <xsd:attribute name="mimetype" type="xsd:string" msdata:Ordinal="4" />
              <xsd:attribute ref="xml:space" />
            </xsd:complexType>
          </xsd:element>
          <xsd:element name="resheader">
            <xsd:complexType>
              <xsd:sequence>
                <xsd:element name="value" type="xsd:string" minOccurs="0" msdata:Ordinal="1" />
              </xsd:sequence>
              <xsd:attribute name="name" type="xsd:string" use="required" />
            </xsd:complexType>
          </xsd:element>
        </xsd:choice>
      </xsd:complexType>
    </xsd:element>
  </xsd:schema>
  <resheader name="resmimetype">
    <value>text/microsoft-resx</value>
  </resheader>
  <resheader name="version">
    <value>2.0</value>
  </resheader>
  <resheader name="reader">
    <value>System.Resources.ResXResourceReader, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
  <resheader name="writer">
    <value>System.Resources.ResXResourceWriter, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089</value>
  </resheader>
	<data name="+Sec Duration Label" xml:space="preserve">
		<value>+3 SEKUNDER VARIGHED</value>
	</data>
	<data name="Activate Label" xml:space="preserve">
		<value>VÆLG</value>
	</data>
	<data name="Activated Label" xml:space="preserve">
		<value>VALGT</value>
	</data>
	<data name="Back Label" xml:space="preserve">
		<value>TILBAGE</value>
	</data>
	<data name="Badge Description 0" xml:space="preserve">
		<value>Hop tre gange</value>
	</data>
	<data name="Badge Description 1" xml:space="preserve">
		<value>Duk tre gange</value>
	</data>
	<data name="Badge Description 10" xml:space="preserve">
		<value>Saml 1000 brændeknuder</value>
	</data>
	<data name="Badge Description 11" xml:space="preserve">
		<value>Løs 15 forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 12" xml:space="preserve">
		<value>Hop eller duk 50x gange i et enkelt spil</value>
	</data>
	<data name="Badge Description 13" xml:space="preserve">
		<value>Køb en power-up og brug det</value>
	</data>
	<data name="Badge Description 14" xml:space="preserve">
		<value>Løb 5000 meter</value>
	</data>
	<data name="Badge Description 15" xml:space="preserve">
		<value>Løs 30 forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 16" xml:space="preserve">
		<value>Pluk 25 sjældne blomster i denne rang</value>
	</data>
	<data name="Badge Description 17" xml:space="preserve">
		<value>Spot 25 sjældne fugle i denne rang</value>
	</data>
	<data name="Badge Description 18" xml:space="preserve">
		<value>Befri en fanget ræv</value>
	</data>
	<data name="Badge Description 19" xml:space="preserve">
		<value>Løb 7500 meter</value>
	</data>
	<data name="Badge Description 2" xml:space="preserve">
		<value>Hav alle de tre spejdere i front mindst én gang</value>
	</data>
	<data name="Badge Description 20" xml:space="preserve">
		<value>Hop eller duk 75x gange i et enkelt spil</value>
	</data>
	<data name="Badge Description 21" xml:space="preserve">
		<value>Løs 10 slangebøsse-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 22" xml:space="preserve">
		<value>Løs 10 økse-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 23" xml:space="preserve">
		<value>Løs 10 reb-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 24" xml:space="preserve">
		<value>Køb en dyre-maskot i shoppen</value>
	</data>
	<data name="Badge Description 25" xml:space="preserve">
		<value>Spot 50 sjældne fugle i denne rang</value>
	</data>
	<data name="Badge Description 26" xml:space="preserve">
		<value>Pluk 50 sjældne blomster i denne rang</value>
	</data>
	<data name="Badge Description 27" xml:space="preserve">
		<value>Saml 5000 brændeknuder</value>
	</data>
	<data name="Badge Description 28" xml:space="preserve">
		<value>Befri en fanget skildpadde</value>
	</data>
	<data name="Badge Description 29" xml:space="preserve">
		<value>Løb fra en bjørn tre gange i et enkelt spil</value>
	</data>
	<data name="Badge Description 3" xml:space="preserve">
		<value>Løs fem forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 30" xml:space="preserve">
		<value>Spot et mystisk væsen</value>
	</data>
	<data name="Badge Description 31" xml:space="preserve">
		<value>Løs 25 slangebøsse-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 32" xml:space="preserve">
		<value>Løs 25 økse-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 33" xml:space="preserve">
		<value>Løs 25 reb-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 34" xml:space="preserve">
		<value>Løb 15000 meter</value>
	</data>
	<data name="Badge Description 35" xml:space="preserve">
		<value>Hop eller duk 100x gange i et enkelt spil</value>
	</data>
	<data name="Badge Description 36" xml:space="preserve">
		<value>Brug en af hver power-up i et enkelt spil</value>
	</data>
	<data name="Badge Description 37" xml:space="preserve">
		<value>Saml 25000 brændeknuder</value>
	</data>
	<data name="Badge Description 38" xml:space="preserve">
		<value>Befri en fanget ørn</value>
	</data>
	<data name="Badge Description 39" xml:space="preserve">
		<value>Spot et mystisk væsen</value>
	</data>
	<data name="Badge Description 4" xml:space="preserve">
		<value>Spot tre sjældne fugle</value>
	</data>
	<data name="Badge Description 5" xml:space="preserve">
		<value>Løs tre reb-forhindringer i enkelt spil</value>
	</data>
	<data name="Badge Description 6" xml:space="preserve">
		<value>Løs tre økse-forhindringer i enkelt spil</value>
	</data>
	<data name="Badge Description 7" xml:space="preserve">
		<value>Løs tre slangebøsse-forhindringer i et enkelt spil</value>
	</data>
	<data name="Badge Description 8" xml:space="preserve">
		<value>Hold pause ved en lejrplads</value>
	</data>
	<data name="Badge Description 9" xml:space="preserve">
		<value>Pluk tre sjældne blomster</value>
	</data>
	<data name="Badge earned!" xml:space="preserve">
		<value>Nyt Spejdermærke!</value>
	</data>
	<data name="Badge Name Label" xml:space="preserve">
		<value>Badge navn</value>
	</data>
	<data name="Badge Title 0" xml:space="preserve">
		<value>Kænguru</value>
	</data>
	<data name="Badge Title 1" xml:space="preserve">
		<value>Lyn Reflekser</value>
	</data>
	<data name="Badge Title 10" xml:space="preserve">
		<value>Brændesamler</value>
	</data>
	<data name="Badge Title 11" xml:space="preserve">
		<value>Problemløser</value>
	</data>
	<data name="Badge Title 12" xml:space="preserve">
		<value>Rend og Hop... og Duk!</value>
	</data>
	<data name="Badge Title 13" xml:space="preserve">
		<value>Vær Beredt</value>
	</data>
	<data name="Badge Title 14" xml:space="preserve">
		<value>På Vandretur</value>
	</data>
	<data name="Badge Title 15" xml:space="preserve">
		<value>Problemløser</value>
	</data>
	<data name="Badge Title 16" xml:space="preserve">
		<value>Botanist</value>
	</data>
	<data name="Badge Title 17" xml:space="preserve">
		<value>Fuglefinder</value>
	</data>
	<data name="Badge Title 18" xml:space="preserve">
		<value>Dyreven</value>
	</data>
	<data name="Badge Title 19" xml:space="preserve">
		<value>På Vandretur</value>
	</data>
	<data name="Badge Title 2" xml:space="preserve">
		<value>Samarbejde!</value>
	</data>
	<data name="Badge Title 20" xml:space="preserve">
		<value>Rend og Hop... og Duk!</value>
	</data>
	<data name="Badge Title 21" xml:space="preserve">
		<value>Slynge(L)!</value>
	</data>
	<data name="Badge Title 22" xml:space="preserve">
		<value>Ren øksetænkning</value>
	</data>
	<data name="Badge Title 23 " xml:space="preserve">
		<value>Knob-fedt!</value>
	</data>
	<data name="Badge Title 24" xml:space="preserve">
		<value>Nuttet!</value>
	</data>
	<data name="Badge Title 25" xml:space="preserve">
		<value>Fuglefinder</value>
	</data>
	<data name="Badge Title 26" xml:space="preserve">
		<value>Botanist</value>
	</data>
	<data name="Badge Title 27" xml:space="preserve">
		<value>Brændesamler</value>
	</data>
	<data name="Badge Title 28" xml:space="preserve">
		<value>Dyreven</value>
	</data>
	<data name="Badge Title 29" xml:space="preserve">
		<value>Bjørne Bekymringer</value>
	</data>
	<data name="Badge Title 3" xml:space="preserve">
		<value>Problemløser</value>
	</data>
	<data name="Badge Title 30" xml:space="preserve">
		<value>Jeg så den! Jeg sværger!</value>
	</data>
	<data name="Badge Title 31" xml:space="preserve">
		<value>Skarpskytte</value>
	</data>
	<data name="Badge Title 32" xml:space="preserve">
		<value>Effektiv Økseføring</value>
	</data>
	<data name="Badge Title 33" xml:space="preserve">
		<value>Knudeknuser</value>
	</data>
	<data name="Badge Title 34" xml:space="preserve">
		<value>På Vandretur</value>
	</data>
	<data name="Badge Title 35" xml:space="preserve">
		<value>Rend og Hop... og Duk!</value>
	</data>
	<data name="Badge Title 36" xml:space="preserve">
		<value>Kraftfuld!</value>
	</data>
	<data name="Badge Title 37" xml:space="preserve">
		<value>Brændesamler</value>
	</data>
	<data name="Badge Title 38" xml:space="preserve">
		<value>Dyreven</value>
	</data>
	<data name="Badge Title 39" xml:space="preserve">
		<value>DEN FINDES!</value>
	</data>
	<data name="Badge Title 4" xml:space="preserve">
		<value>Fuglefinder</value>
	</data>
	<data name="Badge Title 5" xml:space="preserve">
		<value>Kirurgisk Knude</value>
	</data>
	<data name="Badge Title 6" xml:space="preserve">
		<value>Goddag mand økseskaft!</value>
	</data>
	<data name="Badge Title 7" xml:space="preserve">
		<value>Skarpøjet!</value>
	</data>
	<data name="Badge Title 8" xml:space="preserve">
		<value>På Camping</value>
	</data>
	<data name="Badge Title 9" xml:space="preserve">
		<value>Rend og Hop... og Duk!</value>
	</data>
	<data name="BadgeEarnedSprite" xml:space="preserve">
		<value>GUI@BadgeEarnedDAN</value>
	</data>
	<data name="Badges Label" xml:space="preserve">
		<value>SPEJDERMÆRKER</value>
	</data>
	<data name="Cannot post highscore!" xml:space="preserve">
		<value>Kan ikke dele resultat!</value>
	</data>
	<data name="Claim Reward Label" xml:space="preserve">
		<value>INDKASSER PRÆMIE!</value>
	</data>
	<data name="Continue Label" xml:space="preserve">
		<value>FORSÆT</value>
	</data>
	<data name="Continue? Label" xml:space="preserve">
		<value>FORSÆT?</value>
	</data>
	<data name="Credits Label" xml:space="preserve">
		<value>Medvirkende</value>
	</data>
	<data name="CurrencyChoice Label" xml:space="preserve">
		<value>KØB BRÆNDEKNUDER ELLER SKUMFIDUSER?</value>
	</data>
	<data name="Earn badges Label" xml:space="preserve">
		<value>FÅ FAT I SPEJDERMÆRKER!</value>
	</data>
	<data name="Enter name" xml:space="preserve">
		<value>Skriv dit navn</value>
	</data>
	<data name="Exchange Label" xml:space="preserve">
		<value>VALUTA</value>
	</data>
	<data name="FB Share? Label" xml:space="preserve">
		<value>Del på Facebook?</value>
	</data>
	<data name="FB Shared Label" xml:space="preserve">
		<value>Delt på Facebook</value>
	</data>
	<data name="Finish Game Label" xml:space="preserve">
		<value>DU ER FANATASTISK!</value>
	</data>
	<data name="Go to shop Label" xml:space="preserve">
		<value>Gå til shoppen?</value>
	</data>
	<data name="Home? Label" xml:space="preserve">
		<value>Gå tilbage til startskærm?</value>
	</data>
	<data name="Ingame Badge Popup" xml:space="preserve">
		<value>NYT SPEJDERMÆRKE!</value>
	</data>
	<data name="Insufficient funds Label" xml:space="preserve">
		<value>Du har ikke råd...</value>
	</data>
	<data name="Intro Text Label" xml:space="preserve">
		<value>GÅ UD OG SAML SPEJDERMÆRKER!</value>
	</data>
	<data name="Language Label" xml:space="preserve">
		<value>DANSK</value>
	</data>
	<data name="Leaderboard Label" xml:space="preserve">
		<value>RANGLISTE</value>
	</data>
	<data name="Leaderboards Label" xml:space="preserve">
		<value>RANGLISTER</value>
	</data>
	<data name="Locked Pet" xml:space="preserve">
		<value>Du har ikke reddet dette dyr endnu. Led efter det i skoven!</value>
	</data>
	<data name="Mascot shop Label" xml:space="preserve">
		<value>MASKOTTER</value>
	</data>
	<data name="Mock Up Shop Info" xml:space="preserve">
		<value>Dette er ikke en rigtig butik. Ingen rigtige penge vil blive brugt.</value>
	</data>
	<data name="Mock Up Shop Question" xml:space="preserve">
		<value>[Dette er ikke en rigtig transaktion] Vil du bruge "dine hårdt tjente penge" til at købe for?</value>
	</data>
	<data name="More to come Label" xml:space="preserve">
		<value>MERE I VENTE... </value>
	</data>
	<data name="Multiplier" xml:space="preserve">
		<value>bonus</value>
	</data>
	<data name="Music Label" xml:space="preserve">
		<value>MUSIK</value>
	</data>
	<data name="OnDeathTutorial" xml:space="preserve">
		<value>GAME OVER! Det koster SKUMFIDUSER at fortsætte, men her er nogle helt gratis!</value>
	</data>
	<data name="Or Label" xml:space="preserve">
		<value>ELLER</value>
	</data>
	<data name="Out of stamina Label" xml:space="preserve">
		<value>IKKE FLERE LIV!</value>
	</data>
	<data name="Pet Description 0" xml:space="preserve">
		<value>Du befriede ræven Foxy! Vil du beholde ham som maskot?</value>
	</data>
	<data name="Pet Description 1" xml:space="preserve">
		<value>Du befriede skildpadden Ted! Vil du beholde ham som maskot?</value>
	</data>
	<data name="Pet Description 2" xml:space="preserve">
		<value>Du befriede ørnen Earl! Vil du beholde ham som maskot?</value>
	</data>
	<data name="Pet Name 0" xml:space="preserve">
		<value>RÆV</value>
	</data>
	<data name="Pet Name 1" xml:space="preserve">
		<value>SKILDPADDE</value>
	</data>
	<data name="Pet Name 2" xml:space="preserve">
		<value>ØRN</value>
	</data>
	<data name="Pets Label" xml:space="preserve">
		<value>MASKOTTER</value>
	</data>
	<data name="Post Highscore" xml:space="preserve">
		<value>DEL RESULTAT</value>
	</data>
	<data name="Power Up Description 0" xml:space="preserve">
		<value>Rævnadoen laver en magisk hvirvlende cirkel og suger alle brændeknuder op!</value>
	</data>
	<data name="Power Up Description 1" xml:space="preserve">
		<value>Tidskjoldet manipulerer og sænker tiden!</value>
	</data>
	<data name="Power Up Descripton 2" xml:space="preserve">
		<value>Fjerstormen løfter dig og booster dig igennem verden på en sky af fjer!</value>
	</data>
	<data name="Power Up Title 0" xml:space="preserve">
		<value>RÆVNADO</value>
	</data>
	<data name="Power Up Title 1" xml:space="preserve">
		<value>TIDSKJOLD</value>
	</data>
	<data name="Power Up Title 2" xml:space="preserve">
		<value>FJERSTORM</value>
	</data>
	<data name="Power-up shop Label" xml:space="preserve">
		<value>BOOSTS</value>
	</data>
	<data name="Quit? Label" xml:space="preserve">
		<value>SLUT?</value>
	</data>
	<data name="Rank Label" xml:space="preserve">
		<value>RANG</value>
	</data>
	<data name="Rank Multiplier Label" xml:space="preserve">
		<value>RANG BONUS</value>
	</data>
	<data name="RankEarnedSprite" xml:space="preserve">
		<value>GUI@NewRankDAN</value>
	</data>
	<data name="real hard earned money" xml:space="preserve">
		<value>"real" hard earned money</value>
	</data>
	<data name="Reset Ranks Label" xml:space="preserve">
		<value>VIL DU NULSTILLE DIN RANG OG ALLE SPEJDERMÆRKER?</value>
	</data>
	<data name="Rest Label" xml:space="preserve">
		<value>SLÅ LEJR</value>
	</data>
	<data name="Risk bonus" xml:space="preserve">
		<value>RISIKO BONUS</value>
	</data>
	<data name="RiskBonusFrame1" xml:space="preserve">
		<value>GUI@RiskBonusFrame1DAN</value>
	</data>
	<data name="RiskBonusFrame2" xml:space="preserve">
		<value>GUI@RiskBonusFrame2DAN</value>
	</data>
	<data name="Settings Label" xml:space="preserve">
		<value>INDSTILLINGER</value>
	</data>
	<data name="Shop Label" xml:space="preserve">
		<value>BUTIK</value>
	</data>
	<data name="Skip Label" xml:space="preserve">
		<value>SKIP</value>
	</data>
	<data name="Sounds Label" xml:space="preserve">
		<value>LYD</value>
	</data>
	<data name="Start Tutorial Again?" xml:space="preserve">
		<value>START SPILVEJLEDNING IGEN?</value>
	</data>
	<data name="Tutorial Label" xml:space="preserve">
		<value>SPILVEJLEDNING</value>
	</data>
</root>