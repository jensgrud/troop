﻿using UnityEngine;
using System.Collections;

public class DayNightFade : MonoBehaviour {
    //public Material material1;
    //public Material material2;
    public float duration = 10.0F;
    private float time;
    private float texture1 = 0;
    private float texture2 = 1;
    private float lerp = 0;
	// Use this for initialization
	void Start () 
    {
	   // renderer.material = material1;
        time = 0;
        StartCoroutine(NightToDay());
	}
	

    IEnumerator NightToDay()
    {
        while (time < duration)
        {
            time += Time.deltaTime;
            lerp = time / duration;
            if (lerp > 1)
            {
                lerp = 1;
            }
            renderer.material.SetFloat("_Blend", lerp);
            yield return null;
        }

        
    }
}
